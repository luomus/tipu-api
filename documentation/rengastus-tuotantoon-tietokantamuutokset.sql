


alter table event add wingLengthMaxMeasurement number(6,1);
alter table event add wingLengthMinMeasurement number(6,1);

update event set wingLengthMaxMeasurement = wingLengthInMillimeters
where wingLengthInMillimeters is not null
and (wingLengthMeasurementMethod is null
or wingLengthMeasurementMethod IN ('M'));

update event set wingLengthMinMeasurement = wingLengthInMillimeters
where wingLengthInMillimeters is not null
and (wingLengthMeasurementMethod IS NOT NULL
or wingLengthMeasurementMethod NOT IN ('M'));

alter table event rename column wingLengthInMillimeters to winglength_old;
alter table event rename column wingLengthMaxMeasurement to wingLengthInMillimeters;


Uusi versio rengaskirjeestä
 wingLengthInMillimeters -> 
 * wingLengthMaxMeasurement   tämä preferred, perään "(max)"
 * wingLengthMinMeasurement   tämä jos max ei ole annettu mutta tämä on annettu, perään "(min)"

kun tipu-api ym deployattu
alter table event rename column wingLengthMeasurementMethod to wingLengthMeasMethod_old;

---------------------------------------------------
===================================================





1. -sulje järjestelmät: tipu-api,rengastus:undeploy,sulka:stop (prod:pontikka,tammitikka,h92,fmnh-prod)

2. luo taulut (riippuvuusjärjestyksessä) 
 !! eventille ota constraitit pois, paitsi pk_event

3. siirrä tiedot
3.1 muut taulut
 OtherTablesInsertSQL
 
3.2 event

rengastukset pitää pistää pätkissä:
select substr(id, 1, 1), count(1) from rengastus group by substr(id, 1, 1);

INSERT INTO event (
.....
WHERE substr(id, 1, 1) = '1'; -- 1 8 9

select * from (
select 'reng', count(1) from rengastus union
select 'tap', count(1) from tapaaminen union
 (select 'reng+tap', (select count(1) from tapaaminen)+(select count(1) from rengastus) from dual) union
select 'event', count(1) from event
);

create index ix_event_type on event(type);
create index ix_event_diario on event(diario);
create index ix_event_namering on event(nameRing);
create index ix_event_legring on event(legRing); 
create index ix_event_newlegring on event(newLegRing);
create index ix_event_fieldr on event(fieldReadableCode, mainColor, species);
create index ix_event_fieldr2 on event(fieldReadableCode, mainColor);
create index ix_event_fieldr3 on event(fieldReadableCode);
create index ix_event_fieldr4 on event(mainColor,species);
create index ix_event_fieldr5 on event(mainColor);
create index ix_event_ringer on event(ringer);
create index ix_event_municipality on event(municipality);
create index ix_event_birdstation on event(birdStation);
create index ix_event_euringplace on event(euringProvinceCode);
create index ix_event_species on event(species);
create index ix_event_date on event(trunc(eventDate));
create index ix_event_created on event(trunc(created));
create index ix_event_year on event(to_number(to_char(eventDate, 'YYYY')));
create index ix_event_ringstartend on event(ringStart, ringEnd);
create index ix_event_des on event(wgs84DecimalLat, wgs84DecimalLon);
create index ix_event_degree on event(wgs84DegreeLat,wgs84DegreeLon);
create index ix_event_ykj on event(uniformLat,uniformLon);
create index ix_event_euref on event(eurefLat,eurefLon);
create index ix_event_scheme on event(schemeID);
create index ix_event_rec_method on event(recoveryMethod);
create index ix_event_layman on event(laymanID);
create index ix_event_ringNotUsedReason on event(ringNotUsedReason);

update event set newlegring = legring where type = 'ringing';
commit;

-- lintuasema kuntana fix:
update event
set birdstation = municipality
where municipality in (select id from birdstation);
update event
set municipality = (select birdstation.municipality from birdstation where birdstation.id = event.birdstation),
euringprovincecode = (select municipality.euringprovince from municipality where municipality.id = (select birdstation.municipality from birdstation where birdstation.id = event.birdstation))
where municipality in (select id from birdstation)
;
-- kunnan kautta euringprovince jos euringprovince tyhjä
update event set euringprovincecode = (select municipality.euringprovince from municipality where municipality.id = event.municipality) 
where euringprovincecode is null
and municipality is not null;
commit;
-- muuta korjattavaa? euringprovince null -- jos municipality on ringingissä null, niin ei voi tehdä mitään..
select type, municipality, count(1)
from event
where euringprovincecode is null
group by type, municipality;
select * from event where municipality is null and euringprovincecode is null and type = 'recovery';


   select newlegring, count(1)
   from event
   where newlegring is not null
   group by newlegring
   having count(1) > 1;
   
  select species, count(1)
  from event 
  left join species on (event.species = species.id)
  where species.id is null
  group by species
  order by count(1);
 
 update event set euringprovincecode = 'YU--' where euringprovincecode in ('YU74','YU78','YU77');
 update event set intermediaryRinger = null where intermediaryRinger = 0;
 update event set laymanid = null
	where laymanid in (
  	select distinct laymanid
  	from event 
  	left join layman on (event.laymanid = layman.id)
  	where layman.id is null
 );
 commit;
   
   alter table event add CONSTRAINT uniq_event_newLegRing UNIQUE (newLegRing); 
   alter table event add CONSTRAINT uniq_event_diario UNIQUE (diario);
   alter table event add CONSTRAINT fk_event_schemeID FOREIGN KEY (schemeID) REFERENCES scheme(id);
   alter table event add CONSTRAINT fk_event_species FOREIGN KEY (species) REFERENCES species(id);
   alter table event add CONSTRAINT fk_event_euringProvinceCode FOREIGN KEY (euringProvinceCode) REFERENCES euringProvince(id);
   alter table event add CONSTRAINT fk_event_ringer FOREIGN KEY (ringer) REFERENCES ringer(id);
   alter table event add CONSTRAINT fk_event_clutchNumberOwner FOREIGN KEY (clutchNumberOwner) REFERENCES ringer(id);
   alter table event add CONSTRAINT fk_event_birdStation FOREIGN KEY (birdStation) REFERENCES birdStation(id);
   alter table event add CONSTRAINT fk_event_municipality FOREIGN KEY (municipality) REFERENCES municipality(id);
   alter table event add CONSTRAINT fk_event_laymanID FOREIGN KEY (laymanID) REFERENCES layman(id);
   alter table event add CONSTRAINT fk_event_intermediaryRinger FOREIGN KEY (intermediaryRinger) REFERENCES ringer(id);
 
 
 
update birdStation set centerPointUniformLon = 
CASE length(centerPointUniformLon) 
WHEN 4 THEN  '3' || centerPointUniformLon 
WHEN 3 THEN '30' || centerPointUniformLon ELSE to_char(centerPointUniformLon) END;
commit;

update ringer set lastname = replace(lastname,'+','');
update ringer set lastname = replace(lastname,'*','');
update ringer set firstname = replace(firstname,'+','');
update ringer set firstname = replace(firstname,'*','');
commit;

CREATE MATERIALIZED VIEW species_age_records
BUILD IMMEDIATE REFRESH COMPLETE START WITH sysdate+1 NEXT TRUNC(SYSDATE+7)
 AS
  SELECT ringing.species as species,
  ROUND((recovery.eventDate - ringing.eventDate)/10)*10 AS age
  FROM event recovery
  JOIN event ringing ON (ringing.nameRing = recovery.nameRing AND ringing.type = 'ringing')
  WHERE recovery.type = 'recovery' 
  AND (ringing.speciesAccuracy IS NULL OR ringing.speciesAccuracy != 'E')
  AND (recovery.birdConditionEURING IS NULL OR recovery.birdConditionEURING NOT IN ('0', '3'))
  AND (recovery.eventDate  - ringing.eventDate) > 365
  GROUP BY ringing.species, ROUND((recovery.eventDate - ringing.eventDate)/10)
  HAVING COUNT(1) < 4 ;
   
create index ix_species_age_records on species_age_records(species);


---- UUDET KENTÄT TÄNNE -------------------------------------
alter table fieldreadable add codeRepeatedCount number(1);
alter table fieldreadable add heightInMillimeters number(5,1);
alter table fieldreadable add diameterInMillimeters number(5,1);
alter table fieldreadable add material varchar2(40 char);
alter table fieldreadable add otherInfo varchar2(80 char);
alter table fieldreadable add font varchar2(80 char);
alter table fieldreadable add checksumCharacterFormula varchar2(80 char);

alter table event add fieldReadableCondition varchar2(1 char);
insert into codes (code, variable, language, description, metadata)
select 316, '1', 'S', 'Uudenveroinen, hyväkuntoinen', 'A' from dual union
select 316, '1', 'E', 'Brand new, in good state', '' from dual union
select 316, '1', 'R', 'Helt ny, i gott skick', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 316, '2', 'S', 'Hieman vaikealukuinen, ei havaittavia murtumia tai lohkeamia', 'B' from dual union
select 316, '2', 'E', 'Slightly hard to read, not broken or fractured', '' from dual union
select 316, '2', 'R', 'Något svårt att läsa, inte brutits eller fraktur', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 316, '3', 'S', 'Pieni lohkeama, tai murtuma, saattaa olla vaikealukuinen', 'C' from dual union
select 316, '3', 'E', 'Broken or fractured, may be hard to read', '' from dual union
select 316, '3', 'R', 'Bruten eller fraktur, kan vara svårt att läsa', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 316, '4', 'S', 'Lohkeamia tai murtumia, vaikealukuinen (jotakin merkkiä ei voi lukea)', 'D' from dual union
select 316, '4', 'E', 'Broken or fractured, hard to read (some markings missing)', '' from dual union
select 316, '4', 'R', 'Bruten eller fraktur, svårt att läsa (vissa markeringar saknas)', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 316, '5', 'S', 'Rikkinäinen, vaikealukuinen (tippunee pian)', 'E' from dual union
select 316, '5', 'E', 'Very broken and fractured (will probably drop soon)', '' from dual union
select 316, '5', 'R', 'Mycket trasiga och spruckna (kommer förmodligen sjunka snart)', '' from dual;



alter table event add colonyPairCount number(8);
alter table event add colonyPairCountAccuracy number(3);
alter table event add colonySizeInMeters number(8);
alter table event add colonyNotes varchar2(1500 char);





alter table event add batCapturePlace varchar2(2 char); -- code 400
insert into codes (code, variable, language, description, metadata)
select 400, 'L', 'S', 'Lennosta ulkoa', '0' from dual union
select 400, 'L', 'E', 'From flight outside', '0' from dual union
select 400, 'L', 'R', 'Från flygning utanför', '0' from dual;
insert into codes (code, variable, language, description, metadata)
select 400, 'P', 'S', 'Pönttö', '1' from dual union
select 400, 'P', 'E', 'Nestbox', '1' from dual union
select 400, 'P', 'R', 'Holken', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 400, 'R', 'S', 'Rakennus', '2' from dual union
select 400, 'R', 'E', 'Building', '2' from dual union
select 400, 'R', 'R', 'En byggnad', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 400, 'K', 'S', 'Puunkolo', '3' from dual union
select 400, 'K', 'E', 'Tree cavity', '3' from dual union
select 400, 'K', 'R', 'Träd håligheter', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 400, 'M', 'S', 'Muu', '4' from dual union
select 400, 'M', 'E', 'Other', '4' from dual union
select 400, 'M', 'R', 'Andra', '4' from dual;

alter table event add batAge varchar2(2 char); -- code 401
insert into codes (code, variable, language, description, metadata)
select 401, 'J', 'S', 'Juv', '1' from dual union
select 401, 'J', 'E', 'Juv', '' from dual union
select 401, 'J', 'R', 'Juv', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 401, 'S', 'S', 'Subadult', '2' from dual union
select 401, 'S', 'E', 'Subadult', '' from dual union
select 401, 'S', 'R', 'Subadult', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 401, 'A', 'S', 'Adult', '3' from dual union
select 401, 'A', 'E', 'Adult', '' from dual union
select 401, 'A', 'R', 'Adult', '' from dual;

alter table event add batAgeDeterminationMethod varchar2(2 char); -- code 402
insert into codes (code, variable, language, description, metadata)
select 402, 'L', 'S', 'Sorminivelten luutumisaste', '1' from dual union
select 402, 'L', 'E', 'Ossification of finger joints', '' from dual union
select 402, 'L', 'R', 'Ossification of finger joints', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 402, 'H', 'S', 'Huulitäplä', '2' from dual union
select 402, 'H', 'E', 'Chin spot', '' from dual union
select 402, 'H', 'R', 'Chin spot', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 402, 'K', 'S', 'Koko', '3' from dual union
select 402, 'K', 'E', 'Size', '' from dual union
select 402, 'K', 'R', 'Storlek', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 402, 'M', 'S', 'Maitohampaat', '4' from dual union
select 402, 'M', 'E', 'Milk teeth', '' from dual union
select 402, 'M', 'R', 'Mjölktänder', '' from dual;

alter table event add batRightArmLengthInMillis number(6,1);
alter table event add batLeftArmLengthInMillis number(6,1);
alter table event add batFifthFingerInMillis number(6,1);

alter table event add batReproductiveStage varchar2(2 char); -- code 403
insert into codes (code, variable, language, description, metadata)
select 403, 'E', 'S', 'Ei tiedossa', '0' from dual union
select 403, 'E', 'E', 'Not known', '' from dual union
select 403, 'E', 'R', 'Inte känd', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 403, '0', 'S', 'Ei ole imettänyt', '1' from dual union
select 403, '0', 'E', 'Has not lactated', '' from dual union
select 403, '0', 'R', 'Har inte laktat', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 403, '1', 'S', 'On imettänyt aiemmin (nisät erottuvat)', '2' from dual union
select 403, '1', 'E', 'Has lactated (nipples enlarged)', '' from dual union
select 403, '1', 'R', 'Har laktat (buggar synliga)', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 403, '2', 'S', 'Imettävä', '3' from dual union
select 403, '2', 'E', 'Lactading', '' from dual union
select 403, '2', 'R', 'Lakterande', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 403, '3', 'S', 'Kantava', '4' from dual union
select 403, '3', 'E', 'Gravid', '' from dual union
select 403, '3', 'R', 'Gravid', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 403, 'T', 'S', 'Koiraan testikset suurentuneet', '5' from dual union
select 403, 'T', 'E', 'Male’s testes enlarged', '' from dual union
select 403, 'T', 'R', 'Male’s testes enlarged', '' from dual;

alter table event add batHibernation varchar2(1 char); -- code 404
insert into codes (code, variable, language, description, metadata)
select 404, 'H', 'S', 'Horrostava', '5' from dual union
select 404, 'H', 'E', 'Hibernating', '' from dual union
select 404, 'H', 'R', 'Hibernating', '' from dual;




insert into codes (code, variable, language, description, metadata)
select 540, 'K', 'S', 'Kyllä', '1' from dual union
select 540, 'K', 'E', 'Yes', '' from dual union
select 540, 'K', 'R', 'Ja', '' from dual;
insert into codes (code, variable, language, description, metadata)
select 540, 'E', 'S', 'Ei', '2' from dual union
select 540, 'E', 'E', 'No', '' from dual union
select 540, 'E', 'R', 'Nej', '' from dual;


ALTER TABLE event ADD wingWear VARCHAR2(2 CHAR);
ALTER TABLE event ADD wingLengthAbnormalityReason VARCHAR2(2 CHAR);
ALTER TABLE event ADD eggInside VARCHAR2(2 CHAR);
ALTER TABLE event ADD cloacaSize VARCHAR2(2 CHAR);
ALTER TABLE event ADD cloacaStatus VARCHAR2(2 CHAR);
ALTER TABLE event ADD sicknessNotes VARCHAR(1500);
ALTER TABLE event ADD faultBars VARCHAR2(2 CHAR);
ALTER TABLE event ADD injuriesNotes VARCHAR(1500);
ALTER TABLE event ADD moultStatus VARCHAR2(2 CHAR);
ALTER TABLE event ADD ter1Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD ter2Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD ter3Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec6Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec5Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec4Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec3Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec2Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD sec1Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim1Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim2Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim3Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim4Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim5Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim6Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim7Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim8Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim9Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD prim10Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec1Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec2Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec3Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec4Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec5Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD rec6Moult VARCHAR2(2 CHAR);
ALTER TABLE event ADD bodyFeathersMoultIntensity  VARCHAR2(2 CHAR);
ALTER TABLE event ADD bodyFeathersMoultProgress VARCHAR2(2 CHAR);
ALTER TABLE event ADD tarsusLengthToes NUMBER(6,1);
ALTER TABLE event ADD tarsusLengthLastScale NUMBER(6,1);
ALTER TABLE event ADD tarsusDiameter NUMBER(6,1);
ALTER TABLE event ADD nailLength NUMBER(6,1);
ALTER TABLE event ADD prim1WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim2WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim3WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim4WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim5WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim6WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim7WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim8WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim9WingFormula NUMBER(6,1);
ALTER TABLE event ADD prim10WingFormula NUMBER(6,1);
ALTER TABLE event ADD outerSecondaryWingFormula NUMBER(6,1);
ALTER TABLE event ADD prim2Emargination NUMBER(6,1);
ALTER TABLE event ADD tailLength NUMBER(3);
ALTER TABLE event ADD tailLenthVertical NUMBER(3);
ALTER TABLE event ADD moultingAlulaLeft NUMBER(1);
ALTER TABLE event ADD moultingAlulaRight NUMBER(1);
ALTER TABLE event ADD moultingCarpiLeft NUMBER(1);
ALTER TABLE event ADD moultingCarpiRight NUMBER(1);
ALTER TABLE event ADD moultingBiCovertsLeft NUMBER(2);
ALTER TABLE event ADD moultingBiCovertsRight NUMBER(2);
ALTER TABLE event ADD moultingTetrialLeft NUMBER(1);
ALTER TABLE event ADD moultingTetrialRight NUMBER(1);
ALTER TABLE event ADD moultingTailLeft NUMBER(1);
ALTER TABLE event ADD moultingTailRight NUMBER(1);
ALTER TABLE event ADD beakLengthSkull NUMBER(6,1);
ALTER TABLE event ADD beakLengthFeathers NUMBER(6,1);
ALTER TABLE event ADD beakLengthNostril NUMBER(6,1);
ALTER TABLE event ADD headBeakLength NUMBER(6,1);
ALTER TABLE event ADD beakHeight NUMBER(6,1);
ALTER TABLE event ADD beakWidth NUMBER(6,1);
ALTER TABLE event ADD fichypDrost NUMBER(1);
ALTER TABLE event ADD pyrpyrTailSpots VARCHAR2(2 CHAR);
ALTER TABLE event ADD mites VARCHAR2(2 CHAR);
ALTER TABLE event ADD birdsflies VARCHAR2(2 CHAR);
ALTER TABLE event ADD fleas VARCHAR2(2 CHAR);
ALTER TABLE event ADD lices VARCHAR2(2 CHAR);
ALTER TABLE event ADD sarcopticManges VARCHAR2(2 CHAR);
ALTER TABLE event ADD poxVirus VARCHAR2(2 CHAR);
ALTER TABLE event ADD parasiteSamples VARCHAR2(2 CHAR);
ALTER TABLE event ADD parasiteNotes VARCHAR(1500);


insert into codes (code, variable, language, description, metadata)
select 550, '1', 'S', 'Ei kulunut', '1' from dual union
select 550, '1', 'E', 'Ei kulunut', '1' from dual union
select 550, '1', 'R', 'Ei kulunut', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 550, '2', 'S', 'Vähän kulunut', '2' from dual union
select 550, '2', 'E', 'Vähän kulunut', '2' from dual union
select 550, '2', 'R', 'Vähän kulunut', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 550, '3', 'S', 'Huomattavasti kulunut', '3' from dual union
select 550, '3', 'E', 'Huomattavasti kulunut', '3' from dual union
select 550, '3', 'R', 'Huomattavasti kulunut', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 551, 'K', 'S', 'Kasvava poikas/juv kärki', '1' from dual union
select 551, 'K', 'E', 'Kasvava poikas/juv kärki', '1' from dual union
select 551, 'K', 'R', 'Kasvava poikas/juv kärki', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 551, 'S', 'S', 'Kärki sulkasadossa', '2' from dual union
select 551, 'S', 'E', 'Kärki sulkasadossa', '2' from dual union
select 551, 'S', 'R', 'Kärki sulkasadossa', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 551, 'P', 'S', 'Kärkisulat poikki epänormaalilla tavalla', '3' from dual union
select 551, 'P', 'E', 'Kärkisulat poikki epänormaalilla tavalla', '3' from dual union
select 551, 'P', 'R', 'Kärkisulat poikki epänormaalilla tavalla', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 552, '1', 'S', 'Kloaakki matalahko, laakean keilamainen (naarastyyppi)', '1' from dual union
select 552, '1', 'E', 'Kloaakki matalahko, laakean keilamainen (naarastyyppi)', '1' from dual union
select 552, '1', 'R', 'Kloaakki matalahko, laakean keilamainen (naarastyyppi)', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 552, '2', 'S', 'Kloaakki välimuotoinen', '2' from dual union
select 552, '2', 'E', 'Kloaakki välimuotoinen', '2' from dual union
select 552, '2', 'R', 'Kloaakki välimuotoinen', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 552, '3', 'S', 'Kloaakki korkeahko, tynnyrimäinen (koirastyyppi)', '3' from dual union
select 552, '3', 'E', 'Kloaakki korkeahko, tynnyrimäinen (koirastyyppi)', '3' from dual union
select 552, '3', 'R', 'Kloaakki korkeahko, tynnyrimäinen (koirastyyppi)', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 553, '0', 'S', 'Lepotila', '1' from dual union
select 553, '0', 'E', 'Lepotila', '1' from dual union
select 553, '0', 'R', 'Lepotila', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 553, '2', 'S', 'Aktiivinen', '2' from dual union
select 553, '2', 'E', 'Aktiivinen', '2' from dual union
select 553, '2', 'R', 'Aktiivinen', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 553, '1', 'S', 'Siltä väliltä', '3' from dual union
select 553, '1', 'E', 'Siltä väliltä', '3' from dual union
select 553, '1', 'R', 'Siltä väliltä', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 554, '0', 'S', 'Sulkasato ei alkanut', '1' from dual union
select 554, '0', 'E', 'Sulkasato ei alkanut', '1' from dual union
select 554, '0', 'R', 'Sulkasato ei alkanut', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 554, '1', 'S', 'Käynnissä', '2' from dual union
select 554, '1', 'E', 'Käynnissä', '2' from dual union
select 554, '1', 'R', 'Käynnissä', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 554, '2', 'S', 'Ohi', '3' from dual union
select 554, '2', 'E', 'Ohi', '3' from dual union
select 554, '2', 'R', 'Ohi', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '0', 'S', 'Vanha sulka', '1' from dual union
select 555, '0', 'E', 'Vanha sulka', '1' from dual union
select 555, '0', 'R', 'Vanha sulka', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '1', 'S', 'Sulka pudonnut tai uusi sulka vasta piikimäinen tuppi', '2' from dual union
select 555, '1', 'E', 'Sulka pudonnut tai uusi sulka vasta piikimäinen tuppi', '2' from dual union
select 555, '1', 'R', 'Sulka pudonnut tai uusi sulka vasta piikimäinen tuppi', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '2', 'S', 'Tupen kärki avautunut, sulan pituus n. kolmannes', '3' from dual union
select 555, '2', 'E', 'Tupen kärki avautunut, sulan pituus n. kolmannes', '3' from dual union
select 555, '2', 'R', 'Tupen kärki avautunut, sulan pituus n. kolmannes', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '3', 'S', 'Sulan pituus n.2/3 lopullisesta', '4' from dual union
select 555, '3', 'E', 'Sulan pituus n.2/3 lopullisesta', '4' from dual union
select 555, '3', 'R', 'Sulan pituus n.2/3 lopullisesta', '4' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '4', 'S', 'Melkein täysimittainen, tuppea vielä jäljellä', '5' from dual union
select 555, '4', 'E', 'Melkein täysimittainen, tuppea vielä jäljellä', '5' from dual union
select 555, '4', 'R', 'Melkein täysimittainen, tuppea vielä jäljellä', '5' from dual;
insert into codes (code, variable, language, description, metadata)
select 555, '5', 'S', 'Täysimittainen, ei enää tuppea', '6' from dual union
select 555, '5', 'E', 'Täysimittainen, ei enää tuppea', '6' from dual union
select 555, '5', 'R', 'Täysimittainen, ei enää tuppea', '6' from dual;
insert into codes (code, variable, language, description, metadata)
select 556, 'J', 'S', 'Äskettäin pesästä lähtenyt Juv-lintu, käsisulat vielä kasvussa', '1' from dual union
select 556, 'J', 'E', 'Äskettäin pesästä lähtenyt Juv-lintu, käsisulat vielä kasvussa', '1' from dual union
select 556, 'J', 'R', 'Äskettäin pesästä lähtenyt Juv-lintu, käsisulat vielä kasvussa', '1' from dual;
insert into codes (code, variable, language, description, metadata)
select 556, 'U', 'S', 'Ruumiinhöyhenet pääosin vanhoja nuoruuspuvun höyheniä, alle kolmasosa vaihdettu', '2' from dual union
select 556, 'U', 'E', 'Ruumiinhöyhenet pääosin vanhoja nuoruuspuvun höyheniä, alle kolmasosa vaihdettu', '2' from dual union
select 556, 'U', 'R', 'Ruumiinhöyhenet pääosin vanhoja nuoruuspuvun höyheniä, alle kolmasosa vaihdettu', '2' from dual;
insert into codes (code, variable, language, description, metadata)
select 556, 'M', 'S', '1/3-2/3 ruumiinhöyhenistä vaihdettu', '3' from dual union
select 556, 'M', 'E', '1/3-2/3 ruumiinhöyhenistä vaihdettu', '3' from dual union
select 556, 'M', 'R', '1/3-2/3 ruumiinhöyhenistä vaihdettu', '3' from dual;
insert into codes (code, variable, language, description, metadata)
select 556, 'N', 'S', 'Ruumiinhöyhenet pääosin vaihdettu, yli 2/3 vaihdettu', '4' from dual union
select 556, 'N', 'E', 'Ruumiinhöyhenet pääosin vaihdettu, yli 2/3 vaihdettu', '4' from dual union
select 556, 'N', 'R', 'Ruumiinhöyhenet pääosin vaihdettu, yli 2/3 vaihdettu', '4' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, 'E', 'S', 'Ei ole', '01' from dual union
select 541, 'E', 'E', 'Ei ole', '01' from dual union
select 541, 'E', 'R', 'Ei ole', '01' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '0', 'S', '0', '02' from dual union
select 541, '0', 'E', '0', '02' from dual union
select 541, '0', 'R', '0', '02' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '1', 'S', '1', '03' from dual union
select 541, '1', 'E', '1', '03' from dual union
select 541, '1', 'R', '1', '03' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '2', 'S', '2', '04' from dual union
select 541, '2', 'E', '2', '04' from dual union
select 541, '2', 'R', '2', '04' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '3', 'S', '3', '05' from dual union
select 541, '3', 'E', '3', '05' from dual union
select 541, '3', 'R', '3', '05' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '4', 'S', '4', '06' from dual union
select 541, '4', 'E', '4', '06' from dual union
select 541, '4', 'R', '4', '06' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '5', 'S', '5', '07' from dual union
select 541, '5', 'E', '5', '07' from dual union
select 541, '5', 'R', '5', '07' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '6', 'S', '6', '08' from dual union
select 541, '6', 'E', '6', '08' from dual union
select 541, '6', 'R', '6', '08' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '7', 'S', '7', '09' from dual union
select 541, '7', 'E', '7', '09' from dual union
select 541, '7', 'R', '7', '09' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '8', 'S', '8', '10' from dual union
select 541, '8', 'E', '8', '10' from dual union
select 541, '8', 'R', '8', '10' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '9', 'S', '9', '11' from dual union
select 541, '9', 'E', '9', '11' from dual union
select 541, '9', 'R', '9', '11' from dual;
insert into codes (code, variable, language, description, metadata)
select 541, '10', 'S', '10', '12' from dual union
select 541, '10', 'E', '10', '12' from dual union
select 541, '10', 'R', '10', '12' from dual;

insert into codes (code, variable, language, description, metadata)
select 557, '0', 'S', 'Ei kasvavien sulkien tuppia', '0' from dual union
select 557, '0', 'E', 'Ei kasvavien sulkien tuppia', '0' from dual union
select 557, '0', 'R', 'Ei kasvavien sulkien tuppia', '0' from dual;
insert into codes (code, variable, language, description, metadata)
select 557, '1', 'S', 'Tuppia alle 20', '0' from dual union
select 557, '1', 'E', 'Tuppia alle 20', '0' from dual union
select 557, '1', 'R', 'Tuppia alle 20', '0' from dual;
insert into codes (code, variable, language, description, metadata)
select 557, '2', 'S', 'Runsaasti kasvavia vielä tupellisia höyheniä', '0' from dual union
select 557, '2', 'E', 'Runsaasti kasvavia vielä tupellisia höyheniä', '0' from dual union
select 557, '2', 'R', 'Runsaasti kasvavia vielä tupellisia höyheniä', '0' from dual;


insert into codes (code, variable, language, description) values (
900, '316', 'E', 'Field-readable condition');
insert into codes (code, variable, language, description) values (
900, '400', 'E', 'Bat capture place');
insert into codes (code, variable, language, description) values (
900, '401', 'E', 'Bat age');
insert into codes (code, variable, language, description) values (
900, '402', 'E', 'Bat age determination method');
insert into codes (code, variable, language, description) values (
900, '403', 'E', 'Bat reproductive stage');
insert into codes (code, variable, language, description) values (
900, '404', 'E', 'Bat hibernation');
insert into codes (code, variable, language, description) values (
900, '540', 'E', 'Yes/No');
insert into codes (code, variable, language, description) values (
900, '604', 'E', 'Field-readable changes');
insert into codes (code, variable, language, description) values (
900, '503', 'E', 'hirrusMuscleFitness');
insert into codes (code, variable, language, description) values (
900, '509', 'E', 'hirrusBodyMoulting');
insert into codes (code, variable, language, description) values (
900, '501', 'E', 'hirrusBroodPatc');
insert into codes (code, variable, language, description) values (
900, '541', 'E', 'Parasites amount');
insert into codes (code, variable, language, description) values (
900, '510', 'E', 'hirrus remiges');
insert into codes (code, variable, language, description) values (
900, '557', 'E', 'bodyFeathersMoultIntensity');
insert into codes (code, variable, language, description) values (
900, '556', 'E', 'bodyFeathersMoultProgress');
insert into codes (code, variable, language, description) values (
900, '554', 'E', 'MoultStatus (simplified)');
insert into codes (code, variable, language, description) values (
900, '555', 'E', 'MoultStatus status');
insert into codes (code, variable, language, description) values (
900, '552', 'E', 'Cloaca size');
insert into codes (code, variable, language, description) values (
900, '553', 'E', 'Cloaca status');
insert into codes (code, variable, language, description) values (
900, '550', 'E', 'Wing wear');
insert into codes (code, variable, language, description) values (
900, '551', 'E', 'Wing length abnormality reason');

------------------------------------------------------------



drop sequence id;
drop sequence jseq;
drop sequence mid;
drop sequence rid;
drop sequence vroska_seq;
drop sequence vtap_seq;
drop sequence seq_diario;
drop sequence seq_oloid;
drop sequence seq_poikanenid;
drop sequence seq_tarkastusid;
drop sequence seq_maallikko;

select max(id)+10 from layman; 
create sequence seq_layman start with --;


7.5. muuta lintuvaaraan muuttuneiden taulujen nimet ja kentät

ltkm_master:
grant select,update on tipu_production.ringer to lintuvaara_production;

app/legacy_models/legacy_rengastaja.rb  <- muista table: "tipu_production.ringer"
app/views/user_preferences/modify.html.erb
app/views/user_preferences/index.html.erb
lib/legacy_user_behaviour.rb
app/legacy_models/interface_for_legacy_user.rb

app/contr/application..
app/contr/external..
app/model/external..




DROP MATERIALIZED VIEW kontrolli;

CREATE VIEW kontrolli
AS
	SELECT 	
		event.id as id, 
		CASE event.type WHEN 'ringing' THEN 'Rengastus' WHEN 'recovery' THEN 'Tapaaminen' END as tyyppi,
		event.eventDate as pvm,
		event.namering as nimirengas,
		event.legring as jalkarengas,
		event.newlegring as vaihdetturengas,
		event.fieldReadableCode as varitunnus,
		event.mainColor as varitunnus_pohjanvari,
		event.species as laji,
		event.ringer as rengastaja,
		ringing.ringer as omistaja,
		event.laymanid as maallikko,
		to_number(to_char(event.eventDate, 'YYYY')) as vuosi,
		municipality.joinedTo as kunta,
		euringprovince.country as maa,
		event.euringprovincecode as euringprovinssi,
		event.locality as tarkka_paikka,
		event.birdstation as lintuasema,
		event.uniformLat as yht_leveys,
		event.uniformLon as yht_pituus,
		event.wgs84DecimalLat as des_leveys,
		event.wgs84DecimalLon as des_pituus,
		event.hour as klo,
		event.age as ika,
		event.sex as sukupuoli,
		event.weightingrams as paino,
		event.winglengthinmillimeters as siipi,
		event.numberofyoungs as poikuekoko,
		event.clutchnumber as poikuenro,
		event.ageofyoungsindays as poikasten_ika,
		event.capturemethod as pyyntitapa,
		event.recoverymethod as loytotapa
	FROM
		event
		JOIN event ringing ON (ringing.namering = event.namering AND ringing.type = 'ringing')
		LEFT JOIN municipality ON (event.municipality = municipality.id)
		LEFT JOIN euringprovince ON (event.euringprovincecode = euringprovince.id)
	WHERE event.eventDate IS NOT NULL
;


Sulka:
poista user settings, user views


8. -avaa järjestelmät, tipu-api,rengastus:deploy,sulka:start (prod:pontikka,tammitikka,h92,fmnh-prod)
9. deploy tulospalvelu
10. muuta hatikan kyselyt käyttämään uusia tauluja 
 


alter table JAKELU rename to old_jakelu;
alter table KUNTA rename to old_kunta;
alter table LAANI rename to old_laani;
alter table LAJI rename to old_laji;
alter table LINTUASEMA rename to old_lintuasema;
alter table LYL_ALUE rename to old_lyl_alue;
alter table MAAKUNTA rename to old_maakunta;
alter table MAALLIKKO rename to old_maallikko;
alter table OSOITEMAA rename to old_osoitemaa;
alter table RENGAS rename to old_rengas;
alter table RENGASTAJA rename to old_rengastaja;
alter table RENGASTUSKESKUS	rename to old_rengastuskeskus;
alter table RSARJA rename to old_rsarja;
alter table SANASTO rename to old_sanasto;
alter table ULKOMAINEN_PAIKKA rename to old_ulkomainen_paikka;
alter table VRENGAS rename to old_vrengas;
alter table YMPKESK rename to old_ympkesk;
alter table rengastus rename to old_rengastus;
alter table tapaaminen rename to old_tapaaminen;
alter table rengastaja_synonyymit rename to old_rengastaja_synonyymit;
alter table rhirrus rename to old_rhirrus;
alter table thirrus rename to old_thirrus;
alter table vrengastus rename to old_vrengastus;
alter table vtapaaminen rename to old_vtapaaminen;
drop table tohjaus;
drop table tohjaus_temp;
drop materialized view lajien_ikaennatykset;



