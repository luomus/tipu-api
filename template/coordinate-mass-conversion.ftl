<!DOCTYPE html>

<head>
	<meta charset="utf-8" />

	<title>Coordinate mass conversion service | LUOMUS</title>
	<link href="${staticURL}/favicon.ico?${staticContentTimestamp}" type="image/ico" rel="shortcut icon" />
		
	<link href="${staticURL}/luomus.css?${staticContentTimestamp}" rel="stylesheet" />
	
</head>

<body>

	<div id="masthead" role="banner">
		<div id="masthead-inner">
				
			<div id="logo">&nbsp;</div>
						
				<div id="sitetitle">
					Luomus Utils
				</div>
			
			
			<#if inStagingMode>     <span class="devmode">TEST ENVIROMENT</span></#if>	
    		<#if inDevelopmentMode> <span class="devmode">DEV ENVIROMENT</span></#if>	
			
			<div id="navigation-wrap" role="navigation">
				<nav id="mainmenu" role="navigation">
					<ul class="nav-bar" role="menu">
						<li role="menuitem"><a href="${baseURL}/coordinates">Coordinate mass conversion</a></li>
					</ul>
				</nav>
		    </div>
		</div>
	</div>
	
	<!-- Content section -->
	<div id="main-area" role="main">
		<div id="content-wrapper">
		
		<div id="content" class="page-content">
					<h2>Coordinate mass conversion service</h2>
					
					<p>Upload a file that contains coordinates in one (or several) formats and get them back converted to all supported formats.</p>
					<div style="background-color: rgb(201, 228, 255); padding: 20px; display: inline-block;">  
					<form action="${baseURL}/coordinates" method="post" enctype="multipart/form-data">
						<input type="file" name="file" />
						<input type="submit" name="submit" value="Submit" />
					</form>
					</div>
					
					<#if error??>
						<div style="max-width: 300px; padding: 30px; margin: 10px; background-color:rgb(255,240,240); border: 1px dashed red;">${error}</div>
					</#if>
					
					<h5>Input file format</h5>
					<p>Input file is is a text file following the format described bellow.</p>
<pre>
#rowid,lat,lon,type       
rowid1,62.21,31.21,wgs84      # first value (rowid) can be anything: it is returned in the result as it is  
rowid2,62.21,31.21            # if format not given, assumes same as above
rowid3,62.24,33.21            # for decimal types must use dot as decimal separator
rowid4,6345147,3378945,ykj
rowid5,6345,3378              # for ykj a shorter format can be used (zeros will be added to get full length)
# everything after #-sign is ignored (comments)
</pre>
					<h5>Coordinate formats that can be used in the input file</h5>
					<p>Bellow are listed all the values that can be used as type -value in input file format. They are case-insensitive (wgs84 vs WGS84).</p> 
					<p>All lat,lon values are given in N and E coordinates (so S and W coordinates are given minus signed).</p>
					<p>Same formats are used in output file, plus a couple extra format that are not supported for input file.</p>
<table style="border-spacing: 10px; border-collapse: separate;">
	<thead>
		<tr>
			<th style="min-width: 200px;">Type</th>
			<th style="min-width: 250px;">Example value</th>
			<th>Notes</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>etrs89</td>
			<td>31.025</td>
			<td>ETRS89 is in practice the same thing as WGS84</td>
		</tr>
		<tr>
			<td>wgs84</td>
			<td>-11.0</td>
			<td>WGS84 is in practice the same thing as ETRS89</td>
		</tr>
		<tr>
			<td>etrs89-degrees</td>
			<td>(d)ddmmss<br />for example -1213112 = -121°31'12"</td>
			<td>ETRS89 ~ WGS84</td>
		</tr>
		<tr>
			<td>wgs84-degrees</td>
			<td>(d)ddmmss<br />for example 10107 = 1°1'7"</td>
			<td>WGS84 ~ ETRS89</td>
		</tr>
		<tr>
			<td>euref</td>
			<td>6657204 (N)  329900 (E)</td>
			<td>Euref coordinates are also known as ETRS-TM35FIN and TRS-TM35FIN. Unit is meters. Cut-off values can not be used (unlike with Uniform coordinates).</td>
		</tr>
		<tr>
			<td>uniform</td>
			<td>6354717 (N)  3367456 (E) or <br /> 6354 (N)  3367 (E)</td>
			<td>Uniform coordinates are also known as YKJ and KKJ yhtenäiskoordinaatit. Unit is meters. Shorter format can be used. Zeros are added to get full length.</td>
		</tr>
		<tr>
			<td>kkj-decimal</td>
			<td>31.025</td>
			<td>KKJ geographic coordinates (not exactly the same as WGS84/ETRS89 but almost)</td>
		</tr>
		<tr>
			<td>kkj-degrees</td>
			<td>(d)ddmmss<br />for example 10107 = 1°1'7"</td>
			<td>KKJ geographic coordinates (not exactly the same as WGS84/ETRS89 but almost)</td>
		</tr>
	</tbody>
</table>
			
			<h5>Output file format</h5>
<pre>
${outputHeader}
rowid1,666,333,ykj,60.0167,23.9481,600100,235653,6657204,329900,6660000,3330000,60.0166,23.9513,600059,235704,60° 01' 00",23° 56' 53",60° 00' 59",23° 57' 04"
rowid2, ...
rowid3, ...
...
</pre>

<br />
<p>Have fun!</p>
        	</div>
			
			<div class="clear"></div>
		</div>		
	</div>
		
  </body>
</html>



		