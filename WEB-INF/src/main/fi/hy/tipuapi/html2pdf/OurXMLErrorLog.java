package fi.hy.tipuapi.html2pdf;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class OurXMLErrorLog implements ErrorHandler {

		private final StringBuilder builder = new StringBuilder();

		public void append(Exception e) {
			builder.append(e.toString());
			builder.append('\n');
		}

		@Override
		public void error(SAXParseException e) {
			append(e);
		}

		@Override
		public void fatalError(SAXParseException e) {
			append(e);
		}

		@Override
		public void warning(SAXParseException e) {
			append(e);
		}

		@Override  
		public String toString() {
			return builder.toString();
		}

		public boolean hasErrors() {
			return builder.length() > 0;
		}

	}