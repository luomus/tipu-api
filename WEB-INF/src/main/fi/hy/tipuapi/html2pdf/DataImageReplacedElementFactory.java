package fi.hy.tipuapi.html2pdf;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

public class DataImageReplacedElementFactory implements ReplacedElementFactory {

	private static final Pattern IMG_SRC_URI_REGEXP = Pattern.compile("^data:image/(gif|jpeg|png);base64,([A-Za-z0-9+/\\s]+=*)$");

	private final int dotsPerPixel;

	public DataImageReplacedElementFactory(int dotsPerPixel) {
		this.dotsPerPixel = dotsPerPixel;
	}

	@Override
	public ReplacedElement createReplacedElement(LayoutContext c, BlockBox box, UserAgentCallback uac, int cssWidth, int cssHeight) {
		Element element = box.getElement();
		if (!element.getNodeName().equals("img")) return null;
		String imageElementSrc = element.getAttribute("src");
		Matcher match = IMG_SRC_URI_REGEXP.matcher(imageElementSrc);
		if (!match.matches())
			return null;
		// String imageFormat = match.group(1); Not needed for now, maybe later if we want to add support for other formats than PNG
		String imageDataAsBase64 = match.group(2);
		byte[] imageBytes = DatatypeConverter.parseBase64Binary(imageDataAsBase64);
		FSImage fsImage;
		try {
			Image image = Image.getInstance(imageBytes);
			image.scaleAbsolute(
					dotsPerPixel * image.getPlainWidth(),
					dotsPerPixel * image.getPlainHeight());
			fsImage = new ITextFSImage(image);
		} catch (BadElementException e1) {
			return null;
		} catch (IOException e1) {
			return null;
		}
		if ((cssWidth != -1) || (cssHeight != -1)) {
			fsImage.scale(cssWidth, cssHeight);
		}
		return new ITextImageElement(fsImage);
	}

	@Override
	public void reset() {
	}

	@Override
	public void remove(Element e) {
	}

	@Override
	public void setFormSubmissionListener(FormSubmissionListener listener) {
	}

	// If we wanted to fallback to downloading the image from a traditional web URL:
	//
	//   fsImage = uac.getImageResource(srcAttr).getImage();

}
