package fi.hy.tipuapi.html2pdf;


public class HTML2PDFException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public HTML2PDFException(String message) {
		super(message);
	}

	public HTML2PDFException(String message, Exception source) {
		super(message, source);
	}
	
}