package fi.hy.tipuapi.general;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class Resource {

	public static enum Lang { FI, SV, EN, LA, ES }

	/**
	 * Static field which can be used with orderby() to denote the spot of ID field in order by statement
	 */
	public static final Field ID_FIELD = new Field("ID_FIELD", "DEFINITION");

	public static class Field {
		private final String name;
		private final Lang lang;
		private final String databaseColumnName;
		private final List<Field> childFields = new ArrayList<>();
		private final List<Attribute> attributes = new ArrayList<>();

		public Field(String name) {
			this(name, null, null);
		}

		public Field(String name, String databaseColumnName) {
			this(name, null, databaseColumnName);
		}

		public Field(String name, Lang lang, String databaseColumnName) {
			this.name = name.toLowerCase();
			this.lang = lang;
			if (databaseColumnName != null) {
				this.databaseColumnName = databaseColumnName.toLowerCase();
			} else {
				this.databaseColumnName = databaseColumnName;
			}
		}

		public Field(String name, Field... fields) {
			this(name, null, null);
			for (Field f : fields) {
				this.childFields.add(f);
			}
		}

		public String getName() {
			return name;
		}

		public Lang getLang() {
			return lang;
		}

		public String getDatabaseColumnName() {
			return databaseColumnName;
		}

		public boolean hasChildFields() {
			return !childFields.isEmpty();
		}

		public Field defineAttribute(Attribute attribute) {
			attributes.add(attribute);
			return this;
		}

		public boolean hasAttributes() {
			return !attributes.isEmpty();
		}
	}

	public static class Attribute {
		private final String name;
		private final String databaseColumnName;

		public Attribute(String name, String databaseColumnName) {
			this.name = name;
			this.databaseColumnName = databaseColumnName;
		}

		public String getName() {
			return name;
		}

		public String getDatabaseColumnName() {
			return databaseColumnName;
		}
	}

	public static class FilterClause {

		private final String field;
		private final String value;

		public FilterClause(String field, String value) {
			this.field = field;
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public String getField() {
			return field;
		}

	}

	private static final long DEFAULT_CACHE_TIME_SECONDS = 30 * 60;
	private final String resourceName;
	private final String resourceUnitName;
	private final String tableName;
	private final List<Join> joinQueries = new ArrayList<>();
	private final List<Field> fields = new ArrayList<>();
	private final List<Attribute> attributes = new ArrayList<>();
	private final String idDatabaseColumnName;
	private final ConnectionDescription conDescription;
	private final ErrorReporter errorReporter;
	private final List<Replace> definedReplaces = new ArrayList<>();
	private List<String> orderBy = new ArrayList<>();
	private final List<FilterClause> filterClauses = new ArrayList<>();

	private final Cached<String, String> xmlStorage = new Cached<>(new CacheLoader<String, String>() {
		@Override
		public String load(String key) {
			if (key.equals("")) {
				return new XMLWriter(documentStorage.get()).generateXML();
			}
			if (key.startsWith("filter=")) {
				String filter = getValue(key);
				Document allData = documentStorage.get();
				Document filteredData = new Document(resourceName);

				for (Node unit : allData.getRootNode()) {
					String fieldValues = "";
					for (Node field : unit) {
						fieldValues += field.getContents() + " ";
					}
					fieldValues = fieldValues.toLowerCase();
					if (fieldValues.contains(filter)) {
						filteredData.getRootNode().addChildNode(unit);
					}
				}
				return new XMLWriter(filteredData).generateXML();
			}
			if (key.startsWith("id=")) {
				String id = getValue(key);
				Document data = new Document(resourceName);
				TransactionConnection con = openConnection();
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					String sql = constructQuery(id);
					p = con.prepareStatement(sql);
					p.setString(1, id);
					rs = p.executeQuery();
					rs.setFetchSize(4001);
					while (rs.next()) {
						Node node = data.getRootNode().addChildNode(resourceUnitName);
						addUnit(fields, node, rs);
					}

				} catch (Exception e) {
					handle(e);
				} finally {
					Utils.close(p, rs, con);
				}
				return new XMLWriter(data).generateXML();
			}
			throw new RuntimeException("No loader implemented for key:" + key);
		}

		private String getValue(String key) {
			String filter = key.split("=")[1];
			return filter;
		}

	}, DEFAULT_CACHE_TIME_SECONDS, TimeUnit.SECONDS, 50);

	private TransactionConnection openConnection() throws RuntimeException {
		try {
			return new PreparedStatementStoringAndClosingTransactionConnection(conDescription);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private final SingleObjectCache<Document> documentStorage = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<Document>() {
		@Override
		public Document load() {
			Document data = new Document(resourceName);

			TransactionConnection con = openConnection();
			PreparedStatement p = null;
			ResultSet rs = null;

			try {
				String sql = constructQuery(null);
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				rs.setFetchSize(4001);
				Node rootNode = data.getRootNode();
				while (rs.next()) {
					Node unitNode = rootNode.addChildNode(resourceUnitName);
					addUnit(fields, unitNode, rs);
				}
			} catch (Exception e) {
				handle(e);
			} finally {
				Utils.close(p, rs, con);
			}
			return data;
		}
	}, DEFAULT_CACHE_TIME_SECONDS, TimeUnit.SECONDS);



	public Resource(String resourceName, String resourceUnitName, String fromQuery, String as, String idColumnName, ResourceConsumer consumer) {
		this.resourceName = resourceName.toLowerCase();
		this.resourceUnitName = resourceUnitName.toLowerCase();
		this.tableName = fromQuery + " " + as;
		this.idDatabaseColumnName = idColumnName.toUpperCase();
		this.fields.add(new Field("id",  idColumnName));
		this.orderBy.add(this.idDatabaseColumnName);
		this.conDescription = consumer.connectionDescription();
		this.errorReporter = consumer.errorReporter();
	}

	public Resource(String resourceName, String resourceUnitName, String databaseTableName, String idColumnName, ResourceConsumer consumer) {
		this.resourceName = resourceName.toLowerCase();
		this.resourceUnitName = resourceUnitName.toLowerCase();
		this.tableName = databaseTableName.toUpperCase();
		this.idDatabaseColumnName = databaseTableName + "." + idColumnName.toUpperCase();
		this.fields.add(new Field("id",  databaseTableName + "." + idColumnName));
		this.orderBy.add(this.idDatabaseColumnName);
		this.conDescription = consumer.connectionDescription();
		this.errorReporter = consumer.errorReporter();
	}

	public void defineField(Field field) {
		this.fields.add(field);
		orderBy.add(field.getDatabaseColumnName());
	}

	public void defineAttribute(Attribute attribute) {
		attributes.add(attribute);
	}

	public void orderBy(Field... fields) {
		orderBy = new LinkedList<>();
		for (Field f : fields) {
			if (f == ID_FIELD) {
				orderBy.add(this.fields.get(0).getDatabaseColumnName());
			} else {
				orderBy.add(f.getDatabaseColumnName());
			}
		}
	}

	public void orderBy(String... orderbyStatementParts) {
		orderBy = new LinkedList<>();
		for (String s : orderbyStatementParts) {
			orderBy.add(s);
		}
	}

	private String constructQuery(String id) {
		StringBuilder query = new StringBuilder(" SELECT ");

		toSelectStatement(query, fields);
		if (!attributes.isEmpty()) {
			query.append(" , ");
		}
		toSelectStatement(query, attributes);

		query.append(" FROM ").append(tableName);
		for (Join join : joinQueries) {
			query.append(" LEFT JOIN ").append(join.tableName).append(" ").append(join.as);
			query.append(" ON ( ").append(join.on).append(" ) ");
		}
		query.append(" WHERE 1=1 ");

		if (id != null) {
			query.append(" AND ").append(idDatabaseColumnName).append(" = ? ");
		}
		for (FilterClause filterClause : filterClauses) {
			query.append(" AND " + filterClause.getField() + " = " + filterClause.getValue());
		}

		query.append(" ORDER BY ");
		Utils.toCommaSeperatedStatement(query, orderBy);
		return query.toString();
	}

	private static void toSelectStatement(StringBuilder query, List<?> selects) {
		for (Object o : selects) {
			if (o instanceof Field) {
				Field f = (Field) o;
				if (f.hasChildFields()) {
					toSelectStatement(query, f.childFields);
					query.append(" ,");
				} else {
					query.append(" ").append(f.getDatabaseColumnName()).append(" AS ").append(fromColumnnameToAsName(f.getDatabaseColumnName())).append(" ,");
					if (f.hasAttributes()) {
						for (Attribute a : f.attributes) {
							query.append(" ").append(a.getDatabaseColumnName()).append(" AS ").append(fromColumnnameToAsName(a.getDatabaseColumnName())).append(" ,");
						}
					}
				}
			} else if (o instanceof Attribute) {
				Attribute a = (Attribute) o;
				query.append(" ").append(a.getDatabaseColumnName()).append(" AS ").append(fromColumnnameToAsName(a.getDatabaseColumnName())).append(" ,");
			}
		}
		Utils.removeLastChar(query);
	}

	private static Map<String, String> columnNameToAsNameMap = new HashMap<>();

	/**
	 * Kentän nimi voi olla esim kunta.kulyh, mutta Oraclessa ei saa olla as -lauseessa pistettä. Voisimme muuttaa pisteen alaviivaksi
	 * (SELECT kunta.kulyh AS kunta_kulyh), mutta tämä ei auta kun kentän nimi on "trim(maosoite1 || ' ' || maosoite2 || ' ' || maosoite3 || ' ' || maosoite4)".
	 * Siispä tässä generoidaan jokaiselle kentälle GUID, jota käytetään AS lauseena. Esim:
	 *
	 *  SELECT maallikko.manro AS AD3AA90A321434179B27C8DB9ED9BA
	 *
	 * Miksi tarvitaan AS-lausetta (rs.getString(columName)), eikä valita ResultSetistä indeksin mukaan (rs.getString(1))? :
	 * Oli vain helpompi toteuttaa näin.
	 *
	 * @param name
	 * @return
	 */
	private static String fromColumnnameToAsName(String name) {
		if (!columnNameToAsNameMap.containsKey(name)) {
			columnNameToAsNameMap.put(name, ("A" + Utils.generateGUID().replace("-", "")).substring(0, 30));
		}
		return columnNameToAsNameMap.get(name);
	}

	public String getAll() {
		return xmlStorage.get("");
	}

	public String getByFilter(String filter) {
		return xmlStorage.get("filter="+filter);
	}

	public String getById(String id) {
		return xmlStorage.get("id="+id);
	}

	public String getAllForceReload() {
		documentStorage.getForceReload();
		return xmlStorage.getForceReload("");
	}

	public String getByFilterForceReload(String filter) {
		documentStorage.getForceReload();
		return xmlStorage.getForceReload("filter="+filter);
	}

	public String getByIdForceReload(String id) {
		return xmlStorage.getForceReload("id="+id);
	}

	private void addUnit(List<Field> fields, Node parent, ResultSet rs) throws SQLException {
		for (Field f : fields) {
			if (f.hasChildFields()) {
				Node n = parent.addChildNode(f.getName());
				addUnit(f.childFields, n, rs);
			} else {
				String columnName = fromColumnnameToAsName(f.getDatabaseColumnName());
				String value = rs.getString(columnName);
				if (value == null) value = "";
				for (Replace replace : definedReplaces) {
					value = value.replace(replace.value, replace.with);
				}
				Node n = parent.addChildNode(f.getName()).setContents(value);
				if (f.lang != null) {
					n.addAttribute("lang", f.lang.toString());
				}
				if (f.hasAttributes()) {
					for (Attribute a : f.attributes) {
						addAttribute(n, rs, a);
					}
				}
			}
		}
		if (rootNode(fields)) {
			for (Attribute a : attributes) {
				addAttribute(parent, rs, a);
			}
		}
	}

	private boolean rootNode(List<Field> fields) {
		return fields == this.fields;
	}

	private void addAttribute(Node parent, ResultSet rs, Attribute a) throws SQLException {
		String columnName = fromColumnnameToAsName(a.getDatabaseColumnName());
		String value = rs.getString(columnName);
		if (value == null) value = "";
		for (Replace replace : definedReplaces) {
			value = value.replace(replace.value, replace.with);
		}
		parent.addAttribute(a.getName(), value);
	}

	private void handle(Exception e) {
		if (e.getMessage() != null && e.getMessage().contains("ORA-01722")) {
			// nothing to report... non-numeric parameter given as ID for a service which's ID field is numeric
			return;
		}
		errorReporter.report(e);
		throw new RuntimeException(e);
	}

	private class Join {
		private final String tableName;
		private final String as;
		private final String on;
		public Join(String tableName, String as, String on) {
			this.tableName = tableName;
			this.as = as;
			this.on = on;
		}
	}

	private class Replace {
		private final String value;
		private final String with;
		public Replace(String replaceAll, String with) {
			this.value = replaceAll;
			this.with = with;
		}
	}

	public void defineJoin(String tableName, String as, String on) {
		joinQueries.add(new Join(tableName, as, on));
	}

	public void defineReplace(char replaceAll) {
		this.definedReplaces.add(new Replace(Character.toString(replaceAll), ""));
	}

	public void defineFilterClause(FilterClause filterClause) {
		this.filterClauses.add(filterClause);
	}

}