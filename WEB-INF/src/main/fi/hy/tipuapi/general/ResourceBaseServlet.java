package fi.hy.tipuapi.general;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.XML;

import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

/**
 * This servlet prints a file to ServletRequest's result writer, if the token given as a parameter matches one of the authorized file requests.
 * Applications may authorize a file request by calling this class' static authorizeNewFileRequest() method, which returns the token to the caller of the method.
 */
public abstract class ResourceBaseServlet extends BaseServlet implements ResourceConsumer {
		
	protected static final long	serialVersionUID	= 6032478191269655143L;
	protected static final String FILTER_PARAMETER = "filter";
	protected static final String LANGUAGE_FILTER_PARAMETER = "lang";
		
	protected Resource resource;
		
	public ResourceBaseServlet() {
		super();
	}
	
	protected abstract Resource defineResource();
	
	
	/**
	 * Called when the servlet is initialized; either at server startup or when the servlet is called for the first time,
	 * depending on server configuration.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		resource = defineResource();
	}
	
	/**
	 * @param req request
	 * @param res response
	 * @throws IOException if  res.getWriter() throws an exception 
	 * @throws IOException
	 */
	/**
	 * @param req
	 * @param res
	 * @throws IOException
	 */
	@Override
	public void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String format = getFormat(req);
		String id = getId(req);
		String filter = getFilter(req);
		boolean forceReload = req.getParameter("forceReload") != null;
		PrintWriter out;

		String xml = produceXML(id, filter, forceReload);
		if (xml == null) {
			if (given(id)) {
				redirectTo404(res);
				return;
			}
			throw new Exception("Processing request failed");
		}
		
		if (jsonRequested(format)) {
			JSONObject jsonObject = XML.toJSONObject(xml);
			res.setContentType("application/json; charset=utf-8");
			out = res.getWriter();
			out.write(jsonObject.toString());
		} else {
			res.setContentType("application/xml; charset=utf-8");
			out = res.getWriter();
			out.write(xml);
		}

		out.flush();
	}

	protected String produceXML(String id, String filter, boolean forceReload) {
		if (given(id)) {
			if (forceReload) {
				return resource.getByIdForceReload(id);
			} 
			return resource.getById(id);
		} 
		if (given(filter)) {
			if (forceReload) {
				return resource.getByFilterForceReload(filter);
			}
			return resource.getByFilter(filter);
		}
		if (forceReload) {
			return resource.getAllForceReload();
		}
		return resource.getAll();
	}
	
	protected String getFilter(HttpServletRequest req) {
		String filter = req.getParameter(FILTER_PARAMETER);
		if (filter == null) return null;
		return Utils.removeWhitespace(filter).toLowerCase();
	}
	
	@Override
	public ConnectionDescription connectionDescription() {
		return connectionDescription;
	}

	@Override
	public ErrorReporter errorReporter() {
		return errorReporter;
	}
	
}
