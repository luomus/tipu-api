package fi.hy.tipuapi.general;

public class CoordinatesInsideMunicipalityUtil {
	
	public static double EARTH_RADIUS = 6372.795;
	
	/**
	 * Returns how far inside or outside the given municipality a certain point is
	 * @param municipality_des_lat
	 * @param municipality_des_lon
	 * @param municipality_radius
	 * @param point_des_lat
	 * @param point_des_lon
	 * @return
	 */
	public static double offsetToMunicipalityBorder(double municipality_des_lat, double municipality_des_lon, double municipality_radius, double point_des_lat, double point_des_lon) {
		double distance = distance(municipality_des_lat, municipality_des_lon, point_des_lat, point_des_lon);
		return distance - municipality_radius;
	}
	
	/**
	 * Checks that given pesa des coords are inside given kunta des coords/radius
	 * @param municipality_des_lat kunta leveys
	 * @param municipality_des_lon kunta pituus
	 * @param municipality_radius kunta säde
	 * @param point_des_lat pesä leveys
	 * @param point_des_lon pesä pituus
	 * @return true if inside, false if not
	 * @author Hali projects
	 */
	public static boolean insideMunicipality(double municipality_des_lat, double municipality_des_lon, double municipality_radius, double point_des_lat, double point_des_lon) {
		double distance = distance(municipality_des_lat, municipality_des_lon, point_des_lat, point_des_lon);
		return (Double.compare(distance, municipality_radius) < 0);
	}
	
	private static double distance(double municipality_des_lat, double municipality_des_lon, double point_des_lat, double point_des_lon) {
		point_des_lat = Math.toRadians(point_des_lat);
		point_des_lon = Math.toRadians(point_des_lon);
		municipality_des_lat = Math.toRadians(municipality_des_lat);
		municipality_des_lon = Math.toRadians(municipality_des_lon);
		
		double distLon = point_des_lon - municipality_des_lon;
		double distLat = point_des_lat - municipality_des_lat;
		
		double a = Math.pow(Math.sin(distLat / 2), 2);
		double b = Math.cos(municipality_des_lat) * Math.cos(point_des_lat) * Math.pow(distLon / 2, 2);
		double centralAngle = 2 * Math.asin(Math.sqrt(a + b));
				
		double distance = EARTH_RADIUS * centralAngle;
		return distance;
	}	
}
