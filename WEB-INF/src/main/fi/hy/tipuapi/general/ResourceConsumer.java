package fi.hy.tipuapi.general;

import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.reporting.ErrorReporter;

public interface ResourceConsumer {

	public ConnectionDescription connectionDescription();
	
	public ErrorReporter errorReporter();
	
}
