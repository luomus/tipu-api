package fi.hy.tipuapi.general;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReporterViaEmail;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public abstract class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = -1171093723362015629L;
	protected static final String FORMAT_PARAMETER = "format";

	protected final String configFile;
	protected Config config;
	protected ErrorReporter errorReporter;
	protected ConnectionDescription connectionDescription;

	private boolean errorReporting = true;

	protected void errorReporting(boolean value) {
		this.errorReporting = value;
	}

	public static enum FORMAT {
		JSON, XML
	}

	public BaseServlet() {
		this.configFile = resolveConfigFile();
	}

	private String resolveConfigFile() {
		String base = System.getProperty("catalina.base");
		String fullPath = base + File.separator + "app-conf" + File.separator + "tipu-api.properties";
		return fullPath;
	}

	/**
	 * Called when the servlet is initialized; either at server startup or when the servlet is called for the first time,
	 * depending on server configuration.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			this.config = new ConfigReader(configFile);
			this.connectionDescription = this.config.connectionDescription();
			if (this.config.developmentMode()) {
				this.errorReporter = new ErrorReportingToSystemErr();
			} else {
				this.errorReporter = new ErrorReporterViaEmail(this.config);
			}
		} catch (FileNotFoundException fnfe) {
			throw new ServletException("Ei löydy: " + configFile + ". Default folder: " + new File("foo.txt").getAbsolutePath());
		} catch (Exception initException) {
			throw new ServletException(initException);
		}

	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		try {
			req.setCharacterEncoding("UTF-8");
			// log(req);
			process(req, res);
		} catch (Exception e) {
			try {
				if (errorReporting) {
					errorReporter.report(e);
				}
			} catch (Exception errorReportinException) {
				e.printStackTrace();
			}
			throw new ServletException(e);
		}
	}

	protected void log(HttpServletRequest req) {
		String user = req.getRemoteUser();
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("[").append(user).append("] ");
		logMessage.append(req.getMethod()).append(" ").append(req.getRequestURI());
		logParams(req, logMessage);
		LogUtils.write(logMessage.toString(), config.logFolder(), "access-log-");
	}

	private void logParams(HttpServletRequest req, StringBuilder logMessage) {
		logMessage.append(" {");
		Enumeration<String> params = req.getParameterNames();
		while (params.hasMoreElements()) {
			String key = params.nextElement();
			String value = "";
			for (String v : req.getParameterValues(key)) {
				if (v.length() > 0) value += v.trim() + "|";
			}
			value = Utils.removeLastChar(value);
			if (value.length() > 0) {
				logMessage.append("\"").append(key).append("\"=\"").append(removeLinebreaks(value)).append("\" ");
			}
		}
		logMessage.append("}");
	}

	private String removeLinebreaks(String value) {
		value = value.replace("\r", " ");
		value = value.replace("\n", " ");
		return value;
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		doGet(req, res);
	}

	protected abstract void process(HttpServletRequest req, HttpServletResponse res) throws Exception;

	protected String getFormat(HttpServletRequest req) {
		return req.getParameter(FORMAT_PARAMETER);
	}

	protected boolean jsonRequested(String format) {
		return FORMAT.JSON.toString().equalsIgnoreCase(format);
	}

	protected static boolean given(String param) {
		if (param == null) return false;
		return param.length() > 0;
	}

	protected void redirectTo404(HttpServletResponse res) {
		res.setStatus(404);
		res.setHeader("Connection", "close");
	}

	protected void redirectTo500(HttpServletResponse res) {
		res.setStatus(500);
		res.setHeader("Connection", "close");
	}

	protected String getId(HttpServletRequest req) {
		String id = req.getPathInfo();
		if (id == null || id.equals("/")) {
			id = "";
		}
		return id.replaceFirst("/", "");
	}

	protected Config getConfig() {
		return this.config;
	}

}
