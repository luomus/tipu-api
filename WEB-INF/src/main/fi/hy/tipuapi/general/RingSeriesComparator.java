package fi.hy.tipuapi.general;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;

public class RingSeriesComparator implements Comparator<String> {

	private final TransactionConnection con;
	private final Map<String, Integer> seriesOrder;

	public RingSeriesComparator(TransactionConnection con) throws SQLException {
		this.con = con;
		seriesOrder = generateSeriesOrder();
	}

	@Override
	public int compare(String series1, String series2) {
		if (series1 == null || series2 == null) throw new NullPointerException();
		int i = getOrder(series1).compareTo(getOrder(series2));
		if (i != 0) return i;
		return series1.compareTo(series2);
	}

	private Integer getOrder(String series) {
		if (seriesOrder.containsKey(series)) {
			return seriesOrder.get(series);
		}
		return Integer.MIN_VALUE;
	}

	private Map<String, Integer> generateSeriesOrder() throws SQLException {
		Map<String, Integer> orderMap = new HashMap<>();
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			String sql = "" + 
					" SELECT trim(substr(ringStart,1,2)) AS series, height, sum(to_number(substr(ringEnd, 3, 9) - to_number(substr(ringStart, 3, 9)))) AS count " +
					" FROM ring " +
					" GROUP BY trim(substr(ringStart,1,2)), height " + 
					" ORDER BY series, count DESC ";
			p = con.prepareStatement(sql);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				String series = rs.getString(1);
				int height = rs.getInt(2);
				if (!orderMap.containsKey(series)) {
					orderMap.put(series, height);
				}
			}
			return orderMap;
		} finally {
			Utils.close(p, rs);
		}
	}
}
