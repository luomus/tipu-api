package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class DescriptionsOfCodes extends ResourceBaseServlet {

	private static final long serialVersionUID = -2778379649731077319L;

	public DescriptionsOfCodes() {
		super();
	}

	@Override
	protected Resource defineResource() {

		Resource resource = new Resource("descriptions-of-codes", "description", "" +
				" ( select distinct c1.code code, c2.description description" +
				"   from codes c1 " +
				"   left join codes c2 on (c1.code like c2.variable and c2.code = 900 and c2.language = 'E') " + 
				" ) ", 
				"c", "c.code", this);
		Field desc = new Field("name", Resource.Lang.EN, "c.description");
		resource.defineField(desc);

		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}


}
