package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;


public class Schemes extends ResourceBaseServlet {

	private static final long serialVersionUID = -8392106370278640222L;

	public Schemes() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("schemes", "scheme", "scheme", "id", this);
		resource.defineJoin("country", "country", "country.id = scheme.country");
		
		resource.defineAttribute(new Attribute("language", "CASE WHEN country.language = 'S' THEN 'fi' WHEN country.language = 'R' THEN 'sv' ELSE 'en' END"));
		resource.defineField(new Field("name", "scheme.name"));
		resource.defineField(new Field("address", "TRIM(addressLine1 || ' ' || addressLine2 || ' ' || addressLine3 || ' ' || addressLine4)"));
		resource.defineField(new Field("addresslines", 
				new Field("line", "addressLine1"), 
				new Field("line", "addressLine2"),
				new Field("line", "addressLine3"),
				new Field("line", "addressLine4"),
				new Field("line", "country.name")
		));
		resource.defineField(new Field("countryCode", "country.id"));
		resource.defineField(new Field("email", "scheme.email"));
		resource.orderBy(Resource.ID_FIELD); 
		return resource;
	}

	
}
