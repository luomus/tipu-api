package fi.hy.tipuapi.service;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.XML;

import fi.hy.tipuapi.general.BaseServlet;
import fi.hy.tipuapi.general.CoordinatesInsideMunicipalityUtil;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class CoordinateValidationService extends BaseServlet {

	private static final long serialVersionUID = 114252081541153667L;

	private final SingleObjectCache<TipuApiResource> municipalitiesCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<TipuApiResource>() {
		@Override
		public TipuApiResource load()  {
			TipuAPIClient api = null;
			try {
				api = new TipuAPIClientImple(config);
				return api.get(TipuAPIClient.MUNICIPALITIES);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				if (api != null) api.close();
			}
		}}, 12, TimeUnit.HOURS);

	private final SingleObjectCache<TipuApiResource> birdStationsCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<TipuApiResource>() {
		@Override
		public TipuApiResource load()  {
			TipuAPIClient api = null;
			try {
				api = new TipuAPIClientImple(config);
				return api.get(TipuAPIClient.BIRD_STATIONS);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				if (api != null) api.close();
			}
		}}, 12, TimeUnit.HOURS);

	private final SingleObjectCache<TipuApiResource> euringProvincesCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<TipuApiResource>() {
		@Override
		public TipuApiResource load()  {
			TipuAPIClient api = null;
			try {
				api = new TipuAPIClientImple(config);
				return api.get(TipuAPIClient.EURING_PROVINCES);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				if (api != null) api.close();
			}
		}}, 12, TimeUnit.HOURS);

	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		PrintWriter out;
		Document responseContent = buildResponse(req);
		String xml = new XMLWriter(responseContent).generateXML(); 

		String format = getFormat(req);
		if (jsonRequested(format)) {
			JSONObject jsonObject = XML.toJSONObject(xml);
			res.setContentType("application/json; charset=utf-8");
			out = res.getWriter();
			out.write(jsonObject.toString());
		} else {
			res.setContentType("application/xml; charset=utf-8");
			out = res.getWriter();
			out.write(xml);
		}
		out.flush();
	}

	private Document buildResponse(HttpServletRequest req) throws Exception {
		String municipality = req.getParameter("municipality");
		String birdStation = req.getParameter("birdstation");
		String euringProvince = req.getParameter("euringProvince");
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String type =  req.getParameter("type");

		if (given(municipality)) {
			return validate(municipality, lat, lon, type, municipalitiesCache.get());
		}
		if (given(birdStation)) {
			return validate(birdStation, lat, lon, type, birdStationsCache.get());
		}
		if (given(euringProvince)) {
			return validate(euringProvince, lat, lon, type, euringProvincesCache.get());
		}

		Document validationResponse = new Document("validation-response");
		Node response = validationResponse.getRootNode();
		setError(response, "You must give 'municipality', 'birdstation' or 'euringProvince' parameter.");
		return validationResponse;
	}

	public static Document validate(String region, String lat, String lon, String type, TipuApiResource regions) throws Exception {
		Document validationResponse = new Document("validation-response");
		Node response = validationResponse.getRootNode();

		if (!CoordinateConversionService.checkCoordinates(response, lat, lon)) return validationResponse;
		if (!CoordinateConversionService.checkType(response, type)) return validationResponse;

		Node regionInfo = fetchInfo(regions, region);
		if (regionInfo == null) {
			setError(response, "Region not found: " + region);
			return validationResponse;
		} 

		Node conversionResponse = convert(lat, lon, type);
		Double point_des_lat = Double.valueOf(conversionResponse.getNode("wgs84").getAttribute("lat"));
		Double point_des_lon = Double.valueOf(conversionResponse.getNode("wgs84").getAttribute("lon"));

		Double region_des_lat = Double.valueOf(regionInfo.getAttribute("centerpoint-lat"));
		Double region_des_lon = Double.valueOf(regionInfo.getAttribute("centerpoint-lon"));
		Double region_radius  = Double.valueOf(regionInfo.getAttribute("radius"));

		Double offset = CoordinatesInsideMunicipalityUtil.offsetToMunicipalityBorder(region_des_lat, region_des_lon, region_radius, point_des_lat, point_des_lon);

		if (offset.compareTo(0D) > 0) {
			response.addAttribute("pass", "false");
			response.addChildNode("offset").setContents(""+offset.intValue());
		} else {
			response.addAttribute("pass", "true");
		}
		return validationResponse;
	}

	private static Node convert(String lat, String lon, String type) {
		return CoordinateConversionService.convert(lat, lon, type).getRootNode();				
	}

	private static Node fetchInfo(TipuApiResource regions, String region) {
		Node regionInfo = null;
		if (regions.containsUnitById(region)) {
			return regions.getById(region);
		}
		for (Node candidate : regions.getAll()) {
			if (candidate.getNode("id").getContents().equalsIgnoreCase(region)) {
				return candidate;
			}
			for (Node name : candidate.getChildNodes("name")) {
				if (name.getContents().equalsIgnoreCase(region)) {
					regionInfo = candidate;
					break;
				}
			}
		}
		return regionInfo;
	}

	private static void setError(Node responseContent, String message) {
		responseContent.addAttribute("pass", "false");
		responseContent.addChildNode("error").setContents(message);
	}

}
