package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class ELYCentres extends ResourceBaseServlet {

	private static final long serialVersionUID = 4465232966677098415L;

	public ELYCentres() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("ely-centres", "centre", "elyCentre", "id", this);
		resource.defineField(new Field("name", Resource.Lang.FI, "name"));
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
