package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class EuringProvinces extends ResourceBaseServlet {

	private static final long serialVersionUID = -5001610052309728795L;

	public EuringProvinces() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("euring-provinces", "place", "euringProvince", "id", this);
		resource.defineJoin("country", "country", "country.id = euringProvince.country");
		resource.defineField(new Field("name", "euringProvince.name"));
		resource.defineField(new Field("country", Resource.Lang.EN, "country.name").defineAttribute(new Attribute("code", "euringProvince.country")));
		resource.defineField(new Field("province", "euringProvince.name"));
		
		resource.defineAttribute(new Attribute("centerpoint-lat", "euringProvince.centerPointDecimalLat"));
		resource.defineAttribute(new Attribute("centerpoint-lon", "euringProvince.centerPointDecimalLon"));
		resource.defineAttribute(new Attribute("radius", "euringProvince.radius"));
		
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
