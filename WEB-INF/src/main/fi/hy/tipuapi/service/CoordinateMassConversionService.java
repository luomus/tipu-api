package fi.hy.tipuapi.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import fi.luomus.commons.services.BaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

@MultipartConfig
public class CoordinateMassConversionService extends BaseServlet {

	private static final long serialVersionUID = -7122815375874091968L;

	@Override
	protected String configFileName() {
		return "tipu-api.properties";
	}

	@Override
	protected void applicationInit() {
	}

	@Override
	protected void applicationInitOnlyOnce() {
	}

	@Override
	protected void applicationDestroy() {
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData();
	}

	private ResponseData initResponseData() {
		return new ResponseData().setViewName("coordinate-mass-conversion").setData("outputHeader", OUTPUT_HEADER);
	}

	private static final String OUTPUT_HEADER = "#rowid,orig lat,orig lon,orig type,etrs89 lat,lon,etrs89-degrees lat,lon,euref lat,lon,uniform lat,lon,kkj-decimal lat,lon,kkj-degrees lat,lon,etrs89-degrees-ddd-mm-ss-formatted lat,lon,kkj-degrees-ddd-mm-ss-formatted lat,lon";

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		InputStream stream = null;
		try {
			Part filePart = req.getPart("file");
			if (filePart == null || filePart.getSize() == 0) {
				return error("You must submit a file.");
			}
			stream = filePart.getInputStream();
			List<String> lines = getLines(stream);
			return handle(lines, res);
		} finally {
			if (stream !=  null) stream.close();
		}
	}

	private ResponseData handle(List<String> lines, HttpServletResponse res) throws IOException {
		res.setContentType("text/plain; charset=utf-8");
		PrintWriter out = res.getWriter();
		ResponseData response = new ResponseData().setOutputAlreadyPrinted();
		out.write(OUTPUT_HEADER+"\n");
		String previousType = "";
		for (String line : lines) {
			previousType = handle(line, previousType, out);
		}
		out.flush();
		return response;
	}

	private String handle(String line, String previousType, PrintWriter out) {
		line = line.trim();
		if (line.startsWith("#")) return previousType;
		if (line.contains("#")) {
			line = line.substring(0, line.indexOf("#"));
		}
		String[] parts = line.split(",");
		if (parts.length < 3) {
			out.write(line + " -- " +  " Line was not in expected format!\n");
			return previousType;
		}
		String rowid = parse(parts, 0);
		String lat = parse(parts, 1);
		String lon =parse(parts, 2);
		String type = "";
		if (parts.length >= 4) {
			type = parse(parts, 3);
		} else {
			type = previousType;
		}
		try {
			out.write(handle(rowid, lat, lon, type) + "\n");
		} catch (IllegalArgumentException e) {
			out.write(line + " -- " + e.getMessage() + "\n");
			return previousType;
		}
		return type;
	}

	private String handle(String rowid, String lat, String lon, String type) {
		Node response = CoordinateConversionService.convert(lat, lon, type).getRootNode();
		if (response.hasAttribute("pass") && response.getAttribute("pass").equals("false")) {
			throw new IllegalArgumentException(response.getNode("error").getContents());
		}
		return Utils.removeWhitespace(Utils.debugS(
				rowid, 
				lat,
				lon, 
				type,
				response.getNode("etrs89").getAttribute("lat"),
				response.getNode("etrs89").getAttribute("lon"),
				response.getNode("etrs89-degrees").getAttribute("lat"),
				response.getNode("etrs89-degrees").getAttribute("lon"),
				response.getNode("euref").getAttribute("lat"),
				response.getNode("euref").getAttribute("lon"),
				response.getNode("uniform").getAttribute("lat"),
				response.getNode("uniform").getAttribute("lon"),
				response.getNode("kkj-decimal").getAttribute("lat"),
				response.getNode("kkj-decimal").getAttribute("lon"),
				response.getNode("kkj-degrees").getAttribute("lat"),
				response.getNode("kkj-degrees").getAttribute("lon"),
				response.getNode("etrs89-degrees-ddd-mm-ss-formatted").getAttribute("lat"),
				response.getNode("etrs89-degrees-ddd-mm-ss-formatted").getAttribute("lon"),
				response.getNode("kkj-degrees-ddd-mm-ss-formatted").getAttribute("lat"),
				response.getNode("kkj-degrees-ddd-mm-ss-formatted").getAttribute("lon")
				).replace(":", ","));
	}

	private String parse(String[] parts, int i) {
		String value = parts[i].trim();
		return value;
	}

	private List<String> getLines(InputStream stream) throws IOException {
		List<String> lines = IOUtils.readLines(stream, "utf-8");
		return lines;
	}

	private ResponseData error(String error) {
		return initResponseData().setData("error", error);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		throw new UnsupportedOperationException();
	}

}
