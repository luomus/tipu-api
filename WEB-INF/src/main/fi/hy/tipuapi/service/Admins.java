package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class Admins extends ResourceBaseServlet {

	public Admins() {
		super();
	}

	private static final long	serialVersionUID	= -2136965495759792027L;

	@Override
	protected Resource defineResource() {
		String schemaName = config.stagingMode() || config.developmentMode() ? "lintuvaara_testing.admin_users" : "lintuvaara_production.admin_users";
		Resource resource = new Resource("admins", "admin", schemaName, "login_id", this);
		resource.defineField(new Field("name", schemaName+".name"));
		resource.defineField(new Field("email", schemaName+".email"));
		return resource;
	}

}
