package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class Provinces extends ResourceBaseServlet {

	private static final long serialVersionUID = -8962193981192506790L;
	
	public Provinces() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("provinces", "province", "finnishProvince", "id", this);
		resource.defineField(new Field("name", "name"));
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
