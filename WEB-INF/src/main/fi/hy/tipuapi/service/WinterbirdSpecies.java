package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.FilterClause;

public class WinterbirdSpecies extends Species {

	private static final long serialVersionUID = -220727051127545382L;

	public WinterbirdSpecies() {
		super();
	}

	@Override
	protected Resource defineResource() {
		Resource resource = super.defineResource();
		resource.defineFilterClause(new FilterClause("winterbird", "'K'")); 
		return resource;
	}

}
