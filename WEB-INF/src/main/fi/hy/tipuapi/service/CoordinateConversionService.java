package fi.hy.tipuapi.service;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.XML;

import fi.hy.tipuapi.general.BaseServlet;
import fi.laji.Conversion;
import fi.laji.CoordinateConverterProj4jImple;
import fi.laji.DegreePoint;
import fi.laji.MetricPoint;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.CoordinateConverter.CoordinateConversionException;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class CoordinateConversionService extends BaseServlet {

	private static final long serialVersionUID = 6538673992610643727L;

	private static final String OUT_OF_RANGE = "out-of-range";
	private static final Set<String> YKJ = Utils.set("ykj", "uniform", "kkj-yhtenaiskoord");
	private static final Set<String> EUREF = Utils.set("euref", "etrs-tm35fin", "trs-tm35fin");
	private static final Set<String> ETRS89_DECIMAL = Utils.set("etrs89-decimal", "etrs89", "wgs84", "wgs84-decimal");
	private static final Set<String> ETRS89_DEGREES = Utils.set("etrs89-degrees", "wgs84-degrees", "wgs84-dms");
	private static final Set<String> KKJ_DECIMAL = Utils.set("kkj-decimal", "decimal");
	private static final Set<String> KKJ_DEGREES = Utils.set("kkj-degrees", "degrees");
	private static final Set<String> ALL_TYPES = initAllTypes();
	private static final Cached<CoordinateConversionCacheParams, Document> cache = new Cached<>(new CoordinateConversionCacheLoader(), 1, TimeUnit.HOURS, 1000);
	private static final fi.laji.CoordinateConverter CONVERTER = new CoordinateConverterProj4jImple();

	private static class CoordinateConversionCacheParams {

		private final String lat;
		private final String lon;
		private final String type;
		private final String equals;
		public CoordinateConversionCacheParams(String lat, String lon, String type) {
			this.lat = lat;
			this.lon = lon;
			this.type = type;
			this.equals = lat+lon+type;
		}
		public String getLat() {
			return lat;
		}
		public String getLon() {
			return lon;
		}
		public String getType() {
			return type;
		}
		@Override
		public int hashCode() {
			return this.toString().hashCode();
		}
		@Override
		public String toString() {
			return equals;
		}
		@Override
		public boolean equals(Object o) {
			if (o instanceof CoordinateConversionCacheParams) {
				CoordinateConversionCacheParams other = (CoordinateConversionCacheParams) o;
				return other.toString().equals(this.toString());
			}
			throw new UnsupportedOperationException();
		}
	}

	private static class CoordinateConversionCacheLoader implements CacheLoader<CoordinateConversionCacheParams, Document> {

		@Override
		public Document load(CoordinateConversionCacheParams key) {
			try {
				return tryToConvert(key);
			} catch (Exception e) {
				throw new RuntimeException("Coordinate conversion with params " + key, e);
			} 
		}

		private Document tryToConvert(CoordinateConversionCacheParams key) {
			Document responseContent = new Document("conversion-response");
			Node response = responseContent.getRootNode();

			String lat = key.getLat();
			String lon = key.getLon();
			String type =  key.getType();

			if (!checkCoordinates(response, lat, lon)) return responseContent;
			if (!checkType(response, type)) return responseContent;

			if (YKJ.contains(type)) {
				lat = padToLength(lat, 7);
				lon = padToLength(lon, 7);
			}

			CoordinateConverter converter = initConverter(lat, lon, type);

			try {
				convert(response, type, converter);
			} catch (Exception e) {
				return failWithError(e);
			}
			addDegreeFormatWithSymbolsToResponse(response);
			return responseContent;
		}

		private void convert(Node response, String type, CoordinateConverter converter) throws CoordinateConversionException, Exception {
			if (ETRS89_DECIMAL.contains(type) || ETRS89_DEGREES.contains(type)) {
				if (ETRS89_DECIMAL.contains(type)) {
					converter.convertToDegreesOnly();
				} else {
					converter.convertToDecimalsOnly();
				}
				path_ETRS89exists_othersMissing(response, converter);
			} else {
				converter.convert();
				path_othersExistsERS89Missing(response, converter);
			}
		}

		private CoordinateConverter initConverter(String lat, String lon, String type) {
			CoordinateConverter converter = new CoordinateConverter();
			if (EUREF.contains(type)) {
				converter.setEur_leveys(lat);
				converter.setEur_pituus(lon);
			} else if (YKJ.contains(type)) {
				converter.setYht_leveys(lat);
				converter.setYht_pituus(lon);
			} else if (KKJ_DEGREES.contains(type)) {
				converter.setAst_leveys(lat);
				converter.setAst_pituus(lon);
			} else if (KKJ_DECIMAL.contains(type)) {
				converter.setDes_leveys(lat);
				converter.setDes_pituus(lon);
			} else if (ETRS89_DECIMAL.contains(type)) {
				converter.setDes_leveys(lat);
				converter.setDes_pituus(lon);
			} else if (ETRS89_DEGREES.contains(type)) {
				converter.setAst_leveys(lat);
				converter.setAst_pituus(lon);
			} else {
				throw new IllegalStateException("Invalid coordinate type. Validation should not let us get this far!");
			}
			return converter;
		}

		private String padToLength(String s, int length) {
			if (s.contains(".")) {
				s = s.substring(0, s.indexOf("."));
			}
			while (s.length() < length) {
				s += "0";
			}
			return s;
		}

		private Document failWithError(Exception e) {
			Document responseContent = new Document("conversion-response");
			Node response = responseContent.getRootNode();
			setError(response, "Something unexpected went wrong! " + e.getMessage());
			String stacktrace = LogUtils.buildStackTrace(e, 5).replace("\n", " ");
			response.addChildNode("errorCause").setContents(stacktrace);
			return responseContent;
		}
	}

	private static Set<String> initAllTypes(){
		Set<String> allTypes = new HashSet<>();
		allTypes.addAll(YKJ);
		allTypes.addAll(EUREF);
		allTypes.addAll(ETRS89_DECIMAL);
		allTypes.addAll(ETRS89_DEGREES);
		allTypes.addAll(KKJ_DECIMAL);
		allTypes.addAll(KKJ_DEGREES);
		return allTypes;
	}

	public static Document convert(String lat, String lon, String type) {
		if (type != null) type = type.toLowerCase();
		Document responseContent = cache.get(new CoordinateConversionCacheParams(lat, lon, type));
		return responseContent;
	}	

	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		PrintWriter out;

		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String type =  req.getParameter("type");

		Document responseContent = convert(lat, lon, type);
		String xml = new XMLWriter(responseContent).generateXML(); 

		String format = getFormat(req);
		if (jsonRequested(format)) {
			JSONObject jsonObject = XML.toJSONObject(xml);
			res.setContentType("application/json; charset=utf-8");
			out = res.getWriter();
			out.write(jsonObject.toString());
		} else {
			res.setContentType("application/xml; charset=utf-8");
			out = res.getWriter();
			out.write(xml);
		}
		out.flush();
	}

	private static void path_othersExistsERS89Missing(Node response, CoordinateConverter converter) {
		setToResponse(response, EUREF, converter.getEur_leveys(), converter.getEur_pituus());
		setToResponse(response, YKJ, converter.getYht_leveys(), converter.getYht_pituus());
		setToResponse(response, KKJ_DECIMAL, converter.getDes_leveys(), converter.getDes_pituus());
		setToResponse(response, KKJ_DEGREES, converter.getAst_leveys(), converter.getAst_pituus());

		try {
			converter = fetchAndSetETRS89fromUniform(converter.getYht_leveys(), converter.getYht_pituus());
			converter.convert();
			setToResponse(response, ETRS89_DECIMAL, converter.getDes_leveys(), converter.getDes_pituus());
			setToResponse(response, ETRS89_DEGREES, converter.getAst_leveys(), converter.getAst_pituus());
		} catch (Exception e) {
			setToResponse(response, ETRS89_DECIMAL, OUT_OF_RANGE, OUT_OF_RANGE);
			setToResponse(response, ETRS89_DEGREES, OUT_OF_RANGE, OUT_OF_RANGE);
		}
	}

	private static void path_ETRS89exists_othersMissing(Node response, CoordinateConverter converter) {
		setToResponse(response, ETRS89_DECIMAL, converter.getDes_leveys(), converter.getDes_pituus());
		setToResponse(response, ETRS89_DEGREES, converter.getAst_leveys(), converter.getAst_pituus());
		try {
			converter = fetchAndSetUniformFromETRS89(converter.getDes_leveys(), converter.getDes_pituus());
			converter.convert();

			setToResponse(response, EUREF, converter.getEur_leveys(), converter.getEur_pituus());
			setToResponse(response, YKJ, converter.getYht_leveys(), converter.getYht_pituus());
			setToResponse(response, KKJ_DECIMAL, converter.getDes_leveys(), converter.getDes_pituus());
			setToResponse(response, KKJ_DEGREES, converter.getAst_leveys(), converter.getAst_pituus());
		} catch (Exception e) {
			setToResponse(response, EUREF, OUT_OF_RANGE, OUT_OF_RANGE);
			setToResponse(response, YKJ, OUT_OF_RANGE, OUT_OF_RANGE);
			setToResponse(response, KKJ_DECIMAL, OUT_OF_RANGE, OUT_OF_RANGE);
			setToResponse(response, KKJ_DEGREES, OUT_OF_RANGE, OUT_OF_RANGE);
		}
	}

	private  static void setToResponse(Node response, Set<String> types, Object lat, Object lon) {
		String aka = types.iterator().next();
		for (String synonym : types) {
			Node n = response.addChildNode(synonym).addAttribute("lat", lat.toString()).addAttribute("lon", lon.toString()).addAttribute("aka", aka);
			if (aka.endsWith("-degrees")) {
				n.addAttribute("note", "format: (d)ddmmss");
			}
		}
	}

	private static void addDegreeFormatWithSymbolsToResponse(Node response) {
		String etrs89Fieldname = ETRS89_DEGREES.iterator().next();
		String kkjFieldname = KKJ_DEGREES.iterator().next();
		if (response.hasChildNodes(etrs89Fieldname)) {
			addDegreeFormatWithSymbolsToResponse(response, etrs89Fieldname);
		}
		if (response.hasChildNodes(kkjFieldname)) {
			addDegreeFormatWithSymbolsToResponse(response, kkjFieldname);
		}
	}

	private static void addDegreeFormatWithSymbolsToResponse(Node response, String originalFieldName) {
		Node node = response.getNode(originalFieldName);
		String lat = node.getAttribute("lat");
		String lon = node.getAttribute("lon");
		String dddmmFormatFieldName = originalFieldName + "-ddd-mm-formatted";
		String dddmmssFormatFieldName = originalFieldName + "-ddd-mm-ss-formatted";
		response.addChildNode(dddmmFormatFieldName).addAttribute("lat", formatDegreeStringWithSymbols(lat, false)).addAttribute("lon", formatDegreeStringWithSymbols(lon, false));
		response.addChildNode(dddmmssFormatFieldName).addAttribute("lat", formatDegreeStringWithSymbols(lat, true)).addAttribute("lon", formatDegreeStringWithSymbols(lon, true));
	}

	private static String formatDegreeStringWithSymbols(String value, boolean seconds) {
		boolean negative = false;
		if (value.startsWith("-")) {
			value = value.substring(1);
			negative = true;
		}

		if (value.length() > 7) {
			return "";
		}

		while (value.length() < 5) {
			value = "0" + value;
		}

		String d = "";
		String m = "";
		String s = "";

		if (value.length() == 7) {
			d = value.substring(0, 3);
			m = value.substring(3, 5);
			s = value.substring(5);
		} else if (value.length() == 6) {
			d = value.substring(0, 2);
			m = value.substring(2, 4);
			s = value.substring(4);
		} else if (value.length() == 5) {
			d = value.substring(0, 1);
			m = value.substring(1, 3);
			s = value.substring(3);
		}
		String formatted = null;
		if (seconds) {
			formatted = d + "° " + m + "′" + " " + s + "″";
		} else {
			if (Integer.valueOf(s) > 30) {
				m = Integer.toString(Integer.valueOf(m) + 1);
			}
			formatted = d + "° " + m + "′";
		}
		if (negative) {
			return "-" + formatted;
		}
		return formatted;
	}

	private static CoordinateConverter fetchAndSetETRS89fromUniform(Long uniformLat, Long uniformLon) {
		Conversion conversion = CONVERTER.convertFromYKJ(new MetricPoint(uniformLat, uniformLon));
		CoordinateConverter converter = new CoordinateConverter();
		converter.setDes_leveys(conversion.getWgs84().getLat());
		converter.setDes_pituus(conversion.getWgs84().getLon());
		return converter;
	}

	private static CoordinateConverter fetchAndSetUniformFromETRS89(Double etrs89Lat, Double etrs89Lon) {
		Conversion conversion = CONVERTER.convertFromWGS84(new DegreePoint(etrs89Lat, etrs89Lon));
		CoordinateConverter converter = new CoordinateConverter();
		converter.setYht_leveys(conversion.getYkj().getNorthing().toString());
		converter.setYht_pituus(conversion.getYkj().getEasting().toString());
		return converter;
	}

	public static boolean checkType(Node validationResponse, String type) {
		if (type == null) {
			setError(validationResponse, "Invalid coordinate type.");
			return false;
		}
		type = type.toLowerCase();
		if (!ALL_TYPES.contains(type)) {
			setError(validationResponse, "Invalid coordinate type.");
			return false;
		}
		return true;
	}

	public static boolean checkCoordinates(Node validationResponse, String lat, String lon) {
		if (!given(lat) || !given(lon)) {
			setError(validationResponse, "Both coordinates must be given.");
			return false;
		}
		try {
			Double.valueOf(lat);
			Double.valueOf(lon);
		} catch (NumberFormatException e) {
			setError(validationResponse, "Invalid coordinates. They must be numeric. " + lat + " " + lon);
			return false;
		}
		return true;
	}

	protected static void setError(Node responseContent, String message) {
		responseContent.addAttribute("pass", "false");
		responseContent.addChildNode("error").setContents(message);
	}


}
