package fi.hy.tipuapi.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;
import fi.hy.tipuapi.general.RingSeriesComparator;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;

public class RingSeries extends ResourceBaseServlet {

	public RingSeries() {
		super();
	}

	private static final long	serialVersionUID	= -2136965495759792027L;

	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("ring-series", "series", "" +
				"( select distinct series from series )", 
				"s", "s.series", this);
		
		resource.defineField(new Field("human-readable", "CASE WHEN SUBSTR(series, 2, 1 ) = UPPER('L') AND series != UPPER('ML') THEN '___'||SUBSTR(series, 1, 1) ELSE series||'___' end"));
		
		List<String> seriesInOrder = getSeriesInOrder();

		StringBuilder orderBy = new StringBuilder();
		orderBy.append("case "); 
		int i = 0;
		for (String series : seriesInOrder) {
			orderBy.append(" when series = '").append(series).append("' then ").append(i++);
		}
		orderBy.append(" else ").append(i).append(" end ");
		resource.orderBy(orderBy.toString(), "s.series");

		return resource;
	}

	private List<String> getSeriesInOrder() {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		List<String> seriesInOrder = new ArrayList<>();
		try {
			con = openConnection();
			p = con.prepareStatement(" select distinct series from series ");
			rs = p.executeQuery();
			while (rs.next()) {
				seriesInOrder.add(rs.getString(1));
			}
			RingSeriesComparator ringSeriesComparator = new RingSeriesComparator(con);
			Collections.sort(seriesInOrder, ringSeriesComparator);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			Utils.close(p, rs, con);
		}
		return seriesInOrder;
	}

	private TransactionConnection openConnection() throws SQLException {
		return new PreparedStatementStoringAndClosingTransactionConnection(connectionDescription());
	}

}
