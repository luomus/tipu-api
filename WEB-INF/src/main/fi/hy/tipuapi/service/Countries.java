package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;


public class Countries extends ResourceBaseServlet {

	private static final long serialVersionUID = -7424091945657241884L;

	public Countries() {
		super();
	}

	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("countries", "country", "country", "id", this);
		resource.defineField(new Field("name", "name"));
		resource.defineField(new Field("language", "CASE WHEN language = 'S' THEN 'fi' WHEN language = 'R' THEN 'sv' ELSE 'en' END"));
		resource.orderBy("name"); 
		return resource;
	}

	
}
