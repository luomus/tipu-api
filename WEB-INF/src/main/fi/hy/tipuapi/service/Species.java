package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class Species extends ResourceBaseServlet {

	public Species() {
		super();
	}
	
	private static final long	serialVersionUID	= -6150617985029587683L;
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("species", "species", "species", "id", this);
		
		Field nameFinnish = new Field("name", Resource.Lang.FI, "vernacularNameFi");
		Field nameSwedish = new Field("name", Resource.Lang.SV, "vernacularNameSv");
		Field nameEnglish = new Field("name", Resource.Lang.EN, "vernacularNameEn");
		Field nameLatin = new Field("name", Resource.Lang.LA, "scientificName");
		
		resource.defineField(nameFinnish);
		resource.defineField(nameSwedish);
		resource.defineField(nameEnglish);
		resource.defineField(nameLatin);
		
		resource.defineAttribute(new Attribute("euring-code", "speciesnumber"));
		resource.defineAttribute(new Attribute("protection-status", "protectionStatus"));
		
		resource.defineField(new Field("adult-wing-length-in-millimeters").defineAttribute(new Attribute("min", "adultWingLengthMin")).defineAttribute(new Attribute("max", "adultWingLengthMax")));
		resource.defineField(new Field("young-wing-length-in-millimeters").defineAttribute(new Attribute("min", "youngWingLengthMin")).defineAttribute(new Attribute("max", "youngWingLengthMax")));
		
		resource.defineField(new Field("adult-weight-in-grams").defineAttribute(new Attribute("min", "adultWeightMin")).defineAttribute(new Attribute("max", "adultWeightMax")));
		resource.defineField(new Field("young-weight-in-grams").defineAttribute(new Attribute("min", "youngWeightMin")).defineAttribute(new Attribute("max", "youngWeightMax")));

		resource.defineField(new Field("adult-third-feather-length-in-millimeters").defineAttribute(new Attribute("min", "adultThirdFeatherMin")).defineAttribute(new Attribute("max", "adultThirdFeatherMax")));
		resource.defineField(new Field("young-third-feather-length-in-millimeters").defineAttribute(new Attribute("min", "youngThirdFeatherMin")).defineAttribute(new Attribute("max", "youngThirdFeatherMax")));
		
		resource.orderBy("speciesnumber");
		return resource;
	}
	
}
