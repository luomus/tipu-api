package fi.hy.tipuapi.service;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;
import org.json.XML;

import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class MunicipalityByCoordinatesService extends CoordinateConversionService {

	private static final long serialVersionUID = -3411092119852427963L;

	public MunicipalityByCoordinatesService() {
		super();
	}

	private final SingleObjectCache<TipuApiResource> municipalitiesCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<TipuApiResource>() {
		@Override
		public TipuApiResource load() {
			TipuAPIClient api = null;
			try {
				api = new TipuAPIClientImple(getConfig());
				return api.get(TipuAPIClient.CURRENT_MUNICIPALITIES);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				if (api != null) api.close();
			}
		}}, 5, TimeUnit.HOURS);

	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		PrintWriter out;
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String type =  req.getParameter("type");

		Document responseDocument = generateResponse(lat, lon, type);

		String xml = new XMLWriter(responseDocument).generateXML();
		String format = getFormat(req);
		if (jsonRequested(format)) {
			JSONObject jsonObject = XML.toJSONObject(xml);
			res.setContentType("application/json; charset=utf-8");
			out = res.getWriter();
			out.write(jsonObject.toString());
		} else {
			res.setContentType("application/xml; charset=utf-8");
			out = res.getWriter();
			out.write(xml);
		}
		out.flush();
	}

	private Document generateResponse(String lat, String lon, String type) {
		Document responseDocument = new Document("response");
		Node response = responseDocument.getRootNode();

		Node conversionData = convert(lat, lon, type).getRootNode();
		if (conversionData.hasAttribute("pass") && conversionData.getAttribute("pass").equals("false")) {
			setError(response, conversionData.getNode("error").getContents());
			return responseDocument;

		}

		String closestMunicipalityId = null;
		double wgs84Lat = Double.valueOf(conversionData.getNode("wgs84").getAttribute("lat"));
		double wgs84Lon = Double.valueOf(conversionData.getNode("wgs84").getAttribute("lon"));

		try {
			String closestMunicipalityName = getMunicipalityFromElasticSearch(wgs84Lat, wgs84Lon);
			if (!given(closestMunicipalityName)) throw new NoDataFoundException();

			closestMunicipalityId = idByName(closestMunicipalityName);
			if (!given(closestMunicipalityId)) throw new IllegalArgumentException("Unknown municipality resolved by Elastic: " + closestMunicipalityName);
		} catch (Exception e) {
			if (!(e instanceof NoDataFoundException)) {
				errorReporter.report("Elastic municipality search failed: " + Utils.debugS(wgs84Lat, wgs84Lon), e);
			}
			closestMunicipalityId = findClosestLegacy(wgs84Lat, wgs84Lon);
		}

		if (closestMunicipalityId == null) {
			setError(response, "Unknown error");
		} else {
			Node n = municipalitiesCache.get().getById(closestMunicipalityId);
			if (n ==  null) throw new IllegalStateException("Unknown municipality " + closestMunicipalityId);
			response.addChildNode("id").setContents(closestMunicipalityId);
			for (Node name : n.getChildNodes("name")) {
				response.addChildNode(name);
			}
		}
		return responseDocument;
	}

	private Map<String, String> finnishNameToIdMap = null;

	private Map<String, String> getFinnishNameToIdMap() {
		if (finnishNameToIdMap == null) {
			finnishNameToIdMap = initFinnishnameToIdMap();
		}
		return finnishNameToIdMap;
	}

	private Map<String, String> initFinnishnameToIdMap() {
		Map<String, String> map = new HashMap<>();
		for (Node municipality : municipalitiesCache.get().getAll()) {
			String id = municipality.getNode("id").getContents();
			for (Node name : municipality.getChildNodes("name")) {
				if (name.getAttribute("lang").equals("FI")) {
					map.put(name.getContents().toUpperCase(), id);
				}
			}
		}
		return map;
	}

	private String idByName(String closestMunicipality) {
		return getFinnishNameToIdMap().get(closestMunicipality.toUpperCase());
	}

	private String getMunicipalityFromElasticSearch(double wgs84Lat, double wgs84Lon) throws Exception {
		HttpClientService client = null;
		try {
			String url = getConfig().get("MunicipalityElasticClusterURL");
			String username = getConfig().get("MunicipalityElasticCluster_Username");
			String password = getConfig().get("MunicipalityElasticCluster_Password");

			client = new HttpClientService(url, username, password);
			HttpPost request = new HttpPost(url + "/location/municipality/_search");

			StringBuilder data = new StringBuilder();
			data.append("{\"query\": {\"geo_shape\": {\"border\": {\"shape\": {\"type\": \"Point\",\"coordinates\": [");
			data.append(wgs84Lon).append(", ").append(wgs84Lat);
			data.append("]}}}}}");

			request.setEntity(EntityBuilder.create().setText(data.toString()).build());

			fi.luomus.commons.json.JSONObject response = client.contentAsJson(request);
			List<fi.luomus.commons.json.JSONObject> hits = response.getObject("hits").getArray("hits").iterateAsObject();
			if (hits.isEmpty()) return null;
			return hits.get(0).getObject("_source").getObject("name").getString("fi");
		} finally {
			if (client != null) client.close();
		}
	}

	private String findClosestLegacy(double lat, double lon) {
		String closestMunicipality = null;
		double closestDistance = Double.MAX_VALUE;
		for (Node candidate : municipalitiesCache.get().getAll()) {
			try {
				Double municipalityLat = Double.valueOf(candidate.getAttribute("centerpoint-lat"));
				Double municipalityLon = Double.valueOf(candidate.getAttribute("centerpoint-lon"));
				Double distance = Utils.pythagoras(municipalityLat, municipalityLon, lat, lon);
				if (distance < closestDistance) {
					closestMunicipality = candidate.getNode("id").getContents();
					closestDistance = distance;
				}
			} catch (Exception e) {
				continue;
			}
		}
		return closestMunicipality;
	}

	private static class NoDataFoundException extends Exception {
		private static final long serialVersionUID = 1872774330861235917L;
	}
}
