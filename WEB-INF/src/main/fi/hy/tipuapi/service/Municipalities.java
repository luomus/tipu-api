package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;


public class Municipalities extends ResourceBaseServlet {

	public Municipalities() {
		super();
	}

	private static final long	serialVersionUID	= 8063787480691426061L;
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("municipalities", "municipality", "municipality", "id", this);
		resource.defineJoin("municipality", "currentMunicipality", "currentMunicipality.id = municipality.joinedTo");
		resource.defineJoin("elyCentre", "ely", "ely.id = currentMunicipality.elyCentre");
		resource.defineJoin("finnishProvince", "province", "province.id = currentMunicipality.finnishProvince");
		resource.defineJoin("finnishOldCounty", "oldCounty", "oldCounty.id = currentMunicipality.finnishOldCounty");
		resource.defineJoin("euringProvince", "euring", "euring.id = currentMunicipality.euringProvince");
		
		resource.defineField(new Field("name", Resource.Lang.FI, "UPPER(municipality.name)"));
		resource.defineField(new Field("name", Resource.Lang.SV, "UPPER(municipality.nameSwedish)"));
		resource.defineField(new Field("ely-centre", "currentMunicipality.elyCentre").defineAttribute(new Attribute("name", "ely.name")));
		resource.defineField(new Field("old-county", "currentMunicipality.finnishOldCounty").defineAttribute(new Attribute("name", "oldCounty.name")));
		resource.defineField(new Field("merikotka-suuralue", "currentMunicipality.halalbArea"));
		resource.defineField(new Field("province", "currentMunicipality.finnishProvince").defineAttribute(new Attribute("name", "province.name")));
		resource.defineField(new Field("euring-province", "currentMunicipality.euringProvince").defineAttribute(new Attribute("name", "euring.name")));
		
		resource.defineAttribute(new Attribute("centerpoint-lat", "municipality.centerPointDecimalLat"));
		resource.defineAttribute(new Attribute("centerpoint-lon", "municipality.centerPointDecimalLon"));
		resource.defineAttribute(new Attribute("radius", "municipality.radius"));
		resource.defineAttribute(new Attribute("joined-to", "municipality.joinedTo"));
		
		resource.orderBy("municipality.name", "municipality.joinedTo", "municipality.id"); 
		return resource;
	}

	
}
