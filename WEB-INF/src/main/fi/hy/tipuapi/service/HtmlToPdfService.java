package fi.hy.tipuapi.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.FSEntityResolver;
import org.xml.sax.SAXParseException;

import fi.hy.tipuapi.general.BaseServlet;
import fi.hy.tipuapi.html2pdf.ChainingReplacedElementFactory;
import fi.hy.tipuapi.html2pdf.DataImageReplacedElementFactory;
import fi.hy.tipuapi.html2pdf.HTML2PDFException;
import fi.hy.tipuapi.html2pdf.OurXMLErrorLog;
import fi.hy.tipuapi.html2pdf.SVGReplacedElementFactory;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class HtmlToPdfService extends BaseServlet {

	private static final long serialVersionUID = -6214807074116451402L;

	/**
	 * The dotsPerPixel value given to the DataImageReplacedElementFactory constructor
	 * is a magic number that seemed to work (by testing various numbers). We should compute it based on Flying Saucer's settings somehow.
	 */
	private static final int DOTS_PER_PIXELS = 20;

	private static final Collection<String> FONTS = Utils.collection("ARIAL.TTF", "ARIALBD.TTF", "ARIALBI.TTF", "ARIALI.TTF", "ARIALUNI.TTF");

	private static ITextRenderer renderer = null;

	public HtmlToPdfService() {
		super();
		// errorReporting(false);
	}

	protected boolean withSvgSupport() {
		return true;
	}

	protected ITextRenderer getRenderer(Config config) throws Exception {
		if (renderer == null) {
			renderer = initRenderer(config, withSvgSupport());
		}
		return renderer;
	}

	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setContentType("application/pdf; charset=utf-8");
		String html = getHtmlToConvert(req);
		if (notGiven(html)) {
			redirectTo404(res);
			return;
		}

		// In order to make SVG work, we need to use our custom document builder instead of relying on the implicit XML parsing of renderer.setDocumentFromString().
		OurXMLErrorLog errorLog = new OurXMLErrorLog();
		DocumentBuilder documentBuilder = initBuilder(errorLog);

		Document document = null;
		try {
			document = documentBuilder.parse(new ByteArrayInputStream(html.getBytes("UTF-8"))); 
		} catch (SAXParseException e) {
			// We set our own ErrorHandler for the XML parser via setErrorHandler(). All XML parsing exceptions should end up in the ErrorHandler and not here. If we do end
			// up here, however, it's because the parser encountered a fatal error.  Fatal errors apparently get reported to the ErrorHandler and _also_ throw exceptions as
			// usual. So we can just ignore the exception here since it's the same one that the ErrorHandler already got.
		} catch (Exception e) {
			errorLog.append(e);
		}
		if (errorLog.hasErrors() || document == null) {
			throw new HTML2PDFException(errorLog.toString() + "\n" + "The html: " + html);			
		}

		document.getDocumentElement().normalize();

		try {
			ITextRenderer renderer = getRenderer(getConfig());
			renderer.setDocument(document, null);
			renderer.layout();
			renderer.createPDF(res.getOutputStream());
		} catch (Exception e) {
			String stacktrace= LogUtils.buildStackTrace(e, 5);
			throw new HTML2PDFException(e.getMessage() + " " + e.getStackTrace()[0] + "\n" + "The html: " +html + "\nStacktrace:\n" + stacktrace, e);
		}
	}

	protected String getHtmlToConvert(HttpServletRequest req) throws Exception {
		if (isOfContentType("text/html", req)) {
			return readHtmlFromBody(req);
		} 
		return readHtmlParameter(req);
	}

	private DocumentBuilder initBuilder(OurXMLErrorLog errorLog) throws HTML2PDFException {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true); // This is needed for SVG: the <svg> element is in a different  XML namespace from XHTML.
		DocumentBuilder builder;
		try {
			builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new HTML2PDFException(e.toString());
		}

		// With the default settings the XML parser can be extremely slow, presumably because it tries to download DTDs from the
		// internet to validate the XML. Using FSEntityResolver makes the parsing almost instantaneous, presumably because it
		// looks for DTDs in the local file system only.
		builder.setEntityResolver(FSEntityResolver.instance());

		builder.setErrorHandler(errorLog);
		return builder;
	}

	private static synchronized ITextRenderer initRenderer(Config config, boolean svgSupport) throws Exception {
		if (renderer != null) return renderer;
		ITextRenderer renderer = new ITextRenderer();

		for (String font : FONTS) {
			addFont(renderer, font, config);
		}
		if (!svgSupport) return renderer;

		renderer.getSharedContext().setReplacedElementFactory(
				new ChainingReplacedElementFactory(
						new SVGReplacedElementFactory(),
						new DataImageReplacedElementFactory(DOTS_PER_PIXELS)));
		return renderer;
	}

	private boolean isOfContentType(String type, HttpServletRequest req) {
		String contentType = req.getContentType();
		return contentType != null && contentType.toLowerCase().contains(type);
	}

	private boolean notGiven(String html) {
		return html == null || html.length() < 1;
	}

	private static void addFont(ITextRenderer renderer, String font, Config config) throws Exception {
		renderer.getFontResolver().addFont(config.fontFolder() + File.separatorChar + font, "Identity-H", false);
	}

	private String readHtmlFromBody(HttpServletRequest req) throws Exception {
		StringBuffer jb = new StringBuffer();
		String line = null;

		Reader reader = new InputStreamReader(req.getInputStream(), "utf-8");
		BufferedReader bufferedReader = new BufferedReader(reader);

		while ((line = bufferedReader.readLine()) != null) {
			jb.append(line);
		}
		return jb.toString();
	}	

	private String readHtmlParameter(HttpServletRequest req) {
		return req.getParameter("html");
	}
}
