package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;


public class Laymen extends ResourceBaseServlet {

	private static final long serialVersionUID = 8777916488275927279L;

	public Laymen() {
		super();
	}

	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("laymen", "layman", "layman", "id", this);
		resource.defineJoin("country", "country", "country.id = layman.country");
		
		resource.defineAttribute(new Attribute("language", "CASE WHEN layman.language IS NULL THEN 'fi' WHEN layman.language = 'S' THEN 'fi' WHEN layman.language = 'R' THEN 'sv' ELSE 'en' END"));
		resource.defineField(new Field("name", "layman.name"));
		resource.defineField(new Field("address", "TRIM(addressLine1 || ' ' || addressLine2 || ' ' || addressLine3 || ' ' || addressLine4)"));
		resource.defineField(new Field("addressLines",
				new Field("line", "addressLine1"), 
				new Field("line", "addressLine2"), 
				new Field("line", "addressLine3"),
				new Field("line", "addressLine4"),
				new Field("line", "country.name")
		));
		resource.defineField(new Field("countryCode", "layman.country"));
		resource.defineField(new Field("email", "email"));
		resource.defineField(new Field("phone", "TRIM(phone || ' ' || secondPhone)"));
		
		resource.orderBy("layman.name");
		
		return resource;
	}
	
}
