package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class Ringers extends ResourceBaseServlet {

	public Ringers() {
		super();
	}

	private static final long	serialVersionUID	= -2136965495759792027L;

	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("ringers", "ringer", "ringer", "id", this);

		resource.defineAttribute(new Attribute("permission", "resultServicePermission"));
		resource.defineAttribute(new Attribute("FinBIF-permission", "FinBIFPermission"));
		resource.defineAttribute(new Attribute("language", "CASE WHEN language IS NULL THEN 'fi' WHEN language = 'S' THEN 'fi' WHEN language = 'R' THEN 'sv' ELSE 'en' END"));
		
		resource.defineField(new Field("lastname", "lastName"));
		resource.defineField(new Field("firstname", "firstName"));
		resource.defineField(new Field("address", 
				new Field("street", "address"), 
				new Field("postcode", "postCode"), 
				new Field("city", "city"))
		);
		resource.defineField(new Field("email", "email"));
		resource.defineField(new Field("mobile-phone", "mobilePhone"));
		resource.defineField(
				new Field("permit", "permissionNotes")
				.defineAttribute(new Attribute("codes", "permissionTypes"))
				.defineAttribute(new Attribute("year", "permissionYearStamp"))
				.defineAttribute(new Attribute("previous-year-data-reported", "previousYearDataReported"))
		);
		resource.defineField(new Field("yearOfBirth", "birthYear"));
		resource.defineField(new Field("name", "trim(lastname || ' ' || firstname) || ' (' || id || ')'"));
		resource.orderBy("lastName", "firstName", "id");
		resource.defineField(new Field("recovery-letter-method", "case letters when '1' then 'email' else 'paper' end "));
		resource.defineAttribute(new Attribute("person-state", "state"));
		
		return resource;
	}

}
