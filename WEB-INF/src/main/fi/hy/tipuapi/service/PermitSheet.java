package fi.hy.tipuapi.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import fi.hy.tipuapi.general.BaseServlet;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.xml.Document.Node;

public class PermitSheet extends BaseServlet {

	private static final long serialVersionUID = -5294241377832566833L;

	public PermitSheet() {
		super();
	}

	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, String> data = null;
		try {
			String id = getId(req);
			data = getRingerData(id);
		} catch (RingerInfoNotFoundException e) {
			redirectTo404(res);
			return;
		}

		printForCurrentYear(res, data);

		//		if (DateUtils.getCurrentDateTime("MM").equals("12")) {
		//			printForNextYear(res, data);
		//		} else {
		//			printForCurrentYear(res, data);
		//		}
	}

	private Map<String, String> getRingerData(String id) throws Exception, RingerInfoNotFoundException {
		return parseInfo(getRingerInfo(id));
	}

	private void printForCurrentYear(HttpServletResponse res, Map<String, String> data) throws Exception {
		printForYear(DateUtils.getCurrentYear(), res, data);
	}

	@SuppressWarnings("unused")
	private void printForNextYear(HttpServletResponse res, Map<String, String> data) throws Exception {
		printForYear(DateUtils.getCurrentYear()+1, res, data);
	}

	private void printForYear(int year, HttpServletResponse res, Map<String, String> data) throws Exception {
		String ringersPermitValidFor = data.get("permitValidForYear");
		boolean ringerHasReportedPreviusYearData = "1".equals(data.get("previous-year-data-reported"));

		if (shouldPrintPermitForYear(year, ringersPermitValidFor, ringerHasReportedPreviusYearData)) {
			data.put("voimassaoloaika", "1.1." + year + " - " + "31.1." + (year+1));
			printPDF(res, data, "permitsheet.pdf");
		} else {
			data.put("vuosi", String.valueOf(year));
			printPDF(res, data, "fail.pdf");
		}
	}

	private boolean shouldPrintPermitForYear(int year, String ringerPermitValidForYear, boolean ringerHasReportedPreviusYearData) {
		if (!ringerHasReportedPreviusYearData) return false;
		return String.valueOf(year).equals(ringerPermitValidForYear);
	}

	private Map<String, String> parseInfo(Node ringerInfo) throws Exception {
		Map<String, String> data = new HashMap<>();
		data.put("nro", ringerInfo.getNode("id").getContents());
		data.put("nimi", ringerInfo.getNode("firstname").getContents() + " " + ringerInfo.getNode("lastname").getContents() );
		data.put("syntymavuosi", ringerInfo.getNode("yearofbirth").getContents());
		Node permitNode = ringerInfo.getNode("permit");
		data.put("lupatyyppi", permitNode.getAttribute("codes"));
		data.put("kommentti", permitNode.getContents());
		data.put("permitValidForYear", attribute(permitNode, "year"));
		data.put("previous-year-data-reported", attribute(permitNode, "previous-year-data-reported"));
		return data;
	}

	private String attribute(Node node, String attribute) {
		if (!node.hasAttribute(attribute)) return "";
		return node.getAttribute(attribute);
	}

	private Node getRingerInfo(String id) throws Exception, RingerInfoNotFoundException {
		TipuAPIClient tipuapi = null;
		try {
			tipuapi = new TipuAPIClientImple(config);
			Node ringers = tipuapi.forceReload(TipuAPIClient.RINGERS, id).getRootNode();
			if (!ringers.hasChildNodes()) {
				throw new RingerInfoNotFoundException();
			}
			return ringers.getNode("ringer");
		} finally {
			if (tipuapi != null) tipuapi.close();
		}
	}

	private void printPDF(HttpServletResponse res, Map<String, String> data, String sheetName) throws FileNotFoundException, IOException, DocumentException {
		String filename = data.get("nro") + ".pdf";
		res.setHeader("Content-Disposition","inline; filename=\""+filename+"\"");
		res.setContentType("application/pdf; charset=utf-8");
		OutputStream out = res.getOutputStream();
		File templateFile = new File(config.templateFolder() + File.separator + sheetName);
		FileInputStream templateStream = null;
		try {
			templateStream = new FileInputStream(templateFile);
			PdfReader reader = new PdfReader(templateStream);
			PdfStamper stamp = new PdfStamper(reader, out);
			AcroFields form = stamp.getAcroFields();

			for (Map.Entry<String, String> entry : data.entrySet()) {
				String fieldName = entry.getKey();
				String text = entry.getValue();
				if (text == null || text.length() < 1) continue;
				form.setField(fieldName, text.trim());
			}

			stamp.setFormFlattening(true);
			stamp.close();
		} finally {
			FileUtils.close(templateStream);
		}
		out.flush();
	}

	public static class RingerInfoNotFoundException extends Exception {

		private static final long serialVersionUID = -1924743985705073150L;

	}

}
