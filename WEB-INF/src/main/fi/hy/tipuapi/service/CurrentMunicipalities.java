package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.FilterClause;

public class CurrentMunicipalities extends Municipalities {

	private static final long serialVersionUID = -9221496702458832341L;
	
	@Override
	protected Resource defineResource() {
		Resource resource = super.defineResource();
		resource.defineFilterClause(new FilterClause("municipality.joinedTo", "municipality.id"));
		return resource;
	}
	
}
