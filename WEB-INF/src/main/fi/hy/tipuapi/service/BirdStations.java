package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Attribute;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class BirdStations extends ResourceBaseServlet {

	private static final long serialVersionUID = -2240310762632420451L;

	public BirdStations() {
		super();
	}

	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("bird-stations", "bird-station", "birdstation", "id", this);
		resource.defineAttribute(new Attribute("ringer-number", "fakeRingerNumber"));
		resource.defineAttribute(new Attribute("language", "CASE WHEN language = 'S' THEN 'fi' WHEN language = 'R' THEN 'sv' ELSE 'en' END"));
		resource.defineField(new Field("name", "name"));

		resource.defineField(new Field("municipality", "municipality"));
		resource.defineField(new Field("lat", "centerPointUniformLat"));
		resource.defineField(new Field("lon", "centerPointUniformLon"));
		resource.defineField(new Field("radius", "radius || '000'"));

		resource.defineField(new Field("address",
				new Field("person", "contactPersonName"),
				new Field("street", "address"),
				new Field("postcode", "postcode"),
				new Field("city", "city"))
				);
		resource.defineField(new Field("email", "email"));
		resource.defineField(new Field("mobile-phone", "phone"));
		resource.orderBy(Resource.ID_FIELD);

		resource.defineAttribute(new Attribute("centerpoint-lat", "centerPointDecimalLat"));
		resource.defineAttribute(new Attribute("centerpoint-lon", "centerPointDecimalLon"));

		resource.defineAttribute(new Attribute("centerpoint-dms-lat", "centerPointDegreeLat"));
		resource.defineAttribute(new Attribute("centerpoint-dms-lon", "centerPointDegreeLon"));

		resource.defineAttribute(new Attribute("radius", "radius"));

		return resource;
	}

}
