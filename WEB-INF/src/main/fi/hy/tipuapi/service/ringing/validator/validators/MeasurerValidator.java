package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;

public class MeasurerValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column measurer) throws Exception {
		if (!data.notNullNoErrors(measurer)) return;
		if (onlyNumbers(measurer)) {
			String ringerNumber = measurer.getValue();
			TipuApiResource ringers = data.getDao().getTipuApiResource(TipuAPIClient.RINGERS);
			if (!ringers.containsUnitById(ringerNumber)) {
				data.addError("measurer_not_valid_ringer", measurer);
			}
		}
	}

	private boolean onlyNumbers(Column measurer) {
		try {
			measurer.getIntValue();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
