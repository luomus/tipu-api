package fi.hy.tipuapi.service.ringing.dao;

import java.util.List;

import fi.hy.tipuapi.service.ringing.dao.DAO.Action;
import fi.hy.tipuapi.service.ringing.dao.DAO.DataWarehousingEntry;
import fi.hy.tipuapi.service.ringing.dao.DAOImple.DAOImpleBuilder;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.reporting.ErrorReporter;

public class WarehousingDAOImple implements WarehousingDAO {

	private final LajiETLDAO etl;
	private final WarehouseQueueProcessor warehouseQueueProcessor;
	private final Thread warehouseQueueProcessorThread;
	private boolean closed = false;
	private final Object LOCK = new Object();

	public WarehousingDAOImple(LajiETLDAO etl, ErrorReporter errorReporter, DAOImpleBuilder daoBuilder) {
		this.etl = etl;
		//this.elastic = elastic;
		this.warehouseQueueProcessor = new WarehouseQueueProcessor(errorReporter, daoBuilder, this);
		this.warehouseQueueProcessorThread = new Thread(warehouseQueueProcessor);
		this.warehouseQueueProcessorThread.start();
		System.out.println(WarehousingDAOImple.class.getName() + " created!");
	}

	@Override
	public void close() {
		synchronized (LOCK) {
			if (closed) return;
			System.out.println("Closing " + WarehousingDAOImple.class.getName() + " ...");
			try {
				if (this.warehouseQueueProcessor != null) this.warehouseQueueProcessor.stop();
				closed = true;
			} catch (Exception e) {}
			System.out.println(WarehousingDAOImple.class.getName() + " closed!");
		}
	}

	@Override
	public void push(Row row) {
		etl.push(row);
		//elastic.push(row); // TODO restore when elastic again in use
	}

	@Override
	public void delete(int eventId) {
		etl.delete(eventId);
		//elastic.delete(eventId); // TODO restore when elastic again in use
	}

	@Override
	public void flush() {
		etl.flush();
	}

	private static class WarehouseQueueProcessor implements Runnable {

		private static final int BATCH_SIZE = 20;
		private static final int SLEEP_TIME = 60 * 1000;
		private volatile boolean running = true;
		private final ErrorReporter errorReporter;
		private final WarehousingDAO warehousingDAO;
		private final DAOImpleBuilder daoBuilder;

		public WarehouseQueueProcessor(ErrorReporter errorReporter, DAOImpleBuilder daoBuilder, WarehousingDAO warehousingDAO) {
			this.errorReporter = errorReporter;
			this.warehousingDAO = warehousingDAO;
			this.daoBuilder = daoBuilder;
		}

		@Override
		public void run() {
			System.out.println(this.getClass().getName() + " started!");
			while (running) {
				try {
					tryToProcess();
				} catch (Exception e) {
					errorReporter.report(e);
					try { Thread.sleep(SLEEP_TIME); } catch (InterruptedException e2) {}
				}
			}
			System.out.println(this.getClass().getName() + " stopped!");
		}

		public void stop() {
			this.running = false;
		}

		private void tryToProcess() throws Exception {
			boolean hadMoreWork = doWork();
			if (!hadMoreWork) {
				try {
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {}
			}
		}

		private boolean doWork() throws Exception {
			DAO dao = null;
			try {
				dao = daoBuilder.openForUserId(WarehousingDAOImple.class.getName());
				List<DataWarehousingEntry> entries = dao.getLatestWarehouseQueueRows(BATCH_SIZE);
				for (DataWarehousingEntry entry : entries) {
					if (entry.getAction() == Action.DELETE) {
						warehousingDAO.delete(entry.getEventId());
					} else {
						warehousingDAO.push(entry.getRow());
					}
				}
				boolean moreWork = entries.size() >= BATCH_SIZE;
				if (!moreWork) {
					warehousingDAO.flush();
					dao.clearSuccessfulFromWarehousingQueue();
				}
				return moreWork;
			} finally {
				if (dao !=  null) dao.close();
			}
		}

	}


}
