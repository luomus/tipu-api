package fi.hy.tipuapi.service.ringing.models;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Field.Type;

public class Group implements Iterable<Field> {
	
	private final String name;
	private final Map<String, Field> fields = new LinkedHashMap<>();
	
	public Group(String name) {
		this.name = name;
	}
	
	public Field addField(String dbColumn, String name, Type type, Mode ... modes) {
		dbColumn = dbColumn == null ? null : dbColumn.trim();
		name = name == null ? null : name.trim();
		if (fields.containsKey(name)) throw new IllegalStateException("Same field name used twice: " + name);
		Field field = new Field(dbColumn, name, type, modes);
		this.fields.put(name, field);
		field.setParentGroup(this);
		return field;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Iterator<Field> iterator() {
		return fields.values().iterator();
	}

	public boolean contains(String name) {
		return fields.containsKey(name);
	}

	public Field get(String name) {
		return fields.get(name);
	}
	
}
