package fi.hy.tipuapi.service.ringing.models;

import fi.luomus.commons.utils.Utils;

import java.util.Collection;

public class SearchParameter {

	private final String value;
	
	public SearchParameter(String valueOrValues) {
		this.value = valueOrValues == null ? "" : valueOrValues.trim();
	}
	
	public boolean hasMultiple() {
		return value.contains("|");
	}
	
	public boolean isLike() {
		return value.contains("%") || value.contains("_") || value.contains("*");
	}
	
	public Collection<String> getMultiple() {
		return Utils.list(value.split("\\|"));
	}
	
	public String getSingle() {
		return value;
	}

	public boolean hasValue() {
		return value != null && value.length() > 0;
	}

}
