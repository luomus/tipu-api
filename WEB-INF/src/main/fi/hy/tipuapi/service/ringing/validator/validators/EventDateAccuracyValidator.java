package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class EventDateAccuracyValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column eventDateAccuracy) {
		if (data.isForUpdate()) return;
		if (eventDateAccuracy.hasValue() && !eventDateAccuracy.getValue().equals("0")) { // 0 = accurate
			if (data.isForUserValidations()) {
				data.addWarning("accuracy_given_ask_for_information_warning", eventDateAccuracy);
			} else {
				data.addWarning("ADMIN_accuracy_given", eventDateAccuracy);
			}
		}
	}

}
