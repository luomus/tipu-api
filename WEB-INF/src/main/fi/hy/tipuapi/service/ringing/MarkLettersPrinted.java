package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MarkLettersPrinted extends BaseServlet {

	private static final long serialVersionUID = -2170545221957258352L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		List<Integer> recoveryIds = null;
		try {
			recoveryIds = getIds(req);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		}

		DAO dao = null;
		try {
			JSONObject response = new JSONObject();
			dao = createNewDAO(req);
			for (int id : recoveryIds) {
				dao.removeFromLetterPrinting(id);
			}
			response.setBoolean("success", true);
			return print(response, res);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}
}
