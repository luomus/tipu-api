package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class AccuracyOfSomethingValidator implements FieldValidation {

	private final String[] fieldNamesWhichAccuracyThisFieldReflects;

	public AccuracyOfSomethingValidator(String ... fieldNamesWhichAccuracyThisFieldReflects) {
		this.fieldNamesWhichAccuracyThisFieldReflects = fieldNamesWhichAccuracyThisFieldReflects;
	}

	@Override
	public void validate(RowValidationData data, Column accuracyField) throws Exception {
		if (!accuracyField.hasValue()) return;

		boolean valueFound = false;
		for (String field : fieldNamesWhichAccuracyThisFieldReflects) {
			Column c = data.getRow().get(field);
			if (c.hasValue()) valueFound = true;
		}

		if (!valueFound) {
			accuracyField.setValue("");
		}
	}

	protected String[] getFieldNamesWhichAccuracyThisFieldReflects() {
		return fieldNamesWhichAccuracyThisFieldReflects;
	}


}
