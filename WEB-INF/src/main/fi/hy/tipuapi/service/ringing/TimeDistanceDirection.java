package fi.hy.tipuapi.service.ringing;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.CoordinateConversionService;
import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class TimeDistanceDirection extends BaseServlet {

	private static final long serialVersionUID = -7747429614568636353L;
	private static final Map<Integer, String> CARDINAL_DIRECTIONS_SLOTS = initCardinalDirectionSlots();

	private static Map<Integer, String> initCardinalDirectionSlots() {
		Map<Integer, String> slots = new HashMap<>();
		slots.put(0, "N");
		slots.put(1, "NNE");
		slots.put(2, "NNE");
		slots.put(3, "NE");
		slots.put(4, "NE");
		slots.put(5, "ENE");
		slots.put(6, "ENE");
		slots.put(7, "E");
		slots.put(8, "E");
		slots.put(9, "ESE");
		slots.put(10, "ESE");
		slots.put(11, "SE");
		slots.put(12, "SE");
		slots.put(13, "SSE");
		slots.put(14, "SSE");
		slots.put(15, "S");
		slots.put(16, "S");
		slots.put(17, "SSW");
		slots.put(18, "SSW");
		slots.put(19, "SW");
		slots.put(20, "SW");
		slots.put(21, "WSW");
		slots.put(22, "WSW");
		slots.put(23, "W");
		slots.put(24, "W");
		slots.put(25, "WNW");
		slots.put(26, "WNW");
		slots.put(27, "NW");
		slots.put(28, "NW");
		slots.put(29, "NNW");
		slots.put(30, "NNW");
		slots.put(31, "N");
		slots.put(32, "N");
		return slots;
	}

	private static final Cached<CoordinateData, Point2D.Double> cachedCoordinateConversions =
			new Cached<>(
					new CoordinateConversionCacheLoader(),
					3, TimeUnit.HOURS, 50);

	private static class CoordinateData {
		private final String type;
		private final String lat;
		private final String lon;
		private final String toString;
		public CoordinateData(Row row) {
			this.type = row.get("coordinateSystem").getValue().toLowerCase();
			this.lat = row.get("lat").getValue();
			this.lon = row.get("lon").getValue();
			this.toString = Utils.debugS(type, lat, lon);
		}
		@Override
		public String toString() {
			return toString;
		}
		@Override
		public boolean equals(Object o) {
			return this.toString.equals(o.toString());
		}
		@Override
		public int hashCode() {
			return toString.hashCode();
		}
	}

	private static class CoordinateConversionCacheLoader implements CacheLoader<CoordinateData, Point2D.Double> {

		@Override
		public Point2D.Double load(CoordinateData data) {
			try {
				Node response = CoordinateConversionService.convert(data.lat, data.lon, data.type).getRootNode();
				Node wgs84 = response.getNode("wgs84");
				double wgs84lat = Double.valueOf(wgs84.getAttribute("lat"));
				double wgs84lon = Double.valueOf(wgs84.getAttribute("lon"));
				return new Point2D.Double(wgs84lat, wgs84lon);
			} catch (Exception e) {
				throw new RuntimeException("Converting coordinates: " + data.toString, e);
			}
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (notProductionMode()) log(req);
		String nameRing = req.getParameter("ring");

		if (!given(nameRing)) {
			return print(generateExceptionResponse(new IllegalArgumentException("Ring parameter must be given")), res);
		}

		JSONObject response = null;
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			response = generateResponse(dao, nameRing);
		} catch (Exception e) {
			getErrorReporter().report(e);
			response = generateExceptionResponse(nameRing, e);
		} finally {
			if (dao != null) dao.close();
		}
		return print(response, res);
	}

	private JSONObject generateResponse(DAO dao, String nameRing) throws Exception {
		RingHistory ringHistory = dao.getRingHistoryByNameRing(nameRing);
		Row ringing = ringHistory.getRinging();

		JSONObject response = new JSONObject();
		response.setInteger("countOfRows", ringHistory.getRecoveries().size());
		response.setBoolean("success", true);
		JSONObject recoveries = response.getObject("recoveries");

		Row previousRecovery = null;
		for (Row recovery : ringHistory.getRecoveries()) {
			JSONObject entry = recoveries.getObject(recovery.get("id").getValue());
			entry.setObject("toRinging", countTimeDistanceDirection(recovery, ringing));
			if (previousRecovery == null) {
				entry.setObject("toPrevious", countTimeDistanceDirection(recovery, ringing));
			} else {
				entry.setObject("toPrevious", countTimeDistanceDirection(recovery, previousRecovery));
			}
			previousRecovery = recovery;
		}
		return response;
	}

	public static JSONObject countTimeDistanceDirection(Row event, Row precedingEvent) {
		Point2D.Double eventCoordinates = convertToWGS84(event);
		Point2D.Double precedingCoordinates = convertToWGS84(precedingEvent);
		return countTimeDistanceDirection(event, precedingEvent, eventCoordinates, precedingCoordinates);
	}

	public static JSONObject countTimeDistanceDirection(Row event, Row precedingEvent, Point2D.Double eventCoordinates, Point2D.Double precedingCoordinates) {
		JSONObject data = new JSONObject();
		countAndSetTime(event, precedingEvent, data);
		if (eventCoordinates != null && precedingCoordinates != null) {
			countAndSetDistance(data, eventCoordinates, precedingCoordinates);
			countAndSetDirection(data, eventCoordinates, precedingCoordinates);
		}
		return data;
	}

	private static java.awt.geom.Point2D.Double convertToWGS84(Row row) {
		if (!row.get("lat").hasValue() || !row.get("lon").hasValue()) return null;
		return cachedCoordinateConversions.get(new CoordinateData(row));
	}

	private static void countAndSetDirection(JSONObject data, Point2D.Double recCoord, Point2D.Double prevCoord) {
		double bearing = Utils.bearing(prevCoord.x, prevCoord.y, recCoord.x, recCoord.y);
		bearing = (double) Math.round(bearing * 10) / 10;
		data.setDouble("bearingInDegrees", bearing);
		int cardinalDirectionSlot = (int) Math.floor(bearing / 11.25);
		data.setString("cardinalDirection", CARDINAL_DIRECTIONS_SLOTS.get(cardinalDirectionSlot));
	}

	private static void countAndSetDistance(JSONObject data, Point2D.Double recCoord, Point2D.Double prevCoord) {
		int distanceInKilometers = (int) Math.round(Utils.distance(recCoord.x, recCoord.y, prevCoord.x, prevCoord.y));
		data.setInteger("distanceInKilometers", distanceInKilometers);
	}

	private static void countAndSetTime(Row recovery, Row previousRecovery, JSONObject data) {
		long timeDifference = timeDifferenceInSeconds(recovery, previousRecovery);
		int timeInDays = Math.round(timeDifference / 60 / 60 / 24);

		DateValue begin = previousRecovery.get("eventDate").getDateValue();
		DateValue end = recovery.get("eventDate").getDateValue();

		DateValue difference = DateUtils.getYearsMonthsDays(begin, end);

		String humanizedTimeFI = DateUtils.humanize(difference, DateUtils.FI);
		String humanizedTimeEN = DateUtils.humanize(difference, DateUtils.EN);
		String humanizedTimeSV = DateUtils.humanize(difference, DateUtils.SV);

		JSONArray humanizedTime = data.getArray("humanizedTime");
		humanizedTime.appendObject(new JSONObject().setString("lang", "FI").setString("content", humanizedTimeFI));
		humanizedTime.appendObject(new JSONObject().setString("lang", "EN").setString("content", humanizedTimeEN));
		humanizedTime.appendObject(new JSONObject().setString("lang", "SV").setString("content", humanizedTimeSV));

		data.setInteger("timeInDays", timeInDays);
	}

	public static long timeDifferenceInSeconds(Row recovery, Row previousRecovery) {
		long recoveryTimestamp = getEpochDate(recovery);
		long previousTimestamp = getEpochDate(previousRecovery);
		long timeDifference = recoveryTimestamp - previousTimestamp;
		return timeDifference;
	}

	private static long getEpochDate(Row row) {
		Column eventDate = row.get("eventDate");
		if (!eventDate.hasValue()) throw new IllegalStateException("No event date for row: " + row.toString() );
		return DateUtils.getEpoch(eventDate.getDateValue());
	}

	@Override
	protected JSONObject generateExceptionResponse(Exception e) {
		return generateExceptionResponse(null, e);
	}

	private JSONObject generateExceptionResponse(String ring, Exception e) {
		JSONObject response = new JSONObject();
		response.setBoolean("success", false);
		response.getArray("errors").appendObject(error(e.getClass().getName(), e.getMessage() + ": " + ring));
		response.getArray("recoveries");
		return response;
	}

}
