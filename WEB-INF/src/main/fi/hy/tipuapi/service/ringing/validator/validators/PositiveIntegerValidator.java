package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class PositiveIntegerValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (data.notNullNoErrors(column)) {
			if (column.getIntValue() < 1) {
				data.addError("must_be_positive_integer", column);
			}
		}
	}

}
