package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.Iterator;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class UpdateDeleteValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column eventId) throws Exception {
		Row existingData = null;
		try {
			existingData = data.getDao().getRowByEventId(eventId.getIntValue());
		} catch (Exception e) {
			data.addError("ADMIN_error_finding_original_data", eventId);
			return;
		}

		if (data.isForDelete()) {
			deleteValidate(data, existingData, eventId);
		} else if (data.isForUpdate()) {
			updateValidate(data, existingData, eventId);
		}
	}

	private void updateValidate(RowValidationData data, Row existingData, Column eventId) throws Exception {
		Row alteredData = data.getRow();

		canNotChange("type", existingData, alteredData, data);
		canNotChange("ringNotUsedReason", existingData, alteredData, data);
		canNotChange("diario", existingData, alteredData, data);
		if (data.hasErrors()) return;

		boolean dateAltered = dateAltered(existingData, alteredData);
		boolean legRingAltered = altered("legRing", existingData, alteredData);
		boolean newLegRingAltered = altered("newLegRing", existingData, alteredData);
		boolean fieldReadableDataAltered = altered(existingData, alteredData, "fieldReadableCode", "mainColor", "species", "removedFieldReadableCode", "removedFieldReadableMainColor");
		boolean speciesAltered = altered("species", existingData, alteredData);

		if (dateAltered &&
				(legRingAltered || newLegRingAltered || fieldReadableDataAltered)) {
			data.addError("ADMIN_can_not_change_date_and_rings_at_the_same_time", existingData.get("eventDate"));
			return;
		}

		RingHistory ringHistory = data.getDao().getRingHistoryByEventId(eventId.getIntValue());

		if (dateAltered) {
			validateDateAltered(data, existingData, eventId, alteredData, ringHistory);
		}

		if (legRingAltered) {
			validateLegRingAltered(data, existingData, eventId, ringHistory);
		}

		if (newLegRingAltered) {
			validateNewLegRingAlteredRecovery(data, existingData, eventId, ringHistory);
		}

		if (fieldReadableDataAltered) {
			validateFieldReadableAltered(data, existingData, eventId, ringHistory);
		}

		if (speciesAltered && data.getMode() == Mode.RINGINGS) {
			validateSpeciesAlteredRinging(data, existingData, ringHistory);
		}

		if (data.hasErrors()) return;
		// TODO tarkista lopuksi ketju
		// eli siis:
		// jos on tapaamisia:
		// jos otetaan kannasta rivit ja poistetaan siitä setistä nyt muokattava ja pistetään tilalle uudet tiedot, onko rengasketjut (myös lukur.) vielä ok?
		// tehdään vain sille ketjulle jonka nimirenkaaseen kuuluvaa juttua tässä muokataan
	}

	private void validateSpeciesAlteredRinging(RowValidationData data, Row existingData, RingHistory ringHistory) {
		for (Row row : ringHistory.getRecoveries()) {
			if (row.get("species").hasValue() && !"E".equals(row.get("speciesAccuracy").getValue())) {
				data.addError("ADMIN_can_not_change_species_for_ringing_that_has_recoveries_with_reported_species", existingData.get("species"));
			}
		}
	}

	private void validateFieldReadableAltered(RowValidationData data, Row existingData, Column eventId, RingHistory ringHistory) {
		if (data.getMode() == Mode.RINGINGS) {
			validateFieldReadableAlteredRinging(existingData, eventId, ringHistory, data);
		} else {
			validateFieldReadableAlteredRecovery(data, existingData, eventId, ringHistory);
		}
	}

	private void validateFieldReadableAlteredRecovery(RowValidationData data, Row existingData, Column eventId, RingHistory ringHistory) {
		Column chandedOrRemoved = data.getRow().get("fieldReadableChanges");
		if (chandedOrRemoved.getValue().equals("V")) return;
		if (fieldReadableHasChangedInThisRecovery(existingData, ringHistory) && hasRecoveriesAfterThisEventForFieldReadable(eventId, ringHistory, existingData)) {
			data.addError("ADMIN_can_not_change_fieldreadable_to_something_else_when_there_are_recoveries_after_this_event_using_original_fieldreadables", existingData.get("fieldReadableCode"));
		}
	}

	private void validateFieldReadableAlteredRinging(Row existingData, Column eventId, RingHistory ringHistory, RowValidationData data) {
		if (existingData.get("fieldReadableCode").hasValue() && hasRecoveriesAfterThisEventForFieldReadable(eventId, ringHistory, existingData)) {
			data.addError("ADMIN_can_not_change_fieldreadable_to_something_else_when_there_are_recoveries_after_this_event_using_original_fieldreadables", existingData.get("fieldReadableCode"));
		}
	}

	private void validateNewLegRingAlteredRecovery(RowValidationData data, Row existingData, Column eventId, RingHistory ringHistory) throws Exception {
		if (existingData.get("newLegRing").hasValue() && hasRecoveriesAfterThisEvent(eventId, ringHistory)) {
			data.addError("ADMIN_can_not_change_newLegRing_to_something_else_when_there_are_recoveries_after_this_recovery", existingData.get("newLegRing"));
		}
	}

	private void validateLegRingAltered(RowValidationData data, Row existingData, Column eventId, RingHistory ringHistory) throws Exception {
		if (data.getMode() == Mode.RINGINGS) {
			legRingAlteredRinging(data, existingData, ringHistory);
		} else {
			legRingAlteredRecovery(data, existingData, eventId, ringHistory);
		}
	}

	private void legRingAlteredRecovery(RowValidationData data, Row existingData, Column eventId, RingHistory ringHistory) throws Exception {
		if (ringChange(existingData, ringHistory) && hasRecoveriesAfterThisEvent(eventId, ringHistory)) {
			data.addError("ADMIN_can_not_move_recovery_for_different_ring_when_the_recovery_had_ring_changed_that_has_history", existingData.get("legRing"));
		}
	}

	private void legRingAlteredRinging(RowValidationData data, Row existingData, RingHistory ringHistory) {
		if (ringHistory.hasRecoveries()) {
			data.addError("ADMIN_can_not_change_ringing_to_use_different_ring_when_the_ring_has_history", existingData.get("legRing"));
		}
	}

	private void validateDateAltered(RowValidationData data, Row existingData, Column eventId, Row alteredData, RingHistory ringHistory) {
		if (data.getMode() == Mode.RINGINGS) {
			validateDateAlteredRinging(data, alteredData, ringHistory);
		} else {
			validateDateAlteredRecovery(data, existingData, eventId, alteredData, ringHistory);
		}
	}

	private void validateDateAlteredRecovery(RowValidationData data, Row existingData, Column eventId, Row alteredData, RingHistory ringHistory) {
		firstMustBeBeforeSecond(ringHistory.getRinging(), alteredData, data, "ADMIN_recovery_can_not_move_before_ringing");
		if (ringChange(existingData, ringHistory)) {
			Row nextRecovery = getNextRecovery(eventId, ringHistory);
			if (nextRecovery != null) {
				firstMustBeBeforeSecond(alteredData, nextRecovery, data, "ADMIN_ring_change_can_not_move_after_next_recovery");
			}
			Row previousRecovery = getPreviousRecovery(eventId, ringHistory);
			if (previousRecovery != null) {
				firstMustBeBeforeSecond(previousRecovery, alteredData, data, "ADMIN_ring_change_can_not_move_before_previous_recovery");
			}
		}
	}

	private void validateDateAlteredRinging(RowValidationData data, Row alteredData, RingHistory ringHistory) {
		if (ringHistory.hasRecoveries()) {
			firstMustBeBeforeSecond(alteredData, ringHistory.getRecoveries().get(0), data, "ADMIN_ringing_can_not_move_after_first_recovery");
		}
	}

	private boolean ringChange(Row existingData, RingHistory ringHistory) {
		return existingData.get("newLegRing").hasValue() || fieldReadableHasChangedInThisRecovery(existingData, ringHistory);
	}

	private void firstMustBeBeforeSecond(Row first, Row second, RowValidationData data, String errorName) {
		if (RowUtils.firstAfterSecond(first, second)) {
			data.addError(errorName, first.get("eventDate"));
		}
	}

	private boolean altered(Row existingData, Row alteredData, String ... fieldNames) {
		for (String fieldName : fieldNames) {
			if (altered(fieldName, existingData, alteredData)) return true;
		}
		return false;
	}

	private boolean altered(String fieldName, Row existingData, Row alteredData) {
		return !existingData.get(fieldName).getValue().equals(alteredData.get(fieldName).getValue());
	}

	private boolean dateAltered(Row existingData, Row alteredData) {
		return !existingData.get("eventDate").getDateValue().equals(alteredData.get("eventDate").getDateValue());
	}

	private void canNotChange(String field, Row existingData, Row alteredData, RowValidationData data) {
		Column c1 = existingData.get(field);
		Column c2 = alteredData.get(field);
		if (!c1.getValue().equals(c2.getValue())) {
			data.addError("ADMIN_can_not_change_in_update", c1);
		}
	}

	private void deleteValidate(RowValidationData data, Row existingData, Column eventId) throws Exception {
		if (data.getMode() == Mode.RINGINGS) {
			ringingDeleteValidate(data, eventId);
		} else {
			recoveryDeleteValidate(data, existingData, eventId);
		}
	}

	private void recoveryDeleteValidate(RowValidationData data, Row existingData, Column eventId) throws Exception {
		RingHistory ringHistory = data.getDao().getRingHistoryByEventId(eventId.getIntValue());
		if (existingData.get("newLegRing").hasValue()) {
			if (hasRecoveriesAfterThisEvent(eventId, ringHistory)) {
				data.addError("ADMIN_can_not_delete_newring_has_history", existingData.get("newLegRing"));
				return;
			}
		}
		if (fieldReadableHasChangedInThisRecovery(existingData, ringHistory)) {
			if (hasRecoveriesAfterThisEventForFieldReadable(eventId, ringHistory, existingData)) {
				data.addError("ADMIN_can_not_delete_changed_fieldreadable_and_has_history_after", existingData.get("fieldReadableCode"));
				return;
			}
		}
	}

	private void ringingDeleteValidate(RowValidationData data, Column eventId) throws Exception {
		RingHistory ringHistory = data.getDao().getRingHistoryByEventId(eventId.getIntValue());
		if (ringHistory.hasRecoveries()) {
			data.addError("ADMIN_can_not_delete_ringing_has_history", eventId);
			return;
		}
	}

	private boolean fieldReadableHasChangedInThisRecovery(Row existingData, RingHistory ringHistory) {
		Column eventId = existingData.get("id");
		Column fieldReadableChanges = existingData.get("fieldReadableChanges");
		Column fieldReadableCode = existingData.get("fieldReadableCode");
		Column mainColor = existingData.get("mainColor");
		Column species = existingData.get("species");
		Column removedFieldReadableCode = existingData.get("removedFieldReadableCode");

		if (fieldReadableChanges.hasValue()) return true;
		if (removedFieldReadableCode.hasValue()) return true;

		if (!fieldReadableCode.hasValue() || !mainColor.hasValue() || !species.hasValue()) return false;

		String thisInfo = fieldReadableCatenate(existingData);
		String previousInfo = "";
		for (Row row : ringHistory) {
			if (!row.get("fieldReadableCode").hasValue() || !row.get("mainColor").hasValue()) continue;
			if (sameRow(eventId, row)) break;
			String info = fieldReadableCatenate(row);
			if (given(info)) {
				previousInfo = info;
			}
		}
		return !thisInfo.equals(previousInfo);
	}

	private String fieldReadableCatenate(Row row) {
		return row.get("fieldReadableCode").getValue()+"|"+row.get("mainColor").getValue();
	}

	private String removedFieldReadableCatenate(Row row) {
		return row.get("removedFieldReadableCode").getValue()+"|"+row.get("removedFieldReadableMainColor").getValue();
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private boolean hasRecoveriesAfterThisEvent(Column eventId, RingHistory ringHistory) throws Exception {
		Iterator<Row> i = ringHistory.getAll().iterator();
		while (i.hasNext()) {
			Row row = i.next();
			if (sameRow(eventId, row)) {
				return i.hasNext();
			}
		}
		throw new IllegalStateException("Id not found from history gotten by the same id...");
	}

	private boolean hasRecoveriesAfterThisEventForFieldReadable(Column eventId, RingHistory ringHistory, Row existingData) {
		String existingInfo = fieldReadableCatenate(existingData);
		boolean after = false;
		for (Row row : ringHistory.getAll()) {
			if (sameRow(eventId, row)) {
				after = true;
				continue;
			}
			if (after) {
				if (fieldReadableCatenate(row).equals(existingInfo) || removedFieldReadableCatenate(row).equals(existingInfo)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean sameRow(Column eventId, Row row) {
		return row.get("id").getValue().equals(eventId.getValue());
	}

	private Row getNextRecovery(Column eventId, RingHistory ringHistory) {
		Iterator<Row> i = ringHistory.getAll().iterator();
		while (i.hasNext()) {
			Row row = i.next();
			if (sameRow(eventId, row)) {
				if (i.hasNext()) {
					return i.next();
				}
				return null;
			}
		}
		throw new IllegalStateException("Id not found from history gotten by the same id...");
	}

	private Row getPreviousRecovery(Column eventId, RingHistory ringHistory) {
		Iterator<Row> i = ringHistory.getRecoveries().iterator();
		Row previous = null;
		while (i.hasNext()) {
			Row row = i.next();
			if (sameRow(eventId, row)) {
				return previous;
			}
			previous = row;
		}
		throw new IllegalStateException("Id not found from history gotten by the same id...");
	}

}
