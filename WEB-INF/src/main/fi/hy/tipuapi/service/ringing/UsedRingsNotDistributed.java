package fi.hy.tipuapi.service.ringing;

import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;
import fi.luomus.commons.utils.Utils;

public class UsedRingsNotDistributed extends BaseServlet {

	private static final String NEW_LINE = "\n";
	private static final String TAB = "\t";
	private static final long serialVersionUID = -2069834310574490482L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		DAO dao = null;
		ResultSet rs = null;
		try {
			System.out.println("Generating used rings not distributed response");

			dao = createNewDAO(req);
			DistributedRings distributed = dao.getDistributedRings();
			rs = dao.getUsedRings();

			String fileName = "not-distributed-" + DateUtils.getFilenameDatetime() + ".txt";
			res.setContentType("text/plain; charset=utf-8");
			res.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			PrintWriter out = res.getWriter();

			out.write("ring" +TAB+ "formatted" +TAB+ "ringer" +TAB+ "date" +TAB+ "id" +TAB+ "diario" +NEW_LINE);

			int i = 0;
			while (rs.next()) {
				i++;
				if (i % 100000 == 0 || i == 1) {
					System.out.println(" ... " + i);
				}
				Ring ring = new Ring(rs.getString(1));
				if (distributed.getDistribution(ring) == null) {
					String formatted = LegacyRingCodeHandlerUtil.dbToActual(ring.toString());
					String ringer = rs.getString(2);
					if (ringer == null) ringer = "";
					String date = DateUtils.sqlDateToString(rs.getDate(3));
					String id = rs.getString(4);
					String diario = rs.getString(5);
					out.write(ring.toString() +TAB+ formatted +TAB+ ringer +TAB+ date +TAB+ id +TAB+ diario +NEW_LINE);
				}
			}
			out.flush();
			System.out.println(" ... DONE!");
			return new ResponseData().setOutputAlreadyPrinted();
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			Utils.close(rs);
			if (dao != null) dao.close();
		}
	}

}
