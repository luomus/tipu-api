package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.utils.Utils;

import java.util.Set;

public class RingVerificationValidator implements FieldValidation {

	private static final Set<String> ALLOWED_FOR_RINGERS = Utils.set("R", "V");

	@Override
	public void validate(RowValidationData data, Column ringVerification) throws Exception {
		if (data.isForUserValidations()) return;
		if (data.isForUpdate()) return;
		if (ringVerification.hasValue()) {
			if (!ALLOWED_FOR_RINGERS.contains(ringVerification.getValue())) {
				data.addWarning("ADMIN_ring_verification_code_warning", ringVerification);
			}
		}
	}

}
