package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.List;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class DiarioValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column diario) throws Exception {
		if (data.isForInsert() && diario.hasValue()) {
			validateDiarioNotAlreadyUsed(data, diario);
		}
	}

	private void validateDiarioNotAlreadyUsed(RowValidationData data, Column diario) throws Exception {
		DAO dao = data.getDao();
		Row searchParams = dao.emptyRow();
		searchParams.get("diario").setSearchParam(diario.getValue());
		List<Row> matches = data.getDao().search(searchParams, null, null);
		if (!matches.isEmpty()) {
			data.addError("ADMIN_diario_already_used", diario);
		}
	}

}
