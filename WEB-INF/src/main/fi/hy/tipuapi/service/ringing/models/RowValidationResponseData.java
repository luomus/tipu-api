package fi.hy.tipuapi.service.ringing.models;

import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData.ValidationErrorOrWarning;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RowValidationResponseData {

	private final Map<String, String> localizedTexts;
	private final JSONObject response = new JSONObject();
	private boolean hasErrors = false;
	private boolean hasWarnings = false;
	private boolean storedToDb = false;
	private List<Integer> storedIds = null;
	
	public RowValidationResponseData(RowValidationData validatedData) {
		this.localizedTexts = validatedData.getLocalizedTexts();

		for (ValidationErrorOrWarning error : validatedData.getErrors()) {
			addError(error);
		}
		for (ValidationErrorOrWarning warning : validatedData.getWarnings()) {
			addWarning(warning);
		}
	}

	private void addWarning(ValidationErrorOrWarning warning) {
		setToResponse(response.getObject("warnings"), warning);
		hasWarnings = true;
	}

	public void addError(ValidationErrorOrWarning error) {
		setToResponse(response.getObject("errors"), error);
		hasErrors = true;
	}

	private void setToResponse(JSONObject errorsOrWarningsObject, ValidationErrorOrWarning error) {
		JSONObject errorEntry = new JSONObject();
		errorEntry.setString("errorName", error.getErrorName());
		Column c = error.getColumn();
		if (c.hasValue() && error.getValuesToAppend() == null) {
			errorEntry.setString("localizedErrorText", c.getValue() + ": " + getLocalizedMessage(error.getErrorName()));
		} else {
			if (given(error.getValuesToAppend())) {
				errorEntry.setString("localizedErrorText", getLocalizedMessage(error.getErrorName()) + " " + commaSeparate(error.getValuesToAppend()));
			} else {
				errorEntry.setString("localizedErrorText", getLocalizedMessage(error.getErrorName()));
			}
		}
		errorsOrWarningsObject.getArray(c.getName()).appendObject(errorEntry);
	}

	private boolean given(String[] valuesToAppend) {
		return valuesToAppend != null && valuesToAppend.length > 0;
	}

	private String commaSeparate(String[] valuesToAppend) {
		Iterator<String> i = Utils.list(valuesToAppend).iterator();
		StringBuilder b = new StringBuilder();
		while (i.hasNext()) {
			b.append(i.next());
			if (i.hasNext()) b.append(", ");
		}
		return b.toString();
	}

	private String getLocalizedMessage(String errorName) {
		if (!localizedTexts.containsKey(errorName)) {
			return errorName;
		}
		return localizedTexts.get(errorName);
	}

	public boolean isAccepted() {
		return !hasErrors && !hasWarnings;
	}

	public boolean isAcceptedWithWarnings() {
		return !hasErrors && hasWarnings;
	}

	public boolean hasErrors() {
		return hasErrors;
	}

	public JSONObject getResponse() {
		response.setBoolean("passes", this.isAccepted());
		if (hasWarnings && !hasErrors) {
			response.setBoolean("passesWithWarnings", true);
		}
		response.setBoolean("storedToDb", storedToDb);
		if (storedToDb && storedIds != null) {
			for (Integer id : storedIds) {
				response.getArray("storedIds").appendString(id.toString());
			}
		}
		return response;
	}

	public void markStoredToDb() {
		storedToDb = true;
	}

	public boolean isStoredToDb() {
		return storedToDb;
	}

	public void setStoredIds(List<Integer> ids) {
		this.storedIds = ids;
	}

}
