package fi.hy.tipuapi.service.ringing;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DAO.RowHandler;
import fi.hy.tipuapi.service.ringing.dao.RowToDbNonInputFieldsValueResolver;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class ReProcessAll extends BaseServlet {

	private static final long serialVersionUID = -8151075735457634487L;
	private static final int BATCH_SIZE = 10000 - 1;
	private static final List<ReprocessAllRunner> workers = new ArrayList<>();

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		boolean stopAll = "true".equals(req.getParameter("stop"));
		if (stopAll) {
			return stopAll(res);
		}
		boolean start = "true".equals(req.getParameter("start"));
		int startAt = req.getParameter("startAt") == null ? 1 : Integer.valueOf(req.getParameter("startAt"));
		if (!start) {
			return error(res, new IllegalArgumentException("Must give start parameter as a safety"));
		}
		pushAllInThread(startAt);
		return ok("reprocessing started", res);
	}

	private ResponseData stopAll(HttpServletResponse res) throws Exception {
		for (ReprocessAllRunner runner : workers) {
			runner.stopWorking();
		}
		return ok("stopping all", res);
	}

	private void pushAllInThread(final int startAt) {
		ReprocessAllRunner runner = new ReprocessAllRunner(startAt);
		new ReprocessingThread(runner).start();
		workers.add(runner);
	}

	private class ReprocessingThread extends Thread {
		public ReprocessingThread(Runnable runnable) {
			super(runnable);
		}
	}

	private static class RunControl {
		private volatile boolean run = true;
		public void stop() {
			run = false;
		}
		public boolean shouldContinue() {
			return run;
		}
	}

	private class ReprocessAllRunner implements Runnable {
		private final int startAt;
		private final RunControl runControl = new RunControl();

		public ReprocessAllRunner(int startAt) {
			this.startAt = startAt;
		}
		public void stopWorking() {
			runControl.stop();
		}
		@Override
		public void run() {
			reprocessAll(startAt);
		}

		private void reprocessAll(int startAt) {
			DAO dao = null;
			try {
				dao = createNewDAO("reprocess-all", false);
				int start = startAt;
				int end = start + BATCH_SIZE;
				while (runControl.shouldContinue()) {
					ReprocessRowHandler rowHandler = new ReprocessRowHandler(dao, runControl);
					Utils.debug("reprocess all", start, end);
					Row searchParams = dao.emptyRow();
					searchParams.get("id").setSearchParam(start +"/" + end);
					dao.search(rowHandler, searchParams, Utils.list("id"), null);
					if (!rowHandler.hadRows()) {
						break;
					}
					start = end + 1;
					end = start + BATCH_SIZE;
				}
				System.out.println("done");
			} catch (Exception e) {
				getErrorReporter().report("Push all to elastic", e);
			} finally {
				if (dao != null) dao.close();
			}
		}
	}

	private static class ReprocessRowHandler implements RowHandler {

		private final DAO dao;
		private final RunControl runControl;
		private boolean hadRows = false;

		public ReprocessRowHandler(DAO dao, RunControl runControl) {
			this.dao = dao;
			this.runControl = runControl;
		}

		public boolean hadRows() {
			return hadRows;
		}

		@Override
		public boolean handleRow(Row row) {
			hadRows = true;

			try {
				List<String> oldValues = values(row);

				RowToDbNonInputFieldsValueResolver.convertCoordinates(row);

				if (row.get("type").getValue().equals("recovery")) {
					String nameRing = row.get("nameRing").getValue();
					RingHistory history = dao.getRingHistoryByNameRing(nameRing);
					RowToDbNonInputFieldsValueResolver.calculateTimeDistanceDirection(row, nameRing, history);
				}

				List<String> newValues = values(row);
				if (!newValues.equals(oldValues)) {
					dao.update(row);
					log(oldValues, newValues, row);
				}
			} catch (Exception e) {
				log(e, row);
			}
			return runControl.shouldContinue();
		}

		private void log(List<String> oldValues, List<String> newValues, Row row) {
			try {
				int id = row.get("id").getIntValue();

				dao.startTransaction();

				PreparedStatement p = dao.getPreparedStatement(" insert into reprocess_log (eventid, changetext) values (?, ?) ");
				p.setInt(1, id);
				p.setString(2, changeText(oldValues, newValues));
				p.executeUpdate();
				dao.commitTransaction();
			} catch (Exception e) {}
		}

		private String changeText(List<String> oldValues, List<String> newValues) {
			StringBuilder b = new StringBuilder();
			Iterator<String> oldI = oldValues.iterator();
			Iterator<String> newI = newValues.iterator();
			while (oldI.hasNext()) {
				String oldV = oldI.next();
				String newV = newI.next();
				b.append(oldV).append(" => ").append(newV).append(" \n");
			}
			return LogUtils.shorten(b.toString(), 2400);
		}

		private void log(Exception e, Row row) {
			try {
				int id = row.get("id").getIntValue();
				System.out.println("ERROR Event id " + id + ": " + e.getMessage());

				dao.startTransaction();

				PreparedStatement p = dao.getPreparedStatement(" insert into reprocess_log (eventid, errortext, fullerror) values (?, ? ,?) ");
				p.setInt(1, id);

				String shortMessage = LogUtils.shorten(LogUtils.buildStackTrace(e, 3), 1400);
				String message = LogUtils.buildStackTrace(e);

				p.setString(2, shortMessage);
				p.setString(3, message);
				p.executeUpdate();
				dao.commitTransaction();
			} catch (Exception e2) {}

		}

		private List<String> values(Row row) {
			List<String> values = new ArrayList<>();
			values.add("dist: " + row.get("distanceToRingingInKilometers").getValue());
			values.add("dir: " + row.get("directionToRingingInDegrees").getValue());
			values.add("time: " + row.get("timeToRingingInDays").getValue());
			values.add(row.get("uniformLat").getValue());
			values.add(row.get("uniformLon").getValue());
			values.add(row.get("eurefLat").getValue());
			values.add(row.get("eurefLon").getValue());
			values.add(fixDes(row.get("wgs84DecimalLat").getValue()));
			values.add(fixDes(row.get("wgs84DecimalLon").getValue()));
			values.add(row.get("wgs84DegreeLat").getValue());
			values.add(row.get("wgs84DegreeLon").getValue());
			return values;
		}

		private String fixDes(String value) {
			if (value == null) return null;
			if (value.endsWith(".0")) return value.substring(0, value.length() - 2);
			if (value.length() > "xx.1234".length()) return value.substring(0, "xx.1234".length());
			return value;
		}

	}

	private ResponseData ok(String message, HttpServletResponse res) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(200);
		PrintWriter writer = res.getWriter();
		writer.write(message);
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

	private ResponseData error(HttpServletResponse res, Exception e) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(500);
		PrintWriter writer = res.getWriter();
		writer.write(LogUtils.buildStackTrace(e));
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

}
