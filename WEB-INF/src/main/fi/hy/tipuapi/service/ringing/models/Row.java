package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.Pair;

public class Row implements Iterable<Column> {

	private final Map<String, String> values = new HashMap<>();
	private final RowStructure structure;

	public Row(RowStructure structure) {
		this.structure = structure;
	}

	public Column get(String name) {
		return new Column(structure.get(name), values);
	}

	@Override
	public Iterator<Column> iterator() {
		return new ColumnIterator();
	}

	private class ColumnIterator implements Iterator<Column> {

		private final Iterator<Field> iterator;

		public ColumnIterator() {
			this.iterator = structure.getFields();
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public Column next() {
			return new Column(iterator.next(), values);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	public boolean hasField(String name) {
		return structure.hasField(name);
	}

	public boolean groupHasValues(String groupName) {
		for (Column c : getGroupFields(groupName)) {
			if (c.hasValue()) return true;
		}
		return false;
	}

	public List<Column> getGroupFields(String groupName) {
		List<Column> list = new ArrayList<>();
		for (Field f : structure.getGroup(groupName)) {
			list.add(get(f.getName()));
		}
		return list;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (Column c : this) {
			if (c.hasValue()) {
				b.append("{").append(c.getName()).append("=").append(c.getValue()).append("} ");
			}
		}
		return b.toString();
	}

	public Row fillValues(Row row) {
		for (Column c : this) {
			c.setValue(row.get(c.getName()).getValue());
		}
		return this;
	}

	public List<Pair<Column, SearchParameter>> getSearchParameters() {
		List<Pair<Column, SearchParameter>> list = new ArrayList<>();
		for (Column c : this) {
			if (c.hasSearchParameterSet()) {
				list.add(new Pair<>(c, c.getSearchParameter()));
			}
		}
		return list;
	}

}
