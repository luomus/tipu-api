package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.EnumerationValues.EnumerationValue;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Field.Type;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.validators.AccuracyOfSomethingValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.AttachmentLefRightValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.BatReproductiveStageValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.BatRequiredFieldValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.BiometricValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.BirdActivitiesValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.BirdConditionValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.CaptureMethodValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.ClutchNumbersAndOwnerValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.CoordinateAccuracyValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.CoordinateTypeBasedCoordinateValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.CoordinatesInsideRegionValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.CuckooHostValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.DiarioValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.EventDateAccuracyValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.FieldReadableAdditionalInfoValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.FieldReadableChangesValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.FieldReadableCodeValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.LaymanValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.LegRingForRecoveryValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.MeasurerValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.MunicipalitySchemeEuringProviceValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.NewLegRingValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.NumberOfYoungsValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.OnlyForAdultsValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.OnlyForBatsValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.OnlyForBirdsValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.PositiveDecimalValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.PositiveIntegerValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RangeValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RangeWarningValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RequiredBirdFieldExceptForFindingValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RequiredFieldExceptForFindingValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RequiredFieldValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RingLostDestroyedValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RingStartEndValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RingVerificationValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RingerLaymanValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.RingsAttachedValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.SpeciesAccuracyValidator;
import fi.hy.tipuapi.service.ringing.validator.validators.SpeciesMustBeValidator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

public class RowStructure implements Iterable<Group> {

	private static final String PYRPYR = "PYRPYR";
	private static final String FICHYP = "FICHYP";
	private final List<Group> groups = new ArrayList<>();
	private final Config config;
	private final boolean forceReload;
	private final LocalizedTextsContainer localizedTexts;
	private static final FieldValidation REQUIRED_FIELD = new RequiredFieldValidator();
	private static final FieldValidation BAT_REQUIRED = new BatRequiredFieldValidator();
	private static final FieldValidation ONLY_FOR_BAT = new OnlyForBatsValidator();
	private static final FieldValidation ONLY_FOR_BIRD = new OnlyForBirdsValidator();
	private static final FieldValidation POSITIVE_INTEGER = new PositiveIntegerValidator();
	private static final FieldValidation POSITIVE_DECIMAL = new PositiveDecimalValidator();
	private static final FieldValidation ONLY_FOR_ADULTS = new OnlyForAdultsValidator();

	public RowStructure(Config config, LocalizedTextsContainer localizedTexts, boolean forceReload) throws Exception {
		System.out.println("Creating row structure...");
		this.config = config;
		this.localizedTexts =  localizedTexts;
		this.forceReload = forceReload;

		Group common = group("common");
		common.addField("event.", "id", Type.INTEGER, noModes()).setLength(10);
		common.addField("event.", "type", Type.ENUMERATION, Mode.BROWSING).setEnumeration(customEnumeration("ringing", "recovery"));
		common.addField("event.", "nameRing", Type.VARCHAR, modes(Mode.BROWSING)).setLength(10).setRequired().setUpperCase();
		common.addField("COALESCE(event.nameRing, 'preventaccidentalupdate')", "updateRing", Type.VARCHAR, modes(Mode.RINGINGS)).setLength(10);
		common.addField("event.", "legRing", Type.VARCHAR, modes(Mode.RECOVERIES, Mode.BROWSING, Mode.ADMIN_RINGINGS)).setValidationRule(new LegRingForRecoveryValidator()).setLength(10).setUpperCase();
		common.addField("event.", "ringStart", Type.VARCHAR, modes(Mode.RINGINGS, Mode.BROWSING)).setValidationRule(new RingStartEndValidator()).setLength(10).setRequired().setUpperCase();
		common.addField("event.", "ringEnd", Type.VARCHAR, modes(Mode.RINGINGS, Mode.BROWSING)).setLength(10).setUpperCase();
		common.addField("event.", "ringNotUsedReason", Type.ENUMERATION, modes(Mode.RINGINGS, Mode.BROWSING)).setEnumeration(code(681)).setValidationRule(new RingLostDestroyedValidator());
		common.addField("event.", "newLegRing", Type.VARCHAR, modes(Mode.RECOVERIES, Mode.BROWSING)).setValidationRule(new NewLegRingValidator()).setLength(10).setUpperCase();
		common.addField("event.", "ringVerification", Type.ENUMERATION, modes(Mode.ADMIN_RECOVERIES, Mode.BROWSING)).setEnumeration(code(40)).setValidationRule(new RingVerificationValidator());
		common.addField("event.", "diario", Type.INTEGER, modes(Mode.BROWSING)).setLength(9).setValidationRule(new DiarioValidator());

		Group foreign = group("foreign");
		foreign.addField("event.", "foreignRingScheme", Type.ENUMERATION).setEnumeration(resource(TipuAPIClient.SCHEMES));
		foreign.addField("event.", "foreignRing", Type.VARCHAR).setLength(15);
		foreign.addField("event.", "foreignRingNotes", Type.VARCHAR).setLength(1500);

		Group persons = group("persons");
		persons.addField("event.", "ringer", Type.INTEGER_ENUMERATION).setEnumeration(resource(TipuAPIClient.RINGERS)).setValidationRule(new RingerLaymanValidator()).setRequired().setLength(5);
		persons.addField("event.", "measurer", Type.VARCHAR).setLength(500).setValidationRule(new MeasurerValidator());
		persons.addField("event.", "intermediaryRinger", Type.INTEGER_ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(resource(TipuAPIClient.RINGERS)).setLength(5);
		persons.addField("event.", "laymanID", Type.INTEGER, modes(Mode.ADMIN_RECOVERIES)).setLength(6).setValidationRule(new LaymanValidator());
		persons.addField("layman.name", "layman", Type.VARCHAR, modes(Mode.BROWSING)).setLength(80);
		persons.addField("ringing.ringer", "ringingRinger", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.RINGERS));

		Group time = group("time");
		time.addField("to_number(to_char(event.eventDate, 'YYYY'))", "eventYear", Type.INTEGER, noModes()).setLength(4);
		time.addField("event.", "eventDate", Type.DATE).setValidationRule(REQUIRED_FIELD).setRequired();
		time.addField("event.", "eventDateAccuracy", Type.ENUMERATION).setEnumeration(code(67)).setValidationRule(new EventDateAccuracyValidator());
		time.addField("event.", "hour", Type.INTEGER_ENUMERATION).setEnumeration(customEnumeration(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)).setLength(2);
		time.addField("event.", "minute", Type.INTEGER).setLength(2).setValidationRule(new RangeValidator(0, 59));

		Group place = group("place");
		place.addField("country.name", "country", Type.VARCHAR, modes(Mode.BROWSING)).setLength(50);
		place.addField("euringProvince.name", "province", Type.VARCHAR, modes(Mode.BROWSING)).setLength(40);
		place.addField("event.", "euringProvinceCode", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setValidationRule(new CoordinatesInsideRegionValidator()).setEnumeration(resource(TipuAPIClient.EURING_PROVINCES));
		place.addField("event.", "schemeID", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(resource(TipuAPIClient.SCHEMES));
		place.addField("event.", "municipality", Type.ENUMERATION).setEnumeration(resource(TipuAPIClient.MUNICIPALITIES)).setValidationRules( new CoordinatesInsideRegionValidator(), new MunicipalitySchemeEuringProviceValidator()).setRequired();
		place.addField("event.", "birdStation", Type.ENUMERATION).setEnumeration(resource(TipuAPIClient.BIRD_STATIONS)).setValidationRule(new CoordinatesInsideRegionValidator());
		place.addField("event.", "locality", Type.VARCHAR).setLength(200);
		place.addField("event.", "personalPlaceName", Type.VARCHAR).setLength(270);
		place.addField("event.", "netID", Type.VARCHAR).setLength(6);
		place.addField("event.", "nestBoxID", Type.VARCHAR).setLength(6);

		Group coordinates = group("coordinates");
		coordinates.addField(specialCoordinateFieldLatPair(), "lat", Type.DECIMAL).setValidationRule(REQUIRED_FIELD).setDecimalAccuracy(15, 8).setRequired();
		coordinates.addField(specialCoordinateFieldLonPair(), "lon", Type.DECIMAL).setValidationRule(REQUIRED_FIELD).setDecimalAccuracy(15, 8).setRequired();
		coordinates.addField("event.", "coordinateSystem", Type.ENUMERATION).setEnumeration(code(606)).setValidationRules(REQUIRED_FIELD, new CoordinateTypeBasedCoordinateValidator()).setRequired();
		coordinates.addField("event.", "coordinateAccuracy", Type.ENUMERATION).setEnumeration(code(682)).setValidationRules(REQUIRED_FIELD, new CoordinateAccuracyValidator()).setRequired();
		coordinates.addField("event.", "coordinateMeasurementMethod", Type.ENUMERATION).setEnumeration(code(683)).setRequired();

		Group event  = group("event");
		event.addField("event.", "sourceOfRecovery", Type.ENUMERATION, modes(Mode.ADMIN_RECOVERIES)).setEnumeration(code(684));
		event.addField("event.", "captureMethod", Type.ENUMERATION).setEnumeration(code(56)).setValidationRule(new CaptureMethodValidator());
		event.addField("event.", "birdActivities", Type.ENUMERATION).setEnumeration(code(55)).setValidationRules(ONLY_FOR_BIRD, new BirdActivitiesValidator());
		event.addField("event.", "ringsAttached", Type.ENUMERATION).setEnumeration(code(53)).setValidationRule(new RingsAttachedValidator());
		event.addField("event.", "recoveryMethod", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(code(74));
		event.addField("event.", "recoveryMethodAccuracy", Type.ENUMERATION, modes(Mode.ADMIN_RECOVERIES)).setEnumeration(code(71));
		event.addField("event.", "birdConditionEURING", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(code(70));
		event.addField("event.", "birdMoved", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(code(69));
		event.addField("event.", "notes", Type.VARCHAR).setLength(1500);
		event.addField("event.", "hiddenNotes", Type.VARCHAR, modes(Mode.ADMIN_RINGINGS, Mode.ADMIN_RECOVERIES)).setLength(1500);

		Group unit = group("unit");
		unit.addField("event.", "species", Type.ENUMERATION).setEnumeration(resource(TipuAPIClient.SELECTABLE_SPECIES)).setValidationRule(new RequiredFieldExceptForFindingValidator()).setRequired();
		unit.addField("event.", "speciesAccuracy", Type.ENUMERATION).setEnumeration(code(66)).setValidationRule(new SpeciesAccuracyValidator());
		unit.addField("ringing.species", "ringedSpecies", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.SPECIES));
		unit.addField("ringing.speciesAccuracy", "ringedSpeciesAccuracy", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(code(66));
		unit.addField("event.", "sex", Type.ENUMERATION).setEnumeration(code(9)).setValidationRule(BAT_REQUIRED);
		unit.addField("event.", "sexDeterminationMethod", Type.ENUMERATION).setEnumeration(code(10)).setValidationRule(new AccuracyOfSomethingValidator("sex"));
		unit.addField("event.", "age", Type.ENUMERATION).setEnumeration(code(11)).setValidationRules(new RequiredBirdFieldExceptForFindingValidator(), ONLY_FOR_BIRD);
		unit.addField("event.", "ageDeterminationMethod", Type.ENUMERATION).setEnumeration(code(12)).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "weightInGrams", Type.DECIMAL).setDecimalAccuracy(8, 1).setValidationRules(new BiometricValidator("weight-in-grams"), POSITIVE_DECIMAL);
		unit.addField("event.", "wingLengthInMillimeters", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(new BiometricValidator("wing-length-in-millimeters", BiometricValidator.IGNORE_MIN_IF_MOULTING)).setValidationRules(POSITIVE_DECIMAL, ONLY_FOR_BIRD);
		unit.addField("event.", "wingLengthMinMeasurement", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(new BiometricValidator("wing-length-in-millimeters", BiometricValidator.IGNORE_MIN_IF_MOULTING)).setValidationRules(POSITIVE_DECIMAL, ONLY_FOR_BIRD);
		unit.addField("event.", "wingLengthOtherMethod", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(new BiometricValidator("wing-length-in-millimeters", BiometricValidator.IGNORE_MIN_IF_MOULTING)).setValidationRules(POSITIVE_DECIMAL, ONLY_FOR_BIRD);
		unit.addField("event.", "wingLengthAccuracy", Type.DECIMAL_ENUMERATION).setEnumeration(code(605)).setValidationRule(new AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator("wingLengthInMillimeters", "wingLengthMinMeasurement")).setDecimalAccuracy(4, 1).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "wingLengthNotes", Type.VARCHAR).setLength(1500);
		unit.addField("event.", "birdCondition", Type.ENUMERATION).setEnumeration(code(54)).setValidationRules(ONLY_FOR_BIRD, new BirdConditionValidator());
		unit.addField("event.thirdFeatherLengthInMillis", "thirdFeatherLengthInMillimeters", Type.DECIMAL).setValidationRule(new BiometricValidator("third-feather-length-in-millimeters", BiometricValidator.IGNORE_MIN_IF_MOULTING)).setValidationRules(POSITIVE_DECIMAL, ONLY_FOR_BIRD).setDecimalAccuracy(6, 1);
		unit.addField("event.", "moulting", Type.ENUMERATION).setEnumeration(code(29)).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "fat", Type.DECIMAL).setValidationRule(new RangeValidator(0.0, 8.9)).setDecimalAccuracy(3, 1).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "fatAccuracy", Type.DECIMAL_ENUMERATION).setEnumeration(customEnumeration("1", "0,5", "0,25", "0,1")).setValidationRule(new AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator("fat")).setDecimalAccuracy(2, 1).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "muscleFitness", Type.DECIMAL).setDecimalAccuracy(3, 1).setValidationRules(ONLY_FOR_BIRD, new RangeValidator(0.0, 3.1));
		unit.addField("event.", "muscleFitnessAccuracy", Type.DECIMAL_ENUMERATION).setEnumeration(customEnumeration("1", "0,5", "0,25", "0,1")).setValidationRule(new AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator("muscleFitness")).setDecimalAccuracy(2, 1).setValidationRule(ONLY_FOR_BIRD);
		unit.addField("event.", "broodPatch", Type.ENUMERATION).setEnumeration(code(602)).setValidationRules(ONLY_FOR_ADULTS, ONLY_FOR_BIRD);

		Group bat = group("bat");
		bat.addField("event.", "batCapturePlace", Type.ENUMERATION).setEnumeration(code(400)).setValidationRules(ONLY_FOR_BAT);
		bat.addField("event.", "batAge", Type.ENUMERATION).setEnumeration(code(401)).setValidationRules(BAT_REQUIRED, ONLY_FOR_BAT);
		bat.addField("event.", "batAgeDeterminationMethod", Type.ENUMERATION).setEnumeration(code(402)).setValidationRules(ONLY_FOR_BAT);
		bat.addField("event.batRightArmLengthInMillis", "batRightArmLengthInMillimeters", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(new BiometricValidator("wing-length-in-millimeters"), POSITIVE_DECIMAL, ONLY_FOR_BAT);
		bat.addField("event.batLeftArmLengthInMillis", "batLeftArmLengthInMillimeters", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(new BiometricValidator("wing-length-in-millimeters"), POSITIVE_DECIMAL, ONLY_FOR_BAT);
		bat.addField("event.batFifthFingerInMillis", "batFifthFingerInMillimeters", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRules(POSITIVE_DECIMAL, ONLY_FOR_BAT);
		bat.addField("event.", "batReproductiveStage", Type.ENUMERATION).setEnumeration(code(403)).setValidationRules(new BatReproductiveStageValidator(), ONLY_FOR_BAT);
		bat.addField("event.", "batHibernation", Type.ENUMERATION, modes(Mode.RECOVERIES, Mode.BROWSING)).setEnumeration(code(404)).setValidationRule(ONLY_FOR_BAT);

		Group clutch = group("clutch");
		clutch.addField("event.", "clutchNumber", Type.INTEGER).setValidationRule(POSITIVE_INTEGER).setValidationRule(new ClutchNumbersAndOwnerValidator()).setLength(6);
		clutch.addField(null, "someoneElsesClutchNumber", Type.INTEGER, modes(Mode.RINGINGS, Mode.RECOVERIES)).setValidationRule(POSITIVE_INTEGER).setLength(6);
		clutch.addField("event.", "clutchNumberOwner", Type.ENUMERATION).setEnumeration(resource(TipuAPIClient.RINGERS));
		clutch.addField("event.", "numberOfYoungs", Type.INTEGER, modes(Mode.RINGINGS, Mode.BROWSING)).setValidationRule(POSITIVE_INTEGER).setValidationRule(new NumberOfYoungsValidator()).setLength(2);
		clutch.addField("event.", "ageOfYoungsInDays", Type.INTEGER, modes(Mode.RINGINGS, Mode.BROWSING)).setValidationRule(new RangeValidator(0, 100)).setLength(2);
		clutch.addField("event.", "ageOfYoungsAccuracy", Type.ENUMERATION, modes(Mode.RINGINGS, Mode.BROWSING)).setEnumeration(code(33)).setValidationRule(new AccuracyOfSomethingValidator("ageOfYoungsInDays"));

		Group fieldReadable = group("fieldReadable");
		fieldReadable.addField("event.", "attachmentPoint", Type.ENUMERATION).setEnumeration(code(308)).setValidationRule(new FieldReadableAdditionalInfoValidator());
		fieldReadable.addField("event.", "fieldReadableCode", Type.VARCHAR).setLength(10).setValidationRule(new FieldReadableCodeValidator()).setUpperCase();
		fieldReadable.addField("event.", "mainColor", Type.ENUMERATION).setEnumeration(code(303));
		fieldReadable.addField("event.", "codeColor", Type.ENUMERATION).setEnumeration(code(305)).setValidationRule(new FieldReadableAdditionalInfoValidator());
		fieldReadable.addField("event.", "codeAlign", Type.ENUMERATION).setEnumeration(code(307)).setValidationRule(new FieldReadableAdditionalInfoValidator());
		fieldReadable.addField("event.", "upsideDown", Type.ENUMERATION).setEnumeration(code(314));
		fieldReadable.addField("event.", "attachmentLeftRight", Type.ENUMERATION).setEnumeration(code(309)).setValidationRule(new AttachmentLefRightValidator());
		fieldReadable.addField("event.", "mainShade", Type.ENUMERATION).setEnumeration(code(304));
		fieldReadable.addField("event.", "accuracyOfColors", Type.ENUMERATION).setEnumeration(code(306));
		fieldReadable.addField("event.", "numberOfSightnings", Type.INTEGER, modes(Mode.RECOVERIES, Mode.BROWSING)).setValidationRule(POSITIVE_INTEGER).setLength(4);
		fieldReadable.addField("event.", "fieldReadableCondition", Type.ENUMERATION).setEnumeration(code(316));
		fieldReadable.addField("event.", "metalRingColor", Type.ENUMERATION).setEnumeration(code(303));
		fieldReadable.addField("event.", "fieldReadableChanges", Type.ENUMERATION, modes(Mode.RECOVERIES, Mode.BROWSING, Mode.ADMIN_RINGINGS)).setEnumeration(code(604)).setValidationRule(new FieldReadableChangesValidator());
		fieldReadable.addField("event.", "removedFieldReadableCode", Type.VARCHAR, modes(Mode.RECOVERIES, Mode.BROWSING)).setLength(10).setUpperCase();
		fieldReadable.addField("event.", "removedFieldReadableMainColor", Type.ENUMERATION, modes(Mode.RECOVERIES, Mode.BROWSING)).setEnumeration(code(303));
		fieldReadable.addField("event.", "fieldReadableNotes", Type.VARCHAR).setLength(1500);

		Group colony = group("colony");
		colony.addField("event.", "colonyPairCount", Type.INTEGER).setLength(6).setValidationRule(POSITIVE_INTEGER);
		colony.addField("event.", "colonyPairCountAccuracy", Type.INTEGER).setLength(3).setValidationRule(new RangeValidator(10, 100));
		colony.addField("event.", "colonySizeInMeters", Type.INTEGER).setLength(8).setValidationRule(POSITIVE_INTEGER);
		colony.addField("event.", "colonyNotes", Type.VARCHAR).setLength(1500);

		Group parasites = group("parasites");
		parasites.addField("event.", "mites", Type.INTEGER).setValidationRule(new RangeValidator(0, 500)).setLength(3);
		parasites.addField("event.", "birdsflies", Type.INTEGER).setValidationRule(new RangeValidator(0, 500)).setLength(3);
		parasites.addField("event.", "fleas", Type.ENUMERATION).setEnumeration(code(540));
		parasites.addField("event.", "lices", Type.ENUMERATION).setEnumeration(code(540));
		parasites.addField("event.", "sarcopticManges", Type.ENUMERATION).setEnumeration(code(540));
		parasites.addField("event.", "poxVirus", Type.ENUMERATION).setEnumeration(code(540));
		parasites.addField("event.", "parasiteSamples", Type.INTEGER).setValidationRule(new RangeValidator(0, 500)).setLength(3);
		parasites.addField("event.", "parasiteNotes", Type.VARCHAR).setLength(1500);

		Group otherBiometrics = group("otherBiometrics");
		otherBiometrics.addField("event.", "biometricNotes", Type.VARCHAR).setLength(1500);
		otherBiometrics.addField("event.", "wingWear", Type.ENUMERATION).setEnumeration(code(550)).setValidationRule(ONLY_FOR_BIRD);
		otherBiometrics.addField("event.", "wingLengthAbnormalityReason", Type.ENUMERATION).setEnumeration(code(551)).setValidationRule(ONLY_FOR_BIRD);
		otherBiometrics.addField("event.", "eggInside", Type.ENUMERATION).setEnumeration(code(540)).setValidationRule(ONLY_FOR_BIRD);
		otherBiometrics.addField("event.", "cloacaSize", Type.ENUMERATION).setEnumeration(code(552));
		otherBiometrics.addField("event.", "cloacaStatus", Type.ENUMERATION).setEnumeration(code(553));
		otherBiometrics.addField("event.", "sicknessNotes", Type.VARCHAR).setLength(1500);
		otherBiometrics.addField("event.", "faultBars", Type.ENUMERATION).setEnumeration(code(540)).setValidationRule(ONLY_FOR_BIRD);
		otherBiometrics.addField("event.", "injuriesNotes", Type.VARCHAR).setLength(1500);

		Group legMeasurements = group("legMeasurements");
		legMeasurements.addField("event.", "tarsusLengthToes", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(5, 500));
		legMeasurements.addField("event.", "tarsusLengthLastScale", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(5, 500));
		legMeasurements.addField("event.", "tarsusDiameter", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(1, 40));
		legMeasurements.addField("event.", "nailLength", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(1, 60));

		Group beakMeasurements = group("beakMeasurements");
		beakMeasurements.addField("event.", "beakLengthSkull", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(5, 300)).setValidationRule(ONLY_FOR_BIRD);
		beakMeasurements.addField("event.", "beakLengthFeathers", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(4, 300)).setValidationRule(ONLY_FOR_BIRD);
		beakMeasurements.addField("event.", "beakLengthNostril", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(2, 300)).setValidationRule(ONLY_FOR_BIRD);
		beakMeasurements.addField("event.", "headBeakLength", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(2, 400)).setValidationRule(ONLY_FOR_BIRD);
		beakMeasurements.addField("event.", "beakHeight", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(2, 60)).setValidationRule(ONLY_FOR_BIRD);
		beakMeasurements.addField("event.", "beakWidth", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(2, 60)).setValidationRule(ONLY_FOR_BIRD);

		Group featherMeasurements = group("featherMeasurements");
		featherMeasurements.addField("event.", "prim10distancePCWingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(-300, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim10WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim9WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim8WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim7WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim6WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim5WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim4WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim3WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim2WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "prim1WingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(1, 300)).setValidationRule(ONLY_FOR_BIRD);
		featherMeasurements.addField("event.", "outerSecondaryWingFormula", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 300)).setValidationRule(ONLY_FOR_BIRD);

		Group otherFeatherMeasurements = group("otherFeatherMeasurements");
		otherFeatherMeasurements.addField("event.", "prim2Emargination", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 100)).setValidationRule(ONLY_FOR_BIRD);
		otherFeatherMeasurements.addField("event.", "tailLength", Type.INTEGER).setLength(3).setValidationRule(new RangeValidator(0, 700)).setValidationRule(ONLY_FOR_BIRD);
		otherFeatherMeasurements.addField("event.", "tailLenthVertical", Type.INTEGER).setLength(3).setValidationRule(new RangeValidator(0, 700)).setValidationRule(ONLY_FOR_BIRD);

		Group moultState = group("moultState");
		moultState.addField("event.", "moultStatus", Type.ENUMERATION).setEnumeration(code(554)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "ter1Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "ter2Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "ter3Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec6Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec5Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec4Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec3Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec2Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "sec1Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim1Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim2Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim3Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim4Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim5Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim6Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim7Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim8Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim9Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "prim10Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec1Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec2Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec3Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec4Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec5Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "rec6Moult", Type.ENUMERATION).setEnumeration(code(555)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "bodyFeathersMoultIntensity", Type.ENUMERATION).setEnumeration(code(557)).setValidationRule(ONLY_FOR_BIRD);
		moultState.addField("event.", "bodyFeathersMoultProgress", Type.ENUMERATION).setEnumeration(code(556)).setValidationRule(ONLY_FOR_BIRD);

		Group moultExtend = group("moultExtend");
		moultExtend.addField("event.", "moultingAlulaLeft", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 3)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingAlulaRight", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 3)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingCarpiLeft", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 1)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingCarpiRight", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 1)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingBiCovertsLeft", Type.INTEGER).setLength(2).setValidationRule(new RangeValidator(0, 10)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingBiCovertsRight", Type.INTEGER).setLength(2).setValidationRule(new RangeValidator(0, 10)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingTetrialLeft", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 3)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingTetrialRight", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 3)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingTailLeft", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 6)).setValidationRule(ONLY_FOR_BIRD);
		moultExtend.addField("event.", "moultingTailRight", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 6)).setValidationRule(ONLY_FOR_BIRD);

		Group waders = group("waders");
		waders.addField("event.", "wadersAnkletToe", Type.DECIMAL).setDecimalAccuracy(6, 1).setValidationRule(new RangeValidator(0, 200)).setValidationRule(ONLY_FOR_BIRD);
		waders.addField("event.", "wadersFat", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(0, 7)).setValidationRule(ONLY_FOR_BIRD);

		Group speciesSpecial = group("speciesSpecial");
		speciesSpecial.addField("event.", "fichypDrost", Type.INTEGER).setLength(1).setValidationRule(new RangeValidator(1, 7)).setValidationRule(new SpeciesMustBeValidator(FICHYP));
		speciesSpecial.addField("event.", "pyrpyrTailSpots", Type.ENUMERATION).setEnumeration(code(540)).setValidationRule(new SpeciesMustBeValidator(PYRPYR));
		speciesSpecial.addField("event.", "hirrusBroodPatch", Type.ENUMERATION).setEnumeration(code(501)).setValidationRules(ONLY_FOR_ADULTS);
		speciesSpecial.addField("event.hirrusLongestMiddleRectrix", "hirrusLongestMiddleRectrixLengthInMillimeters", Type.DECIMAL).setValidationRule(new RangeWarningValidator(40, 50)).setDecimalAccuracy(5, 1);
		speciesSpecial.addField("event.hirrusOutmostLeftRectrix", "hirrusOutmostLeftRectrixLengthInMillimeters", Type.DECIMAL).setValidationRule(ONLY_FOR_ADULTS).setValidationRule(new RangeWarningValidator(70, 140)).setDecimalAccuracy(5, 1);
		speciesSpecial.addField("event.hirrusOutmostRightRectrix", "hirrusOutmostRightRectrixLengthInMillimeters", Type.DECIMAL).setValidationRule(ONLY_FOR_ADULTS).setValidationRule(new RangeWarningValidator(70, 140)).setDecimalAccuracy(5, 1);
		speciesSpecial.addField("event.hirrusTailNotchDeepness", "hirrusTailNotchDeepnessInMillimeters", Type.DECIMAL).setValidationRule(ONLY_FOR_ADULTS).setValidationRule(new RangeWarningValidator(30, 136)).setDecimalAccuracy(5, 1);
		speciesSpecial.addField("event.", "hirrusBodyMoulting", Type.ENUMERATION).setEnumeration(code(509));
		speciesSpecial.addField("event.", "hirrusTenthRemiges", Type.ENUMERATION).setEnumeration(code(510));
		speciesSpecial.addField("event.", "hirrusNinthRemiges", Type.ENUMERATION).setEnumeration(code(510));
		speciesSpecial.addField("event.", "hirrusEightRemiges", Type.ENUMERATION).setEnumeration(code(510));
		speciesSpecial.addField("event.", "hirrusNotes", Type.VARCHAR).setLength(1500);
		speciesSpecial.addField("event.", "cuckooHostSpecies", Type.ENUMERATION, modes(Mode.RINGINGS, Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.SELECTABLE_SPECIES)).setValidationRule(new CuckooHostValidator());

		Group customFields = group("customFields");
		for (int i = 1; i <= 8; i++) {
			customFields.addField("event.", "customField"+i, Type.VARCHAR).setLength(50);
			customFields.addField("event.", "customValue"+i, Type.VARCHAR).setLength(255);
		}

		Group misc = group("misc");
		misc.addField("event.", "interestingRecovery", Type.ENUMERATION, modes(Mode.ADMIN_RECOVERIES)).setEnumeration(code(690));
		misc.addField("event.", "validForLongevity", Type.ENUMERATION, modes(Mode.ADMIN_RECOVERIES)).setEnumeration(code(695));
		misc.addField("event.", "noLetter", Type.INTEGER_ENUMERATION, modes(Mode.ADMIN_RECOVERIES)).setEnumeration(customEnumeration(1)).setLength(1);
		misc.addField("event.", "additionalInformationCode", Type.ENUMERATION).setEnumeration(code(680));
		misc.addField("event.", "fieldReadableChangeEuringCode", Type.ENUMERATION, modes(Mode.BROWSING, Mode.ADMIN_RECOVERIES)).setEnumeration(code(73));
		misc.addField("event.", "legacySomethingUncertain", Type.VARCHAR, noModes()).setLength(1);
		misc.addField("event.", "legacyFat", Type.INTEGER).setLength(1);
		misc.addField("event.", "legacyWing", Type.DECIMAL).setDecimalAccuracy(6, 1);
		misc.addField("event.", "legacyWingMin", Type.DECIMAL, modes(Mode.ADMIN_RINGINGS, Mode.ADMIN_RECOVERIES)).setDecimalAccuracy(6, 1);
		misc.addField("event.", "dataEntry", Type.ENUMERATION).setEnumeration(code(696));
		misc.addField("event.", "created", Type.DATE, Mode.BROWSING);
		misc.addField("event.", "modified", Type.DATE, Mode.BROWSING);
		misc.addField("event.", "modifiedBy", Type.ENUMERATION, Mode.BROWSING).setEnumeration(resource(TipuAPIClient.ADMINS));

		Group additionalPlaceInfo = group("additionalPlaceInfo");
		additionalPlaceInfo.addField("country.id", "countryCode", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.COUNTRIES));
		additionalPlaceInfo.addField("elyCentre.id", "elyCentreCode", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.ELY_CENTRES));
		additionalPlaceInfo.addField("elyCentre.name", "elyCentre", Type.VARCHAR, modes(Mode.BROWSING)).setLength(50);
		additionalPlaceInfo.addField("finnishProvince.id", "finnishProvinceCode", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.PROVINCES));
		additionalPlaceInfo.addField("finnishProvince.name", "finnishProvince", Type.VARCHAR, modes(Mode.BROWSING)).setLength(40);
		additionalPlaceInfo.addField("finnishOldCounty.id", "finnishOldCountyCode", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.OLD_COUNTIES));
		additionalPlaceInfo.addField("finnishOldCounty.name", "finnishOldCounty", Type.VARCHAR, modes(Mode.BROWSING)).setLength(50);
		additionalPlaceInfo.addField("lylArea.id", "lylAreaCode", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.LYL_AREAS));
		additionalPlaceInfo.addField("lylArea.name", "lylArea", Type.VARCHAR, modes(Mode.BROWSING)).setLength(70);
		additionalPlaceInfo.addField("currentMunicipality.id", "currentMunicipality", Type.ENUMERATION, modes(Mode.BROWSING)).setEnumeration(resource(TipuAPIClient.CURRENT_MUNICIPALITIES));

		Group converted = group("convertedCoordinates");
		converted.addField("event.", "uniformLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("event.", "uniformLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("event.", "eurefLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("event.", "eurefLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(6);
		converted.addField("event.", "wgs84DecimalLat", Type.DECIMAL, modes(Mode.BROWSING)).setDecimalAccuracy(11, 8);
		converted.addField("event.", "wgs84DecimalLon", Type.DECIMAL, modes(Mode.BROWSING)).setDecimalAccuracy(11, 8);
		converted.addField("event.", "wgs84DegreeLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(8);
		converted.addField("event.", "wgs84DegreeLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(8);
		converted.addField("ringing.uniformLat", "ringingUniformLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("ringing.UniformLon", "ringingUniformLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("ringing.eurefLat", "ringingEurefLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(7);
		converted.addField("ringing.eurefLon", "ringingEurefLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(6);
		converted.addField("ringing.wgs84DecimalLat", "ringingWgs84DecimalLat", Type.DECIMAL, modes(Mode.BROWSING)).setDecimalAccuracy(11, 8);
		converted.addField("ringing.wgs84DecimalLon", "ringingWgs84DecimalLon", Type.DECIMAL, modes(Mode.BROWSING)).setDecimalAccuracy(11, 8);
		converted.addField("ringing.wgs84DegreeLat", "ringingWgs84DegreeLat", Type.INTEGER, modes(Mode.BROWSING)).setLength(8);
		converted.addField("ringing.wgs84DegreeLon", "ringingWgs84DegreeLon", Type.INTEGER, modes(Mode.BROWSING)).setLength(8);

		Group calculated = group("calculatedValues");
		calculated.addField("event.", "distanceToRingingInKilometers", Type.INTEGER, modes(Mode.BROWSING)).setLength(5);
		calculated.addField("event.", "directionToRingingInDegrees", Type.INTEGER, modes(Mode.BROWSING)).setLength(3);
		calculated.addField("event.", "timeToRingingInDays", Type.INTEGER, modes(Mode.BROWSING)).setLength(5);
		calculated.addField("to_number(to_char(event.eventDate, 'DDD'))", "eventDayOfYear", Type.INTEGER, modes(Mode.BROWSING)).setLength(3);

		fields = new LinkedHashMap<>();
		for (Group group : groups) {
			for (Field f : group) {
				fields.put(f.getName(), f);
			}
		}
		System.out.println("Row structure created!");
	}

	private final Map<String, Field> fields;

	@Override
	public Iterator<Group> iterator() {
		return groups.iterator();
	}

	public Field get(String name) {
		Field f = fields.get(name);
		if (f != null) return f;
		throw new IllegalArgumentException("No column " + name);
	}

	public Iterator<Field> getFields() {
		return fields.values().iterator();
	}

	public boolean hasField(String name) {
		return fields.containsKey(name);
	}

	public Group getGroup(String name) {
		for (Group g : groups) {
			if (g.getName().equals(name)) {
				return g;
			}
		}
		throw new IllegalArgumentException("No group " + name);
	}

	private String specialCoordinateFieldLatPair() {
		return "  CASE " +
				"  WHEN event.coordinateSystem='YKJ' THEN event.uniformLat " +
				"  WHEN event.coordinateSystem='WGS84-DMS' THEN event.wgs84DegreeLat "+
				"  WHEN event.coordinateSystem='WGS84' THEN event.wgs84DecimalLat " +
				"  WHEN event.coordinateSystem='EUREF' THEN event.eurefLat " +
				"  ELSE null " +
				" END ";
	}

	private String specialCoordinateFieldLonPair() {
		return specialCoordinateFieldLatPair().replace("Lat", "Lon");
	}

	private Mode[] noModes() {
		return Field.noModes();
	}

	private Mode[] modes(Mode ... modes) {
		return modes;
	}

	private EnumerationValues customEnumeration(Object ... values) {
		EnumerationValues enumerationValues = new EnumerationValues();
		for (Object o : values) {
			String value = o.toString();
			EnumerationValue enumerationValue = new EnumerationValue(value);
			if (localizedTexts.getAllTexts("fi").containsKey(value)) {
				enumerationValue.setDescription(localizedTexts.getText(value, "fi"), "fi");
			} else {
				enumerationValue.setDescription(value);
			}
			enumerationValues.add(enumerationValue);
		}
		return enumerationValues;
	}

	private EnumerationValues code(int code) throws Exception {
		EnumerationValues enumerationValues = new EnumerationValues();
		TipuAPIClient client = null;
		try {
			client = new TipuAPIClientImple(config);

			Document codes = null;
			if (forceReload) {
				codes = client.forceReload(TipuAPIClient.CODES, Integer.toString(code));
			} else {
				codes = client.getAsDocument(TipuAPIClient.CODES, Integer.toString(code));
			}

			for (Node codeNode : codes.getRootNode()) {
				EnumerationValue value = new EnumerationValue(codeNode.getNode("code").getContents());
				for (Node descNode : codeNode.getChildNodes("desc")) {
					value.setDescription(descNode.getContents(), descNode.getAttribute("lang").toLowerCase());
				}
				for (Node descNode : codeNode.getChildNodes("detailed-desc")) {
					if (descNode.getContents().length() > 0) {
						value.setDetailedDescription(descNode.getContents(), descNode.getAttribute("lang").toLowerCase());
					}
				}
				enumerationValues.add(value);
			}
		} finally {
			if (client != null) client.close();
		}
		return enumerationValues;
	}

	private EnumerationValues resource(String resourceName) throws Exception {
		EnumerationValues enumerationValues = new EnumerationValues();
		TipuAPIClient client = null;
		try {
			client = new TipuAPIClientImple(config);

			Document resource = null;
			if (forceReload) {
				resource = client.forceReload(resourceName);
			} else {
				resource = client.getAsDocument(resourceName);
			}

			for (Node resourceNode : resource.getRootNode()) {
				EnumerationValue value = new EnumerationValue(resourceNode.getNode("id").getContents());
				for (Node nameNode : resourceNode.getChildNodes("name")) {
					if (nameNode.hasAttribute("lang")) {
						value.setDescription(nameNode.getContents(), nameNode.getAttribute("lang").toLowerCase());
					} else {
						value.setDescription(nameNode.getContents());
					}
				}
				enumerationValues.add(value);
			}
		} finally {
			if (client != null) client.close();
		}
		return enumerationValues;
	}

	private Group group(String name) {
		Group group = new Group(name);
		this.groups.add(group);
		return group;
	}

}
