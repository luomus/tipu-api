package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ForceReloadOfCaches extends BaseServlet {

	private static final long serialVersionUID = 5821421529305095507L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			dao.forceReloadOfCachedResources();
			JSONObject response = new JSONObject();
			response.setBoolean("success", true);
			return print(response, res);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}
}
