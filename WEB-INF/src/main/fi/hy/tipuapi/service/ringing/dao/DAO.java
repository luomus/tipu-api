package fi.hy.tipuapi.service.ringing.dao;

import java.io.Closeable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import fi.hy.tipuapi.service.ringing.models.DistributedFieldReadable;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.FieldReadable;
import fi.hy.tipuapi.service.ringing.models.NameRingsRecoveryIDs;
import fi.hy.tipuapi.service.ringing.models.RingBatches;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.RingSeriesContainer.RingSeries;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;

public interface DAO extends Closeable {

	public static class DataWarehousingEntry {
		private final Action action;
		private final int eventId;
		private final Row row;
		public DataWarehousingEntry(Action action, int eventId, Row row) {
			this.action = action;
			this.eventId = eventId;
			this.row = row;
		}
		public Action getAction() {
			return action;
		}
		public int getEventId() {
			return eventId;
		}
		public Row getRow() {
			return row;
		}
		@Override
		public String toString() {
			return Utils.debugS(action, eventId, row);
		}
	}

	public interface RowHandler {
		boolean handleRow(Row row);
	}

	String RECOVERY_TYPE_CONTROL = "1";
	String RECOVERY_TYPE_FINDING = "2";

	public static enum LetterType { ONLY_FINDING_LETTERS, ONLY_CONTROL_LETTERS, ALL }

	// Rows

	enum Action { INSERT, UPDATE, DELETE, MASS_UPDATE }

	RowStructure getRowStructure();

	Row emptyRow();


	// Db stuff

	@Override
	void close();

	PreparedStatement getPreparedStatement(String sql) throws SQLException;

	void startTransaction() throws Exception;

	void commitTransaction() throws Exception;

	void rollbackTransaction() throws Exception;


	// Row search

	List<Row> search(Row searchParams, List<String> sortBy, String ringerOrBirdStation) throws Exception;

	int countTotal(Row searchParams, String ringerOrBirdStation) throws Exception;

	Row getRowByEventId(int eventId) throws Exception;

	void search(RowHandler rowHandler, Row searchParams, List<String> sortBy, String ringerOrBirdStation) throws Exception;


	// Letters

	NameRingsRecoveryIDs lettersNotYetPrinted(LetterType letterType) throws Exception;

	NameRingsRecoveryIDs lettersNotYetPrinted(List<Integer> recoveryIds) throws Exception;

	void removeFromLetterPrinting(int diario) throws Exception;

	void markRecoveriesForLetterPrintingUsingDiarios(List<Integer> diarioNumbers) throws Exception;

	void markRecoveriesForLetterPrintingUsingEventIDs(List<Integer> eventIds) throws Exception;


	// Datawarehousing

	List<DataWarehousingEntry> getLatestWarehouseQueueRows(int limit) throws Exception;

	void markElasticPushSuccess(int eventId) throws Exception;

	void markETLPushSuccess(int eventId) throws Exception;

	void clearSuccessfulFromWarehousingQueue() throws Exception;

	// Tipu-API

	TipuApiResource getTipuApiResource(String resourceName) throws Exception;


	// Series

	boolean seriesExists(Ring ring) throws Exception;

	RingSeries getSeries(Ring ring) throws Exception;


	// Ring distribution / use

	boolean ringDistributed(Ring ring, int ringerNumber, DateValue eventDate) throws Exception;

	boolean ringDistributedToSomeone(Ring ring, DateValue dateValue) throws Exception;

	DistributedRings getDistributedRings() throws Exception;

	void forceReloadOfCachedResources() throws Exception;

	boolean ringUsed(String ring) throws Exception;

	RingBatches getRingersUsedRings(int ringerNumber) throws Exception;

	RingBatches getBirdStationUsedRings(String birdStationCode) throws Exception;

	ResultSet getUsedRings() throws SQLException;

	// Fieldreadable distribution / use

	boolean fieldReadableDistributed(String fieldReadableCode, String backgroundColor, int ringerNumber, DateValue eventDate, String species) throws Exception;

	DistributedFieldReadable getDistributedFieldReadables(int ringerNumber) throws Exception;

	boolean fieldReadableUsed(String fieldReadableCode, String backgroundColor, String species) throws Exception;

	Set<FieldReadable> getRingersUsedFieldReadables(int ringerNumber) throws SQLException;



	// Name ring stuff

	String getNameRing(String legRing) throws Exception;

	RingHistory getRingHistoryByNameRing(String nameRing) throws Exception;

	RingHistory getRingHistoryByLegRing(String legRing) throws Exception;

	List<String> getNameRingByFieldReadable(String fieldReadableCode, String backgroundColor, String species) throws Exception;

	RingHistory getRingHistoryByEventId(int eventId) throws Exception;


	// Misc

	List<Integer> getTopAgeRecords(String species) throws Exception;

	Row getFieldReadableDistributionInfo(String fieldReadableCode, String backgroundColor, String species) throws Exception;

	boolean fieldReadableDoubleDistributed(String code, String backgroundColor, String species) throws Exception;

	boolean laymanExists(String laymanId) throws Exception;

	List<String> getAllNameRingsBySpeciesThatHaveRecoveries(String species) throws Exception;


	// Db row store stuff

	int insert(Row row, Mode mode) throws Exception;

	void update(Row row) throws Exception;

	void delete(int eventId) throws Exception;



}
