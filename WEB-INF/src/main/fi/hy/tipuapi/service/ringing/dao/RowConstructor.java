package fi.hy.tipuapi.service.ringing.dao;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.json.JSONObject;

public class RowConstructor {

	public static Row constructTo(Row row, JSONObject json) {
		for (Column c : row) {
			if (json.hasKey(c.getName())) {
				String value = json.getString(c.getName());
				value = value.trim();
				if (c.isUpperCaseColumn()) {
					value = value.toUpperCase();
				}
				c.setValue(value);
			}
		}
		return row;
	}

}
