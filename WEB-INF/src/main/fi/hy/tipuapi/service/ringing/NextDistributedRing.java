package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NextDistributedRing extends BaseServlet {

	private static final long serialVersionUID = -7608072270904888077L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		DAO dao = null;
		try {
			String previousRing = getPreviousRing(req);

			if (notValid(previousRing)) {
				return returnNextRing("", "previous-was-not-valid: " + previousRing, res);
			}

			dao = createNewDAO(req);

			DistributedRings distributedRings = dao.getDistributedRings();
			String nextRing = distributedRings.nextRing(previousRing);
			if (nextRing == null) {
				return returnNextRing("", "no-next-ring-distributed-in-this-series-for-the-ringer", res);
			}
			if (dao.ringUsed(nextRing)) {
				return returnNextRing("", "next-ring-was-already-used", res);
			}
			nextRing = LegacyRingCodeHandlerUtil.dbToActual(nextRing);
			return returnNextRing(nextRing, "ok", res);  
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private ResponseData returnNextRing(String ring, String message, HttpServletResponse res) throws IOException {
		JSONObject response = new JSONObject();
		response.setBoolean("success", true);
		response.setString("nextRing", ring);
		if (given(message)) {
			response.setString("message", message);
		}
		return print(response, res);
	}

	private boolean notValid(String previousRing) {
		return !new Ring(previousRing).validFormat();
	}

	private String getPreviousRing(HttpServletRequest req) {
		String previousRing = req.getParameter("previousRing");
		if (!given(previousRing)) {
			throw new IllegalArgumentException("previousRing parameter must be given.");
		}
		return previousRing;
	}

}
