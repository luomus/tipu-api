package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class RequiredBirdFieldExceptForFindingValidator extends BirdRequiredFieldValidator {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		Column sourceOfRecovery = data.getRow().get("sourceOfRecovery");
		if (!sourceOfRecovery.getValue().equals(DAO.RECOVERY_TYPE_FINDING)) {
			super.validate(data, column);
		}
	}
	
}
