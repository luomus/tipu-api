package fi.hy.tipuapi.service.ringing.r99;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.luomus.commons.containers.Card;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;

public class Rengas99ToJSONConverter {

	private static Map<Integer, String> RENGAS99_VARIABLE_NUMBER_TO_ROW_FIELDNAME_MAP = initRengas99VariableNumberToRowFieldNameMap();

	public Rengas99ToJSONConverter(RowStructure rowStructure) {
		validateVariableNameMap(rowStructure);
	}

	private void validateVariableNameMap(RowStructure rowStructure) {
		for (String fieldName : RENGAS99_VARIABLE_NUMBER_TO_ROW_FIELDNAME_MAP.values()) {
			if (fieldName.startsWith("_")) continue;
			if (!rowStructure.hasField(fieldName)) {
				throw new IllegalStateException(fieldName + " misstyped / not found from row structure.");
			}
		}
	}

	private static Map<Integer, String> initRengas99VariableNumberToRowFieldNameMap() {
		Map<Integer, String> map = new HashMap<>();

		//yleiset
		//		map.put(100, ""); 	 // PVM
		//		map.put(101, ""); 	 // RENGAS_SARJA
		map.put(102, "ringer"); 	 // RENGASTAJA_NRO
		map.put(103, "species"); 	 // LAJI
		map.put(104, "sex"); 	 // SUKUPUOLI
		map.put(105, "sexDeterminationMethod"); 	 // SP_PERUSTE
		map.put(106, "age"); 	 // IKA
		map.put(107, "ageDeterminationMethod"); 	 // IKA_PERUSTE
		map.put(108, "birdCondition"); 	 // KUNTO
		map.put(109, "birdActivities"); 	 // TILA
		map.put(110, "captureMethod"); 	 // PYYNTITAPA
		map.put(111, "clutchNumber"); 	 // POIKUEEN_NRO
		map.put(112, "ringNotUsedReason"); 	 // TARKKUUS -- oikeasti tässä kentässä ilmoitetaan tuhotuneet/hävinneet renkata eikä tarkkuutta!  
		map.put(113, "_wingLengthMeasurementMethod"); 	 // SIIVEN_MITMEN
		map.put(114, "wingLengthInMillimeters"); 	 // SIIVEN_PITUUS
		map.put(115, "thirdFeatherLengthInMillimeters"); 	 // KOLM_KASIS_PIT
		map.put(116, "weightInGrams"); 	 // PAINO
		map.put(117, "legacyFat"); 	 // RASVA_5
		map.put(118, "fat"); 	 // RASVA_8
		map.put(119, "hour"); 	 // KELLO
		map.put(120, "ringsAttached"); 	 // RENG_TAPA
		map.put(121, "moulting"); 	 // SULKASATO
		map.put(122, "municipality"); 	 // KUNTA
		map.put(123, "lat"); 	 // POHJ
		map.put(124, "lon"); 	 // ITA
		map.put(125, "personalPlaceName"); 	 // NIMETTY_PAIKKA
		map.put(126, "additionalInformationCode"); 	 // LISATIEDOT
		map.put(127, "locality"); 	 // TARKKA_PAIKKA
		map.put(128, "notes"); 	 // KOMMENTTI_1
		//		map.put(140, ""); 	 // TALLENNUS_PVM
		//		map.put(141, ""); 	 // LAHETYS_PVM
		map.put(142, "_date_yyyy");	// PVM_VUOSI
		map.put(143, "_date_mm"); 	// PVM_KK
		map.put(144, "_date_dd"); 	// PVM_PAIVA

		// väri
		map.put(129, "fieldReadableCode"); 	 // TUNNUS
		map.put(130, "mainColor"); 	 // POHJAN_VARI
		map.put(131, "mainShade"); 	 // POHJAN_SAVY
		map.put(132, "codeColor"); 	 // TUNNUS_VARI
		map.put(133, "codeAlign"); 	 // TUNNUS_ASENTO
		map.put(134, "upsideDown"); 	 // YLOSALAISIN
		map.put(135, "attachmentPoint"); 	 // KIINNITYSKOHTA
		map.put(136, "attachmentLeftRight"); 	 // KIINNITYSPUOLI
		//		map.put(137, ""); 	 // KIINN_PVM
		map.put(138, "metalRingColor"); 	 // METALLIR_VARI
		map.put(139, "fieldReadableNotes"); 	 // KOMMENTTI_2

		// rengastus 
		//		map.put(200, ""); 	 // MISTA
		//		map.put(201, ""); 	 // MIHIN
		map.put(202, "numberOfYoungs"); 	 // POIKASTEN_LKM
		map.put(203, "ageOfYoungsInDays"); 	 // POIKASTEN_IKA
		map.put(204, "ageOfYoungsAccuracy"); 	 // POIK_IAN_TARK

		// tapaaminen 
		//		map.put(300, ""); 	 // RENGAS_NRO
		//		map.put(301, ""); 	 // TUNN_TARKKUUS 
		map.put(302, "accuracyOfColors"); 	 // VARI_TARKK
		map.put(303, "numberOfSightnings"); 	 // RIIPP_LUVUT
		// map.put(304, ""); 	 // POISTO_PVM
		map.put(305, "removedFieldReadableCode"); 	 // POIS_VAIHD_TUNN
		map.put(306, "ringVerification"); 	 // VARMISTUS
		//		map.put(307, ""); 	 // UUSI_SARJA
		//		map.put(308, ""); 	 // UUSI_NRO
		// map.put(309, ""); 	 // VARI_TILA 

		// hirrus 
		map.put(9100, "broodPatch"); 	 // HAUTOMALAIKKU
		map.put(9101, "muscleFitness"); 	 // LIHASKUNTO
		map.put(9102, "hirrusLongestMiddleRectrixLengthInMillimeters"); 	 // KESK_PS
		map.put(9103, "hirrusOutmostLeftRectrixLengthInMillimeters"); 	 // UL_VAS
		map.put(9104, "hirrusOutmostRightRectrixLengthInMillimeters"); 	 // UL_OIK
		map.put(9105, "hirrusTailNotchDeepnessInMillimeters"); 	 // LOVI
		map.put(9106, "hirrusBodyMoulting"); 	 // RUUMIS_SSATO
		map.put(9107, "hirrusTenthRemiges"); 	 // KASI_10
		map.put(9108, "hirrusNinthRemiges"); 	 // KASI_9
		map.put(9109, "hirrusEightRemiges"); 	 // KASI_8
		map.put(9110, "biometricNotes"); 	 // KOMMENTTI

		return map;
	}

	private static class VariableOfRengas99FileLine {
		private final int beginningIndex;
		private final int length;
		private final String name;
		public VariableOfRengas99FileLine(int beginningIndex, int length, String name) {
			this.beginningIndex = beginningIndex;
			this.length  = length;
			this.name = name;
		}
	}

	private static class Template {
		private List<VariableOfRengas99FileLine> _1;
		private List<VariableOfRengas99FileLine> _2;
		private List<VariableOfRengas99FileLine> _3_vari;
		private List<VariableOfRengas99FileLine> _3_hirrus;
		private List<VariableOfRengas99FileLine> _4;
	}

	private static Template RINGING_TEMPLATE = initRingingTemplate();
	private static Template RECOVERY_TEMPLATE = initRecoveryTemplate();

	private static Template initRingingTemplate() {
		Template template = new Template();
		template._1 = parseVariables("R1<101,2><200,7><102,4><142,4><201,7>  <103,7><104,1><105,1><106,2><107,1><144,2><143,2><122,6><123,5><124,4><120,1><108,1><109,1><110,1><202,2><111,3><112,1><113,1><114,3><115,4><116,6><121,1><117,1><118,1><119,2><203,2><204,1><126,1>");
		template._2 = parseVariables("R2<101,2><200,7><127,80>");
		template._3_hirrus = parseVariables("R3<101,2><200,7>HIRRUS<9100,1><118,1><9101,1> <9102,4> <9103,4> <9104,4> <115,4> <9105,4> <9106,1> <9107,1><9108,1><9109,1><9110,75>");
		template._3_vari = parseVariables("R3<101,2><200,7>VARI  <129,8>    <130,6> <131,1> <132,6>   <133,2> <135,1> <136,1><134,1>  <137,8>         <138,6>        <139,40>");
		template._4 = parseVariables("R4<101,2><200,7><128,80>");
		return template;
	}

	private static Template initRecoveryTemplate() {
		Template template = new Template();
		template._1 = parseVariables("L1<101,2><300,7><306,1><102,4><142,4><308,7><307,2><103,7><104,1><105,1><106,2><107,1><144,2><143,2><122,6><123,5><124,4><120,1><108,1><109,1><110,1><111,3><113,1><114,3><115,4><116,6><121,1><117,1><118,1><119,2><126,1>");
		template._2 = parseVariables("L2<101,2><300,7><127,80>");
		template._3_hirrus = parseVariables("L3<101,2><300,7>HIRRUS<9100,1><118,1><9101,1> <9102,4> <9103,4> <9104,4> <115,4> <9105,4> <9106,1> <9107,1><9108,1><9109,1><9110,75>");
		template._3_vari = parseVariables("L3<101,2><300,7>VARI  <129,8> <309,1>  <130,6> <131,1> <132,6> <302,1> <133,2> <135,1> <136,1><134,1><303,1> <137,8><304,8> <138,6><305,8><139,40>");
		template._4 = parseVariables("L4<101,2><300,7><128,80>");
		return template;
	}

	public List<JSONObject> convert(List<String> lines, Mode mode) {
		List<JSONObject> rows = new LinkedList<>();
		int countRingins = 0;
		int countRecoveries = 0;
		JSONObject currentObject = new JSONObject();
		boolean first = true;

		for (String line : lines) {
			if (line.trim().length() < 1) continue;

			Card card = new Card(line);

			if (card.v(1).equals("R")) {
				countRingins++;
			}
			else if (card.v(1).equals("L")) {
				countRecoveries++;
			}
			if (both(countRingins, countRecoveries)) {
				throw new RingingsAndRecoveriesMixedException();
			}
			if (mode == Mode.RECOVERIES) {
				if (countRingins > 0) throw new RingingsAndRecoveriesMixedException();
			} else {
				if (countRecoveries > 0) throw new RingingsAndRecoveriesMixedException();
			}

			if (!first && eventChanges(card)) {
				rows.add(currentObject);
				currentObject = new JSONObject();
			}
			addData(card, currentObject);
			first = false;
		}
		rows.add(currentObject);
		for (JSONObject json : rows) {
			combineDateAndRemovePlaceHolders(json);
			handleWingMeasurement(json);
		}
		return rows;
	}

	private void handleWingMeasurement(JSONObject json) {
		String measurementMethod = getAndRemove(json, "_wingLengthMeasurementMethod");
		if ("S".equals(measurementMethod)) {
			String wingLengthInMillimeters = getAndRemove(json, "wingLengthInMillimeters");
			json.setString("wingLengthMinMeasurement", wingLengthInMillimeters);
		}
	}

	private static boolean eventChanges(Card card) {
		return card.v(2).equals("1");
	}

	private static void combineDateAndRemovePlaceHolders(JSONObject json) {
		String yyyy = getAndRemove(json, "_date_yyyy");
		String mm = getAndRemove(json, "_date_mm");
		String dd = getAndRemove(json, "_date_dd");
		String date = dd.trim() + "." + mm.trim() + "." + yyyy.trim();
		json.setString("eventDate", date);
	}

	private static String getAndRemove(JSONObject json, String key) {
		String v = json.getString(key);
		json.remove(key);
		return v;
	}

	private static void addData(Card card, JSONObject jsonObject) {
		String type = card.v(1);
		String cardType =  card.v(2);
		if (type.equals("R")) {
			if (cardType.equals("1")) {
				addRingingData(card, jsonObject);
			}
			addRingingOrRecoveryLineData(card, RINGING_TEMPLATE, jsonObject);
		} else if (type.equals("L")){
			if (cardType.equals("1")) {
				addRecoveryData(card, jsonObject);
			}
			addRingingOrRecoveryLineData(card, RECOVERY_TEMPLATE, jsonObject);
		} else {
			throw new InvalidDataException("Unknown row type: '" + type + "' " + card.data());
		}
	}

	private static void addRingingData(Card card, JSONObject json) {
		String ringPrefix = card.v(3, 2);
		String ringStart = card.v(5, 7);
		String ringEnd = card.v(20, 7);

		ringStart = LegacyRingCodeHandlerUtil.dbToActual(ringPrefix + ringStart);
		json.setString("ringStart", ringStart);

		if (given(ringEnd)) {
			ringEnd = LegacyRingCodeHandlerUtil.dbToActual(ringPrefix + ringEnd);
			json.setString("ringEnd", ringEnd);	
		}
	}

	private static void addRecoveryData(Card card, JSONObject json) {
		String ringPrefix = card.v(3, 2);
		String legRingNumbers = card.v(5, 7);
		if (given(legRingNumbers)) {
			String legRing = LegacyRingCodeHandlerUtil.dbToActual(ringPrefix + legRingNumbers);
			json.setString("legRing", legRing);
		}
		String newLegRingNumbers = card.v(21, 7);
		if (given(newLegRingNumbers)) {
			String newLegRing = LegacyRingCodeHandlerUtil.dbToActual(ringPrefix + newLegRingNumbers);
			json.setString("newLegRing", newLegRing);
		}
	}

	private static void addRingingOrRecoveryLineData(Card card, Template template, JSONObject json) {
		if (card.data().trim().length() < 10) {
			throw new InvalidDataException("Too short line.:" + card.data());
		}

		String cardType =  card.v(2);

		if (cardType.equals("1")) {
			addData(json, card, template._1);
			return;
		}
		if (cardType.equals("2")) {
			addData(json, card, template._2);
			return;
		}
		if (cardType.equals("3")) {
			// jos on seka VARI etta HIRRUS, VARI on oltava aina ensin
			if (card.data().contains("HIRRUS") && card.data().contains("VARI")) {
				if (card.data().indexOf("HIRRUS") < card.data().indexOf("VARI")) {
					throw new InvalidDataException("Both HIRRUS and VARI information given but VARI is not first:" + card.data());
				}
			}
			String cardType3Type = card.v(12, 6);
			if (cardType3Type.equals("VARI  ")) {
				String onlyVariData = card.v(1, 128);
				addData(json, new Card(onlyVariData.trim()), template._3_vari);
				String maybeHirrus = card.v(128, 999).trim();
				if (maybeHirrus.startsWith("HIRRUS")) {
					String prefix = card.data().substring(0, 11);
					String hirrusdata = prefix + maybeHirrus;
					addData(json, new Card(hirrusdata.trim()), template._3_hirrus);
				} else if (maybeHirrus.length() > 0){
					throw new InvalidDataException("Long card 3 type line but doesn't start with HIRRUS: " + card.data());
				}
				return;
			}
			if (cardType3Type.equals("HIRRUS")) {
				addData(json, card, template._3_hirrus);
				return;
			}

		} else if (cardType.equals("4")) {
			addData(json, card, template._4);
			return;
		} else {
			throw new InvalidDataException("Unknown card type: '" + cardType + "' : "+ card.data());
		}
	}

	private static void addData(JSONObject json, Card card, List<VariableOfRengas99FileLine> variablesOfRengas99FileLine) {
		if (card.data().trim().length() > maxLength(variablesOfRengas99FileLine)) {
			throw new InvalidDataException("Too long line.: " + card.data());
		}
		for (VariableOfRengas99FileLine variable : variablesOfRengas99FileLine) {
			if (given(variable.name)) {
				String value = card.v(variable.beginningIndex, variable.length).trim();
				if (given(value)) {
					json.setString(variable.name, value);
				}
			}
		}
	}

	private static int maxLength(List<VariableOfRengas99FileLine> variablesOfRengas99FileLine) {
		int maxBegingIndex = 0;
		int lengthOfThat = 0;
		for (VariableOfRengas99FileLine variable : variablesOfRengas99FileLine) {
			if (variable.beginningIndex > maxBegingIndex) {
				maxBegingIndex = variable.beginningIndex;
				lengthOfThat = variable.length;
			}
		}
		return maxBegingIndex + lengthOfThat;
	}

	private static boolean given(Object o) {
		return o != null && o.toString().trim().length() > 0;
	}

	private static List<VariableOfRengas99FileLine> parseVariables(String lineTemplate) {
		// R1<101,2><200,7><102,4><142,4><201,7>  <103,7><104,1><105,1><106,2><107,1><144,2><143,2><122,6><123,5><124,4><120,1><108,1><109,1><110,1><202,2><111,3><112,1><113,1><114,3><115,4><116,6><121,1><117,1><118,1><119,2><203,2><204,1><126,1>
		// L3<101,2><300,7>VARI  <129,8> <309,1>  <130,6> <131,1> <132,6> <302,1> <133,2> <135,1> <136,1><134,1><303,1> <137,8><304,8> <138,6><305,8><139,40>
		List<VariableOfRengas99FileLine> variablesOfRengas99FileLine = new ArrayList<>();
		int beginningIndex = 1;
		boolean variableNumberOpen = false;
		boolean variableLengthOpen = false;
		String currentVariableNumber = "";
		String currentVariableLength = "";
		for (char c : lineTemplate.toCharArray()) {
			if (c == '<') {
				variableNumberOpen = true;
				continue;
			}
			if (variableNumberOpen) {
				if (c == ',') {
					variableNumberOpen = false;
					variableLengthOpen = true;
					continue;
				} 
				currentVariableNumber += c;
				continue;
			}
			if (variableLengthOpen) {
				if (c == '>') {
					variableLengthOpen = false;
					int variableNumber = Integer.valueOf(currentVariableNumber);
					int variableLength = Integer.valueOf(currentVariableLength);
					String variableName = RENGAS99_VARIABLE_NUMBER_TO_ROW_FIELDNAME_MAP.get(variableNumber);
					VariableOfRengas99FileLine variable = new VariableOfRengas99FileLine(beginningIndex, variableLength, variableName);
					variablesOfRengas99FileLine.add(variable);
					currentVariableNumber = "";
					currentVariableLength = "";
					beginningIndex += variableLength;
					continue;
				}
				currentVariableLength += c;
				continue;
			}
			beginningIndex++;
		}
		return variablesOfRengas99FileLine;
	}


	private static boolean both(int countRingins, int countRecoveries) {
		return countRingins > 0 && countRecoveries > 0;
	}

}
