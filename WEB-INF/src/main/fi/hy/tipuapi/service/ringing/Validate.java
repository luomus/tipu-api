package fi.hy.tipuapi.service.ringing;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.RowConstructor;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowValidationResponseData;
import fi.hy.tipuapi.service.ringing.models.RowsValidatedResponse;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.hy.tipuapi.service.ringing.validator.RowValidator;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

public class Validate extends BaseServlet {

	private static final long serialVersionUID = -5579894518825975726L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return this.processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (notProductionMode()) log(req);
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			RowsValidatedResponse response = validateRows(dao, req);
			return print(response.getResponse(), res);
		} catch (IllegalArgumentException e) {
			return print(generateExceptionResponse(e), res);
		} catch (Exception e) {
			getErrorReporter().report("Data: " + req.getParameter("data"), e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private RowsValidatedResponse validateRows(DAO dao, HttpServletRequest req) throws Exception {
		Map<String, String> localizedTexts = getLocalizedTexts().getAllTexts(getLocale(req));
		Mode mode = getMode(req);
		DAO.Action action = getAction(req);
		String data = req.getParameter("data");
		boolean acceptWarnings = "true".equals(req.getParameter("acceptWarnings")); // This is for RowsToDb
		String userid = req.getParameter("userid");
		if (!given(data)) {
			throw new IllegalArgumentException("You must give 'data' parameter");
		}
		return validateRowsData(dao, localizedTexts, mode, action, acceptWarnings, userid, new JSONObject(data));
	}

	private RowsValidatedResponse validateRowsData(DAO dao, Map<String, String> localizedTexts, Mode mode, DAO.Action action, boolean acceptWarnings, String userid, JSONObject inputData) {
		RowsValidatedResponse rowsValidatedResponse = new RowsValidatedResponse(getValidationType(), action);
		for (Map.Entry<String, Row> e : getRowsByIds(dao, inputData).entrySet()) {
			String id = e.getKey();
			Row row = e.getValue();
			validateRowData(dao, localizedTexts, mode, action, acceptWarnings, rowsValidatedResponse, id, userid, row);
		}
		return rowsValidatedResponse;
	}

	private void validateRowData(DAO dao, Map<String, String> localizedTexts, Mode mode, DAO.Action action, boolean acceptWarnings, RowsValidatedResponse rowsValidatedResponse, String id, String userid, Row row) {
		RowValidationResponseData rowValidationResponseData = validateRow(dao, row, mode, action, userid, localizedTexts);
		storeToDbHook(dao, rowValidationResponseData, row, mode, action, acceptWarnings);
		rowsValidatedResponse.setRow(id, rowValidationResponseData);
	}

	@SuppressWarnings("unused")
	protected void storeToDbHook(DAO dao, RowValidationResponseData rowValidationResponseData, Row row, Mode mode, DAO.Action action, boolean acceptWarnings) {
		// for RowsToDb to override
	}

	protected Map<String, Row> getRowsByIds(DAO dao, JSONObject inputData) {
		Map<String, Row> map = new HashMap<>();
		JSONObject inputRows = inputData.getObject("rows");
		for (String rowID : inputRows.getKeys()) {
			JSONObject rowData = inputRows.getObject(rowID);
			map.put(rowID, RowConstructor.constructTo(dao.emptyRow(), rowData));
		}
		return map;
	}

	private String getValidationType() {
		return validationMode() ? "admin-validate" : "user-validate";
	}

	protected RowValidationResponseData validateRow(DAO dao, Row row, Mode mode, DAO.Action action, String userid, Map<String, String> localizedTexts) {
		RowValidationData data = new RowValidationData(row, mode, action, getConfig(), getErrorReporter(), dao, localizedTexts)
				.setAdminValidationMode(validationMode())
				.setUserId(userid);
		RowValidator rowValidator = new RowValidator(data);
		return rowValidator.getValidationResponse();
	}

	protected boolean validationMode() {
		return RowValidator.USER_VALIDATIONS;
	}

}
