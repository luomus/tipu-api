package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.Set;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field;
import fi.hy.tipuapi.service.ringing.models.Group;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.utils.Utils;

public class RingLostDestroyedValidator implements FieldValidation {

	private static Set<String> ALLOWED_VALUES = Utils.set("T", "H");
	private static Set<String> REQUIRED_FIELDS_INSERT = Utils.set("ringStart", "eventDate", "ringNotUsedReason", "ringer");
	private static Set<String> REQUIRED_FIELDS_UPDATE = Utils.set("legRing", "eventDate", "ringNotUsedReason", "ringer");
	private static Set<String> ALLOWED_GROUPS = Utils.set("time", "place", "coordinates", "misc", "convertedCoordinates", "calculatedValues");
	private static Set<String> ALLOWED_FIELDS = Utils.set("id", "type", "ringer", "birdStation", "ringStart", "ringEnd", "nameRing", "legRing", "newLegRing", "created", "notes", "hiddenNotes");

	public static boolean ringMarkedLostOrDestroyed(RowValidationData data) {
		Column ringNotUsedReasonColumn = data.getRow().get("ringNotUsedReason");
		if (ringNotUsedReasonColumn.hasValue()) {
			if (ALLOWED_VALUES.contains(ringNotUsedReasonColumn.getValue())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void validate(RowValidationData data, Column ringNotUsedColumn) throws Exception {
		if (notLostOrDestroyed(data, ringNotUsedColumn)) return;

		validateRequiredFields(data);
		checkNotAllowedFieldsAreNotUsed(data);
	}

	private void validateRequiredFields(RowValidationData data) {
		for (String requiredField : getRequiredFieldsForLostOrDestroyed(data)) {
			data.require(data.getRow().get(requiredField));
		}
	}

	private boolean notLostOrDestroyed(RowValidationData data, Column ringNotUsedColumn) {
		return !data.notNullNoErrors(ringNotUsedColumn);
	}

	private void checkNotAllowedFieldsAreNotUsed(RowValidationData data) {
		for (Group group : data.getDao().getRowStructure()) {
			if (!ALLOWED_GROUPS.contains(group.getName())) {
				checkFieldsAreNotUsed(data, group);
			}
		}
	}

	private void checkFieldsAreNotUsed(RowValidationData data, Group group) {
		Row row = data.getRow();
		for (Field field : group) {
			if (field.isAggregated()) continue;
			if (getRequiredFieldsForLostOrDestroyed(data).contains(field.getName())) continue;
			if (ALLOWED_FIELDS.contains(field.getName())) continue;
			Column c = row.get(field.getName());
			if (c.hasValue()) {
				data.addError("ringNotUsed_not_allowed_column", c);
			}
		}
	}

	private Set<String> getRequiredFieldsForLostOrDestroyed(RowValidationData data) {
		return data.isForInsert() ? REQUIRED_FIELDS_INSERT : REQUIRED_FIELDS_UPDATE;
	}

}
