package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class RingerLaymanValidator extends RequiredFieldValidator {

	@Override
	public void validate(RowValidationData data, Column ringer) throws Exception {
		if (data.isForAdminValidations() || adminUser(data)) { // Sulka syötössä tehdään admin käyttäjille admin-validoinnit, vaikka onkin syöttömoodi
			adminValidate(data, ringer);
		} else {
			userValidate(data, ringer);
		}
	}

	private boolean adminUser(RowValidationData data) {
		if (notGiven(data.getUserId())) return true;
		return data.getUserId().startsWith("admin_");
	}

	private boolean notGiven(String userId) {
		return userId == null || userId.length() < 1;
	}

	private void adminValidate(RowValidationData data, Column ringer) throws Exception {
		if (data.getMode() == Mode.RINGINGS) {
			require(data, ringer);
			return;
		}
		Column laymanID = data.getRow().get("laymanID");
		Column schemeID = data.getRow().get("schemeID");

		if (!ringer.hasValue() && !laymanID.hasValue() && !schemeID.hasValue()) {
			data.addError("ADMIN_ringer_or_layman_or_scheme_must_be_given", ringer);
			data.addError("ADMIN_ringer_or_layman_or_scheme_must_be_given", laymanID);
			data.addError("ADMIN_ringer_or_layman_or_scheme_must_be_given", schemeID);
			return;
		}
		if (ringer.hasValue() && laymanID.hasValue()) {
			data.addError("ADMIN_ringer_or_layman_both_given", ringer);
			data.addError("ADMIN_ringer_or_layman_both_given", laymanID);
		}
	}

	private void userValidate(RowValidationData data, Column ringer) throws Exception {
		require(data, ringer);
//		if (data.noErrors(ringer)) {
//			if (!data.getUserId().equals(ringer.getValue())) {
//				data.addWarning("reporting_for_other_ringer_warning", ringer);
//			}
//		}
	}

	private void require(RowValidationData data, Column ringer) throws Exception {
		super.validate(data, ringer);
	}

}
