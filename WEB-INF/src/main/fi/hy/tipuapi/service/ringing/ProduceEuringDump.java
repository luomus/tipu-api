package fi.hy.tipuapi.service.ringing;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.RowToEuring2020Converter;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.xml.Document.Node;

public class ProduceEuringDump extends BaseServlet {

	private static final long serialVersionUID = 2870775329440919979L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String singleSpecies = req.getParameter("species");
		boolean all = "true".equals(req.getParameter("all"));
		if (!given(singleSpecies) && !all) {
			return error(res, new IllegalArgumentException("Must give species or all=true parameter"));
		}

		DAO dao = null;
		try {
			dao = createNewDAO(this.getClass().getName(), false);
			RowToEuring2020Converter converter = new RowToEuring2020Converter(dao.getTipuApiResource(TipuAPIClient.SPECIES), dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES));
			List<String> speciesToProcess = getSpeciesToProcess(singleSpecies, all, dao);
			procudeEuringDumpInThread(speciesToProcess, converter);
			return ok("producing dump started for species " + speciesToProcess, res);
		} catch (Exception e) {
			return error(res, e);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private void procudeEuringDumpInThread(final List<String> speciesToProcess, final RowToEuring2020Converter converter) throws Exception {
		new Thread(new Producer(speciesToProcess, converter)).start();
	}

	private class Producer implements Runnable {

		private final List<String> speciesToProcess;
		private final DAO dao;
		private final RowToEuring2020Converter converter;

		public Producer(List<String> speciesToProcess, RowToEuring2020Converter converter) throws Exception {
			this.speciesToProcess = speciesToProcess;
			this.dao = createNewDAO(this.getClass().getName(), false);
			this.converter = converter;
		}

		@Override
		public void run() {
			try {
				produceDump();
			} finally {
				if (dao != null) dao.close();
			}
		}

		private void produceDump() {
			String filenamePostfix = DateUtils.getFilenameDatetime();
			for (String species : speciesToProcess) {
				try {
					produceDump(dao, species, filenamePostfix, converter);
				} catch (Exception e) {
					return;
				}
			}
			System.out.println("done");
		}

		private void produceDump(final DAO dao, String species, String filenamePostfix, RowToEuring2020Converter converter) throws Exception {
			System.out.println("Generating dump for " + species);
			File out = new File(getConfig().reportFolder(), species + "_" + filenamePostfix + ".txt");
			List<String> nameRings = null;
			try {
				nameRings = dao.getAllNameRingsBySpeciesThatHaveRecoveries(species);
				System.out.println(nameRings.size() + " name rings for " + species);
			} catch (Exception e) {
				getErrorReporter().report("Get name rings for species " + species, e);
				throw e;
			}
			produceDump(dao, nameRings, converter, out);
			System.out.println("Dump ready: " + out.getName());
		}

		private void produceDump(final DAO dao, List<String> nameRings, RowToEuring2020Converter converter, File out) {
			for (String nameRing : nameRings) {
				try {
					List<String> nameRingRows = produceDump(dao, converter, nameRing);
					write(nameRingRows, out);
				} catch (Exception e) {
					try {
						LogUtils.write("Error for " + nameRing +" :\n" + LogUtils.buildStackTrace(e), getConfig().reportFolder(), "Errors");
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
		}

		private void write(List<String> nameRingRows, File out) throws IOException {
			StringBuilder b = new StringBuilder();
			for (String line : nameRingRows) {
				b.append(line).append("\n");
			}
			FileUtils.writeToFile(out, b.toString(), true);
		}

		private List<String> produceDump(final DAO dao, RowToEuring2020Converter converter, String nameRing) throws Exception {
			RingHistory ringHistory = dao.getRingHistoryByNameRing(nameRing);
			List<String> rows = converter.convert(ringHistory);
			return rows;
		}
	}

	private List<String> getSpeciesToProcess(String singleSpecies, boolean all, DAO dao) throws Exception {
		List<String> speciesToProcess = new ArrayList<>();
		if (all) {
			TipuApiResource allSpecies = dao.getTipuApiResource(TipuAPIClient.SPECIES);
			for (Node n : allSpecies.getAll()) {
				if (shouldInclude(n)) {
					speciesToProcess.add(n.getNode("id").getContents());
				}
			}
			speciesToProcess.add(null);
		} else {
			if (singleSpecies.equalsIgnoreCase("null")) {
				speciesToProcess.add(null);
			} else {
				speciesToProcess.add(singleSpecies);
			}
		}
		return speciesToProcess;
	}

	private boolean shouldInclude(Node n) {
		Integer euringCode = Integer.valueOf(n.getAttribute("euring-code"));
		return euringCode < 50000 || euringCode >= 90000;
	}

	private ResponseData ok(String message, HttpServletResponse res) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(200);
		PrintWriter writer = res.getWriter();
		writer.write(message);
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

	private ResponseData error(HttpServletResponse res, Exception e) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(500);
		PrintWriter writer = res.getWriter();
		writer.write(LogUtils.buildStackTrace(e));
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

}
