package fi.hy.tipuapi.service.ringing.models;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class RowToEuring2020Converter {

	private static final Map<String, String> AGE_MAP = initAgeMap();
	private final TipuApiResource species;
	private final TipuApiResource municipalities;

	public RowToEuring2020Converter(TipuApiResource species, TipuApiResource municipalities) {
		this.species = species;
		this.municipalities = municipalities;
	}

	private static final Set<String> BAD_DEAD_RECOVERY_METHODS = Utils.set("0", "1", "2", "3", "99");

	public List<String> convert(RingHistory ringHistory) throws Exception {
		List<String> euringRows = new ArrayList<>();

		Row ringingRow = ringHistory.getRinging();
		RingsAttachedInformation current = new RingsAttachedInformation(ringingRow);
		RingsAttachedInformation previous = RingsAttachedInformation.empty();
		euringRows.add(convert(ringingRow, current, previous));

		previous = current;

		Row onlyOneDead = resolveDeadRowToUse(ringHistory);

		for (Row recovery : ringHistory.getRecoveries()) {
			if (onlyOneDead != null && RowUtils.birdDead(recovery) && !onlyOneDead.get("id").getValue().equals(recovery.get("id").getValue())) continue;
			current = previous.updateWith(recovery);
			String row = convert(recovery, current, previous);
			if (row != null) {
				euringRows.add(row);
			}
			previous = current;
		}
		return euringRows;
	}

	private Row resolveDeadRowToUse(RingHistory ringHistory) {
		Row onlyOneDead = null;
		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.birdDead(recovery)) {
				String recoveryMethod = recovery.get("recoveryMethod").getValue();
				if (!BAD_DEAD_RECOVERY_METHODS.contains(recoveryMethod)) {
					onlyOneDead = recovery;
					break;
				}
			}
		}
		if (onlyOneDead == null) {
			for (Row recovery : ringHistory.getRecoveries()) {
				if (RowUtils.birdDead(recovery)) {
					onlyOneDead = recovery;
					break;
				}
			}
		}
		return onlyOneDead;
	}

	private String convert(Row row, RingsAttachedInformation current, RingsAttachedInformation previous) throws Exception {
		int speciesProtectionLevel = speciesProtectionLevel(row);
		EuringRow r = new EuringRow();
		r.append("SFH");
		r.append(identificatioMethod(row, current));
		r.append(formatRing(row));
		r.append(metalRingVerification(row));
		r.append(metalRingInformation(row, current));
		r.append(otherMarksInformation(row, current, previous));
		r.append(speciesNumber(row.get("species").getValue()));
		if (row.get("ringedSpeciesAccuracy").getValue().equals("E")) {
			r.append(null);
		} else {
			r.append(speciesNumber(row.get("ringedSpecies").getValue()));
		}
		r.append(manipulated(row));
		r.append(movedBeforeEncounter(row));
		r.append(catchingMethod(row));
		r.append(catchingLures(row));
		r.append(sexAsReported(row));
		r.append("U"); // sex as determined by scheme
		r.append(ageAsReported(row));
		r.append(ageAsReported(row)); // age as detarmined by scheme
		r.append(status(row));
		r.append(clutchSize(row));
		r.append(pullusAge(row));
		r.append(accuracyOfPullusAge(row));
		r.append(eventDate(row));
		r.append(eventDateAccuracy(row));
		r.append(time(row));
		r.append(placeCode(row));
		boolean hasCoordinates = appendCoordinatesAndAccuracy(r, row, speciesProtectionLevel);
		if (!hasCoordinates) return null; // skip entire record
		r.append(condition(row));
		r.append(circumstances(row));
		r.append(circumstancesPresumed(row));
		r.append("3");
		r.append(distance(row));
		r.append(direction(row));
		r.append(timeToRinging(row));
		r.append(row.get("wingLengthInMillimeters").getValue());
		r.append(row.get("thirdFeatherLengthInMillimeters").getValue());
		r.append(stateOfWingPoint(row));
		r.append(row.get("weightInGrams").getValue());
		r.append(moult(row));
		r.append("U");
		r.append(row.get("nailLength").getValue());
		r.append(billLength(row));
		r.append(billLengthMethod(row));
		r.append(row.get("headBeakLength").getValue());
		r.append(tarsus(row));
		r.append(tarsusMethod(row));
		r.append(row.get("tailLength").getValue());
		r.append(row.get("hirrusTailNotchDeepnessInMillimeters").getValue());
		r.append(fat(row));
		r.append(fatMethod(row));
		r.append(muscle(row));
		r.append(broodPatch(row));
		r.append(null);
		r.append(primaryMoult(row));
		r.append(null);
		r.append(null);
		r.append(null);
		r.append(sexDeterminationMethod(row));
		r.append(localeString(row, speciesProtectionLevel));
		r.append(notes(row, speciesProtectionLevel));
		r.append(internalId(row));
		r.append(lat(row, speciesProtectionLevel));
		r.append(lon(row, speciesProtectionLevel));
		r.append(placeCode(row));
		r.append(null);
		return r.toString();
	}

	private String internalId(Row row) {
		return "SFH"+row.get("id").getValue();
	}

	private String notes(Row row, int speciesProtectionLevel) {
		if (speciesProtectionLevel > 0) return "";
		return row.get("notes").getValue().replace("\n", "").replace("\r", "");
	}

	private String localeString(Row row, int speciesProtectionLevel) {
		if (speciesProtectionLevel >= 2) return "";
		String municipalityCode = row.get("municipality").getValue();
		String municipalityName = "";
		String locality = row.get("locality").getValue();
		if (given(municipalityCode)) {
			municipalityName = municipalities.getById(municipalityCode).getNode("name").getContents();
		}
		if (speciesProtectionLevel > 0) {
			return municipalityName;
		}
		if (given(municipalityName) && given(locality)) {
			return municipalityName + ", " + locality;
		}
		if (given(municipalityName)) {
			return municipalityName;
		}
		return locality;
	}

	private String sexDeterminationMethod(Row row) {
		if ("U".equals(sexAsReported(row))) return null;
		String sexDeterminationMethod = row.get("sexDeterminationMethod").getValue();
		if (!given(sexDeterminationMethod)) return "U";
		if (sexDeterminationMethod.equals("L")) return "A";
		if (sexDeterminationMethod.equals("H")) return "B";
		if (sexDeterminationMethod.equals("P")) return "C";
		if (sexDeterminationMethod.equals("V")) return "P";
		if (sexDeterminationMethod.equals("K")) return "S";
		return "U";
	}

	private String primaryMoult(Row row) {
		String prim1Moult = row.get("prim1Moult").getValue();
		String prim2Moult = row.get("prim2Moult").getValue();
		String prim3Moult = row.get("prim3Moult").getValue();
		String prim4Moult = row.get("prim4Moult").getValue();
		String prim5Moult = row.get("prim5Moult").getValue();
		String prim6Moult = row.get("prim6Moult").getValue();
		String prim7Moult = row.get("prim7Moult").getValue();
		String prim8Moult = row.get("prim8Moult").getValue();
		String prim9Moult = row.get("prim9Moult").getValue();
		String prim10Moult = row.get("prim10Moult").getValue();
		if (!given(prim1Moult, prim2Moult, prim3Moult, prim4Moult, prim5Moult, prim6Moult, prim7Moult, prim8Moult, prim9Moult, prim10Moult)) return null;
		return catenade(prim1Moult, prim2Moult, prim3Moult, prim4Moult, prim5Moult, prim6Moult, prim7Moult, prim8Moult, prim9Moult, prim10Moult);
	}

	private String catenade(String ...strings) {
		StringBuilder b = new StringBuilder();
		for (String s : strings) {
			b.append(s);
		}
		return b.toString();
	}

	private String broodPatch(Row row) {
		String broodPatch = row.get("broodPatch").getValue();
		String hirrusBroodPatch = row.get("hirrusBroodPatch").getValue();
		if (given(broodPatch)) return broodPatch;
		if (hirrusBroodPatch.equals("0")) return "0";
		if (oneOf("12", hirrusBroodPatch)) return "P";
		return null;
	}

	private String muscle(Row row) {
		String muscleFitness = row.get("muscleFitness").getValue();
		String v = null;
		if (given(muscleFitness)) v = muscleFitness;
		if (v == null) return null;
		return Long.toString(Math.round(Double.valueOf(v)));
	}

	private String fatMethod(Row row) {
		String fat = row.get("fat").getValue();
		if (!given(fat)) return null;
		return "P";
	}

	private String fat(Row row) {
		String fat = row.get("fat").getValue();
		if (!given(fat)) return null;
		Double fatD = Double.valueOf(fat);
		return Long.toString(Math.round(fatD));
	}

	private String tarsusMethod(Row row) {
		String tarsusLengthToes = row.get("tarsusLengthToes").getValue();
		String tarsusLengthLastScale = row.get("tarsusLengthLastScale").getValue();
		if (given(tarsusLengthToes)) return "M";
		if (given(tarsusLengthLastScale)) return "S";
		return null;
	}

	private String tarsus(Row row) {
		String tarsusLengthToes = row.get("tarsusLengthToes").getValue();
		String tarsusLengthLastScale = row.get("tarsusLengthLastScale").getValue();
		if (given(tarsusLengthToes)) return tarsusLengthToes;
		if (given(tarsusLengthLastScale)) return tarsusLengthLastScale;
		return null;
	}

	private String billLengthMethod(Row row) {
		String beakLengthFeathers = row.get("beakLengthFeathers").getValue();
		String beakLengthNostril = row.get("beakLengthNostril").getValue();
		String beakLengthSkull = row.get("beakLengthSkull").getValue();
		if (given(beakLengthFeathers)) return "F";
		if (given(beakLengthNostril)) return "N";
		if (given(beakLengthSkull)) return "S";
		return null;
	}

	private String billLength(Row row) {
		String beakLengthFeathers = row.get("beakLengthFeathers").getValue();
		String beakLengthNostril = row.get("beakLengthNostril").getValue();
		String beakLengthSkull = row.get("beakLengthSkull").getValue();
		if (given(beakLengthFeathers)) return beakLengthFeathers;
		if (given(beakLengthNostril)) return beakLengthNostril;
		if (given(beakLengthSkull)) return beakLengthSkull;
		return null;
	}

	private String moult(Row row) {
		String moulting = row.get("moulting").getValue();
		String hirrusBodyMoulting = row.get("hirrusBodyMoulting").getValue();
		String bodyFeathersMoultIntensity = row.get("bodyFeathersMoultIntensity").getValue();
		String bodyFeathersMoultProgress = row.get("bodyFeathersMoultProgress").getValue();
		String moultStatus = row.get("moultStatus").getValue();
		String age = row.get("age").getValue();

		if (bodyFeathersMoultProgress.equals("J")) return "J"; // J wholly Juvenile plumage
		if (moultStatus.equals("1") || moulting.equals("S")) return "M"; // M active Moult which includes flight feathers (primary and/or secondaries)
		if (oneOf("02", moultStatus) && !bodyFeathersMoultProgress.equals("J")) return "X"; // X Examined for moult, no moult found and not in juvenile plumage.
		if (oneOf("12", bodyFeathersMoultIntensity) || oneOf("12", hirrusBodyMoulting)) {
			if (age.equals("FL") || age.equals("1")) {
				return "P"; // P partial Post-juvenile moult which does not include flight feathers.
			}
		}
		return "U"; // U Unknown, not examined for moult or not recorded
	}

	private String stateOfWingPoint(Row row) {
		String wingWear = row.get("wingWear").getValue();
		String wingLengthAbnormalityReason = row.get("wingWear").getValue();
		if (!given(wingWear) && !given(wingLengthAbnormalityReason)) return null;
		if (wingWear.equals("2")) return "A"; // A Abraded
		if (wingLengthAbnormalityReason.equals("P")) return "B"; // B Broken
		if (oneOf("KS", wingLengthAbnormalityReason)) return "G"; // G Growing
		if (wingWear.equals("3")) return "V"; // V Very abraded
		return null;
	}

	private String timeToRinging(Row row) {
		if (ringing(row)) return "-----";
		String days = row.get("timeToRingingInDays").getValue();
		if (!given(days)) return "-----";
		if (days.length() > 5) throw new IllegalStateException("timeToRinging " + days);
		days = addFrontZeros(days, 5);
		return days;
	}

	private String direction(Row row) {
		if (ringing(row)) return "---";
		String direction = row.get("directionToRingingInDegrees").getValue();
		if (!given(direction)) return "---";
		if (direction.length() > 3) throw new IllegalStateException("directionToRingingInDegrees " + direction);
		direction = addFrontZeros(direction, 3);
		return direction;
	}

	private String distance(Row row) {
		if (ringing(row)) return "-----";
		String distance = row.get("distanceToRingingInKilometers").getValue();
		if (!given(distance)) return "00000";
		if (distance.length() > 5) return "99999";
		distance = addFrontZeros(distance, 5);
		return distance;
	}

	private String circumstancesPresumed(Row row) {
		if (ringing(row)) return "0";
		String recoveryMethodAccuracy = row.get("recoveryMethodAccuracy").getValue();
		if (!given(recoveryMethodAccuracy)) return "0";
		if (oneOf("02", recoveryMethodAccuracy)) return "0";
		if (recoveryMethodAccuracy.equals("1")) return "1";
		throw new UnsupportedOperationException("Unresolvable circumstances presumed " + row);
	}

	private String circumstances(Row row) {
		if (ringing(row)) return "20";
		return addFrontZeros(row.get("recoveryMethod").getValue(), 2);
	}

	private String condition(Row row) {
		if (ringing(row)) return "8";
		return row.get("birdConditionEURING").getValue();
	}

	private boolean notInteger(String coordinateAccuracy) {
		try {
			Integer.valueOf(coordinateAccuracy);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	private boolean appendCoordinatesAndAccuracy(EuringRow r, Row row, int speciesProtectionLevel) {
		if (hasCoordinates(row)) {
			r.append("...............");
			r.append(coordinateAccuracy(row, speciesProtectionLevel));
			return true;
		}
		return false;
	}

	private boolean hasCoordinates(Row row) {
		return row.get("wgs84DecimalLat").hasValue() && row.get("wgs84DecimalLon").hasValue();
	}

	private String lat(Row row, int speciesProtectionLevel) {
		return coord(row.get("wgs84DecimalLat"), speciesProtectionLevel);
	}

	private String lon(Row row, int speciesProtectionLevel) {
		return coord(row.get("wgs84DecimalLon"), speciesProtectionLevel);
	}

	private String coord(Column c, int speciesProtectionLevel) {
		if (speciesProtectionLevel == 2) return ""+Utils.round(c.getDoubleValue(), 0);
		if (speciesProtectionLevel == 1) return ""+Utils.round(c.getDoubleValue(), 1);
		return ""+Utils.round(c.getDoubleValue(), 4);
	}

	private String coordinateAccuracy(Row row, int speciesProtectionLevel) {
		String coordinateAccuracy = row.get("coordinateAccuracy").getValue();
		if (!given(coordinateAccuracy)) return "9";
		if (notInteger(coordinateAccuracy)) return "9";
		int accuracy = Integer.valueOf(coordinateAccuracy);
		if (accuracy <= 100000 && speciesProtectionLevel != 0) {
			if (speciesProtectionLevel == 2) return "5";
			if (speciesProtectionLevel == 1) return "2";
		}
		if (accuracy < 5000) return "0";
		if (accuracy == 5000) return "1";
		if (accuracy <= 10000) return "2";
		if (accuracy <= 20000) return "3";
		if (accuracy <= 50000) return "4";
		if (accuracy <= 100000) return "5";
		if (accuracy <= 500000) return "6";
		if (accuracy <= 1000000) return "7";
		return "9";
	}

	private int speciesProtectionLevel(Row row) {
		String ringerSpecies = row.get("ringedSpecies").getValue();
		if (!given(ringerSpecies)) ringerSpecies = row.get("species").getValue();
		if (!given(ringerSpecies)) return 0;
		String protectionStatus = species.getById(ringerSpecies).getAttribute("protection-status");
		if (!given(protectionStatus)) return 0;
		return Integer.valueOf(protectionStatus);
	}

	private String placeCode(Row row) {
		String code = row.get("euringProvinceCode").getValue();
		if (!given(code)) return "SF--";
		return code;
	}

	private String time(Row row) {
		String hour = row.get("hour").getValue();
		String minute = row.get("minute").getValue();
		if (!given(hour) || hour.equals("-1")) hour = "--";
		if (!given(minute) || minute.equals("-1")) minute = "--";
		return addFrontZeros(hour, 2) + addFrontZeros(minute, 2);
	}

	private String eventDateAccuracy(Row row) {
		String eventDateAccuracy = row.get("eventDateAccuracy").getValue();
		if (!given(eventDateAccuracy)) return "0";
		if (eventDateAccuracy.equals("-")) return "9";
		return eventDateAccuracy;
	}

	private String eventDate(Row row) throws ParseException {
		String date = row.get("eventDate").getValue();
		if (!given(date)) return null;
		return DateUtils.format(DateUtils.convertToDate(date), "ddMMyyyy");
	}

	private String accuracyOfPullusAge(Row row) {
		if (!ageIsPull(row)) return "-";
		String ageOfYoungsAccuracy = row.get("ageOfYoungsAccuracy").getValue();
		if (!given(ageOfYoungsAccuracy)) return "U";
		if (ageOfYoungsAccuracy.equals("T")) return "0";
		if (ageOfYoungsAccuracy.equals("Y")) return "1";
		if (ageOfYoungsAccuracy.equals("N")) return "4";
		if (ageOfYoungsAccuracy.equals("S")) return "7";
		throw new UnsupportedOperationException("Unresolvable accuracy of pullus age " + row);
	}

	private String pullusAge(Row row) {
		if (!ageIsPull(row)) return "--";
		String ageOfYoungsInDays = row.get("ageOfYoungsInDays").getValue();
		if (!given(ageOfYoungsInDays)) return "99";
		if (ageOfYoungsInDays.length() > 2) return "99";
		if (ageOfYoungsInDays.equals("-1")) return "99";
		return addFrontZeros(ageOfYoungsInDays, 2);
	}

	private String clutchSize(Row row) {
		if (!ageIsPull(row)) return "--";
		String numberOfYoungs = row.get("numberOfYoungs").getValue();
		if (!given(numberOfYoungs)) return "00";
		return addFrontZeros(numberOfYoungs, 2);
	}

	private static String addFrontZeros(String s, int desiredLength) {
		while (s.length() < desiredLength) {
			s = "0" + s;
		}
		return s;
	}

	private String status(Row row) {
		if (ageIsPull(row)) return "-"; // - bird a pullus (one ‘hyphen’)
		String birdActivities = row.get("birdActivities").getValue();
		if (!given(birdActivities)) return "U"; // U Unknown or unrecorded.
		if (birdActivities.equals("P")) return "N"; // N Nesting or Breeding.
		if (birdActivities.equals("Y")) return "R"; // R Roosting assemblage.
		if (birdActivities.equals("K")) return "K"; // K In Colony (not necessarily breeding but not pullus).
		if (birdActivities.equals("S")) return "M"; // M Moulting assemblage (whether bird moulting or not).
		if (row.get("moulting").getValue().equals("S")) return "T"; // T MoulTing.
		if (birdActivities.equals("E")) return "L"; // L Apparently a Local bird, but not breeding.
		if (birdActivities.equals("T")) return "W"; // W Apparently a bird Wintering in the locality.
		if (birdActivities.equals("O")) return "P"; // P On Passage - certainly not a local breeding nor wintering bird (includes birds atracted to lighthouses).
		if (birdActivities.equals("M")) return "S"; // S At Sea - birds on boats, lightships or oil rigs.
		throw new UnsupportedOperationException("Unresolvable status " + row);
	}

	private String ageAsReported(Row row) {
		String age = row.get("age").getValue();
		if (!given(age)) return "0";
		if (ageIsPull(row)) return "1";
		String v = AGE_MAP.get(age);
		if (given(v)) return v;
		return "Z";
	}

	private String sexAsReported(Row row) {
		String sex = row.get("sex").getValue();
		if (sex.equals("K")) return "M";
		if (sex.equals("N")) return "F";
		return "U";
	}

	private String catchingLures(Row row) {
		// U = Unknown
		// - = Not applicable
		if (ringing(row)) return "U";
		if (ringerFinding(row)) return "U";
		return "-";
	}

	private boolean ringerFinding(Row row) {
		return row.get("birdConditionEURING").getValue().equals("8");
	}

	private String catchingMethod(Row row) {
		String captureMethod = row.get("captureMethod").getValue();
		String recoveryMethod = row.get("recoveryMethod").getValue();

		// - (one ‘hyphen’) not applicable, because there was no catching at all (for example ‘found’ or ‘found dead’ or ‘shot’).
		// Field sightings of colour-marked birds or metal rings should be coded with the hyphen but given Circumstances code 28 or 80 - 89.
		if (!given(captureMethod)) {
			if (controlledByRingerFromAbroad(row, recoveryMethod)) {
				return "Z";
			}
			return "-";
		}
		if (finding(row) && dead(row)) return "-";
		if (oneOf("RT", captureMethod)) return "-";
		if (given(recoveryMethod)) {
			int method = Integer.valueOf(recoveryMethod);
			if (method == 28) return "-";
			if (method >= 80 && method <= 89) return "-";
		}

		if (oneOf("ZX", captureMethod)) return "F"; // F caught in Flight by anything other than a static mist net (e.g. flicked) over it
		if (captureMethod.equals("H")) return "H"; // H by Hand (with or without hook, noose etc.)
		if (oneOf("VWLABC", captureMethod)) return "M"; // M Mist net
		if (captureMethod.equals("P")) return "N"; // N on Nest (any method)
		if (ageIsPull(row)) return "N"; // N on Nest (any method)
		if (oneOf("SMI", captureMethod)) return "O"; // O any Other system
		if (captureMethod.equals("F")) return "T"; // T Helgoland Trap or duck decoy
		if (captureMethod.equals("Y")) return "V"; // V roosting in caVity
		if (captureMethod.equals("K")) return "W"; // W passive Walk-in / maze trap

		return "Z";
	}

	private boolean controlledByRingerFromAbroad(Row row, String recoveryMethod) {
		return row.get("birdConditionEURING").getValue().equals("8") && recoveryMethod.equals("20") && !row.get("euringProvinceCode").getValue().startsWith("SF");
	}

	private boolean ageIsPull(Row row) {
		return row.get("age").getValue().startsWith("P");
	}

	private boolean given(String ... strings) {
		for (String s : strings) {
			if (!given(s)) return false;
		}
		return true;
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private boolean dead(Row row) {
		return oneOf("123", row.get("birdConditionEURING").getValue());
	}

	private boolean finding(Row row) {
		return row.get("sourceOfRecovery").getValue().equals("2");
	}

	private String movedBeforeEncounter(Row row) {
		Column birdMoved = row.get("birdMoved");
		Column birdCondition = row.get("birdCondition");
		if (!birdMoved.hasValue() && !oneOf("SA", birdCondition.getValue())) {
			return "0"; // 0 Not moved (excluding short movements, usually on foot, from catching place to ringing station).
		}
		if (birdMoved.getValue().equals("S")) return "2"; // 2 Moved unintentionally by man or other agency.
		if (birdMoved.getValue().equals("T") || oneOf("SA", birdCondition.getValue())) {
			return "4"; // 4 Moved intentionally by man.
		}
		if (birdMoved.getValue().equals("V")) return "6"; // 6 Moved by water (e.g. found on shoreline).
		throw new UnsupportedOperationException("Unresolvable moved before encounter " + row);
		// 9 Uncoded or unknown if moved or not.
	}

	private String manipulated(Row row) {
		Column birdCondition = row.get("birdCondition");
		if (!birdCondition.hasValue()) return "N"; // N Normal, not manipulated bird.
		if (birdCondition.getValue().equals("E")) return "H"; // H Hand reared.
		if (oneOf("VA", birdCondition.getValue())) return "C"; // C Captive for more than 24 hours (code date of release).
		if (birdCondition.getValue().equals("S")) {
			if (ringing(row)) {
				return "T";  // T Transported (more than 10 km) to co-ordinates coded.
			}
			return "F"; // F Transported (more than 10 km) from co-ordinates coded.
		}
		if (oneOf("MK", birdCondition.getValue())) return "M"; // M Manipulated (injection, biopsy, radio- or satellite telemetry etc.).
		if (oneOf("RD", birdCondition.getValue())) return "R"; // R Ringing accident.
		if (birdCondition.getValue().equals("H")) return "P"; // P Poor condition when caught.
		return "U"; // U Uncoded or unknown if manipulated or not.
	}

	private String speciesNumber(String speciesCode) throws Exception {
		if (!given(speciesCode)) return "00000";
		Node info = species.getById(speciesCode);
		if (info == null) throw new UnsupportedOperationException("Uknown species " + speciesCode);
		int number = Integer.valueOf(info.getAttribute("euring-code"));
		return addFrontZeros(Integer.toString(number), 5);
	}

	private String otherMarksInformation(Row row, RingsAttachedInformation current, RingsAttachedInformation previous) {
		if (current.hasSomeOtherMarks()) return "OP"; // Some other marks
		if (!previous.hasAdditionalFieldReadable() && !current.hasAdditionalFieldReadable())  return "ZZ"; // Not present

		if (!current.hasAdditionalFieldReadable() && previous.hasAdditionalFieldReadable()) {
			String firstChar = otherMarksCode(previous, row);
			return firstChar + "D";
		}

		String firstChar = otherMarksCode(current, row);
		String secondChar = otherMarksChanges(row, current, previous);
		return firstChar + secondChar;
	}

	private String otherMarksCode(RingsAttachedInformation current, Row row) {
		// B Coloured or numbered leg-ring(s) or flags.
		if (current.hasFieldRedableLegRing()) return "B";

		// C Coloured or numbered neck-ring(s).
		if (current.hasNeckRing()) return "C";

		// D Coloured or numbered wingtag(s).
		if (current.hasAdditionalWingTags()) return "D";

		throw new UnsupportedOperationException("Unresolvable other marks code for " + debug(row));
	}

	private String otherMarksChanges(Row row, RingsAttachedInformation current, RingsAttachedInformation previous) {
		Column fieldReadableChanges = row.get("fieldReadableChanges");
		if (fieldReadableChanges.hasValue()) {
			String v = fieldReadableChanges.getValue();
			if (v.equals("L")) return "B"; // B mark added.
			if (v.equals("V")) return "E"; // E mark changed.
			if (v.equals("P")) return "D"; // D mark removed.
			throw new UnsupportedOperationException("Unrespolvable fieldreadable change code " + v);
		}
		if (!previous.hasAdditionalFieldReadable() && current.hasAdditionalFieldReadable()) {
			return "B"; // B mark added.
		}
		if (previous.hasAdditionalFieldReadable() && !current.hasAdditionalFieldReadable()) {
			return "D"; // D mark removed.
		}
		if (previous.hasAdditionalFieldReadable() && current.hasAdditionalFieldReadable()) {
			if (previous.getCurrentFieldReadableCode().equals(current.getCurrentFieldReadableCode()) &&
					previous.getCurrentFieldReadableColor().equals(current.getCurrentFieldReadableColor())) {
				return "C"; // C mark already present.
			}
			return "E"; // E mark changed.
		}
		throw new UnsupportedOperationException("Unresolvable other marks changes for " + debug(row));
	}

	private String debug(Row row) {
		return "event id " + row.get("id").getValue() + " (diario " + row.get("diario").getValue()+ ")";
	}

	private String metalRingInformation(Row row, RingsAttachedInformation current) {
		// 0 Metal ring is not present.
		if (current.hasPrimaryVirtualRing()) {
			if (current.hasAdditionalFieldReadable()) return "0";
			throw new UnsupportedOperationException("Metal ring information can not be resolved for " + row);
		}

		String ringsAttached = row.get("ringsAttached").getValue();
		if (ringing(row)) {
			// 1 Metal ring added (where no metal ring was present), position (on tarsus or above) unknown or unrecorded.
			if (current.hasPrimaryWingTag() || ringsAttached.equals("R")) return "1";

			// 3 Metal ring added (where no metal ring was present), definitely above tarsus.
			if (ringsAttached.equals("Y")) return "3";

			// 2 Metal ring added (where no metal ring was present), definitely on tarsus.
			return "2";

		}

		// 6 Metal ring removed and bird released alive
		if (virtualRing(row.get("newLegRing"))) return "6"; // Metal ring REMOVED


		// 		5 Metal ring changed.
		if (current.legRingHasChanged(row)) {
			return "5";
		}

		// 4 Metal ring is already present.
		return "4";
	}

	private boolean virtualRing(Column ring) {
		return ring.getValue().startsWith("VR");
	}

	private boolean ringing(Row row) {
		return row.get("type").getValue().equals("ringing");
	}

	private String metalRingVerification(Row row) {
		if (ringing(row)) return "0";
		if (virtualRing(row.get("nameRing"))) return "0";
		Column ringVerification = row.get("ringVerification");
		if (oneOf("KLMRT", ringVerification.getValue())) return "1";
		return "0";
	}

	public static String formatRing(String originalRing) {
		originalRing = Utils.removeWhitespace(originalRing);
		StringBuilder b = new StringBuilder();
		boolean dotsAdded = false;
		for (char c : originalRing.toCharArray()) {
			if ((!Character.isAlphabetic(c) && c != '+') && !dotsAdded) {
				addDots(10 - originalRing.length(), b);
				dotsAdded = true;
			}
			b.append(c);
		}
		return b.toString();
	}

	public String formatRing(Row row) {
		return formatRing(row.get("nameRing").getValue());
	}

	private static void addDots(int count, StringBuilder b) {
		for (int i = 0; i<count; i++) {
			b.append(".");
		}
	}

	private String identificatioMethod(Row row, RingsAttachedInformation current) {
		if (!current.hasPrimaryVirtualRing()) {
			return "A0"; // Normal metal ring
		}
		if (current.hasFieldRedableLegRing) {
			return "B0"; // coloured leg ring
		}
		if (current.hasNeckRing) {
			return "C0"; // neck ring
		}
		if (current.hasAdditionalWingTags) {
			return "D0"; // wing tags
		}
		throw new UnsupportedOperationException("Unresolvable identification method " + row);
	}

	private static class EuringRow {
		private static final String SEPARATOR_CHAR = "|";
		private final StringBuilder b = new StringBuilder();
		public EuringRow append(String s) {
			if (s != null && s.length() > 0) {
				b.append(s);
			}
			b.append(SEPARATOR_CHAR);
			return this;
		}
		@Override
		public String toString() {
			Utils.removeLastChar(b);
			return b.toString();
		}
	}

	private static Map<String, String> initAgeMap() {
		Map<String, String> map = new HashMap<>();
		map.put("FL", "2");
		map.put("1", "3");
		map.put("+1", "4");
		map.put("2", "5");
		map.put("+2", "6");
		map.put("3", "7");
		map.put("+3", "8");
		map.put("4", "9");
		map.put("+4", "A");
		map.put("5", "B");
		map.put("+5", "C");
		map.put("6", "D");
		map.put("+6", "E");
		map.put("7", "F");
		map.put("+7", "G");
		map.put("8", "H");
		map.put("+8", "J");
		map.put("9", "K");
		map.put("+9", "L");
		map.put("10", "M");
		map.put("+10", "N");
		map.put("11", "O");
		map.put("+11", "P");
		map.put("12", "Q");
		map.put("+12", "R");
		map.put("13", "S");
		map.put("+13", "T");
		map.put("14", "U");
		map.put("+14", "V");
		map.put("15", "W");
		map.put("+15", "X");
		map.put("16", "Y");
		return map;
	}

	public static class RingsAttachedInformation {
		private final Ring nameRing;
		private Ring currentLegRing;
		private String currentFieldReadableCode = "";
		private String currentFieldReadableColor = "";
		private boolean hasFieldRedableLegRing = false;
		private boolean hasNeckRing = false;
		private boolean hasAdditionalWingTags = false;
		private boolean hasSomeOtherMarks = false;

		private RingsAttachedInformation() {
			this.nameRing = new Ring("");
		}
		public RingsAttachedInformation(RingsAttachedInformation source) {
			this.nameRing = source.getNameRing();
			this.currentLegRing = source.getCurrentLegRing();
			this.currentFieldReadableCode = source.getCurrentFieldReadableCode();
			this.currentFieldReadableColor = source.getCurrentFieldReadableColor();
			this.hasFieldRedableLegRing = source.hasFieldRedableLegRing;
			this.hasNeckRing = source.hasNeckRing;
			this.hasAdditionalWingTags = source.hasAdditionalWingTags;
			this.hasSomeOtherMarks = source.hasSomeOtherMarks;
		}
		public RingsAttachedInformation(Row ringing) {
			this.nameRing = new Ring(ringing.get("nameRing").getValue());
			this.currentLegRing = nameRing;
			this.currentFieldReadableCode = ringing.get("fieldReadableCode").getValue();
			this.currentFieldReadableColor = ringing.get("mainColor").getValue();
			String fieldReadableAttachmentPoint = ringing.get("attachmentPoint").getValue();
			String ringsAttached = ringing.get("ringsAttached").getValue();
			if (ringsAttached.equals("M")) {
				hasSomeOtherMarks = true;
			} else if (given(fieldReadableAttachmentPoint)) {
				resolveWhatAttachedFromAttachmentPoint(fieldReadableAttachmentPoint);
			} else {
				if (this.hasPrimaryVirtualRing()) {
					if (ringing.get("species").getValue().startsWith("ANSFAB")) {
						hasNeckRing = true;
					} else {
						hasFieldRedableLegRing = true;
					}
				}
			}
		}
		private void resolveWhatAttachedFromAttachmentPoint(String fieldReadableAttachmentPoint) {
			if (oneOf("AYJ", fieldReadableAttachmentPoint)) {
				hasFieldRedableLegRing = true;
			} else if (fieldReadableAttachmentPoint.equals("S")) {
				hasAdditionalWingTags = true;
			} else if (fieldReadableAttachmentPoint.equals("K")) {
				hasNeckRing = true;
			} else {
				throw new UnsupportedOperationException("Unresolvable fieldreadable attachment point " + fieldReadableAttachmentPoint);
			}
		}
		public RingsAttachedInformation updateWith(Row recovery) {
			RingsAttachedInformation copy = new RingsAttachedInformation(this);
			update(recovery, copy);
			return copy;
		}
		private void update(Row recovery, RingsAttachedInformation copy) {
			copy.updateCurrentLegRing(recovery);
			copy.updateFieldReadable(recovery);
		}
		private void updateFieldReadable(Row recovery) {
			if (recovery.get("fieldReadableChanges").getValue().equals("P")) {
				fieldReadableRemoved();
				return;
			}
			String reportedFieldReadableCode = recovery.get("fieldReadableCode").getValue();
			String reportedFieldReadableColor = recovery.get("mainColor").getValue();
			String reportedRemovedFieldReadableCode = recovery.get("removedFieldReadableCode").getValue();
			if (given(reportedRemovedFieldReadableCode) && !given(reportedFieldReadableCode)) {
				fieldReadableRemoved();
				return;
			}
			if (given(reportedFieldReadableCode)) {
				if (hasChanged(reportedFieldReadableCode, reportedFieldReadableColor)) {
					fieldReadableChanged(recovery, reportedFieldReadableCode, reportedFieldReadableColor);
				}
			}
		}
		private void fieldReadableRemoved() {
			this.currentFieldReadableCode = "";
			this.currentFieldReadableColor = "";
			this.hasFieldRedableLegRing = false;
			this.hasNeckRing = false;
			this.hasAdditionalWingTags = false;
		}
		private boolean hasChanged(String reportedFieldReadableCode, String reportedFieldReadableColor) {
			return !reportedFieldReadableCode.equals(currentFieldReadableCode) || !reportedFieldReadableColor.equals(currentFieldReadableColor);
		}
		private void fieldReadableChanged(Row recovery, String reportedFieldReadableCode, String reportedFieldReadableColor) {
			this.currentFieldReadableCode = reportedFieldReadableCode;
			this.currentFieldReadableColor = reportedFieldReadableColor;
			String fieldReadableAttachmentPoint = recovery.get("attachmentPoint").getValue();
			String ringsAttached = recovery.get("ringsAttached").getValue();
			if (ringsAttached.equals("M")) {
				hasSomeOtherMarks = true;
			}
			if (given(fieldReadableAttachmentPoint)) {
				this.hasFieldRedableLegRing = false;
				this.hasNeckRing = false;
				this.hasAdditionalWingTags = false;
				resolveWhatAttachedFromAttachmentPoint(fieldReadableAttachmentPoint);
			} else {
				// Maintain old attachment point
			}
		}
		private void updateCurrentLegRing(Row recovery) {
			String reportedLegRing = recovery.get("legRing").getValue();
			String reportedNewLegRing = recovery.get("newLegRing").getValue();
			if (given(reportedNewLegRing)) {
				this.currentLegRing = new Ring(reportedNewLegRing);
			} else {
				if (given(reportedLegRing)) {
					Ring legRing = new Ring(reportedLegRing);
					if (!legRing.equals(this.currentLegRing)) {
						this.currentLegRing = legRing;
					}
				}
			}
		}
		public boolean legRingHasChanged(Row recovery) {
			if (recovery.get("newLegRing").hasValue()) return true;
			String reportedLegRing = recovery.get("legRing").getValue();
			if (given(reportedLegRing)) {
				Ring legRing = new Ring(reportedLegRing);
				return !legRing.equals(this.currentLegRing);
			}
			return false;
		}
		private boolean given(String s) {
			return s != null && s.length() > 0;
		}
		public boolean hasPrimaryVirtualRing() {
			return nameRing.getSeries().equals("VR");
		}
		public boolean hasPrimaryWingTag() {
			return nameRing.getSeries().equals("W");
		}
		public Ring getCurrentLegRing() {
			return currentLegRing;
		}
		public void setCurrentLegRing(Ring currentLegRing) {
			this.currentLegRing = currentLegRing;
		}
		public String getCurrentFieldReadableCode() {
			return currentFieldReadableCode;
		}
		public void setCurrentFieldReadableCode(String currentlFieldReadableCode) {
			this.currentFieldReadableCode = currentlFieldReadableCode;
		}
		public String getCurrentFieldReadableColor() {
			return currentFieldReadableColor;
		}
		public void setCurrentFieldReadableColor(String currentFieldReadableColor) {
			this.currentFieldReadableColor = currentFieldReadableColor;
		}
		public Ring getNameRing() {
			return nameRing;
		}
		public boolean hasFieldRedableLegRing() {
			return hasFieldRedableLegRing;
		}
		public boolean hasNeckRing() {
			return hasNeckRing;
		}
		public boolean hasAdditionalWingTags() {
			return hasAdditionalWingTags;
		}
		public void setHasFieldRedableLegRing(boolean hasFieldRedableLegRing) {
			this.hasFieldRedableLegRing = hasFieldRedableLegRing;
		}
		public void setHasNeckRing(boolean hasNeckRing) {
			this.hasNeckRing = hasNeckRing;
		}
		public void setHasAdditionalWingTags(boolean hasWingTags) {
			this.hasAdditionalWingTags = hasWingTags;
		}
		public boolean hasAdditionalFieldReadable() {
			return hasFieldRedableLegRing || hasNeckRing || hasAdditionalWingTags || hasSomeOtherMarks;
		}
		@Override
		public String toString() {
			return Utils.debugS(nameRing, currentLegRing, currentFieldReadableCode, currentFieldReadableColor, hasFieldRedableLegRing, hasNeckRing, hasAdditionalWingTags);
		}
		public boolean hasSomeOtherMarks() {
			return hasSomeOtherMarks;
		}
		public void setHasSomeOtherMarks(boolean hasSomeOtherMarks) {
			this.hasSomeOtherMarks = hasSomeOtherMarks;
		}
		public static RingsAttachedInformation empty() {
			return new RingsAttachedInformation();
		}
	}

	public static boolean oneOf(String values, String code) {
		if (code.isEmpty()) return false;
		return values.contains(code);
	}


}
