package fi.hy.tipuapi.service.ringing.models;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;

public class Column {

	private static final String SEARCHPARAM = "searchparam_";
	private final Field field;
	private final Map<String, String> values;

	public Column(Field field, Map<String, String> values) {
		this.field = field;
		this.values = values;
	}

	@Override
	public String toString() {
		return field.getName();
	}

	public Field getFieldDescription() {
		return field;
	}

	public Column setSearchParams(List<String> parameters) {
		Iterator<String> i = parameters.iterator();
		StringBuilder value = new StringBuilder();
		while (i.hasNext()) {
			value.append(i.next());
			if (i.hasNext()) {
				value.append("|");
			}
		}
		return setSearchParam(value.toString());
	}

	public Column setSearchParam(String value) {
		if (value == null) return this;
		value = value.trim();
		if (!given(value)) return this;

		String paramMapKey = searchParamMapKey();
		if (values.containsKey(paramMapKey)) {
			values.put(paramMapKey, values.get(paramMapKey) + "|" + value);
		} else {
			values.put(paramMapKey, value);
		}
		return this;
	}

	public void clearSearchParameter() {
		values.remove(searchParamMapKey());
	}

	private String searchParamMapKey() {
		return SEARCHPARAM + field.getName();
	}

	public String getValue() {
		String value = values.get(field.getName());
		if (value == null) return "";
		return value;
	}

	public boolean hasSearchParameterSet() {
		String value = values.get(searchParamMapKey());
		return given(value); 
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

	public SearchParameter getSearchParameter() {
		return new SearchParameter(values.get(searchParamMapKey()));
	}

	public void setValue(String value) {
		if (value != null) {
			value = value.trim().replaceAll("\\s+", " ");
			if (getFieldDescription().isDecimal()) {
				value = value.replace(",", ".");
			}
		}
		values.put(field.getName(), value);
	}

	public void setValue(int value) {
		setValue(Integer.toString(value));
	}

	public void setValue(double value) {
		setValue(Double.toString(value));
	}

	public String getName() {
		return field.getName();
	}

	public boolean hasValue() {
		String value = getValue();
		return value != null && value.length() > 0;
	}

	public List<FieldValidation> getFieldValidationRules() {
		return field.getValidationRules();
	}

	public int getIntValue() {
		return Integer.valueOf(getValue());
	}

	public DateValue getDateValue() {
		return DateUtils.convertToDateValue(this.getValue());
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Compare to value not to Column!");
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException();
	}

	public Double getDoubleValue() {
		return Double.valueOf(getValue());
	}

	public String getDatabaseColumn() {
		return getFieldDescription().getDatabaseColumn();
	}

	public boolean isAggregated() {
		return getFieldDescription().isAggregated();
	}

	public boolean isUpperCaseColumn() {
		return field.isUpperCaseColumn();
	}

}
