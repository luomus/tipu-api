package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MarkRecoveriesForLetterPrinting extends BaseServlet {

	private static final long serialVersionUID = 3265809151608463460L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception { 
		return processGet(req, res);
	}
	
	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception { 
		return processGet(req, res);
	}
	
	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception { 
		log(req);
		
		List<Integer> recoveryIds = null;
		try {
			recoveryIds = getIds(req);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		}

		DAO dao = null;
		try {
			dao = createNewDAO(req);
			dao.startTransaction();
			dao.markRecoveriesForLetterPrintingUsingDiarios(recoveryIds);
			dao.commitTransaction();
			JSONObject response = new JSONObject();
			response.setBoolean("success", true);
			return print(response, res);
		} catch (Exception e) {
			if (dao != null) dao.rollbackTransaction();
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}

}
