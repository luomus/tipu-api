package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class RangeValidator implements FieldValidation {

	private final double min;
	private final double max;

	public RangeValidator(double min, double max) {
		this.min = min;
		this.max = max;
	}

	public RangeValidator(int min, int max) {
		this.min = min;
		this.max = max;
	}

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		validate(data, column, column.getValue());
	}

	public void validate(RowValidationData data, Column column, String value) throws Exception {
		if (!data.notNullNoErrors(column)) return;
		double dValue = Double.valueOf(value);
		if (dValue < min) {
			data.addError("too_small_value", column);
		}
		if (dValue > max) {
			data.addError("too_large_value", column);
		}
	}

}
