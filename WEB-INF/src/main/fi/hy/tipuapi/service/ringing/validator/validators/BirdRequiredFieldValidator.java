package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class BirdRequiredFieldValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (!data.isForUserValidations()) return;
		
		Column species = data.getRow().get("species");
		if (!species.hasValue() || data.hasErrors(species)) return;

		if (!RowUtils.isBatEvent(data.getRow(), data.getDao())) {
			data.require(column);
		}
	}

}
