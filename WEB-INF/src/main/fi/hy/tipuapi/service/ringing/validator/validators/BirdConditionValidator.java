package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class BirdConditionValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column birdCondition) throws Exception {
		if ("D".equals(birdCondition.getValue())) {
			if (data.getMode() == Mode.RECOVERIES) {
				if (isControl(data)) {
					data.addError("bird_condition_D_control", birdCondition);
				}
			}
		}
	}

	private boolean isControl(RowValidationData data) {
		return !data.getRow().get("sourceOfRecovery").getValue().equals(DAO.RECOVERY_TYPE_FINDING);
	}

}
