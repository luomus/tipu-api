package fi.hy.tipuapi.service.ringing.validator.validators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.FileUtils;

public class NumberOfYoungsValidator implements FieldValidation {

	private static Map<String, Integer> maxValues = null;

	public static Map<String, Integer> getMaxValues(Config config) throws FileNotFoundException, IOException {
		if (maxValues != null) return maxValues;
		maxValues = new HashMap<>();
		File file = new File(new File(config.templateFolder()), "pesimisen_raja-arvoja.txt");
		for (String line : FileUtils.readLines(file)) {
			if (line.trim().length() < 1) continue;
			String[] data = line.split(":");
			String laji = data[0].trim().toUpperCase();
			int max = Integer.valueOf(data[1].trim());
			maxValues.put(laji, max);
		}
		return maxValues;
	}

	@Override
	public void validate(RowValidationData data, Column numberOfYoungs) throws Exception {
		if (!data.notNullNoErrors(numberOfYoungs)) return;
		
		Column species = data.getRow().get("species");
		if (!data.notNullNoErrors(species)) return;
		
		Integer max = getMaxValues(data.getConfig()).get(species.getValue());
		if (max == null) return;
		
		if (numberOfYoungs.getIntValue() > max) {
			data.addWarning("young-count-species-max-warning", numberOfYoungs);
		}
	}

}
