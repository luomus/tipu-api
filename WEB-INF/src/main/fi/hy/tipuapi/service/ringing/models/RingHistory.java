package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class RingHistory implements Iterable<Row> {

	private final Row ringing;
	private final List<Row> recoveries = new ArrayList<>();

	public RingHistory(List<Row> rows) {
		Iterator<Row> i = rows.iterator();
		if (!i.hasNext()) {
			this.ringing = null;
			return;
		}
		
		Row ringing = i.next();
		if (notRingingEvent(ringing)) {
			throw new IllegalStateException("First row was not a ringing event.:" + ringing.toString());
		}
		this.ringing = ringing;

		while (i.hasNext()) {
			Row recovery = i.next();
			if (notRecoveryEvent(recovery)) {
				throw new IllegalStateException("Expected only recoveries after first ringing event -- there were more than one ringing! " + recovery);
			}
			this.recoveries.add(recovery);
		}
	}

	public RingHistory() {
		this.ringing = null;
	}

	private boolean notRecoveryEvent(Row recovery) {
		return !recovery.get("type").getValue().equals("recovery");
	}

	private boolean notRingingEvent(Row ringing) {
		return !ringing.get("type").getValue().equals("ringing");
	}

	public Row getRinging() {
		return ringing;
	}

	public List<Row> getRecoveries() {
		return recoveries;
	}

	public boolean hasHistory() {
		return ringing != null;
	}

	public boolean hasRecoveries() {
		return !this.recoveries.isEmpty();
	}

	@Override
	public String toString() {
		if (!hasHistory()) return "no history";
		StringBuilder b = new StringBuilder();
		b.append("ringing: ").append(ringing.toString()).append("\n");
		for (Row recovery : recoveries) {
			b.append("recovery: ").append(recovery.toString()).append("\n");
		}
		return b.toString();
	}

	public List<Row> getAll() {
		if (!hasHistory()) return Collections.emptyList();
		List<Row> combined = new ArrayList<>(this.recoveries.size() + 1);
		combined.add(ringing);
		combined.addAll(this.recoveries);
		return combined;
	}

	@Override
	public Iterator<Row> iterator() {
		return getAll().iterator();
	}
	
}
