package fi.hy.tipuapi.service.ringing.r99;

public class InvalidDataException extends RuntimeException {

	private static final long serialVersionUID = -1821659489140073206L;

	public InvalidDataException(String cause) {
		super(cause);
	}
	
}
