package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class RangeWarningValidator implements FieldValidation {

	private final double min;
	private final double max;
	
	public RangeWarningValidator(double min, double max) {
		this.min = min;
		this.max = max;
	}

	public RangeWarningValidator(int min, int max) {
		this.min = min;
		this.max = max;
	}

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (!data.notNullNoErrors(column)) return;
		double dValue = Double.valueOf(column.getValue()); 
		if (dValue < min) {
			data.addWarning("too_small_value", column);
		} 
		if (dValue > max) {
			data.addWarning("too_large_value", column);
		}
	}
	
}
