package fi.hy.tipuapi.service.ringing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.DistributedFieldReadable;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.hy.tipuapi.service.ringing.models.FieldReadable;
import fi.hy.tipuapi.service.ringing.models.RingBatches;
import fi.hy.tipuapi.service.ringing.models.RingBatches.RingBatch;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document.Node;

public class DistributedAndUsedRings extends BaseServlet {

	private static final long serialVersionUID = -2069834310574490482L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		DAO dao = null;
		try {
			int ringerNumber = getRingerNumber(req);
			dao = createNewDAO(req);
			JSONObject response = new JSONObject();
			response.setBoolean("success", true);
			response.setInteger("ringer", ringerNumber);
			response.setArray("distributedAndUsedRings", generateDistributedAndUsedRings(dao, ringerNumber));
			response.setArray("distributedAndUsedFieldReadables", generateDistributedAndUsedFieldReadables(dao, ringerNumber));
			return print(response, res);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private JSONArray generateDistributedAndUsedFieldReadables(DAO dao, int ringerNumber) throws Exception {
		JSONArray array = new JSONArray();
		DistributedFieldReadable distributed = dao.getDistributedFieldReadables(ringerNumber);
		if (distributed.isEmpty()) return array;

		Set<FieldReadable> used = dao.getRingersUsedFieldReadables(ringerNumber);

		for (String species : distributed.getSpecies()) {
			array.appendObject(generateFieldReadableJSON(species, distributed, used));
		}
		return array;
	}

	private JSONObject generateFieldReadableJSON(String species, DistributedFieldReadable distributed, Set<FieldReadable> used) {
		JSONObject fieldReadablesBySpecies = new JSONObject();
		fieldReadablesBySpecies.setString("species", species);
		JSONArray fieldReadablesArray = new JSONArray();
		fieldReadablesBySpecies.setArray("fieldReadables", fieldReadablesArray);
		for (FieldReadable fieldReadable : distributed.getBySpecies(species)) {
			fieldReadablesArray.appendObject(generateFieldReadableJSON(fieldReadable, used.contains(fieldReadable)));
		}
		return fieldReadablesBySpecies;
	}

	private JSONObject generateFieldReadableJSON(FieldReadable fieldReadable, boolean used) {
		JSONObject batch = new JSONObject();
		batch.setString("code", fieldReadable.getCode());
		batch.setString("backgroundColor", fieldReadable.getBackgroundColor());
		batch.setBoolean("used", used);
		return batch;
	}

	private JSONArray generateDistributedAndUsedRings(DAO dao, int ringerNumber) throws Exception {
		JSONArray array = new JSONArray();
		DistributedRings distributedRings = dao.getDistributedRings();
		RingBatches ringersDistributedRings = distributedRings.getRingersRings(ringerNumber);
		if (ringersDistributedRings == null) return array;

		RingBatches ringersUsedRings = dao.getRingersUsedRings(ringerNumber);

		String birdStationCode = getBirdStationCode(dao, ringerNumber);
		if (given(birdStationCode)) {
			RingBatches birdStationUsedRings = dao.getBirdStationUsedRings(birdStationCode);
			if (ringersUsedRings == null) {
				ringersUsedRings = birdStationUsedRings;
			} else {
				ringersUsedRings.addAll(birdStationUsedRings);
			}
		}

		RingBatches explodedBatches = ringersUsedRings == null ? ringersDistributedRings : ringersDistributedRings.explodeBy(ringersUsedRings);

		for (String serie : ringersDistributedRings.getSortedSeries()) {
			array.appendObject(generateMetalRingJSON(serie, explodedBatches));
		}
		return array;
	}

	private String getBirdStationCode(DAO dao, int ringerNumber) throws Exception {
		TipuApiResource birdStations = dao.getTipuApiResource(TipuAPIClient.BIRD_STATIONS);
		for (Node station : birdStations.getAll()) {
			String ringerFakeNumber = station.getAttribute("ringer-number");
			if (Integer.valueOf(ringerFakeNumber).equals(ringerNumber)) {
				return station.getNode("id").getContents();
			}
		}
		return null;
	}

	private JSONObject generateMetalRingJSON(String serie, RingBatches explodedBatches) {
		JSONObject seriesBatches = new JSONObject();
		seriesBatches.setString("series", serieToPrint(serie));


		Map<DateValue, List<RingBatch>> batchesByDate = new TreeMap<>(new DateValueComparator());
		for (RingBatch ringBatch : explodedBatches.getBatches(serie)) {
			DateValue distributionDate = ringBatch.getDistribution().getDistributionDate();
			if (!batchesByDate.containsKey(distributionDate)) {
				batchesByDate.put(distributionDate, new ArrayList<RingBatch>());
			}
			batchesByDate.get(distributionDate).add(ringBatch);
		}

		JSONArray distributionDates = new JSONArray();
		seriesBatches.setArray("distributionsByDate", distributionDates);

		for (DateValue distributionDate : batchesByDate.keySet()) {
			JSONObject distributionsByDate = new JSONObject();
			distributionsByDate.setString("date", distributionDate.toString());
			distributionDates.appendObject(distributionsByDate);

			JSONArray batches = new JSONArray();
			distributionsByDate.setArray("batches", batches);
			for (RingBatch ringBatch : batchesByDate.get(distributionDate)) {
				batches.appendObject(generateMetalRingJSON(ringBatch.start(), ringBatch.end(), ringBatch.isUsed()));
			}
		}
		return seriesBatches;
	}

	private static class DateValueComparator implements Comparator<DateValue> {

		@Override
		public int compare(DateValue d1, DateValue d2) {
			return new Long(DateUtils.getEpoch(d1)).compareTo(new Long(DateUtils.getEpoch(d2)));
		}

	}

	private String serieToPrint(String serie) {
		if (serie.endsWith("L") && !serie.equals("ML")) {
			return "___" + serie.substring(0, 1) + " (" + serie + ")";
		}
		return serie + " ___";
	}

	private JSONObject generateMetalRingJSON(int firstOfBatch, int lastOfBatch, boolean currentBatchContainsUsedRings) {
		JSONObject batch = new JSONObject();
		batch.setInteger("start", firstOfBatch);
		batch.setInteger("end", lastOfBatch);
		batch.setBoolean("used", currentBatchContainsUsedRings);
		return batch;
	}

	private int getRingerNumber(HttpServletRequest req) {
		String ringerNumber = req.getParameter("ringer");
		if (!given(ringerNumber)) {
			throw new IllegalArgumentException("ringer parameter must be given.");
		}
		try {
			return Integer.valueOf(ringerNumber);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("ringer number must be numeric.");
		}
	}

}
