package fi.hy.tipuapi.service.ringing.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class DatasourceDefinition {

	public static HikariDataSource initDataSource(Config tipuConfig) {
		ConnectionDescription desc = tipuConfig.connectionDescription();
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());

		config.setAutoCommit(false); // transaction mode: all changes must be committed

		config.setConnectionTimeout(5*60000); // 5 minutes -- the longest possible query
		config.setMaximumPoolSize(30);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(1800000); // 30 mins

		return new HikariDataSource(config);
	}

}
