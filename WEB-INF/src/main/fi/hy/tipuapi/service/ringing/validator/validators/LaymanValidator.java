package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class LaymanValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column laymanId) throws Exception {
		if (!laymanId.hasValue()) return;
		if (!data.getDao().laymanExists(laymanId.getValue())) {
			data.addError("ADMIN_laymanId_does_not_exist", laymanId);
		}
	}

}
