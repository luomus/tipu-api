package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class RequiredFieldValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (RingLostDestroyedValidator.ringMarkedLostOrDestroyed(data)) return;
		data.require(column);
	}

}
