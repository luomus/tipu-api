package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NameRingsRecoveryIDs {

	private final Map<String, List<String>> map = new HashMap<>();
	
	public void addRecoveryIdOfNameRing(String nameRing, String recoveryId) {
		if (!map.containsKey(nameRing)) {
			map.put(nameRing, new ArrayList<String>());
		}
		map.get(nameRing).add(recoveryId);
	}
	
	public Collection<String> getNameRings() {
		return map.keySet();
	}
	
	public Collection<String> getRecoveryIDs(String nameRing) {
		return map.get(nameRing);
	}
	
}
