package fi.hy.tipuapi.service.ringing.models;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.EnumerationValues.EnumerationValue;

public class EnumerationValues implements Iterable<EnumerationValue> {

	public static class EnumerationValue {

		private final String value;
		private final Map<String, String> descriptions = new HashMap<>(3);
		private final Map<String, String> detailedDescriptions = new HashMap<>(3);
		
		public EnumerationValue(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public String getDescription(String locale) {
			return getDescFromMap(descriptions, locale);
		}

		public String getDetailedDescription(String locale) {
			return getDescFromMap(detailedDescriptions, locale);
		}
		
		private String getDescFromMap(Map<String, String> descriptionsMap, String locale) {
			String text = descriptionsMap.get(locale);
			if (given(text)) {
				return text;
			}
			if (locale.equals("en") || locale.equals("sv")) {
				text = descriptionsMap.get("la");
				if (given(text)) return text;
			}
			return "";
		}

		private boolean given(String text) {
			return text != null && text.length() > 0;
		}

		public void setDescription(String text) {
			setToMap(descriptions, text);
			setToMap(detailedDescriptions, text);
		}

		private void setToMap(Map<String, String> descriptionsMap, String text) {
			descriptionsMap.put("fi", text);
			descriptionsMap.put("en", text);
			descriptionsMap.put("sv", text);
		}

		public void setDescription(String text, String locale) {
			descriptions.put(locale, text);
			detailedDescriptions.put(locale, text);
		}

		public void setDetailedDescription(String text, String locale) {
			detailedDescriptions.put(locale, text);
		}

	}

	private final Map<String, EnumerationValue> enumerationValues = new LinkedHashMap<>();

	public EnumerationValues() {

	}

	public EnumerationValues(int ... values) {
		for (int value : values) {
			enumerationValues.put(Integer.toString(value), new EnumerationValue(Integer.toString(value)));
		}
	}

	public EnumerationValues(String ... values) {
		for (String value : values) {
			enumerationValues.put(value, new EnumerationValue(value));
		}
	}

	@Override
	public Iterator<EnumerationValue> iterator() {
		return enumerationValues.values().iterator();
	}

	public void add(EnumerationValue enumerationValue) {
		enumerationValues.put(enumerationValue.getValue(), enumerationValue);
	}

	public boolean contains(String value) {
		return enumerationValues.containsKey(value);
	}

}
