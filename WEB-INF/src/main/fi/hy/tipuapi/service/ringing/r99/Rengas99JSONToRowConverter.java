package fi.hy.tipuapi.service.ringing.r99;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;

public class Rengas99JSONToRowConverter {

	private static final Set<String> TENTH_FIELDS = Utils.set("thirdFeatherLengthInMillimeters", "weightInGrams","hirrusTailNotchDeepnessInMillimeters", "hirrusLongestMiddleRectrixLengthInMillimeters", "hirrusOutmostLeftRectrixLengthInMillimeters", "hirrusOutmostRightRectrixLengthInMillimeters");
	
	private final DAO dao;
	
	public Rengas99JSONToRowConverter(DAO dao) {
		this.dao = dao;
	}

	public List<Row> convert(List<JSONObject> rowsAsJson, Mode mode) throws Exception {
		List<Row> rows = new LinkedList<>();
		for (JSONObject json : rowsAsJson) {
			rows.add(convert(json, mode));
		}
		return rows;
	}

	public Row convert(JSONObject json, Mode mode) throws Exception {
		Row row = dao.emptyRow();
		for (String key : json.getKeys()) {
			if (!row.hasField(key)) {
				throw new InvalidDataException("No such field: " + key);
			}
			Column c = row.get(key);
			if (!c.getFieldDescription().hasMode(mode)) {
				throw new InvalidDataException("Field " + key + " is not of the correct mode : " + mode.toString());
			}
			addValue(json, row, key);
		}
		if (ykj(json)) {
			row.get("coordinateSystem").setValue("YKJ");
			row.get("coordinateAccuracy").setValue("100");
		} else if (row.get("lat").hasValue() && row.get("lon").hasValue()) {
			row.get("coordinateSystem").setValue("WGS84-DMS");
			row.get("coordinateAccuracy").setValue("1000");
			String lat = row.get("lat").getValue() + "00";
			String lon = row.get("lon").getValue() + "00";
			row.get("lat").setValue(lat);
			row.get("lon").setValue(lon);
		}
		Column ringStart = row.get("ringStart");
		Column ringEnd = row.get("ringEnd");
		if (ringStart.getValue().equals(ringEnd.getValue())) {
			ringEnd.setValue("");
		}
		return row;
	}

	private void addValue(JSONObject json, Row row, String fieldName) throws Exception {
		String value = json.getString(fieldName);
		if (!given(value)) return;
		
		if (fieldName.equals("municipality")) {
			TipuApiResource birdStations = dao.getTipuApiResource(TipuAPIClient.BIRD_STATIONS);
			if (birdStations.containsUnitById(value)) {
				row.get("birdStation").setValue(value);
				String municipality = birdStations.getById(value).getNode("municipality").getContents();
				row.get("municipality").setValue(municipality);
				return;
			}
		}
		
		if (fieldName.equals("lon") && ykj(json)) {
			value = "3" + value;
		}
		
		if (TENTH_FIELDS.contains(fieldName)) {
			value = Double.toString((Double.valueOf(value) / 10.0));
		}
		
		row.get(fieldName).setValue(value);
	}

	private boolean ykj(JSONObject json) {
		return json.getString("lat").length() == 5;
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

}
