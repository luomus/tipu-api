package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DAO.LetterType;
import fi.hy.tipuapi.service.ringing.models.NameRingsRecoveryIDs;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LettersNotYetPrinted extends BaseServlet {

	private static final long serialVersionUID = 3265809151608463460L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		JSONObject response = null;
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			response = new JSONObject();
			NameRingsRecoveryIDs data = pickRecoveries(dao, req);
			response.setBoolean("success", true);
			JSONObject nameRings = response.getObject("nameRings");
			for (String nameRing : data.getNameRings()) {
				JSONArray recoveryIDs = nameRings.getArray(nameRing);
				for (String recoveryId : data.getRecoveryIDs(nameRing)) {
					recoveryIDs.appendString(recoveryId);
				}
			}
		} catch (Exception e) {
			getErrorReporter().report(e);
			response = generateExceptionResponse(e);
		} finally {
			if (dao != null) dao.close();
		}
		return print(response, res);
	}

	protected NameRingsRecoveryIDs pickRecoveries(DAO dao, HttpServletRequest req) throws Exception {
		LetterType desiredLetterType = getDesiredLetterType(req);
		NameRingsRecoveryIDs data = dao.lettersNotYetPrinted(desiredLetterType);
		return data;
	}

	private LetterType getDesiredLetterType(HttpServletRequest req) {
		String param = req.getParameter("letterType");
		if (!given(param)) return LetterType.ALL;

		if ("onlyFindingLetters".equals(param)) {
			return LetterType.ONLY_FINDING_LETTERS;
		} 
		if ("onlyControlLetters".equals(param)) {
			return LetterType.ONLY_CONTROL_LETTERS;
		}
		throw new IllegalArgumentException("Unknown letter type: " + param);
	}

}
