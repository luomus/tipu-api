package fi.hy.tipuapi.service.ringing;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.hy.tipuapi.service.ringing.validator.validators.UpdateDeleteValidator;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;

public class DeleteRow extends BaseServlet {

	private static final long serialVersionUID = 4136361770306167334L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		int id = Integer.valueOf(getResourceId(req));
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			JSONObject response = new JSONObject();
			boolean okToDelete = validateDelete(id, response, dao, req);
			if (okToDelete) {
				dao.delete(id);
				getWarehousingDAO(); // make sure thread is running
				response.setBoolean("success", true);
			} else {
				response.setBoolean("success", false);
			}
			return print(response, res);
		} catch (Exception e) {
			JSONObject response = new JSONObject();
			response.setBoolean("success", false);
			String stackTrace = LogUtils.buildStackTrace(e, 7);
			response.setString("error", stackTrace);
			return print(response, res);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private boolean validateDelete(int eventId, JSONObject response, DAO dao, HttpServletRequest req) throws Exception {
		Map<String, String> localizedTexts = getLocalizedTexts().getAllTexts(getLocale(req));

		Row row = null;
		try {
			row = dao.getRowByEventId(eventId);
		} catch (Exception e) {
			response.setString("error", "nothing found with id " + eventId);
			return false;
		}
		Mode mode = row.get("type").getValue().equals("ringing") ? Mode.RINGINGS : Mode.RECOVERIES;

		RowValidationData data = new RowValidationData(row, mode, DAO.Action.DELETE, getConfig(), getErrorReporter(), dao, localizedTexts);
		new UpdateDeleteValidator().validate(data, row.get("id"));

		if (data.hasErrors()) {
			response.setString("error", data.getErrors().toString());
			return false;
		}
		return true;
	}
}
