package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.List;

import fi.hy.tipuapi.service.ringing.TimeDistanceDirection;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class LegRingForRecoveryValidator implements FieldValidation {

	private static final int SECONDS_IN_DAY = 86400;

	@Override
	public void validate(RowValidationData data, Column legRing) throws Exception {
		if (data.getMode() != Mode.RECOVERIES) return;

		Column fieldReadableCode = data.getRow().get("fieldReadableCode");
		Column fieldReadableChanges = data.getRow().get("fieldReadableChanges");

		if (fieldReadableChanges.hasValue()) {
			if (!legRing.hasValue()) {
				data.addError("metal_leg_ring_must_be_given", legRing);
				return;
			}
		}

		if (!legRing.hasValue() && !fieldReadableCode.hasValue()) {
			data.addError("metal_ring_or_field_readable_must_be_given", legRing);
			data.addError("metal_ring_or_field_readable_must_be_given", fieldReadableCode);
			return;
		}

		if (data.notNullNoErrors(legRing)) {
			if (!new Ring(legRing.getValue()).validFormat()) {
				data.addError("not-valid-ring-format", legRing);
				return;
			}

			RingHistory ringHistory = data.getDao().getRingHistoryByLegRing(legRing.getValue());
			if (data.isForAdminValidations()) {
				adminValidateLegRingIsInBirdsLegAndSpeciesOkAndBirdNotDeadAndTimeDistance(data, new Ring(legRing.getValue()), legRing, ringHistory);
			}

			checkForDuplicateRecoveries(data, ringHistory);


		}

		// If no legRing, validations in fieldReadableCodeValidator
	}

	private void checkForDuplicateRecoveries(RowValidationData data, RingHistory ringHistory) {
		if (hasDuplicateRecovery(ringHistory, data.getRow())) {
			data.addWarning("dublicate_recovery_exists", data.getRow().get("legRing"));
		}
	}

	private boolean hasDuplicateRecovery(RingHistory ringHistory, Row thisRow) {
		Column eventDate = thisRow.get("eventDate");
		Column hour = thisRow.get("hour");
		Column ringer = thisRow.get("ringer");
		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.sameEvent(recovery, thisRow)) continue;
			if (ringer.hasValue() && recovery.get("ringer").hasValue()) {
				if (!ringer.getValue().equals(recovery.get("ringer").getValue())) continue;
			}
			if (!eventDate.getDateValue().equals(recovery.get("eventDate").getDateValue())) continue;
			if (hour.hasValue() && recovery.get("hour").hasValue()) {
				if (!hour.getValue().equals(recovery.get("hour").getValue())) continue;
			}
			return true;
		}
		return false;
	}

	protected void adminValidateLegRingIsInBirdsLegAndSpeciesOkAndBirdNotDeadAndTimeDistance(RowValidationData data, Ring legRing, Column ringColumn, RingHistory ringHistory) throws Exception {
		Column eventDate = data.getRow().get("eventDate");
		if (!data.notNullNoErrors(eventDate)) return;

		if (!data.getDao().ringDistributedToSomeone(legRing, eventDate.getDateValue())) {
			data.addError("ring-not-distributed-at-the-date", ringColumn);
			return;
		}

		validateRingInBirdsLeg(data, legRing, ringColumn, eventDate, ringHistory);

		if (!data.getRow().get("speciesAccuracy").getValue().equals("E")) {
			validateSpecies(data, ringColumn, ringHistory);
		}

		if (data.noErrors(ringColumn)) {
			validateSpeciesAge(data, ringHistory);
		}

		validateForBirdDead(data, ringHistory, eventDate);

		validateTimeDistance(data, ringHistory, eventDate);
	}

	private void validateTimeDistance(RowValidationData data, RingHistory ringHistory, Column eventDate) {
		if (!data.notNullNoErrors(data.getRow().get("lat"), data.getRow().get("lon"), data.getRow().get("coordinateSystem"))) return;

		Row previousEvent = null;
		for (Row row : ringHistory) {
			if (RowUtils.sameEvent(row, data.getRow())) continue;
			if (RowUtils.rowAfterEventDate(row, eventDate)) break;
			previousEvent = row;
		}
		if (previousEvent == null || !previousEvent.get("lat").hasValue() || !previousEvent.get("lon").hasValue() || !previousEvent.get("coordinateSystem").hasValue()) return;

		JSONObject tdd = TimeDistanceDirection.countTimeDistanceDirection(data.getRow(), previousEvent);
		int timeInDays = tdd.getInteger("timeInDays");
		if (timeInDays <= 0) timeInDays = 1;
		int distanceInKilometers = tdd.getInteger("distanceInKilometers");
		double kilometersPerDay = distanceInKilometers / timeInDays;
		if (kilometersPerDay > 200) {
			data.addWarning("ADMIN_kilometers_per_day_warning", eventDate, String.valueOf(Math.round(kilometersPerDay)));
		}
	}

	private void validateForBirdDead(RowValidationData data, RingHistory ringHistory, Column eventDate) {
		Row rowWhereReportedDead = getRowWhereReportedDead(ringHistory);
		if (rowWhereReportedDead == null) return;
		if (RowUtils.sameEvent(data.getRow(), rowWhereReportedDead)) return;

		Column birdCondition = data.getRow().get("birdCondition");
		Column birdConditionEuring = data.getRow().get("birdConditionEURING");

		if (RowUtils.birdDead(data.getRow())) {
			if (birdCondition.hasValue()) {
				data.addWarning("ADMIN_already_reported_dead_in_some_event", birdCondition);
			}
			if (birdConditionEuring.hasValue()) {
				data.addWarning("ADMIN_already_reported_dead_in_some_event", birdConditionEuring);
			}
		} else if (!RowUtils.rowAfterEventDate(rowWhereReportedDead, eventDate)) {
			if (birdCondition.hasValue()) {
				data.addWarning("ADMIN_reported_dead_before_this_event", birdCondition);
			}
			if (birdConditionEuring.hasValue()) {
				data.addWarning("ADMIN_reported_dead_before_this_event", birdConditionEuring);
			}
		}
	}

	private Row getRowWhereReportedDead(RingHistory ringHistory) {
		for (Row row : ringHistory) {
			if (RowUtils.birdDead(row)) {
				return row;
			}
		}
		return null;
	}

	private void validateSpeciesAge(RowValidationData data, RingHistory ringHistory) throws Exception {
		Column observedSpecies = data.getRow().get("species");
		Column ringedSpecies = data.getRow().get("ringedSpecies");
		Column eventDate = data.getRow().get("eventDate");

		if (!data.notNullNoErrors(eventDate)) return;
		if (data.hasWarning(observedSpecies, "ADMIN_warning_species_age")) return;
		if (data.hasWarning(ringedSpecies, "ADMIN_warning_species_age")) return;

		if (observedSpecies.getValue().equals(ringedSpecies.getValue())) {
			validateSpeciesAge(data, ringHistory, observedSpecies, eventDate);
		} else {
			validateSpeciesAge(data, ringHistory, observedSpecies, eventDate);
			validateSpeciesAge(data, ringHistory, ringedSpecies, eventDate);
		}
	}

	private void validateSpeciesAge(RowValidationData data, RingHistory ringHistory, Column species, Column eventDate) throws Exception {
		if (!species.hasValue()) return;
		DateValue ringingDate = ringHistory.getRinging().get("eventDate").getDateValue();
		String ringingAge = ringHistory.getRinging().get("age").getValue();
		DateValue recoveryDate = eventDate.getDateValue();

		int ageInDays = (int) ((DateUtils.getEpoch(recoveryDate) - DateUtils.getEpoch(ringingDate)) / SECONDS_IN_DAY);
		int ageOfBirdAtRinging = ageOfBirdAtRinging(ringingAge);
		ageInDays = ageInDays + ageOfBirdAtRinging;

		List<Integer> records = data.getDao().getTopAgeRecords(species.getValue());
		int threshold = getTenthHighestOrMaxValue(records);
		if (ageInDays > threshold) {
			data.addWarning("ADMIN_warning_species_age", species);
		}
	}

	private int getTenthHighestOrMaxValue(List<Integer> records) {
		int threshold = Integer.MAX_VALUE;
		int count = 0;
		for (int record : records) {
			if (count++ >= 10) break;
			threshold = record;
		}
		return threshold;
	}

	private int ageOfBirdAtRinging(String ringingAge) {
		if (!given(ringingAge)) return 0;
		ringingAge = ringingAge.replace("+", ""); // change  +7  to  7
		try {
			return Integer.valueOf(ringingAge) * 365;
		} catch (Exception e) {
			return 0;
		}
	}

	private void validateSpecies(RowValidationData data, Column ringColumn, RingHistory ringHistory) {
		if (!ringHistory.hasHistory()) return;
		String previousSpecies = speciesNotMarkedInaccurate(ringHistory.getRinging()) ? sixLength(ringHistory.getRinging().get("species")) : "";
		for (Row recovery : ringHistory.getRecoveries()) {
			Column recoverySpecies = recovery.get("species");
			if (recoverySpecies.hasValue() && speciesNotMarkedInaccurate(recovery)) {
				if (given(previousSpecies) && !previousSpecies.equals(sixLength(recoverySpecies))) {
					data.addWarning("ADMIN_species_history_mismatch", ringColumn);
					return;
				}
			}
			if (!given(previousSpecies) && recoverySpecies.hasValue()) {
				previousSpecies = sixLength(recoverySpecies);
			}
		}
		if (!given(previousSpecies)) {
			data.addWarning("ADMIN_species_history_mismatch", ringColumn);
			return;
		}
		Column eventSpecies = data.getRow().get("species");
		if (eventSpecies.hasValue() && !previousSpecies.equals(sixLength(eventSpecies))) {
			data.addWarning("ADMIN_recovery_species_mismatch", eventSpecies);
		}
	}

	protected boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private void validateRingInBirdsLeg(RowValidationData data, Ring legRing, Column ringColumn, Column eventDate, RingHistory ringHistory) {
		if (!ringHistory.hasHistory()) {
			data.addError("ADMIN_leg_ring_not_used", ringColumn);
			return;
		}

		if (RowUtils.rowAfterEventDate(ringHistory.getRinging(), eventDate)) {
			data.addError("ADMIN_reporting_recovery_before_ringing", ringColumn);
			return;
		}

		// If at the same date as the event date a ring change has occured, we accept both rings that
		// were on the birds leg that day. (Hence a list and not simply String ringAtBirdsLeg).
		// Multiple ring changes on same day are not allowed.
		List<String> ringsAtBirdsLegAtEventDate = getRingsAtBirdsLegAtEventDate(eventDate, ringHistory);
		boolean matchFound = matchFound(legRing, ringsAtBirdsLegAtEventDate);
		if (!matchFound) {
			data.addError("ring_not_in_birds_leg_at_the_date", ringColumn);
			return;
		}
	}

	private boolean matchFound(Ring legRing, List<String> ringsAtBirdsLegAtEventDate) {
		for (String ringAtLeg : ringsAtBirdsLegAtEventDate) {
			if (new Ring(ringAtLeg).equals(legRing)) {
				return true;
			}
		}
		return false;
	}

	private boolean speciesNotMarkedInaccurate(Row row) {
		return !row.get("speciesAccuracy").getValue().equals("E");
	}

	private String sixLength(Column species) {
		String value = species.getValue();
		if (value.length() > 6) {
			return value.substring(0, 6);
		}
		return value;
	}

	private List<String> getRingsAtBirdsLegAtEventDate(Column eventDate, RingHistory ringHistory) {
		String ringAtBirdsLeg = ringHistory.getRinging().get("legRing").getValue();
		if (!ringHistory.hasRecoveries()) {
			return Utils.list(ringAtBirdsLeg);
		}

		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.rowAfterEventDate(recovery, eventDate)) {
				return Utils.list(ringAtBirdsLeg);
			}
			Column newLegRing = recovery.get("newLegRing");
			if (newLegRing.hasValue()) {
				if (eventDate.getDateValue().equals(recovery.get("eventDate").getDateValue())) {
					return Utils.list(ringAtBirdsLeg, newLegRing.getValue());
				}
				ringAtBirdsLeg = newLegRing.getValue();
			}
		}
		return Utils.list(ringAtBirdsLeg);
	}

}
