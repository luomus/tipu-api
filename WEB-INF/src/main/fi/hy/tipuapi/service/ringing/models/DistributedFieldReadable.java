package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;

public class DistributedFieldReadable {

	private final Map<FieldReadable, DateValue> distributedRings = new TreeMap<>();
	private final Set<String> species;
	
	public DistributedFieldReadable(Comparator<String> speciesComparator) {
		this.species = new TreeSet<>(speciesComparator);
	}

	public void add(String code, String backgroundColor, String species, DateValue distributionDate) {
		FieldReadable fieldReadable = new FieldReadable(code, backgroundColor, species);
		distributedRings.put(fieldReadable, distributionDate);
		this.species.add(fieldReadable.getShortenedSpeciesCode());
	}

	public boolean isDistributedAtDate(String code, String backgroundColor, String species, DateValue eventDate) {
		FieldReadable fieldReadable = new FieldReadable(code, backgroundColor, species);
		if (!distributedRings.containsKey(fieldReadable)) return false;

		DateValue distributionDate = distributedRings.get(fieldReadable);
		long eventDateTimestamp = DateUtils.getEpoch(eventDate);
		long distributionDateTimestamp = DateUtils.getEpoch(distributionDate);
		return (eventDateTimestamp >= distributionDateTimestamp);
	}

	public boolean isEmpty() {
		return distributedRings.isEmpty();
	}

	public Set<String> getSpecies() {
		return species;
	}

	public List<FieldReadable> getBySpecies(String species) {
		List<FieldReadable> list = new ArrayList<>();
		for (FieldReadable fieldReadable : distributedRings.keySet()) {
			if (fieldReadable.getShortenedSpeciesCode().equals(species)) {
				list.add(fieldReadable);
			}
		}
		return list;
	}
	
}
