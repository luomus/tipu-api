package fi.hy.tipuapi.service.ringing.models;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class RowToLajiETLJSONConverter {

	private static final Qname COLLECTION_ID = new Qname("HR.48");
	private static final Qname SOURCE_ID = new Qname("KE.67");
	private static final Set<String> FACTS_TO_IGNORE = Utils.set(
			"schemeID", "personalPlaceName", "notes", "eventYear", "hiddenNotes",
			"clutchNumber", "someoneElsesClutchNumber", "clutchNumberOwner", "numberOfYoungs",
			"sourceOfRecovery", "fieldReadableCode", "removedFieldReadableCode", "fieldReadableNotes",
			"sicknessNotes", "injuriesNotes", "hirrusNotes", "eventDate", "coordinateAccuracy",
			"coordinateSystem", "lat", "lon", "country", "province", "municipality", "currentMunicipality",
			"finnishOldCounty", "finnishOldCountyCode", "finnishProvince", "elyCentreCode", "finnishProvinceCode",
			"elyCentre", "hour", "locality", "ringedSpecies", "species");

	private final Row row;
	private final TipuApiResource municipalities;
	private final TipuApiResource ringers;
	private final String secretSalt;

	public RowToLajiETLJSONConverter(Row row, TipuApiResource municipalities, TipuApiResource ringers, String secretSalt) {
		this.row = row;
		this.municipalities = municipalities;
		this.ringers = ringers;
		this.secretSalt = secretSalt;
	}

	public JSONObject toJson() throws Exception {
		DwRoot root = toModel();
		root.clearProcesstime();
		return ModelToJson.rootToJson(root);
	}

	private DwRoot toModel() throws Exception {
		DwRoot root = root();

		Document publicDocument = root.createPublicDocument();
		fillPublicInfo(publicDocument);

		Document privateDocument = publicDocument.copy(Concealment.PRIVATE);

		fillUnsecuredInfo(privateDocument);
		fillSecuredInfo(publicDocument);

		root.setPrivateDocument(privateDocument);

		return root;
	}

	public static Qname toQname(int eventId) {
		return Qname.fromURI(SOURCE_ID.toURI() + "/" + eventId);
	}

	private DwRoot root() throws CriticalParseFailure {
		int eventId = row.get("id").getIntValue();
		Qname documentId = toQname(eventId);
		DwRoot root = new DwRoot(documentId, SOURCE_ID);
		root.setCollectionId(COLLECTION_ID);
		return root;
	}

	private void fillSecuredInfo(Document publicDocument){
		if (publicDocument.getGatherings().isEmpty()) return; // Lost/destroyed ring
		Gathering gathering = publicDocument.getGatherings().get(0);

		if (row.get("ringer").hasValue()) {
			String ringer = row.get("ringer").getValue();
			addTeamMemberRinger(gathering, ringer, true);
		}
		clutchNumber(gathering, true);
	}

	private void fillUnsecuredInfo(Document privateDocument) {
		if (privateDocument.getGatherings().isEmpty()) return; // Lost/destroyed ring
		Gathering gathering = privateDocument.getGatherings().get(0);

		groupFieldsToFacts("persons", gathering);

		if (row.get("ringer").hasValue()) {
			String ringer = row.get("ringer").getValue();
			addTeamMemberRinger(gathering, ringer, false);
			addOwnerRinger(privateDocument, ringer);
		}
		if (row.get("ringingRinger").hasValue()) {
			String ringer = row.get("ringingRinger").getValue();
			addOwnerRinger(privateDocument, ringer);
		}
		if (row.get("intermediaryRinger").hasValue()) {
			String ringer = row.get("intermediaryRinger").getValue();
			addOwnerRinger(privateDocument, ringer);
		}
		clutchNumber(gathering, false);
	}

	private void clutchNumber(Gathering gathering, boolean secure) {
		if (row.get("clutchNumber").hasValue()) {
			String clutchNumber = row.get("clutchNumber").getValue();
			String clutchNumberOwner = row.get("clutchNumberOwner").getValue();
			gathering.addFact("clutchNumber", clutchNumber);
			if (secure) {
				gathering.addFact("clutchNumberOwner", secure(clutchNumberOwner));
			} else {
				gathering.addFact("clutchNumberOwner", clutchNumberOwner);
			}
		}
	}

	private void addOwnerRinger(Document document, String ringer) {
		document.addEditorUserId(toUserId(ringer));
	}

	private void addTeamMemberRinger(Gathering gathering, String ringer, boolean publicDocument) {
		if (!publicDocument) {
			gathering.addObserverUserId(toUserId(ringer));
		}
		String ringerName = getRingerName(ringer, publicDocument);
		gathering.addTeamMember(ringerName);
	}

	private String toUserId(String ringer) {
		return "lintuvaara:"+ringer;
	}

	private String getRingerName(String ringer, boolean publicDocument) {
		Node data = ringers.getById(ringer);
		if (data == null) return "Unknown";
		String firstName = data.getNode("firstname").getContents();
		String lastName = data.getNode("lastname").getContents();
		String name = given(firstName) ? firstName + " " : "";
		if (given(lastName)) name += lastName;
		if (publicDocument && !allowsNamePublish(data)) {
			return "Hidden";
		}
		return Utils.capitalizeName(name);
	}

	private boolean allowsNamePublish(Node data) {
		if (!data.hasAttribute("FinBIF-permission")) return false;
		return "1".equals(data.getAttribute("FinBIF-permission"));
	}

	private void fillPublicInfo(Document document) throws Exception {
		if (row.get("ringNotUsedReason").hasValue()) return; // Lost/destroyed ring
		Gathering gathering = new Gathering(Qname.fromURI(document.getDocumentId().toURI() + "#Gathering"));
		Unit unit = new Unit(Qname.fromURI(document.getDocumentId().toURI() + "#Unit"));
		document.addGathering(gathering);
		gathering.addUnit(unit);

		unit.setTaxonVerbatim(row.get("ringedSpecies").getValue());
		if ("E".equals(row.get("ringedSpeciesAccuracy").getValue())) {
			unit.setReportedTaxonConfidence(TaxonConfidence.UNSURE);
		}
		unit.setIndividualIdUsingQname(individualId());
		unit.addFact("ringVerification", row.get("ringVerification").getValue());
		unit.setRecordBasis(recordBasis());
		unit.setAbundanceString("1");
		unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
		unit.setSex(sex());
		unit.setLifeStage(lifeStage());
		unit.setWild(wild());
		unit.setAlive(!RowUtils.birdDead(row));

		Qname atlasCode = atlasCode();
		if (atlasCode != null) {
			unit.setAtlasCodeUsingQname(atlasCode);
			unit.setBreedingSite(true);
		} else {
			unit.setBreedingSite(nestsite());
		}

		diario(document);
		time(gathering);
		coordinates(gathering);
		places(gathering);
		layman(gathering);
		facts(gathering);
		facts(unit);

		gathering.setNotes(row.get("colonyNotes").getValue());
		createdModifiedDates(document);
	}

	private Boolean wild() {
		if (row.get("recoveryMethod").getValue().equals("21")) return false;
		return true;
	}

	private void createdModifiedDates(Document document) throws DateValidationException, ParseException {
		if (row.get("created").hasValue()) {
			document.setCreatedDate(DateUtils.convertToDate(row.get("created").getValue()));
		}
		if (row.get("modified").hasValue()) {
			document.setModifiedDate(DateUtils.convertToDate(row.get("modified").getValue()));
		}
	}

	private void layman(Gathering gathering) {
		if (row.get("layman").hasValue()) {
			gathering.addTeamMember(row.get("layman").getValue());
		}
	}

	private void diario(Document document) {
		if (row.get("diario").hasValue()) {
			document.addKeyword(row.get("diario").getValue());
		}
	}

	private boolean nestsite() {
		if (row.get("age").getValue().startsWith("P")) return true;
		if (row.get("captureMethod").getValue().equals("P")) return true;
		if (row.get("birdActivities").getValue().equals("P")) return true;
		if (row.get("batCapturePlace").hasValue() && !row.get("batCapturePlace").getValue().equals("L")) return true;
		if (row.groupHasValues("colony")) return true;
		return false;
	}

	private Qname atlasCode() {
		boolean ringing = row.get("type").getValue().equals("ringing");
		String age = row.get("age").getValue();
		String captureMethod = row.get("captureMethod").getValue();

		if (ringing) {
			if (age.equals("PP")) return new Qname("MY.atlasCodeEnum82");
			if (age.startsWith("P") && notWaterBird()) return new Qname("MY.atlasCodeEnum73");
		}

		if (!age.startsWith("P") && captureMethod.equals("P")) return new Qname("MY.atlasCodeEnum7");

		return null;
	}

	private static Set<String> WATERBIRDS = Utils.set("CYGOLO","CYGCOL","CYGCYG","ANSFAB","ANSBRA","ANSALB","ANSERY","ANSANS","ANSIND","BRACAN","BRAHUT","BRALEU","BRABER","BRARUF","TADFER","TADTAD","AIXGAL","ANAPEN","ANAAME","ANASTR","ANAFOR","ANACRE","ANACAR","ANAPLA","ANARUB","ANAACU","ANAQUE","ANADIS","ANACLY","NETRUF","AYTFER","AYTCOL","AYTNYR","AYTFUL","AYTMAR","AYTAFF","SOMMOL","SOMSPE","POLSTE","HISHIS","CLAHYE","MELNIG","MELAME","MELPER","MELFUS","MELDEG","BUCCLA","MERALB","MERSER","MERMER","OXYJAM","GAVSTE","GAVARC","GAVPAC","GAVIMM","GAVADA","TACRUF","PODCRI","PODGRI","PODAUR","PODNIG","GRUGRU");

	private boolean notWaterBird() {
		String species = row.get("ringedSpecies").getValue();
		return !WATERBIRDS.contains(species);
	}

	private LifeStage lifeStage() {
		String birdAge = row.get("age").getValue();
		String batAge = row.get("batAge").getValue();
		if (given(birdAge)) {
			if (birdAge.startsWith("P")) {
				return LifeStage.JUVENILE;
			}
			return LifeStage.ADULT;
		}
		if (given(batAge)) {
			if (batAge.equals("A")) {
				return LifeStage.ADULT;
			}
			return LifeStage.JUVENILE;
		}
		return null;
	}

	private Sex sex() {
		String sex = row.get("sex").getValue();
		if (given(sex)) {
			if ("NO".contains(sex)) return Sex.FEMALE;
			if ("KL".contains(sex)) return Sex.MALE;
		}
		return null;
	}

	private void places(Gathering gathering) {
		gathering.setCountry(row.get("country").getValue());
		gathering.setProvince(row.get("province").getValue()); // EURING province name
		if (row.get("currentMunicipality").hasValue()) {
			Node municipalityInfo = municipalities.getById(row.get("currentMunicipality").getValue());
			gathering.setMunicipality(municipalityInfo.getNode("name").getContents());
		}
		gathering.setLocality(row.get("locality").getValue());
	}

	private void coordinates(Gathering gathering) {
		String system = row.get("coordinateSystem").getValue();
		if (!given(system)) return;

		int coordinateAccuracyInMeters = 1;
		String coordinateAccuracy = row.get("coordinateAccuracy").getValue();
		if (given(coordinateAccuracy)) {
			if (coordinateAccuracy.equals("EPÄT")) {
				coordinateAccuracyInMeters = 1000000;
			} else {
				coordinateAccuracyInMeters = Integer.valueOf(coordinateAccuracy);
			}
		}

		String verbatim = null;
		String lat = null;
		String lon = null;
		Coordinates.Type type = null;
		if (system.startsWith("WGS84")) { // use wgs84 decimal for wgs84 and wgs84-dms
			lat = row.get("wgs84DecimalLat").getValue();
			lon = row.get("wgs84DecimalLon").getValue();
			type = Type.WGS84;
			verbatim = row.get("lat").getValue() + ", " + row.get("lon").getValue() + " " + system;
		} else {
			lat = row.get("lat").getValue();
			lon = row.get("lon").getValue();
			type = Type.valueOf(system);
			verbatim = lat + ":" + lon + " " + system;
			if (type == Type.YKJ) {
				int accuracyLength = ykjAccuracyLength(coordinateAccuracyInMeters);
				lat = Utils.trimToLength(lat, accuracyLength);
				lon = Utils.trimToLength(lon, accuracyLength);
			}
		}
		if (!given(lat) || !given(lon)) return;

		try {
			Coordinates coordinates = new Coordinates(Double.valueOf(lat), Double.valueOf(lon), type);
			coordinates.setAccuracyInMeters(coordinateAccuracyInMeters);
			gathering.setCoordinates(coordinates);
		} catch (NumberFormatException | DataValidationException e) {
			Quality.Issue issue = getCoordinateIssueForType(type);
			gathering.createQuality().setLocationIssue(new Quality(issue, Quality.Source.ORIGINAL_DOCUMENT, e.getMessage()));
		}
		gathering.setCoordinatesVerbatim(verbatim);
	}

	private Issue getCoordinateIssueForType(Type type) {
		if (type == Type.EUREF) return Quality.Issue.INVALID_EUREF_COORDINATES;
		if (type == Type.YKJ) return Quality.Issue.INVALID_YKJ_COORDINATES;
		if (type == Type.EUREF) return Quality.Issue.INVALID_WGS84_COORDINATES;
		throw new UnsupportedOperationException("Unknown type " + type);
	}

	private int ykjAccuracyLength(int coordinateAccuracyInMeters) {
		if (coordinateAccuracyInMeters == 1) return 7;
		if (coordinateAccuracyInMeters == 10) return 6;
		if (coordinateAccuracyInMeters == 100) return 5;
		if (coordinateAccuracyInMeters < 10000) return 4;
		return 3;
	}

	private void time(Gathering gathering) {
		try {
			String eventDate = row.get("eventDate").getValue();
			if (given(eventDate)) {
				String eventDateAccuracy = row.get("eventDateAccuracy").getValue();
				DateRange dateRange = new DateRange(DateUtils.convertToDate(eventDate));
				if (isInaccurate(eventDateAccuracy)) {
					dateRange = getInaccurateDateRange(eventDateAccuracy, dateRange);
				} else {
					String hour = row.get("hour").getValue();
					if (hour.equals("-1")) hour = "";
					if (given(hour)) {
						if (hour.equals("24")) hour = "23";
						gathering.setHourBegin(Integer.valueOf(hour));
					}
				}
				gathering.setEventDate(dateRange);
			}
		} catch (DateValidationException e) {
			gathering.createQuality().setTimeIssue(new Quality(e.getIssue(), Quality.Source.ORIGINAL_DOCUMENT, e.getMessage()));
		} catch (Exception e) {
			gathering.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_DATE, Quality.Source.ORIGINAL_DOCUMENT, e.getMessage()));
		}
	}

	private boolean isInaccurate(String eventDateAccuracy) {
		return given(eventDateAccuracy) && !"0".equals(eventDateAccuracy);
	}

	private DateRange getInaccurateDateRange(String eventDateAccuracy, DateRange reportedDate) throws DateValidationException {
		if ("-89".contains(eventDateAccuracy)) {
			return noFuture(reportedDate.conceal(SecureLevel.HIGHEST));
		}
		Calendar begin = Calendar.getInstance();
		begin.setTime(reportedDate.getBegin());
		Calendar end = Calendar.getInstance();
		end.setTime(reportedDate.getBegin());

		int inaccuracyInDays = INACCURACY_IN_DAYS.get(eventDateAccuracy);

		begin.add(Calendar.DAY_OF_YEAR, -1*inaccuracyInDays);
		end.add(Calendar.DAY_OF_YEAR, inaccuracyInDays);
		return noFuture(new DateRange(begin.getTime(), end.getTime()));
	}

	private DateRange noFuture(DateRange dateRange) throws DateValidationException {
		Date begin = noFuture(dateRange.getBegin());
		Date end = noFuture(dateRange.getEnd());
		return new DateRange(begin, end);
	}

	private Date noFuture(Date date) {
		Date today = new Date();
		if (date.after(today)) return today;
		return date;
	}

	private static final Map<String, Integer> INACCURACY_IN_DAYS;
	static {
		INACCURACY_IN_DAYS = new HashMap<>();
		INACCURACY_IN_DAYS.put("1", 1);
		INACCURACY_IN_DAYS.put("2", 3);
		INACCURACY_IN_DAYS.put("3", 7);
		INACCURACY_IN_DAYS.put("4", 2*7);
		INACCURACY_IN_DAYS.put("5", 6*7);
		INACCURACY_IN_DAYS.put("6", 3*30);
		INACCURACY_IN_DAYS.put("7", 6*30);
	}

	private void facts(Unit unit) {
		groupFieldsToFacts("clutch", unit);

		if (row.get("fieldReadableCode").hasValue()) {
			unit.addFact("fieldReadableCode", secure(row.get("fieldReadableCode").getValue()));
		}
		if (row.get("removedFieldReadableCode").hasValue()) {
			unit.addFact("removedFieldReadableCode", secure(row.get("removedFieldReadableCode").getValue()));
		}
		groupFieldsToFacts("fieldReadable", unit);

		groupFieldsToFacts("unit", unit);
		groupFieldsToFacts("bat", unit);
		groupFieldsToFacts("parasites", unit);
		groupFieldsToFacts("otherBiometrics", unit);
		groupFieldsToFacts("legMeasurements", unit);
		groupFieldsToFacts("beakMeasurements", unit);
		groupFieldsToFacts("featherMeasurements", unit);
		groupFieldsToFacts("otherFeatherMeasurements", unit);
		groupFieldsToFacts("moultState", unit);
		groupFieldsToFacts("moultExtend", unit);
		groupFieldsToFacts("speciesSpecial", unit);
	}

	private void facts(Gathering gathering) {
		gathering.addFact("type", row.get("type").getValue());

		groupFieldsToFacts("time", gathering);
		groupFieldsToFacts("place", gathering);
		groupFieldsToFacts("additionalPlaceInfo", gathering);
		groupFieldsToFacts("coordinates", gathering);
		groupFieldsToFacts("event", gathering);
		groupFieldsToFacts("colony", gathering);
	}

	private void groupFieldsToFacts(String group, BaseModel model) {
		for (Column field : row.getGroupFields(group)) {
			if (FACTS_TO_IGNORE.contains(field.getName())) continue;
			if (field.hasValue()) {
				model.addFact(field.getName(), field.getValue());
			}
		}
	}

	private Qname individualId() {
		return new Qname("RE." + row.get("nameRing").getValue().replace(" ", ""));
	}

	private RecordBasis recordBasis() {
		if (row.get("type").getValue().equals("ringing")) {
			return RecordBasis.HUMAN_OBSERVATION_HANDLED;
		}

		String ringVerification = row.get("ringVerification").getValue();
		if ("M".equals(ringVerification) || "M".equals(row.get("ringedSpeciesAccuracy").getValue())) {
			return RecordBasis.PRESERVED_SPECIMEN;
		}

		if (given(ringVerification) && "RV".contains(ringVerification)) {
			return RecordBasis.HUMAN_OBSERVATION_PHOTO;
		}

		String captureMethod = row.get("captureMethod").getValue();
		if (given(captureMethod) && !"TR".contains(captureMethod)) {
			return RecordBasis.HUMAN_OBSERVATION_HANDLED;
		}
		if (row.get("ringsAttached").hasValue()) {
			return RecordBasis.HUMAN_OBSERVATION_HANDLED;
		}

		String birdConditionEURING = row.get("birdConditionEURING").getValue();
		if (given(birdConditionEURING) && "13".contains(birdConditionEURING)) {
			return RecordBasis.HUMAN_OBSERVATION_INDIRECT;
		}
		String recoveryMethod = row.get("recoveryMethod").getValue();
		if (given(recoveryMethod) && "0;2;".contains(recoveryMethod+";")) {
			return RecordBasis.HUMAN_OBSERVATION_INDIRECT;
		}
		if ("86".equals(recoveryMethod)) return RecordBasis.MACHINE_OBSERVATION_SATELLITE_TRANSMITTER;

		return RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED;
	}

	private static final MessageDigest MD;
	static {
		try {
			MD = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private String secure(String value) {
		value = value + secretSalt;
		byte[] digest = MD.digest(value.getBytes(StandardCharsets.UTF_8));
		String hex = (new HexBinaryAdapter()).marshal(digest);
		return hex;
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

}
