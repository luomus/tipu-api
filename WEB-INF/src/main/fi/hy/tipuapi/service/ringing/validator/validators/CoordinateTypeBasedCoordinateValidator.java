package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class CoordinateTypeBasedCoordinateValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column type) throws Exception {
		Column lat = data.getRow().get("lat");
		Column lon = data.getRow().get("lon");
		if (!data.notNullNoErrors(type, lat, lon)) return;

		int latLength = lat.getValue().replace("-", "").length();
		int lonLength = lon.getValue().replace("-", "").length();

		if (type.getValue().equals("WGS84-DMS")) {
			validateLength(lat, latLength, 4, 6, data);
			validateLength(lon, lonLength, 4, 7, data);
			if (!data.notNullNoErrors(type, lat, lon)) return;
			new RangeValidator(0, 90).validate(data, lat, getD(lat));
			new RangeValidator(0, 59).validate(data, lat, getM(lat));
			new RangeValidator(0, 59).validate(data, lat, getS(lat));
			new RangeValidator(0, 180).validate(data, lon, getD(lon));
			new RangeValidator(0, 59).validate(data, lon, getM(lon));
			new RangeValidator(0, 59).validate(data, lon, getS(lon));
		} else if (type.getValue().equals("WGS84")) {
			new RangeValidator(-90, 90).validate(data, lat);
			new RangeValidator(-180, 180).validate(data, lon);
		} else if (type.getValue().equals("YKJ")) {
			validateLength(lat, latLength, 5, 7, data);
			validateLength(lon, lonLength, 5, 7, data);
		} else if (type.getValue().equals("EUREF")) {
			validateLength(lat, latLength, 7, 7, data);
			validateLength(lon, lonLength, 5, 7, data);
		} else {
			throw new IllegalStateException("Uknown type: " + type.getValue());
		}
	}

	private String getD(Column c) {
		return dms(c).substring(0, 3);
	}

	private String getM(Column c) {
		return dms(c).substring(3, 5);
	}

	private String getS(Column c) {
		return dms(c).substring(5, 7);
	}

	private String dms(Column c) {
		String val = c.getValue().replace("-", "");
		while (val.length() < 7) {
			val = "0" + val;
		}
		return val;
	}

	private void validateLength(Column c, int valueLength, int minLength, int maxLength, RowValidationData data) {
		if (valueLength < minLength) {
			data.addError("invalid_length_too_short", c);
		}
		if (valueLength > maxLength) {
			data.addError("invalid_length_too_long", c);
		}
	}

}
