package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.CoordinateValidationService;
import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData.ValidationErrorOrWarning;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class CoordinatesInsideRegionValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column region) {
		if (!region.hasValue()) {
			if (region.getName().equals("birdStation")) {
				validateMissingBirdStation(data, region);
				return;
			}
		}

		if (data.isForUpdate() && region.getName().equals("euringProvinceCode")) {
			Column municipality = data.getRow().get("municipality");
			if (municipality.hasValue()) {
				return; // Euring code will be filled based on municipality so no point to validate -> data has the old code that may be now wrong
			}
		}

		Column type = data.getRow().get("coordinateSystem");
		Column lat = data.getRow().get("lat");
		Column lon = data.getRow().get("lon");
		if (ok(data, region, type, lat, lon)) {
			validateInsideRegion(data, region, type, lat, lon);
		}
	}

	private void validateMissingBirdStation(RowValidationData data, Column region) {
		Column coordinateSystem = data.getRow().get("coordinateSystem");
		Column lat = data.getRow().get("lat");
		Column lon = data.getRow().get("lon");
		if (ok(data, coordinateSystem, lat, lon)) {
			try {
				String insideBirdStation = insideBirdStation(coordinateSystem, lat, lon, data.getDao());
				if (insideBirdStation != null) {
					data.addWarning("close_to_bird_station_but_station_not_given", region, insideBirdStation);
				}
			} catch (Exception e) {
				data.getErrorReporter().report("Coordinates inside bird station check: " + Utils.debugS(coordinateSystem.getValue(), lat.getValue(), lon.getValue()), e);
				data.addError("could_not_complete_validation", region);
			}
		}
	}

	private String insideBirdStation(Column coordinateSystem, Column lat, Column lon, DAO dao) throws Exception {
		TipuApiResource birdStations = dao.getTipuApiResource(TipuAPIClient.BIRD_STATIONS);
		for (Node n : birdStations.getAll()) {
			String id = n.getNode("id").getContents();
			if (insideBirdStation(id, coordinateSystem, lat, lon, birdStations)) {
				return id;
			}
		}
		return null;
	}

	private boolean insideBirdStation(String id, Column coordinateSystem, Column lat, Column lon, TipuApiResource birdStations) throws Exception {
		Node res = CoordinateValidationService.validate(id, lat.getValue(), lon.getValue(), coordinateSystem.getValue(), birdStations).getRootNode();
		if (res.hasChildNodes("error")) {
			throw new RuntimeException("Bird station validation failed: " + Utils.debugS(id, coordinateSystem, lat, lon) + ". Cause: " + res.getNode("error").getContents());
		}
		boolean isInside = res.hasAttribute("pass") && "true".equals(res.getAttribute("pass"));
		return isInside;
	}

	private boolean ok(RowValidationData data, Column ... cols) {
		// Usually we'd only perform validations if all fields are ok (have values, no errors).
		// But we want to perform bird station validation even if municipalities validation would have failed
		// So..:
		for (Column c : cols) {
			if (!c.hasValue()) return false;
			if (data.noErrors(c)) continue;
			int countOfErrors = 0;
			boolean hasCoordinateNotInsideRegionError = false;
			for (ValidationErrorOrWarning e : data.getErrors()) {
				if (e.getColumn().getName().equals(c.getName())) {
					countOfErrors++;
					if (e.getErrorName().startsWith("coordinates_not_inside_")) {
						hasCoordinateNotInsideRegionError = true;
					}
				}
			}
			if (countOfErrors == 1 && hasCoordinateNotInsideRegionError) {
				// this we can ignore: the only error was "not inside region" -error
				continue;
			}
			return false;
		}
		return true;
	}

	private void validateInsideRegion(RowValidationData data, Column region, Column type, Column lat, Column lon) {
		try {
			Node response = getValidationResponseFromValidationService(data, region, type, lat, lon);
			boolean passes = response.getAttribute("pass").equals("true");
			if (!passes) {
				data.addError("coordinates_not_inside_" + region.getName(), region);
				data.addError("coordinates_not_inside_" + region.getName(), lat);
				data.addError("coordinates_not_inside_" + region.getName(), lon);
				data.addError("coordinates_not_inside_" + region.getName(), type);
			}
		} catch (Exception e) {
			data.getErrorReporter().report("Coordinates inside region check: " + Utils.debugS(type.getValue(), lat.getValue(), lon.getValue(),  region.getName(), region.getValue()), e);
			data.addError("could_not_complete_validation", region);
			data.addError("coordinates_inside_region_failure", region, region.getName(), region.getValue());
		}
	}

	private Node getValidationResponseFromValidationService(RowValidationData data, Column region, Column type, Column lat, Column lon) throws Exception {
		TipuApiResource regions = pickRegions(data.getDao(), region);
		return CoordinateValidationService.validate(region.getValue(), lat.getValue(), lon.getValue(), type.getValue(), regions).getRootNode();
	}

	private TipuApiResource pickRegions(DAO dao, Column region) throws Exception {
		if (region.getName().equals("municipality")) {
			return dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES);
		} else if (region.getName().equals("birdStation")) {
			return dao.getTipuApiResource(TipuAPIClient.BIRD_STATIONS);
		} else if (region.getName().equals("euringProvinceCode")) {
			return dao.getTipuApiResource(TipuAPIClient.EURING_PROVINCES);
		} else {
			throw new UnsupportedOperationException("Invalid type: " + region.getName());
		}
	}

}
