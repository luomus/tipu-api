package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.Set;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.utils.Utils;

public class CuckooHostValidator implements FieldValidation {

	public CuckooHostValidator() {}

	private static final Set<String> CUCKOO = Utils.set("CUCCAN", "CUCCANH", "CUCCANP");

	@Override
	public void validate(RowValidationData data, Column host) throws Exception {
		Column species = data.getRow().get("species");
		Column age = data.getRow().get("age");
		if (CUCKOO.contains(species.getValue())) {
			if (age.getValue().startsWith("P") && !host.hasValue()) {
				data.addError("cuckoo_host_required", host);
			}
		} else {
			if (host.hasValue()) {
				data.addError("cuckoo_host_disallowed", host);
			}
		}
	}

}
