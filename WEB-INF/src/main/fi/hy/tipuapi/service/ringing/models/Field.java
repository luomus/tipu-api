package fi.hy.tipuapi.service.ringing.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.luomus.commons.utils.Utils;


public class Field {

	public static enum Type { VARCHAR, DATE, INTEGER, DECIMAL, ENUMERATION, DECIMAL_ENUMERATION, INTEGER_ENUMERATION }
	public static enum Mode { BROWSING, RINGINGS, RECOVERIES, NO_MODES, ADMIN_RINGINGS, ADMIN_RECOVERIES }
	private final String dbColumn;
	private final String name;
	private final Type type;
	private final Set<Mode> modes;
	private final List<FieldValidation> validationRules = new ArrayList<>();
	private EnumerationValues enumerationValues; 
	private int length = -1;
	private int decimalAccuracy = -1;
	private Group parentGroup;
	private boolean required = false;
	private boolean uppercase = false;

	public Field(String dbColumn, String name, Type type, Mode ... modes) {
		if ("event.".equals(dbColumn)) {
			dbColumn = "event." + name;
		}
		this.dbColumn = dbColumn == null ? "" : dbColumn;
		this.name = name;
		this.type = type;
		if (modes.length == 0) {
			this.modes = Utils.set(allModes());
		} else if (containsNoModes(modes)) {
			this.modes = Collections.emptySet();
		} else {
			this.modes = Utils.set(modes);
			if (this.modes.contains(Mode.RINGINGS)) {
				this.modes.add(Mode.ADMIN_RINGINGS);
			}
			if (this.modes.contains(Mode.RECOVERIES)) {
				this.modes.add(Mode.ADMIN_RECOVERIES);
			}
		}

		if (dbColumn != null && hasInputMode() && !dbColumn.contains("event.")) {
			throw new IllegalStateException("Must be event. -field for input mode");
		}
	}

	private boolean hasInputMode() {
		return hasMode(Mode.RINGINGS) || hasMode(Mode.RECOVERIES) || hasMode(Mode.ADMIN_RINGINGS) || hasMode(Mode.ADMIN_RECOVERIES);
	}

	private boolean containsNoModes(Mode... modes) {
		for (Mode mode : modes) {
			if (mode == Mode.NO_MODES) return true;
		}
		return false;
	}

	private void validateIsEnumeration() {
		if (!isEnumeration()) throw new UnsupportedOperationException("Not enumeration, this is " + type.toString());
	}

	public Field setEnumeration(EnumerationValues enumerationValues) {
		validateIsEnumeration();
		this.enumerationValues = enumerationValues;
		return this;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		if (type == Type.DECIMAL_ENUMERATION || type == Type.INTEGER_ENUMERATION) {
			return Type.ENUMERATION.toString();
		}
		return type.toString();
	}

	public boolean isNumeric() {
		return isInteger() || isDecimal();
	}

	public boolean isInteger() {
		return type == Type.INTEGER || type == Type.INTEGER_ENUMERATION;
	}

	public boolean isDecimal() {
		return type == Type.DECIMAL || type == Type.DECIMAL_ENUMERATION;
	}


	public boolean isDate() {
		return type == Type.DATE;
	}

	public boolean isEnumeration() {
		return type == Type.ENUMERATION || type == Type.DECIMAL_ENUMERATION || type == Type.INTEGER_ENUMERATION;
	}

	public EnumerationValues getEnumerationValues() {
		return enumerationValues;
	}

	public String getDatabaseColumn() {
		return dbColumn;
	}

	public static Mode[] allModes() {
		Mode[] allModes = new Mode[Mode.values().length - 1];
		int i = 0;
		for (Mode mode : Mode.values()) {
			if (mode == Mode.NO_MODES) continue;
			allModes[i++] = mode;
		}
		return allModes;
	}

	public Collection<Mode> getModes() {
		return modes;
	}

	public static Mode[] noModes() {
		return new Mode[] { Mode.NO_MODES };
	}

	public boolean hasMode(Mode mode) {
		return modes.contains(mode);
	}

	public Field setValidationRule(FieldValidation validation) {
		validationRules.add(validation);
		return this;
	}

	public Field setValidationRules(FieldValidation ... validations) {
		for (FieldValidation v : validations) {
			setValidationRule(v);
		}
		return this;
	}

	public List<FieldValidation> getValidationRules() {
		return validationRules;
	}

	public Field setLength(int length) {
		this.length = length;
		return this;
	}

	public Field setDecimalAccuracy(int length, int accuracy) {
		this.length = length;
		this.decimalAccuracy = accuracy;
		return this;
	}

	public int getDecimalAccuracy() {
		return decimalAccuracy;
	}

	public int getLength() {
		if (length == -1) throw new IllegalStateException("Length not set for " + this.getName());
		return length;
	}

	public Group getParentGroup() {
		if (parentGroup == null) throw new IllegalStateException("No parent set for " + this.getName());
		return parentGroup;
	}

	public void setParentGroup(Group parentGroup) {
		if (this.parentGroup == null) {
			this.parentGroup = parentGroup;
		} else {
			throw new IllegalStateException("Parent already set for " + this.getName());
		}
	}

	public String getDbType() {
		if (type == Type.ENUMERATION) return Type.VARCHAR.toString();
		if (type == Type.DECIMAL_ENUMERATION) return Type.DECIMAL.toString();
		if (type == Type.INTEGER_ENUMERATION) return Type.INTEGER.toString();
		return type.toString();
	}

	public boolean isAggregated() {
		return !dbColumn.startsWith("event.");
	}

	public boolean isRequired() {
		return required;
	}

	public Field setRequired() {
		required = true;
		return this;
	}

	public Field setUpperCase() {
		uppercase = true;
		return this;
	}

	public boolean isUpperCaseColumn() {
		return uppercase;
	}

}
