package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class CaptureMethodValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column captureMethod) throws Exception {
		if (data.getMode() == Mode.RECOVERIES) {
			validateRecovery(data, captureMethod);
		} else {
			validateRinging(data, captureMethod);
		}
	}

	private void validateRecovery(RowValidationData data, Column captureMethod) {
		if (isControl(data)) {
			valideForBoth(data, captureMethod);
		}
	}

	private void validateRinging(RowValidationData data, Column captureMethod) {
		if (RingLostDestroyedValidator.ringMarkedLostOrDestroyed(data)) return;

		valideForBoth(data, captureMethod);

		if (captureMethod.getValue().equals("T") || captureMethod.getValue().equals("R")) {
			data.addError("this_capture_method_not_allowed_for_ringing", captureMethod);
		}
	}

	private void valideForBoth(RowValidationData data, Column captureMethod) {
		Column age = data.getRow().get("age");
		if (!age.getValue().startsWith("P")) {
			data.require(captureMethod);
		} else if (age.getValue().equals("PP") && captureMethod.hasValue()) {
			data.addError("capture_method_not_allowed_for_nestling", captureMethod);
		} else if (age.getValue().equals("PM") && captureMethod.getValue().equals("P")) {
			data.addError("this_capture_method_now_allowed_for_young_bird_that_has_left_the_nest", captureMethod);
		}
	}


	private boolean isControl(RowValidationData data) {
		return !data.getRow().get("sourceOfRecovery").getValue().equals(DAO.RECOVERY_TYPE_FINDING);
	}

}
