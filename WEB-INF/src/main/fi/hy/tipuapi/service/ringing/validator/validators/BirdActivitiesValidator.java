package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class BirdActivitiesValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column birdActions) throws Exception {
		Column age = data.getRow().get("age");
		if (age.getValue().startsWith("P")) {
			if (birdActions.hasValue()) {
				data.addError("bird_activities_not_allowed_for_pull", birdActions);
			}
		}
	}

}
