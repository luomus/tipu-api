package fi.hy.tipuapi.service.ringing.validator.validators;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.utils.Utils;

public class FieldReadableCodeValidator extends LegRingForRecoveryValidator {

	private static final Set<String> ATTACHED_TO_LEG = Utils.set("A", "Y", "J");
	private static final String NECK = "K";

	@Override
	public void validate(RowValidationData data, Column fieldReadableCode) throws Exception {
		if (data.getMode() == Mode.RINGINGS) {
			ringingValidate(data, fieldReadableCode);
		} else {
			recoveryValidate(data, fieldReadableCode);
		}
	}

	private void recoveryValidate(RowValidationData data, Column fieldReadableCode) throws Exception {
		Column eventDate = data.getRow().get("eventDate");
		Column ringer = data.getRow().get("ringer");
		Column backgroundColor = data.getRow().get("mainColor");
		Column species = data.getRow().get("species");
		Column removedFieldReadableCode = data.getRow().get("removedFieldReadableCode");
		Column removedBackgroundColor = data.getRow().get("removedFieldReadableMainColor");
		Column chandedOrRemoved = data.getRow().get("fieldReadableChanges");
		Column attachmentLeftRight = data.getRow().get("attachmentLeftRight");
		Column attachmentPoint = data.getRow().get("attachmentPoint");

		if (requireAttachmentLeftRight(data, fieldReadableCode, attachmentPoint)) {
			data.require(attachmentLeftRight);
			if (data.isForInsert() && data.isForAdminValidations()) {
				if ("E".equals(attachmentLeftRight.getValue())) {
					data.addWarning("ADMIN_warning_attachment_unknown", attachmentLeftRight);
				}
			}
		}

		if (data.notNullNoErrors(fieldReadableCode, backgroundColor, species)) {
			if (data.getDao().fieldReadableDoubleDistributed(fieldReadableCode.getValue(), backgroundColor.getValue(), species.getValue())) {
				data.addError("fieldreadable_double_distributed", fieldReadableCode);
				return;
			}
		}

		if (!data.getRow().get("legRing").hasValue() && fieldReadableCode.hasValue() && !species.hasValue()) {
			data.addError("species_must_be_given_if_reporting_via_field_readable", species);
			return;
		}

		if (chandedOrRemoved.hasValue() && !ringer.hasValue()) {
			data.addError("ringer_must_be_given_for_fieldreadable_change_or_remove", ringer); // Tämä ehkä täytyy poistaa... vaihtelevat ulkomailla
		}

		requireBackgroundIfGivenAndViceVersa(data, fieldReadableCode, backgroundColor);
		requireBackgroundIfGivenAndViceVersa(data, removedFieldReadableCode, removedBackgroundColor);

		if (removedFieldReadableCode.hasValue() && !chandedOrRemoved.hasValue()) {
			data.addError("should-give-fieldreadable-changedOrRemoved-code", chandedOrRemoved);
			return;
		}

		if (chandedOrRemoved.getValue().equals("P")) {
			if (fieldReadableCode.hasValue()) {
				data.addError("should-not-give-new-info-if-fieldreadable-removed", fieldReadableCode);
			}
			if (!removedFieldReadableCode.hasValue()) {
				data.addError("should-give-removedFieldReadableCode", removedFieldReadableCode);
			}
		}
		if (chandedOrRemoved.getValue().equals("L")) {
			if (removedFieldReadableCode.hasValue()) {
				data.addError("should-not-give-removedFieldReadableCode", removedFieldReadableCode);
			}
			if (!fieldReadableCode.hasValue()) {
				data.addError("should-give-normal-fieldReadableCode", fieldReadableCode);
			}
		}
		if (chandedOrRemoved.getValue().equals("V")) {
			if (!fieldReadableCode.hasValue()) {
				data.addError("should-give-normal-and-removed-fieldReadableCode", fieldReadableCode);
			}
			if (!removedFieldReadableCode.hasValue()) {
				data.addError("should-give-normal-and-removed-fieldReadableCode", removedFieldReadableCode);
			}
		}

		if (data.hasErrors(fieldReadableCode, backgroundColor, removedFieldReadableCode, removedBackgroundColor, species)) return;

		// all ok for checks: valid recovery or maybe even ring change or remove

		Column legRing = data.getRow().get("legRing");

		if (chandedOrRemoved.getValue().equals("V")) {
			if (data.isForAdminValidations()) {
				mustBeAtBirdsLeg(removedFieldReadableCode, removedBackgroundColor, eventDate, species, data);
				mustNotHaveRecoveriesForTheFieldReadableBeingTakenOut(legRing, eventDate, data);
			}
			validateAttachingNewFieldreadable(fieldReadableCode, backgroundColor, eventDate, species, ringer, data);

			if (fieldReadableCode.hasValue() && removedFieldReadableCode.hasValue()) {
				if (fieldReadableCode.getValue().equals(removedFieldReadableCode.getValue())) {
					if (backgroundColor.getValue().equals(removedBackgroundColor.getValue())) {
						data.addError("removed_and_added_fieldreadable_cant_be_the_same", fieldReadableCode);
						data.addError("removed_and_added_fieldreadable_cant_be_the_same", removedFieldReadableCode);
						data.addError("removed_and_added_fieldreadable_cant_be_the_same", backgroundColor);
						data.addError("removed_and_added_fieldreadable_cant_be_the_same", removedBackgroundColor);
					}
				}
			}
		}
		if (chandedOrRemoved.getValue().equals("P")) {
			if (data.isForAdminValidations()) {
				mustBeAtBirdsLeg(removedFieldReadableCode, removedBackgroundColor, eventDate, species, data);
				mustNotHaveRecoveriesForTheFieldReadableBeingTakenOut(legRing, eventDate, data);
			}
		}
		if (chandedOrRemoved.getValue().equals("L")) {
			validateAttachingNewFieldreadable(fieldReadableCode, backgroundColor, eventDate, species, ringer, data);
			if (data.isForAdminValidations()) {
				mustNotHaveFieldReadableAlreadyAtTheDate(legRing, eventDate, data);
			}
		}
		if (!chandedOrRemoved.hasValue()) {
			if (data.isForAdminValidations()) {
				mustBeAtBirdsLeg(fieldReadableCode, backgroundColor, eventDate, species, data);
			}
		}

		if (data.hasErrors()) return;

		if (data.getRow().groupHasValues("fieldReadable")) {
			if (!fieldReadableCode.hasValue() && !removedFieldReadableCode.hasValue()) {
				for (Column c : data.getRow().getGroupFields("fieldReadable")) {
					if (c.getName().equals("fieldReadableNotes")) continue;
					if (c.getName().equals("metalRingColor") && c.hasValue()) {
						if (!data.getRow().get("ringsAttached").getValue().equals("V")) continue;
					}
					if (c.hasValue() && !c.getName().endsWith("Color") && !c.getName().endsWith("Code")) {
						data.addError("must_give_some_identifying_fields_if_field_readable_values_given", c);
					}
				}
			}
		}

		if (data.hasErrors()) return;

		if (data.isForAdminValidations()) {
			if (data.notNullNoErrors(fieldReadableCode, backgroundColor, eventDate, species, ringer)) {
				crossCheckWithFieldReadableDistribution(fieldReadableCode, backgroundColor, species, data);
			}
		}
	}

	private boolean requireAttachmentLeftRight(RowValidationData data, Column fieldReadableCode, Column attachmentPoint) {
		if (!fieldReadableCode.hasValue()) return false;
		if (isFinding(data)) return false;
		if (attachmentPoint.getValue().equals(NECK)) return false;
		return true;
	}

	private void mustNotHaveRecoveriesForTheFieldReadableBeingTakenOut(Column legRing, Column eventDate, RowValidationData data) throws Exception {
		if (data.hasErrors(legRing)) return;
		if (data.isForUpdate()) return; // liian vaikeaa? joten poistettu päivityksille; huoh
		RingHistory ringHistory = data.getDao().getRingHistoryByLegRing(legRing.getValue());
		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.rowAfterEventDate(recovery, eventDate)) {
				if (recovery.get("legRing").hasValue()) {
					data.addError("ADMIN_ring_change_but_recoveries_exist_after_change_for_the_old_ring", data.getRow().get("removedFieldReadableCode"));
				}
			}
		}
	}

	private void mustNotHaveFieldReadableAlreadyAtTheDate(Column legRing, Column eventDate, RowValidationData data) throws Exception {
		if (data.hasErrors(legRing)) return;
		RingHistory ringHistory = data.getDao().getRingHistoryByLegRing(legRing.getValue());
		if (!ringHistory.hasHistory()) {
			return;
		}
		Set<String> currentFieldReadables = new HashSet<>();
		for (Row historyRow : ringHistory) {
			if (RowUtils.rowAfterEventDate(historyRow, eventDate)) break;
			if (data.isForUpdate() && historyRow.get("id").getValue().equals(data.getRow().get("id").getValue())) break;
			if (historyRow.get("fieldReadableCode").hasValue()) {
				currentFieldReadables.add(historyRow.get("fieldReadableCode").getValue());
			}
			if (historyRow.get("removedFieldReadableCode").hasValue()) {
				currentFieldReadables.remove(historyRow.get("removedFieldReadableCode").getValue());
			}
		}
		if (!currentFieldReadables.isEmpty()) {
			data.addError("ADMIN_bird_already_has_fieldreadable_can_not_add_must_change", data.getRow().get("fieldReadableCode"), currentFieldReadables.toString());
		}
	}

	private void validateAttachingNewFieldreadable(Column code, Column backgroundColor, Column eventDate, Column species, Column ringer, RowValidationData data) throws Exception {
		if (!data.notNullNoErrors(code, backgroundColor, eventDate, species, ringer)) return;

		Column birdStation = data.getRow().get("birdStation");
		int ringerNumber = RingStartEndValidator.ringerNumberOrBirdStationFakeRinger(ringer, birdStation, data);

		if (data.getDao().fieldReadableDoubleDistributed(code.getValue(), backgroundColor.getValue(), species.getValue())) {
			data.addError("fieldreadable_double_distributed", code);
			return;
		}

		if (!data.getDao().fieldReadableDistributed(code.getValue(), backgroundColor.getValue(), ringerNumber, eventDate.getDateValue(), species.getValue())) {
			data.addError("fieldreadable-not-distributed-to-ringer-at-the-date", code, ""+ringerNumber);
			return;
		}

		if (data.isForInsert()) {
			if (data.getDao().fieldReadableUsed(code.getValue(), backgroundColor.getValue(), species.getValue())) {
				data.addError("fieldreadable-already-used", code);
				return;
			}
		} else {
			Row originalData = data.getDao().getRowByEventId(data.getRow().get("id").getIntValue());
			Column originalRingCode = originalData.get("fieldReadableCode");
			Column originalBackground = originalData.get("mainColor");
			Column originalSpecies = originalData.get("species");
			if (code.getValue().equals(originalRingCode.getValue())
					&& backgroundColor.getValue().equals(originalBackground.getValue())
					&& species.getValue().equals(originalSpecies.getValue())) {
				// nothing changed so no need to validate if ring is used (and it _is_ used)
			} else {
				if (data.getDao().fieldReadableUsed(code.getValue(), backgroundColor.getValue(), species.getValue())) {
					data.addError("ADMIN_ring-that-is-being-changed-in-update-is-already-used", code);
					return;
				}
			}
		}

		if (data.hasErrors()) return;

		crossCheckWithFieldReadableDistribution(code, backgroundColor, species, data);
	}

	private void crossCheckWithFieldReadableDistribution(Column ringCode, Column backgroundColor, Column species, RowValidationData data) throws Exception {
		Row distInfo = data.getDao().getFieldReadableDistributionInfo(ringCode.getValue(), backgroundColor.getValue(), species.getValue());
		crossCheckAttachmentPoint(data, distInfo);
		crossCheck("codeColor", data, distInfo);
	}

	private void crossCheck(String fieldName, RowValidationData data, Row distInfo) {
		Column distr = distInfo.get(fieldName);
		Column thisValue = data.getRow().get(fieldName);
		if (!distr.hasValue() || !thisValue.hasValue()) return;
		if (!distr.getValue().equals(thisValue.getValue())) {
			data.addError("ADMIN_"+fieldName+"_distribution_mismatch", thisValue, thisValue.getValue(), distr.getValue());
		}
	}

	private void crossCheckAttachmentPoint(RowValidationData data, Row distInfo) {
		Column attachmentPoint = data.getRow().get("attachmentPoint");
		Column distr = distInfo.get("attachmentPoint");
		if (!attachmentPoint.hasValue()) return;
		if (!distr.hasValue()) return;

		if (ATTACHED_TO_LEG.contains(distr.getValue())) {
			if (!ATTACHED_TO_LEG.contains(attachmentPoint.getValue())) {
				data.addError("ADMIN_attachmentPoint_distribution_mismatch", attachmentPoint, attachmentPoint.getValue(), distr.getValue());
			}
		} else if (!distr.getValue().equals(attachmentPoint.getValue())) {
			data.addError("ADMIN_attachmentPoint_distribution_mismatch", attachmentPoint, attachmentPoint.getValue(), distr.getValue());
		}
	}

	private void mustBeAtBirdsLeg(Column ringCode, Column backgroundColor, Column eventDate, Column species, RowValidationData data) throws Exception {
		if (!data.notNullNoErrors(ringCode, backgroundColor, eventDate, species)) return;

		Column reportedLegRing = data.getRow().get("legRing");

		List<String> nameRings = data.getDao().getNameRingByFieldReadable(ringCode.getValue(), backgroundColor.getValue(), species.getValue());

		if (nameRings.isEmpty()) {
			data.addError("ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", ringCode, ringCode.getValue(), backgroundColor.getValue(), species.getValue());
			return;
		}
		if (nameRings.size() > 1) {
			data.addError("ADMIN_multiple-name-rings-for-fieldReadable", ringCode);
			return;
		}
		RingHistory ringHistory = data.getDao().getRingHistoryByNameRing(nameRings.get(0));
		if (!ringHistory.hasHistory()) {
			data.addError("ring_not_in_birds_leg_at_the_date", ringCode);
			return;
		}
		String resolvedLegRing = latestLegRing(eventDate, ringHistory);

		if (reportedLegRing.hasValue()) {
			if (!new Ring(resolvedLegRing).equals(new Ring(reportedLegRing.getValue()))) {
				data.addError("ADMIN_fieldReadableCodeResolvedLegRing_and_reportedLegRing_mismatch", reportedLegRing);
			}
		}

		adminValidateLegRingIsInBirdsLegAndSpeciesOkAndBirdNotDeadAndTimeDistance(data, new Ring(resolvedLegRing), reportedLegRing, ringHistory);
		if (data.hasErrors(ringCode)) return;
		adminValidateFieldreadableRingIsInBirdsLeg(ringHistory, eventDate, ringCode, backgroundColor, data);
	}

	private String latestLegRing(Column eventDate, RingHistory ringHistory) {
		String resolvedLegRing = ringHistory.getRinging().get("legRing").getValue();
		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.rowAfterEventDate(recovery, eventDate)) break;
			Column legRing = recovery.get("legRing");
			Column newLegRing = recovery.get("newLegRing");
			if (legRing.hasValue()) {
				resolvedLegRing = legRing.getValue();
			}
			if (newLegRing.hasValue()) {
				resolvedLegRing = newLegRing.getValue();
			}
		}
		return resolvedLegRing;
	}

	private void adminValidateFieldreadableRingIsInBirdsLeg(RingHistory ringHistory, Column eventDate, Column ringCode, Column backgroundColor, RowValidationData data) {
		Row ringing = ringHistory.getRinging();

		String fieldReadableRingAndBackgroundAtBirdsLeg = "";

		if (ringing.get("fieldReadableCode").hasValue()) {
			fieldReadableRingAndBackgroundAtBirdsLeg = catenateRingAndBackground(ringing.get("fieldReadableCode"), ringing.get("mainColor"));
		}

		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.sameEvent(recovery, data.getRow())) break;
			if (RowUtils.rowAfterEventDate(recovery, eventDate)) break;
			String fieldReadableChanges = recovery.get("fieldReadableChanges").getValue();
			if (fieldReadableChanges.equals("P")) {
				fieldReadableRingAndBackgroundAtBirdsLeg = "";
			} else if (recovery.get("fieldReadableCode").hasValue() && (fieldReadableChanges.equals("L") || fieldReadableChanges.equals("V"))) {
				fieldReadableRingAndBackgroundAtBirdsLeg = catenateRingAndBackground(recovery.get("fieldReadableCode"), recovery.get("mainColor"));
			}
		}

		String proposedRingToBeInLeg = catenateRingAndBackground(ringCode, backgroundColor);

		if (!proposedRingToBeInLeg.equals(fieldReadableRingAndBackgroundAtBirdsLeg)) {
			data.addError("ring_not_in_birds_leg_at_the_date", ringCode);
		}
	}

	private String catenateRingAndBackground(Column ring, Column background) {
		return ring.getValue() +"||"+background.getValue();
	}

	private void requireBackgroundIfGivenAndViceVersa(RowValidationData data, Column ringCode, Column backgroundColor) {
		if (ringCode.hasValue()) {
			data.require(backgroundColor);
		}
		if (backgroundColor.hasValue()) {
			data.require(ringCode);
		}
	}

	private void ringingValidate(RowValidationData data, Column fieldReadableCode) throws Exception {
		if (data.isForUserValidations()) {
			if (fieldReadableCode.hasValue() && data.getRow().get("ringEnd").hasValue()) {
				data.addError("can_only_add_fieldreadable_for_single_birds_not_for_batch", fieldReadableCode);
				return;
			}
		}

		Column eventDate = data.getRow().get("eventDate");
		Column ringer = data.getRow().get("ringer");
		Column backgroundColor = data.getRow().get("mainColor");
		Column species = data.getRow().get("species");

		if (data.getRow().groupHasValues("fieldReadable")) {
			data.require(fieldReadableCode);
			data.require(backgroundColor);
			requireBackgroundIfGivenAndViceVersa(data, fieldReadableCode, backgroundColor);
		}

		Column attachmentLeftRight = data.getRow().get("attachmentLeftRight");
		Column attachmentPoint = data.getRow().get("attachmentPoint");

		if (requireAttachmentLeftRight(data, fieldReadableCode, attachmentPoint)) {
			data.require(attachmentLeftRight);
			if (data.isForInsert() && data.isForAdminValidations()) {
				if ("E".equals(attachmentLeftRight.getValue())) {
					data.addWarning("ADMIN_warning_attachment_unknown", attachmentLeftRight);
				}
			}
		}

		Column removedFieldReadable = data.getRow().get("removedFieldReadableCode");
		if (removedFieldReadable.hasValue()) {
			data.addError("can_not_remove_fieldreadable_in_ringing", removedFieldReadable);
		}

		validateAttachingNewFieldreadable(fieldReadableCode, backgroundColor, eventDate, species, ringer, data);
	}

	private boolean isFinding(RowValidationData data) {
		return data.getRow().get("sourceOfRecovery").getValue().equals(DAO.RECOVERY_TYPE_FINDING);
	}
}
