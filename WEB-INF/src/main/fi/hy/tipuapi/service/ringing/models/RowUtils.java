package fi.hy.tipuapi.service.ringing.models;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

import java.util.Set;

public class RowUtils {

	private static final Set<String> BIRD_READ_EURING = Utils.set("1", "2", "3");

	public static boolean rowAfterEventDate(Row row, Column eventDate) {
		long rowEpoch = DateUtils.getEpoch(row.get("eventDate").getDateValue());
		long eventDateEpoch = DateUtils.getEpoch(eventDate.getDateValue());
		return rowEpoch > eventDateEpoch;
	}

	public static boolean firstAfterSecond(Row first, Row second) {
		return rowAfterEventDate(first, second.get("eventDate"));
	}

	public static boolean birdDead(Row row) {
		return row.get("birdCondition").getValue().equals("D") || BIRD_READ_EURING.contains(row.get("birdConditionEURING").getValue());
	}

	public static boolean sameEvent(Row row1, Row row2) {
		return row1.get("id").getValue().equals(row2.get("id").getValue());
	}

	public static void fixdecimals(Column c) {
		if (!c.hasValue()) return;
		String value = c.getValue().replace(".", ",");
		if (value.startsWith(",")) {
			value = "0" + value;
		} else if (value.startsWith("-,")) {
			value = "-0" + value.replace("-", "");
		}
		c.setValue(value);
	}

	public static boolean isBirdEvent(Row row, DAO dao) throws Exception {
		if (!row.get("species").hasValue()) return true; // Jos lajia ei tiedetä, oletetaan että on lintu eikä lepakko -- lepakoille laji pakollinen myös tapaamisissa
		return speciesNumberBetween(1, 49999, row.get("species").getValue(), dao);
	}

	public static boolean isBatEvent(Row row, DAO dao) throws Exception {
		if (!row.get("species").hasValue()) return false;
		return speciesNumberBetween(50000, 59999, row.get("species").getValue(), dao);
	}

	private static boolean speciesNumberBetween(int start, int end, String species, DAO dao) throws Exception {
		Node speciesInfo = dao.getTipuApiResource(TipuAPIClient.SPECIES).getById(species);
		if (speciesInfo == null) {
			throw new IllegalStateException("Unknown species " + species);
		}
		int speciesNumber = Integer.valueOf(speciesInfo.getAttribute("euring-code"));

		return speciesNumber >= start && speciesNumber <= end;
	}

}

