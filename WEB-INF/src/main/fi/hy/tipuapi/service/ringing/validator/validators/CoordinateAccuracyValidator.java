package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class CoordinateAccuracyValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column coordinateAccuracy) throws Exception {
		Column type = data.getRow().get("coordinateSystem");
		Column lat = data.getRow().get("lat");
		Column lon = data.getRow().get("lon");
		
		if (!data.notNullNoErrors(coordinateAccuracy, type, lat, lon)) return;
		
		if (type.getValue().equals("YKJ")) {
			checkMetricLengthAccuracy(lat, coordinateAccuracy, data);
			checkMetricLengthAccuracy(lon, coordinateAccuracy, data);
			return;
		}
		
	}

	private void checkMetricLengthAccuracy(Column value, Column coordinateAccuracy, RowValidationData data) {
		if (coordinateAccuracy.getValue().equals("EPÄT")) return;
		
		int accLenght = coordinateAccuracy.getValue().length();
		int minLength = 8 - accLenght; // 1(1) == 7, 100(3) == 5, 200(3) == 5, 1000(4) == 4 ..   
		
		if (value.getValue().length() < minLength) {
			data.addError("coordinate_length_accuracy_missmatch", coordinateAccuracy);
			data.addError("coordinate_length_accuracy_missmatch", value);
		}		
	}

}
