package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class BatReproductiveStageValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column repoductiveStage) throws Exception {
		if (!repoductiveStage.hasValue()) return;
		Column sex = data.getRow().get("sex");
		if (!sex.hasValue()) return;
		if (female(sex)) {
			if (repoductiveStage.getValue().equals("T")) {
				data.addError("bat_reproductive_stage_only_for_males", repoductiveStage);
			}
		}
		if (male(sex)) {
			if (!repoductiveStage.getValue().equals("T")) {
				data.addError("bat_reproductive_stage_only_for_females", repoductiveStage);
			}
		}
	}

	private boolean male(Column sex) {
		return sex.getValue().equals("K") || sex.getValue().equals("L");
	}

	private boolean female(Column sex) {
		return sex.getValue().equals("N") || sex.getValue().equals("O");
	}

}
