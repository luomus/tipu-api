package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class FieldReadableAdditionalInfoValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (column.hasValue()) return;
		if (!data.getRow().get("fieldReadableCode").hasValue()) return;
		String changeAdd = data.getRow().get("fieldReadableChanges").getValue();
		if (data.getMode() == Mode.RECOVERIES && (!changeAdd.equals("V") && !changeAdd.equals("L"))) return;
		
		data.addWarning("fieldreadable_"+column.getName()+"_not_given_will_come_from_distribution_warning", column);
	}

}
