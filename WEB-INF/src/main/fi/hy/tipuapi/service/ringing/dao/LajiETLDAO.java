package fi.hy.tipuapi.service.ringing.dao;

import fi.hy.tipuapi.service.ringing.models.Row;

public interface LajiETLDAO {

	public void delete(int eventId);

	public void push(Row row);

	public void close();

	public void flush();

}
