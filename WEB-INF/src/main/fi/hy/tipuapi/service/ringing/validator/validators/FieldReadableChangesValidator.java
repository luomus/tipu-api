package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class FieldReadableChangesValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column changes) throws Exception {
		if (data.getMode() == Mode.RINGINGS) {
			if (changes.hasValue() && !changes.getValue().equals("L")) {
				data.addError("only_adding_field_readable_is_allowed_for_ringing", changes);
			}
		}
	}

}
