package fi.hy.tipuapi.service.ringing;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DAO.Action;
import fi.hy.tipuapi.service.ringing.dao.RowToDbNonInputFieldsValueResolver;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowValidationResponseData;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData.ValidationErrorOrWarning;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;

public class RowsToDb extends AdminValidate {

	private static final long serialVersionUID = -3696458037232134252L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return this.processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return this.processPut(req, res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		return super.processPost(req, res);
	}

	@Override
	protected void storeToDbHook(DAO dao, RowValidationResponseData rowValidationResponseData, Row row, Mode mode, Action action, boolean acceptWarnings) {
		if (action == Action.MASS_UPDATE) {
			store(dao, rowValidationResponseData, row, mode, action);
		} else {
			if (!shouldStore(rowValidationResponseData, acceptWarnings)) return;
			store(dao, rowValidationResponseData, row, mode, action);
		}
	}

	private void store(DAO dao, RowValidationResponseData rowValidationResponseData, Row row, Mode mode, DAO.Action action) {
		try {
			tryToStore(dao, rowValidationResponseData, row, mode, action);
		} catch (Exception e) {
			try {
				dao.rollbackTransaction();
			} catch (Exception rollbackException) {
				getErrorReporter().report("Rollback: ", rollbackException);
			}
			Column errorColumn = mode == Mode.RINGINGS ? row.get("ringStart") : row.get("legRing");
			String stackTrace = LogUtils.buildStackTrace(e, 5);
			if (e instanceof IllegalStateException || e instanceof IllegalArgumentException) {
				rowValidationResponseData.addError(new ValidationErrorOrWarning(errorColumn, stackTrace));
			} else {
				rowValidationResponseData.addError(new ValidationErrorOrWarning(errorColumn, "UNEXPECTED DATABASE ERROR: " + stackTrace));
			}
			getErrorReporter().report("Row data: " + row.toString(), e);
		}
	}

	private void tryToStore(DAO dao, RowValidationResponseData rowValidationResponseData, Row row, Mode mode, DAO.Action action) throws Exception {
		dao.startTransaction();
		if (action == DAO.Action.INSERT) {
			List<Integer> ids = insertAndReturnIDs(dao, row, mode);
			if (mode == Mode.RECOVERIES) {
				dao.markRecoveriesForLetterPrintingUsingEventIDs(ids);
			}
			rowValidationResponseData.setStoredIds(ids);
		} else if (action == DAO.Action.UPDATE || action == DAO.Action.MASS_UPDATE) {
			update(dao, row, mode);
		} else {
			throw new UnsupportedOperationException(action.toString());
		}
		rowValidationResponseData.markStoredToDb();
		dao.commitTransaction();
		getWarehousingDAO(); // make sure thread is running
	}

	private boolean shouldStore(RowValidationResponseData rowValidationResponseData, boolean acceptWarnings) {
		return rowValidationResponseData.isAccepted() || isAcceptWithWarnings(rowValidationResponseData, acceptWarnings);
	}

	private boolean isAcceptWithWarnings(RowValidationResponseData rowValidationResponseData, boolean acceptWarnings) {
		return rowValidationResponseData.isAcceptedWithWarnings() && acceptWarnings;
	}

	private List<Integer> insertAndReturnIDs(DAO dao, Row row, Mode mode) throws Exception {
		RowToDbNonInputFieldsValueResolver inputFieldsValueResolver = new RowToDbNonInputFieldsValueResolver(dao);
		List<Row> rows = inputFieldsValueResolver.fillValues(row, mode, DAO.Action.INSERT);
		List<Integer> ids = new ArrayList<>(rows.size());
		for (Row readyRow : rows) {
			int id = dao.insert(readyRow, mode);
			ids.add(id);
		}
		return ids;
	}

	private void update(DAO dao, Row row, Mode mode) throws Exception {
		RowToDbNonInputFieldsValueResolver inputFieldsValueResolver = new RowToDbNonInputFieldsValueResolver(dao);
		List<Row> rows = inputFieldsValueResolver.fillValues(row, mode, DAO.Action.UPDATE);
		for (Row readyRow : rows) {
			dao.update(readyRow);
			if (mode == Mode.RINGINGS) {
				updateRecoveryTimeDistanceDirectionInfo(readyRow, dao);
			}
		}
	}

	private void updateRecoveryTimeDistanceDirectionInfo(Row ringing, DAO dao) throws Exception {
		RingHistory ringHistory = dao.getRingHistoryByNameRing(ringing.get("nameRing").getValue());
		for (Row recovery : ringHistory.getRecoveries()) {
			JSONObject timeDistanceDirectionData = TimeDistanceDirection.countTimeDistanceDirection(recovery, ringing);
			recovery.get("distanceToRingingInKilometers").setValue(timeDistanceDirectionData.getInteger("distanceInKilometers"));
			recovery.get("directionToRingingInDegrees").setValue(Long.toString(Math.round(timeDistanceDirectionData.getDouble("bearingInDegrees"))));
			recovery.get("timeToRingingInDays").setValue(timeDistanceDirectionData.getInteger("timeInDays"));
			dao.update(recovery);
		}
	}

}
