package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class MeasurementMethodOfSomethingValidator implements FieldValidation {

	private final String fieldNameWhichMeasurementMethodThisFieldReflects;

	public MeasurementMethodOfSomethingValidator(String fieldNameWhichMeasurementMethodThisFieldReflects) {
		this.fieldNameWhichMeasurementMethodThisFieldReflects = fieldNameWhichMeasurementMethodThisFieldReflects; 
	}

	@Override
	public void validate(RowValidationData data, Column accuracyField) throws Exception {
		Column field = data.getRow().get(fieldNameWhichMeasurementMethodThisFieldReflects);
		if (!field.hasValue() && accuracyField.hasValue()) {
			data.addError("measurement_method_can_only_be_given_if_field_value_given", accuracyField);
		}
	}
	
}
