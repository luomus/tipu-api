package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class SpeciesMustBeValidator implements FieldValidation {

	private final String requiredSpecies;
	
	public SpeciesMustBeValidator(String requiredSpecies) {
		this.requiredSpecies = requiredSpecies;
	}

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (!column.hasValue()) return;
		Column species = data.getRow().get("species");
		if (data.notNullNoErrors(species)) {
			if (!species.getValue().equals(requiredSpecies)) {
				data.addError("species_must_be", column, requiredSpecies);
			}
		}
	}

}
