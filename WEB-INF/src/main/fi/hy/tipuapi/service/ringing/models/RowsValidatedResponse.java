package fi.hy.tipuapi.service.ringing.models;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.luomus.commons.json.JSONObject;

public class RowsValidatedResponse {

	private final JSONObject response = new JSONObject();
	private final JSONObject rowValidationResponses = new JSONObject();
	private int acceptedRowsCount = 0;
	private int rowsWithWarningsCount = 0;
	private int rejectedRowsCount = 0;
	private int storedToDbCount = 0;

	public RowsValidatedResponse(String validationType, DAO.Action action) {
		response.setString("validation-type", validationType);
		response.setString("validation-for-action", action.toString().toLowerCase());
		response.setObject("rows", rowValidationResponses);
	}

	public void setRow(String id, RowValidationResponseData rowValidationResponseData) {
		rowValidationResponses.setObject(id, rowValidationResponseData.getResponse());
		if (rowValidationResponseData.isAccepted()) {
			this.acceptedRowsCount++;
		} else if (rowValidationResponseData.hasErrors()) {
			this.rejectedRowsCount++;
		} else {
			this.rowsWithWarningsCount++;
		}
		if (rowValidationResponseData.isStoredToDb()) {
			storedToDbCount++;
		}
	}

	public JSONObject getResponse() {
		response.setInteger("acceptedRows", acceptedRowsCount);
		response.setInteger("rowsWithWarnings", rowsWithWarningsCount);
		response.setInteger("rejectedRows", rejectedRowsCount);
		response.setInteger("rowsStoredToDb", storedToDbCount);
		return response;
	}

}
