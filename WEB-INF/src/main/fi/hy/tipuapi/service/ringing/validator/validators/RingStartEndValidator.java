package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.DistributedRings.Distribution;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingSeriesContainer.RingSeries;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

public class RingStartEndValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column ringStart) throws Exception {
		if (data.getMode() == Mode.RECOVERIES) return;
		Column legRing = data.getRow().get("legRing");
		Column ringEnd = data.getRow().get("ringEnd");

		if (!legRing.hasValue()) {
			data.require(ringStart);
		}
		if (data.hasErrors(ringStart)) return;

		if (data.isForInsert() && (ringStart.hasValue() || ringEnd.hasValue())) { // Sulasta tai Tipusta rengasvälillä ilmoitettu
			validateRingStartEnd(data, ringStart);
		} else {
			validateSingleEntry(data, legRing); // Tipusta
		}
	}

	private void validateSingleEntry(RowValidationData data, Column legRing) throws Exception {
		if (!legRing.hasValue()) return;

		Ring ring = new Ring(legRing.getValue());
		if (!ring.validFormat()) {
			data.addError("not-valid-ring-format", legRing);
			return;
		}

		if (!validateSeries(data, legRing, ring)) return;

		validateRingSeriesFitsForSpecies(data, legRing, data.getRow().get("species"), ring);

		validateDistributionAndUse(legRing, ring, data);
	}

	private void validateRingStartEnd(RowValidationData data, Column ringStart) throws Exception {
		Column ringEnd = data.getRow().get("ringEnd");
		Column species = data.getRow().get("species");
		Column numberOfYoungs = data.getRow().get("numberOfYoungs");

		data.require(ringStart);
		if (!ringStart.hasValue()) return;

		if (!validFormat(data, ringStart, ringEnd)) return;

		Ring start = new Ring(ringStart.getValue());

		if (!validateSeries(data, ringStart, start)) return;

		validateRingSeriesFitsForSpecies(data, ringStart, species, start);

		if (ringEnd.hasValue()) {
			Ring end = new Ring(ringEnd.getValue());
			int delta = end.getNumbers() - start.getNumbers();
			if (start.getNumbers() > end.getNumbers()) {
				data.addError("ring-end-must-be-greater-than-start", ringEnd);
				return;
			}
			if (delta > 200) {
				data.addError("ring-amount-too-large", ringEnd);
				return;
			}
			if (data.notNullNoErrors(numberOfYoungs)) {
				if (delta > numberOfYoungs.getIntValue()) {
					data.addError("numberOfYoungs-ring-amount-conflict", ringEnd);
					data.addError("numberOfYoungs-ring-amount-conflict", numberOfYoungs);
				}
			}
			for (int i = start.getNumbers(); i <= end.getNumbers(); i++) {
				boolean shouldContinue = validateDistributionAndUse(ringStart, new Ring(start.getSeries() + i), data);
				if (!shouldContinue) break;
			}
		} else {
			validateDistributionAndUse(ringStart, start, data);
		}
	}

	protected boolean validateDistributionAndUse(Column ringColumn, Ring ring, RowValidationData data) throws Exception {
		if (!data.getRow().get("ringer").hasValue()) return true;

		if (validateDistribution(ringColumn, ring, data)) {
			return validateUse(ringColumn, ring, data);
		}
		return false;
	}

	protected boolean validateSeries(RowValidationData data, Column ringColumn, Ring ring) throws Exception {
		if (!data.getDao().seriesExists(ring)) {
			data.addError("invalid-ring-series", ringColumn);
			return false;
		}
		return true;
	}

	protected void validateRingSeriesFitsForSpecies(RowValidationData data, Column ringColumn, Column species, Ring ring) throws Exception {
		if (species.hasValue()) {
			RingSeries ringSeries = data.getDao().getSeries(ring);
			if (!ringSeries.isAllowedForSpecies(species.getValue())) {
				data.addError("ring-series-not-allowed-for-species", ringColumn);
			} else if (ringSeries.requiresWarningForSpecies(species.getValue())) {
				if (!data.isForMassDataEntry()) {
					data.addWarning("ring-series-warning-for-species", ringColumn);
				}
			}
		}
	}

	private boolean validateDistribution(Column ringColumn, Ring ring, RowValidationData data) throws Exception {
		Column eventDate = data.getRow().get("eventDate");
		Column ringer = data.getRow().get("ringer");
		Column birdStation = data.getRow().get("birdStation");

		TipuApiResource birdStations = data.getDao().getTipuApiResource(TipuAPIClient.BIRD_STATIONS);

		int ringerNumber = ringerNumberOrBirdStationFakeRinger(ringer, birdStation, data, birdStations);

		if (data.notNullNoErrors(eventDate) && ringerNumber > 0) {
			Distribution distribution = data.getDao().getDistributedRings().getDistribution(ring);
			if (distribution == null) {
				data.addError("ring-not-distributed-to-ringer-at-the-date", ringColumn, ring.toString());
				return false;
			}

			if (isBirdStationRinger(distribution.getRingerNumber(), birdStations)) {
				if (!birdStation.hasValue()) {
					data.addWarning("ring-distributed-to-bird-station-but-station-not-given", birdStation, ring.toString());
				}
			}

			if (!data.getDao().ringDistributed(ring, ringerNumber, eventDate.getDateValue())) {
				data.addError("ring-not-distributed-to-ringer-at-the-date", ringColumn, ring.toString());
				return false;
			}
		}
		return true;
	}

	private boolean isBirdStationRinger(int ringerNumber, TipuApiResource birdStations) {
		for (Node n : birdStations.getAll()) {
			String birdStationRinger = n.getAttribute("ringer-number");
			if (Integer.valueOf(birdStationRinger) == ringerNumber) return true;
		}
		return false;
	}

	private boolean validateUse(Column ringColumn, Ring ring, RowValidationData data) throws Exception {
		if (data.isForInsert()) {
			if (data.getDao().ringUsed(ring.toString())) {
				data.addError("ring-already-used", ringColumn, ring.toString());
				return false;
			}
		} else {
			Row originalData = data.getDao().getRowByEventId(data.getRow().get("id").getIntValue());
			Ring originalRing = new Ring(originalData.get(ringColumn.getName()).getValue());
			if (ring.equals(originalRing)) {
				// nothing changed so no need to validate if ring is used (and it _is_ used)
			} else {
				if (data.getDao().ringUsed(ring.toString())) {
					data.addError("ADMIN_ring-that-is-being-changed-in-update-is-already-used", ringColumn, ring.toString());
					return false;
				}
			}
		}
		return true;
	}

	public static int ringerNumberOrBirdStationFakeRinger(Column ringer, Column birdStation, RowValidationData data) throws Exception {
		return ringerNumberOrBirdStationFakeRinger(ringer, birdStation, data, data.getDao().getTipuApiResource(TipuAPIClient.BIRD_STATIONS));
	}

	private static int ringerNumberOrBirdStationFakeRinger(Column ringer, Column birdStation, RowValidationData data, TipuApiResource birdStations) throws Exception {
		if (data.notNullNoErrors(birdStation)) {
			String ringerFakeNumber = birdStations.getById(birdStation.getValue()).getAttribute("ringer-number");
			return Integer.valueOf(ringerFakeNumber);
		}
		if (data.notNullNoErrors(ringer)) {
			return ringer.getIntValue();
		}
		return -1;
	}

	protected boolean validFormat(RowValidationData data, Column ring) {
		return validFormat(data, ring, ring);
	}

	protected boolean validFormat(RowValidationData data, Column ringStart, Column ringEnd) {
		boolean ok = true;
		Ring start = new Ring(ringStart.getValue().replace(" ", ""));
		Ring end = new Ring(ringEnd.getValue().replace(" ", ""));

		if (!start.validFormat()) {
			data.addError("not-valid-ring-format", ringStart);
			ok = false;
		}
		if (!end.validFormat()) {
			data.addError("not-valid-ring-format", ringEnd);
			ok = false;
		}
		if (ringEnd.hasValue()) {
			if (!start.getFormatString().equals(end.getFormatString())) {
				data.addError("not-same-ring-format", ringEnd);
				ok = false;
			}
		}
		if (!ok) return false;

		if (ringEnd.hasValue()) {
			if (!start.getSeries().equals(end.getSeries())) {
				data.addError("not-same-ring-format", ringEnd);
				return false;
			}
		}
		return true;
	}

}
