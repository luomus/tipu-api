package fi.hy.tipuapi.service.ringing;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.RowConstructor;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

public class BirdHistory extends Rows {

	private static final long serialVersionUID = -5579894518825975726L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (notProductionMode()) log(req);
		String data = req.getParameter("data");
		if (!given(data)) {
			throw new IllegalArgumentException("You must give 'data' parameter");
		}
		String userid = req.getParameter("userid");
		String protectedFieldLocalizedString = getLocalizedTexts().getAllTexts(getLocale(req)).get("protectedField");

		JSONObject response = null;
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			response = generateBirdHistoryResponse(dao, data, userid, protectedFieldLocalizedString);
		} catch (Exception e) {
			getErrorReporter().report(e);
			response = generateExceptionResponse(e);
		} finally {
			if (dao != null) dao.close();
		}
		return print(response, res);
	}

	private JSONObject generateBirdHistoryResponse(DAO dao, String data, String userid, String protectedFieldLocalizedString) throws Exception {
		JSONObject response = new JSONObject();
		RingHistory history = getRingHistory(dao, data);
		if (history.hasHistory()) {
			List<Row> allRows = history.getAll();
			appendBeautifyAndProtectRows(response, allRows, userid, protectedFieldLocalizedString, null, false, dao);
			response.setInteger("countOfRows", allRows.size());
		} else {
			response.setInteger("countOfRows", 0);
		}
		response.setBoolean("success", true);
		return response;
	}

	private RingHistory getRingHistory(DAO dao, String data) throws Exception {
		JSONObject inputData = new JSONObject(data);
		Row row = RowConstructor.constructTo(dao.emptyRow(), inputData);

		Column nameRing = row.get("nameRing");
		if (nameRing.hasValue()) {
			return dao.getRingHistoryByNameRing(nameRing.getValue());
		}

		Column legRing = row.get("legRing");
		if (legRing.hasValue()) {
			return dao.getRingHistoryByLegRing(legRing.getValue());
		}

		Column fieldReadableCode = row.get("fieldReadableCode");
		Column backgroundColor = row.get("mainColor");
		Column species = row.get("species");
		if (fieldReadableCode.hasValue() && backgroundColor.hasValue() && species.hasValue()) {
			List<String> nameRings = dao.getNameRingByFieldReadable(fieldReadableCode.getValue(), backgroundColor.getValue(), species.getValue());
			if (nameRings.size() == 1) {
				return dao.getRingHistoryByNameRing(nameRings.get(0));
			}
		}

		return new RingHistory();
	}

}
