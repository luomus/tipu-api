package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class OnlyForAdultsValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column xxx) throws Exception {
		if (!xxx.hasValue()) return;
		Column age = data.getRow().get("age");
		if (!data.notNullNoErrors(age)) return;
		if (age.getValue().startsWith("P") || age.getValue().equals("1")) {
			data.addError("only_for_adults", xxx);
		}
	}

}
