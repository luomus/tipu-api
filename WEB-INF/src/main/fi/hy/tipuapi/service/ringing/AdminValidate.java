package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.validator.RowValidator;


public class AdminValidate extends Validate {

	private static final long serialVersionUID = 2855027881383917387L;

	@Override
	protected boolean validationMode() {
		return RowValidator.ADMIN_VALIDATIONS;
	}
	
}
