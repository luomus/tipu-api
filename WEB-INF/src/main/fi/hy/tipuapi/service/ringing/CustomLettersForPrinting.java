package fi.hy.tipuapi.service.ringing;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.NameRingsRecoveryIDs;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class CustomLettersForPrinting extends LettersNotYetPrinted {

	private static final long serialVersionUID = -4176883362290745601L;

	@Override
	protected NameRingsRecoveryIDs pickRecoveries(DAO dao,HttpServletRequest req) throws Exception {
		List<Integer> recoveryIds = getIds(req);
		NameRingsRecoveryIDs data = dao.lettersNotYetPrinted(recoveryIds);
		return data;
	}

}
