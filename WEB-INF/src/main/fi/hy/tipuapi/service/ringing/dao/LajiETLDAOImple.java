package fi.hy.tipuapi.service.ringing.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

import fi.hy.tipuapi.service.ringing.dao.DAOImple.DAOImpleBuilder;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowToLajiETLJSONConverter;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.URIBuilder;

public class LajiETLDAOImple implements LajiETLDAO {

	private static final int BULK_SIZE = 20;
	private final HttpClientService client;
	private final String pushApi;
	private final String apiKey;
	private final ErrorReporter errorReporter;
	private final DAOImpleBuilder daoBuilder;
	private final Map<Integer, JSONObject> bulk = new HashMap<>(BULK_SIZE);
	private final String secretSalt;

	public LajiETLDAOImple(ErrorReporter errorReporter, Config config, DAOImpleBuilder daoBuilder) throws Exception {
		this.errorReporter = errorReporter;
		this.apiKey = config.get("LajiETLPushApiKey");
		this.pushApi = config.get("LajiETLPushURL");
		this.client = new HttpClientService();
		this.secretSalt = config.get("LajiETLSecretSalt");
		this.daoBuilder = daoBuilder;
	}

	@Override
	public void close() {
		try {
			if (client != null) client.close();
		} catch (Exception e) {}
	}

	@Override
	public void delete(int eventId) {
		CloseableHttpResponse response = null;
		DAO dao = null;
		try {
			Qname documentId = RowToLajiETLJSONConverter.toQname(eventId);
			URIBuilder uri = new URIBuilder(pushApi).addParameter(Const.API_KEY, apiKey).addParameter("documentId", documentId.toURI());
			response = client.execute(new HttpDelete(uri.getURI()));
			if (response.getStatusLine().getStatusCode() != 200) {
				String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
				throw new Exception("DELETE " +uri.toString()+ " failed with message: " + errorMessage);
			}
			dao = daoBuilder.openForUserId(LajiETLDAOImple.class.getName());
			markSuccess(dao, eventId);
		} catch (Exception e) {
			errorReporter.report("ETL delete for event id " + eventId, e);
		} finally {
			if (response != null) try { response.close(); } catch (Exception e) {}
			if (dao != null) try { dao.close(); } catch (Exception e) {}
		}
	}

	@Override
	public void push(Row row) {
		synchronized (bulk) {
			try (DAO dao = daoBuilder.openForUserId(LajiETLDAOImple.class.getName())) {
				JSONObject json = convert(row, dao);
				bulk.put(row.get("id").getIntValue(), json);
			} catch (Exception e) {
				errorReporter.report("ETL push for row " + row, e);
			}
			if (bulk.size() > BULK_SIZE) {
				pushBulk();
			}
		}
	}

	private JSONObject convert(Row row, DAO dao) throws Exception {
		return new RowToLajiETLJSONConverter(
				row,
				dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES),
				dao.getTipuApiResource(TipuAPIClient.RINGERS),
				secretSalt)
				.toJson();
	}

	private void pushBulk() {
		if (bulk.isEmpty()) return;
		CloseableHttpResponse response = null;
		DAO dao = null;
		try {
			URIBuilder uri = new URIBuilder(pushApi).addParameter(Const.API_KEY, apiKey);
			response = pushBulk(uri);
			handleFail(response, uri);
			dao = daoBuilder.openForUserId(LajiETLDAOImple.class.getName());
			markBulkSuccess(dao);
			bulk.clear();
		} catch (Exception e) {
			errorReporter.report("ETL push", e);
		} finally {
			if (response != null) try { response.close(); } catch (Exception e) {}
			if (dao != null) try { dao.close(); } catch (Exception e) {}
		}
	}

	private void markBulkSuccess(DAO dao) {
		for (Integer eventId : bulk.keySet()) {
			markSuccess(dao, eventId);
		}
	}

	private void markSuccess(DAO dao, Integer eventId) {
		try {
			dao.markETLPushSuccess(eventId);
		} catch (Exception successMarkingException) {
			errorReporter.report("ETL mark success", successMarkingException);
		}
	}

	private void handleFail(CloseableHttpResponse response, URIBuilder uri) throws IOException, Exception {
		if (response.getStatusLine().getStatusCode() != 200) {
			String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
			throw new Exception("POST " +uri.toString()+ " failed with message: " + errorMessage);
		}
	}

	private CloseableHttpResponse pushBulk(URIBuilder uri) throws Exception {
		JSONObject roots = new JSONObject();
		roots.setString("schema", "laji-etl");
		JSONArray rootsArray = roots.getArray("roots");
		for (JSONObject o : bulk.values()) {
			rootsArray.appendObject(o);
		}
		HttpPost request = new HttpPost(uri.getURI());
		request.setHeader("content-type", "application/json");
		HttpEntity entity = new ByteArrayEntity(roots.reveal().toString().getBytes("UTF-8"));
		request.setEntity(entity);
		return client.execute(request);
	}

	@Override
	public void flush() {
		synchronized (bulk) {
			pushBulk();
		}
	}

}
