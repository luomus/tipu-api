package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class SpeciesAccuracyValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column speciesAccuracy) throws Exception {
		if (data.isForUpdate()) return;
		if (data.getMode() == Mode.RINGINGS) {
			validateRinging(data, speciesAccuracy);
		} else {
			validateRecovery(data);
		}
	}

	private void validateRecovery(RowValidationData data) throws Exception {
		if (data.isForAdminValidations()) {
			Column legRing = data.getRow().get("legRing");
			if (data.hasErrors(legRing)) return;
			RingHistory history = data.getDao().getRingHistoryByLegRing(legRing.getValue());
			if (!history.hasHistory()) return;
			Row ringing = history.getRinging();
			if (ringing.get("speciesAccuracy").getValue().equals("E")) {
				data.addWarning("ADMIN_ringed_species_not_accurate", data.getRow().get("species"), ringing.get("nameRing").getValue());
			}
		}
	}

	private void validateRinging(RowValidationData data, Column speciesAccuracy) {
		if (!speciesAccuracy.hasValue()) return;
		if (data.isForUserValidations()) {
			data.addWarning("accuracy_given_ask_for_information_warning", speciesAccuracy);
		} else {
			if (!speciesAccuracy.getValue().equals("E")) {
				data.addWarning("ADMIN_species_accuracy_non_E_given", speciesAccuracy, speciesAccuracy.getValue());
			} else {
				data.addWarning("ADMIN_ringed_species_not_accurate", speciesAccuracy);
			}
		}
	}

}
