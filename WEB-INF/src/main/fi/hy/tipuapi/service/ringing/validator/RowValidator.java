package fi.hy.tipuapi.service.ringing.validator;

import java.util.Calendar;
import java.util.Collection;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.EnumerationValues;
import fi.hy.tipuapi.service.ringing.models.Field;
import fi.hy.tipuapi.service.ringing.models.RowValidationResponseData;
import fi.hy.tipuapi.service.ringing.validator.validators.UpdateDeleteValidator;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;


public class RowValidator {
	public static final boolean ADMIN_VALIDATIONS = true;
	public static final boolean USER_VALIDATIONS = false;

	private static final int MIN_YEAR = 1880;
	private static final int MAX_YEAR = 2080;

	private final RowValidationData data;

	public RowValidator(RowValidationData validationData) {
		this.data = validationData;
	}

	public RowValidationResponseData getValidationResponse() {
		typeValidate(data);
		if (!data.isForMassUpdate()) {
			nonTypeValidate(data);
		}
		return new RowValidationResponseData(data);
	}

	private void typeValidate(RowValidationData data) {
		for (Column c : data.getRow()) {
			typeValidate(c);
		}
	}

	private boolean typeValidate(Column c) {
		if (!c.hasValue()) return true;
		Field columnDescription = c.getFieldDescription();
		if (columnDescription.isDate()) {
			return validateDate(c);
		} else if (columnDescription.isInteger()) {
			return validateIngeger(c);
		} else if (columnDescription.isDecimal()) {
			return validateDecimal(c);
		} else if (columnDescription.isEnumeration()) {
			return validateEnumeration(c);
		}
		return validateLength(c);
	}

	private boolean validateEnumeration(Column c) {
		if (c.getName().equals("modifiedBy")) return true;
		EnumerationValues v = c.getFieldDescription().getEnumerationValues();
		if (data.isForAdminValidations() && c.getName().equals("species")) {
			v = data.getDao().getRowStructure().get("ringedSpecies").getEnumerationValues(); // All species not only TipuApiClient.SELECTED_SPECIES
		}
		if (!v.contains(c.getValue())) {
			error("invalid_enumeration_value", c);
			return false;
		}
		return true;
	}

	private void error(String errorName, Column column) {
		data.addError(errorName, column);
	}

	private boolean validateDecimal(Column c) {
		try {
			Double.valueOf(c.getValue());

		} catch (NumberFormatException e) {
			error("invalid_decimal", c);
			return false;
		}

		int maxLength = c.getFieldDescription().getLength();
		int maxScale = c.getFieldDescription().getDecimalAccuracy();
		if (maxScale <0) {
			throw new IllegalStateException("No decimal accuracy for " + c.getName());
		}

		String value = negativeSignRemovedValue(c);
		String[] values = Utils.splitDecimal(value);

		if (values[0].length() > maxLength - maxScale)  {
			error("invalid_length_too_long", c);
			return false;
		}
		if (values[1].length() > maxScale + 1) {
			error("decimal_too_precise", c);
			return false;
		}
		return true;
	}

	private boolean validateIngeger(Column c) {
		if (isInteger(c.getValue())) {
			return validateLength(c);
		}
		error("invalid_integer", c);
		return false;
	}

	private boolean validateLength(Column c) {
		int maxLength = c.getFieldDescription().getLength();
		if (maxLength < 1) {
			throw new IllegalStateException("No length defined for " + c.getName());
		}
		String value = negativeSignRemovedValue(c);
		if (value.length() > maxLength) {
			error("invalid_length_too_long", c);
			return false;
		}
		return true;
	}

	private String negativeSignRemovedValue(Column c) {
		String value = c.getValue();
		if (c.getFieldDescription().isNumeric()) {
			if (value.startsWith("-")) {
				value = value.replaceFirst("-", "");
			}
		}
		return value;
	}

	private boolean isInteger(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean validateDate(Column c) {
		return validateDate(c, DateUtils.convertToDateValue(c.getValue()));
	}

	private boolean validateDate(Column c, DateValue date) {
		if (date.isEmpty()) return true;
		if (date.hasEmptyFields()) {
			error("invalid_date", c);
			return false;
		}
		if (!containsIntegers(Utils.collection(date.getYear(), date.getMonth(), date.getDay()))) {
			error("invalid_date", c);
			return false;
		}
		int dd = Integer.parseInt(date.getDay());
		int mm = Integer.parseInt(date.getMonth());
		int yyyy = Integer.parseInt(date.getYear());

		if (dd < 1 || dd > 31) {
			error("invalid_date", c);
			return false;
		}
		if (mm < 1 || mm > 12) {
			error("invalid_date", c);
			return false;
		}
		if ((yyyy < MIN_YEAR || yyyy > MAX_YEAR)) {
			error("invalid_date", c);
			return false;
		}

		Calendar k = Calendar.getInstance();
		k.setLenient(false);
		try {
			k.set(yyyy, mm - 1, dd);
			k.getTime();
		} catch (IllegalArgumentException e) {
			error("invalid_calendar_date", c);
			return false;
		}
		if (k.after(now())) {
			error("can_not_be_in_future", c);
			return false;
		}

		if (yyyy < DateUtils.getCurrentYear() - 1) {
			if (!data.isForMassDataEntry()) {
				warning("old_event", c);
			}
		}

		return true;
	}

	private void warning(String warningName, Column column) {
		data.addWarning(warningName, column);
	}

	private boolean containsIntegers(Collection<String> values) {
		for (String value : values) {
			if (!isInteger(value)) return false;
		}
		return true;
	}

	private Calendar now() {
		Calendar k = Calendar.getInstance();
		k.add(Calendar.SECOND, 1); // So that <somedate>.after(now()) will retun true if <somedate> and <now()> are the exact same moment
		return k;
	}

	private void nonTypeValidate(RowValidationData data) {
		if (!data.isForDelete()) {
			for (Column c : data.getRow()) {
				nonTypeValidate(c);
			}
		}

		if (data.isForUpdate() || data.isForDelete()) {
			FieldValidation updateDeleteValidations = new UpdateDeleteValidator();
			try {
				updateDeleteValidations.validate(data, data.getRow().get("id"));
			} catch (Exception e) {
				data.addError("could_not_complete_validation", data.getRow().get("id"));
				data.getErrorReporter().report("Update/Delete validator", e);
			}
		}
	}

	private void nonTypeValidate(Column column) {
		for (FieldValidation validation : column.getFieldValidationRules()) {
			try {
				validation.validate(data, column);
			} catch (Exception e) {
				data.addError("could_not_complete_validation", column);
				data.getErrorReporter().report("Validation for " + column.getName(), e);
			}
		}
	}

}
