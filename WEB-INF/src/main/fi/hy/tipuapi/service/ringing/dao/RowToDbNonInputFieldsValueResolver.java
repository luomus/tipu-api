package fi.hy.tipuapi.service.ringing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.tipuapi.service.CoordinateConversionService;
import fi.hy.tipuapi.service.ringing.RingFormatter;
import fi.hy.tipuapi.service.ringing.TimeDistanceDirection;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class RowToDbNonInputFieldsValueResolver {

	private final DAO dao;
	private final static Map<String, String> attachmentPointToRecoveryMethodMap = initAttachmentPointToRecoveryMethodMap();

	private static Map<String, String> initAttachmentPointToRecoveryMethodMap() {
		Map<String, String> map = new HashMap<>();
		map.put("A", "81");
		map.put("Y", "81");
		map.put("J", "81");
		map.put("K", "82");
		map.put("S", "83");
		return map;
	}

	public RowToDbNonInputFieldsValueResolver(DAO dao) {
		this.dao = dao;
	}

	public List<Row> fillValues(Row row, Mode mode, DAO.Action action) throws Exception {
		fillCommon(row);
		if (mode == Mode.RECOVERIES) {
			fillRecoveryValues(row);
			return Utils.list(row);
		}
		return fillRingingValuesPossiblyExplodeRingSeries(row, action);
	}

	private List<Row> fillRingingValuesPossiblyExplodeRingSeries(Row row, DAO.Action action) {
		fillRingingValues(row);

		Column ringStart = row.get("ringStart");
		Column ringEnd = row.get("ringEnd");
		Column legRing = row.get("legRing");

		if (action == DAO.Action.INSERT && ringStart.hasValue()) {
			return explodeRingSeries(row, ringStart, ringEnd);
		}
		if (action == DAO.Action.INSERT) {
			ringStart.setValue(legRing.getValue());
		}
		row.get("newLegRing").setValue(legRing.getValue());
		row.get("nameRing").setValue(legRing.getValue());
		return Utils.list(row);
	}

	private List<Row> explodeRingSeries(Row row, Column ringStart, Column ringEnd) {
		Ring start = new Ring(ringStart.getValue());
		List<Row> explodedRows = new ArrayList<>();

		if (ringEnd.hasValue()) {
			explode(row, ringEnd, start, explodedRows);
		} else {
			row.get("legRing").setValue(ringStart.getValue());
			row.get("newLegRing").setValue(ringStart.getValue());
			row.get("nameRing").setValue(ringStart.getValue());
			explodedRows.add(row);
		}
		return explodedRows;
	}

	private void fillRingingValues(Row row) {
		if (row.get("fieldReadableCode").hasValue()) {
			row.get("fieldReadableChanges").setValue("L");
		}
		cleanForeignRing(row);
	}

	private void cleanForeignRing(Row row) {
		Column foreignRing = row.get("foreignRing");
		if (foreignRing.hasValue()) {
			foreignRing.setValue(cleanForeignRing(foreignRing.getValue()));
		}
	}

	private String cleanForeignRing(String value) {
		return RingFormatter.removeSpecialChars(value.toUpperCase());
	}

	private void explode(Row row, Column ringEnd, Ring start, List<Row> explodedRows) {
		Ring end = new Ring(ringEnd.getValue());
		for (int i = start.getNumbers(); i <= end.getNumbers(); i++) {
			Row explodedRow = dao.emptyRow().fillValues(row);
			String explodedRing = start.getSeries() + " " + i;
			explodedRow.get("legRing").setValue(explodedRing);
			explodedRow.get("newLegRing").setValue(explodedRing);
			explodedRow.get("nameRing").setValue(explodedRing);
			explodedRows.add(explodedRow);
		}
	}

	private void fillCommon(Row row) throws Exception {
		clearRingEndIfSameThanRingStart(row);
		convertCoordinates(row);
		handleClutchNumberOwner(row);
		resolveFieldReadableChangeEuringCode(row);
		euringProvinceByMunicipality(row);
		fieldReadableAttachmentAndCodeColorAndAlignFromDistribution(row);
	}

	private void clearRingEndIfSameThanRingStart(Row row) {
		Column ringStart = row.get("ringStart");
		Column ringEnd = row.get("ringEnd");
		if (ringEnd.hasValue() && ringStart.hasValue()) {
			Ring start = new Ring(ringStart.getValue());
			Ring end = new Ring(ringEnd.getValue());
			if (start.getSeries().equals(end.getSeries()) && start.getNumbers() == end.getNumbers()) {
				ringEnd.setValue("");
			}
		}
	}

	private void fieldReadableAttachmentAndCodeColorAndAlignFromDistribution(Row row) throws Exception {
		Column fieldReadableCode = row.get("fieldReadableCode");
		Column attachmentPoint = row.get("attachmentPoint");
		Column fieldReadableChanges = row.get("fieldReadableChanges");
		Column codeColor = row.get("codeColor");
		Column codeAlign = row.get("codeAlign");

		if (fieldReadableAttached(fieldReadableChanges)) {
			if (!attachmentPoint.hasValue() || !codeColor.hasValue() || !codeAlign.hasValue()) {
				Row distributionInfo = dao.getFieldReadableDistributionInfo(fieldReadableCode.getValue(), row.get("mainColor").getValue(), row.get("species").getValue());
				if (!attachmentPoint.hasValue()) {
					attachmentPoint.setValue(distributionInfo.get("attachmentPoint").getValue());
				}
				if (!codeColor.hasValue()) {
					codeColor.setValue(distributionInfo.get("codeColor").getValue());
				}
				if (!codeAlign.hasValue()) {
					codeAlign.setValue(distributionInfo.get("codeAlign").getValue());
				}
			}
		}
	}

	private void euringProvinceByMunicipality(Row row) throws Exception {
		Column municipality = row.get("municipality");
		Column euringProvinceCode = row.get("euringProvinceCode");
		if (municipality.hasValue()) {
			fillEuringProvinceByMunicipality(euringProvinceCode, municipality);
		}
	}

	private boolean fieldReadableAttached(Column fieldReadableChanges) {
		return fieldReadableChanges.getValue().equals("L") || fieldReadableChanges.getValue().equals("V");
	}

	private void resolveFieldReadableChangeEuringCode(Row row) {
		Column euringCode = row.get("fieldReadableChangeEuringCode");
		if (euringCode.hasValue()) return;

		Column chandedOrRemoved = row.get("fieldReadableChanges");
		Column fieldReadableRing = row.get("fieldReadableCode");

		if (chandedOrRemoved.hasValue()) {
			if (chandedOrRemoved.getValue().equals("P")) {
				euringCode.setValue("3");
				return;
			}
			if (chandedOrRemoved.getValue().equals("V") || chandedOrRemoved.getValue().equals("L")) {
				euringCode.setValue("2");
				return;
			}
			throw new IllegalStateException("Uknown fieldreadable ring changed or removed value: " + chandedOrRemoved.getValue());
		}

		if (fieldReadableRing.hasValue()) {
			euringCode.setValue("1");
			return;
		}
	}

	private void handleClutchNumberOwner(Row row) {
		Column clutchNumber = row.get("clutchNumber");
		Column someoneElsesClutchNumber = row.get("someoneElsesClutchNumber");
		Column clutchNumberOwner = row.get("clutchNumberOwner");

		if (someoneElsesClutchNumber.hasValue() && clutchNumber.hasValue()) {
			throw new IllegalStateException("Both clutch numbers set");
		}
		if (someoneElsesClutchNumber.hasValue()) {
			clutchNumber.setValue(someoneElsesClutchNumber.getValue());
			someoneElsesClutchNumber.setValue("");
			return;
		}
		if (clutchNumber.hasValue()) {
			clutchNumberOwner.setValue(row.get("ringer").getValue());
		}
	}

	public static void convertCoordinates(Row row) throws Exception {
		String lat = row.get("lat").getValue();
		String lon = row.get("lon").getValue();
		String coordinateSystem = row.get("coordinateSystem").getValue();
		if (!given(lat, lon, coordinateSystem)) return;

		Node response = CoordinateConversionService.convert(lat, lon, coordinateSystem).getRootNode();
		if (response.hasAttribute("pass") && response.getAttribute("pass").equals("false")) {
			throw new IllegalStateException("Coordinate conversion failed: " + Utils.debugS(lat, lon, coordinateSystem));
		}

		Node wgs84Decimal = response.getNode("wgs84");
		Node wgs84Degrees = response.getNode("wgs84-dms");
		Node ykj = response.getNode("ykj");
		Node euref = response.getNode("euref");

		if (finnishPlace(row)) {
			row.get("uniformLat").setValue(ykj.getAttribute("lat"));
			row.get("uniformLon").setValue(ykj.getAttribute("lon"));
			row.get("eurefLat").setValue(euref.getAttribute("lat"));
			row.get("eurefLon").setValue(euref.getAttribute("lon"));
		}
		row.get("wgs84DecimalLat").setValue(wgs84Decimal.getAttribute("lat"));
		row.get("wgs84DecimalLon").setValue(wgs84Decimal.getAttribute("lon"));
		row.get("wgs84DegreeLat").setValue(wgs84Degrees.getAttribute("lat"));
		row.get("wgs84DegreeLon").setValue(wgs84Degrees.getAttribute("lon"));
	}

	private static boolean finnishPlace(Row row) {
		return row.get("municipality").hasValue();
	}

	private static boolean given(String ... strings) {
		for (String s : strings) {
			if (!given(s)) return false;
		}
		return true;
	}

	private boolean given(Column ... columns) {
		for (Column c : columns) {
			if (!given(c.getValue())) return false;
		}
		return true;
	}

	private void fillRecoveryValues(Row row) throws Exception {
		String nameRing = resolveNameRing(row);
		row.get("nameRing").setValue(nameRing);
		row.get("recoveryMethod").setValue(resolveRecoveryMethod(row));
		row.get("birdConditionEURING").setValue(resolvebirdConditionEURING(row));

		cleanForeignRing(row);

		Column sourceOfRecovery = row.get("sourceOfRecovery");
		if (!sourceOfRecovery.hasValue()) {
			sourceOfRecovery.setValue(DAO.RECOVERY_TYPE_CONTROL);
		}

		calculateTimeDistanceDirection(row, nameRing);
	}

	private void fillEuringProvinceByMunicipality(Column euringProvinceCode, Column municipality) throws Exception {
		TipuApiResource municipalities = dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES);
		Node municipalityInfo = municipalities.getById(municipality.getValue());
		String euringProvinceOfMunicipality = municipalityInfo.getNode("euring-province").getContents();
		euringProvinceCode.setValue(euringProvinceOfMunicipality);
	}

	private void calculateTimeDistanceDirection(Row row, String nameRing) throws Exception {
		RingHistory ringHistory = dao.getRingHistoryByNameRing(nameRing);
		calculateTimeDistanceDirection(row, nameRing, ringHistory);
	}

	public static void calculateTimeDistanceDirection(Row row, String nameRing, RingHistory ringHistory) {
		if (!ringHistory.hasHistory()) {
			throw new IllegalStateException("No history for " + nameRing);
		}
		JSONObject timeDistanceDirectionData = TimeDistanceDirection.countTimeDistanceDirection(row, ringHistory.getRinging());
		if (timeDistanceDirectionData.hasKey("distanceInKilometers") && timeDistanceDirectionData.hasKey("bearingInDegrees")) {
			row.get("distanceToRingingInKilometers").setValue(timeDistanceDirectionData.getInteger("distanceInKilometers"));
			row.get("directionToRingingInDegrees").setValue(Long.toString(Math.round(timeDistanceDirectionData.getDouble("bearingInDegrees"))));
		}
		row.get("timeToRingingInDays").setValue(timeDistanceDirectionData.getInteger("timeInDays"));
	}

	private String resolveNameRing(Row row) throws Exception {
		Column legRing = row.get("legRing");
		Column fieldReadableCode = row.get("fieldReadableCode");
		Column fieldReadableRingColor = row.get("mainColor");
		Column species = row.get("species");

		String resolvedNameRing = null;
		if (legRing.hasValue()) {
			resolvedNameRing = dao.getNameRing(legRing.getValue());
			if (!given(resolvedNameRing)) {
				throw new IllegalStateException("Name ring could not be resolved for " + legRing);
			}
		} else {
			if (!given(fieldReadableCode, fieldReadableRingColor, species)) {
				throw new IllegalStateException("Leg ring was not given and fieldreadable ring not given either.");
			}
			List<String> nameRings = dao.getNameRingByFieldReadable(fieldReadableCode.getValue(), fieldReadableRingColor.getValue(), species.getValue());
			if (nameRings.size() != 1) {
				throw new IllegalStateException("Could not resolve name ring for " + Utils.debugS(fieldReadableCode.getValue(), fieldReadableRingColor.getValue() + ". Found " + nameRings.size() + " matches"));
			}
			resolvedNameRing = nameRings.get(0);
			if (!given(resolvedNameRing)) {
				throw new IllegalStateException("Name ring could not be resolved for " + Utils.debugS(fieldReadableCode.getValue(), fieldReadableRingColor.getValue()));
			}
		}
		return resolvedNameRing;
	}

	private static boolean given(String string) {
		return string != null && string.length() > 0;
	}

	private String resolvebirdConditionEURING(Row row) {
		Column birdConditionEURING =  row.get("birdConditionEURING");
		if (birdConditionEURING.hasValue()) return birdConditionEURING.getValue();

		if (row.get("birdCondition").getValue().equals("O")) {
			return "2";
		}

		if (RowUtils.birdDead(row)) {
			return "2";
		}

		return "8";
	}

	private String resolveRecoveryMethod(Row row) throws Exception {
		Column recoveryMethod = row.get("recoveryMethod");
		if (recoveryMethod.hasValue()) {
			return recoveryMethod.getValue();
		}

		if (row.get("birdCondition").getValue().equals("O")) {
			return "8";
		}

		if (RowUtils.birdDead(row)) {
			return "8";
		}

		String captureMethod = row.get("captureMethod").getValue();
		if (captureMethod.equals("R")) { // R = Ring number of metal ring read in field without the bird being caught
			return "28";
		}

		if (identifiedFromFieldReadableRing(row) || captureMethod.equals("T")) { // T = Coloured or numbered mark read in field without the bird being caught.
			Column code = row.get("fieldReadableCode");
			Column color = row.get("mainColor");
			Column species = row.get("species");
			if (code.hasValue() && color.hasValue() && species.hasValue()) {

				String attachmentPoint = row.get("attachmentPoint").getValue();
				if (!given(attachmentPoint)) {
					Row distInfo = dao.getFieldReadableDistributionInfo(code.getValue(), color.getValue(), species.getValue());
					attachmentPoint = distInfo.get("attachmentPoint").getValue();
				}
				if (attachmentPointToRecoveryMethodMap.containsKey(attachmentPoint)) {
					return attachmentPointToRecoveryMethodMap.get(attachmentPoint);
				}
			}
			return "29";
		}

		return "20";
	}

	private static boolean identifiedFromFieldReadableRing(Row row) {
		return !row.get("legRing").hasValue();
	}


}
