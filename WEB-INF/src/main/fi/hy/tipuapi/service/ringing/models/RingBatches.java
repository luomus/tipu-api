package fi.hy.tipuapi.service.ringing.models;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import fi.hy.tipuapi.service.ringing.models.DistributedRings.Distribution;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.utils.Utils;

public class RingBatches {

	public static class RingBatch implements Iterable<Integer>, Comparable<RingBatch> {

		private final String series;
		private int start;
		private int end;
		private boolean used = false;
		private Distribution distribution = null;

		public RingBatch(String series, int start, int end) {
			this.series = series;
			this.start = start;
			this.end = end;
		}

		@Override
		public Iterator<Integer> iterator() {
			return new Iterator<Integer>() {

				private int current = start;
				@Override
				public Integer next() {
					if (!hasNext()) {
						throw new NoSuchElementException();
					}
					int retVal = current;
					current++;
					return retVal;
				}

				@Override
				public boolean hasNext() {
					return current <= end;
				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}

		public int start() {
			return start;
		}

		public int end() {
			return end;
		}

		public RingBatch setDistribution(Distribution distribution) {
			this.distribution = distribution;
			return this;
		}

		public Distribution getDistribution() {
			return distribution;
		}

		public String getSeries() {
			return series;
		}

		@Override
		public String toString() {
			if (distribution != null)
				return Utils.debugS(series, start, end, distribution);
			if (used) {
				return Utils.debugS(series, start, end, "used");
			}
			return Utils.debugS(series, start, end);
		}

		@Override
		public int compareTo(RingBatch o) {
			int c = this.series.compareTo(o.series);
			if (c != 0) return c;
			return Integer.valueOf(this.start).compareTo(Integer.valueOf(o.start));
		}

		@Override
		public boolean equals(Object other) {
			if (other instanceof RingBatch) {
				return this.toString().equals(other.toString());
			}
			throw new UnsupportedOperationException("Can only compare " + this.getClass().getName() + " to same class.");
		}

		@Override
		public int hashCode() {
			return this.toString().hashCode();
		}

		public RingBatch joinGreater(RingBatch ringBatch) {
			this.end = ringBatch.end;
			return this;
		}

		public RingBatch joinLesser(RingBatch ringBatch) {
			this.start = ringBatch.start;
			return this;
		}

		public boolean distributionMatches(RingBatch other) {
			if (this.distribution == null && other.distribution == null) {
				return true;
			}
			if (this.distribution == null && other.getDistribution() != null) {
				return false;
			}
			if (this.distribution != null && other.getDistribution() == null) {
				return false;
			}
			return this.distribution.equals(other.getDistribution());
		}

		public RingBatch setUsed(boolean isUsed) {
			this.used = isUsed;
			return this;
		}

		public boolean isUsed() {
			return used;
		}

	}

	private final Comparator<String> ringSeriesComparator;
	private final SortedMap<String, TreeSet<RingBatch>> seriesBatches;

	public RingBatches(Comparator<String> ringSeriesComparator) {
		this.ringSeriesComparator = ringSeriesComparator;
		this.seriesBatches = new TreeMap<>(ringSeriesComparator);
	}


	public RingBatches addAll(RingBatches batches) {
		batches.seriesBatches.values().forEach(set->set.forEach(b->this.add(b)));
		return this;
	}

	public RingBatch add(RingBatch ringBatch) {
		String series = ringBatch.getSeries();
		if (!seriesBatches.containsKey(series)) {
			seriesBatches.put(series, new TreeSet<RingBatch>());
		}
		boolean joined = false;

		RingBatch joinLeft = null;
		RingBatch joinRight = null;

		if (this.contains(series, ringBatch.start-1)) {
			RingBatch joinTo = getBatch(series, ringBatch.start-1);
			if (distributionAndUseMatch(ringBatch, joinTo)) {
				joinLeft = joinTo;
				ringBatch = joinTo.joinGreater(ringBatch);
				joined = true;
			}
		}
		if (this.contains(series, ringBatch.end+1)) {
			RingBatch joinTo = getBatch(series, ringBatch.end+1);
			if (distributionAndUseMatch(ringBatch, joinTo)) {
				joinRight = joinTo;
				ringBatch = joinTo.joinLesser(ringBatch);
				joined = true;
			}
		}

		if (joinLeft != null && joinRight != null) {
			if (distributionAndUseMatch(joinLeft, joinRight)) {
				seriesBatches.get(series).remove(joinRight);
				ringBatch = joinLeft.joinGreater(joinRight);
			}
		}

		if (!joined) {
			seriesBatches.get(series).add(ringBatch);
		}
		return ringBatch;
	}

	private boolean distributionAndUseMatch(RingBatch ringBatch, RingBatch joinTo) {
		return joinTo.distributionMatches(ringBatch) && joinTo.used == ringBatch.used;
	}

	public RingBatch add(String ring) {
		return add(ring, ring);
	}

	public RingBatch add(String ringStartDb, String ringEndDb) {
		Ring start = new Ring(ringStartDb);
		Ring end = new Ring(ringEndDb);
		if (!start.getSeries().equals(end.getSeries())) {
			throw new IllegalStateException("Ring distribution not for the same series: " + Utils.debugS(start, end));
		}

		if (start.getNumbers() > end.getNumbers()) {
			throw new IllegalStateException("Ring distribution number start larger than number end: " + Utils.debugS(start, end));
		}
		RingBatch ringBatch = new RingBatch(start.getSeries(), start.getNumbers(), end.getNumbers());
		return add(ringBatch);
	}

	public Set<String> getSortedSeries() {
		return seriesBatches.keySet();
	}

	public TreeSet<RingBatch> getBatches(String serie) {
		return seriesBatches.get(serie);
	}

	public boolean contains(String ring) {
		return contains(new Ring(ring));
	}

	private boolean contains(Ring ring) {
		return contains(ring.getSeries(), ring.getNumbers());
	}

	private RingBatch getBatch(Ring ring) {
		return getBatch(ring.getSeries(), ring.getNumbers());
	}

	public boolean contains(String serie, int number) {
		return getBatch(serie, number) != null;
	}

	private RingBatch getBatch(String series, int number) {
		if (!seriesBatches.containsKey(series)) {
			return null;
		}

		TreeSet<RingBatch> treeSet = seriesBatches.get(series);
		RingBatch floor = treeSet.floor(new RingBatch(series, number, number));
		if (floor == null) {
			return null;
		}

		if (floor.end < number) {
			return null;
		}

		return floor;
	}

	public Distribution getDistribution(Ring ring) {
		RingBatch ringBatch = getBatch(ring);
		if (ringBatch == null) return null;
		return ringBatch.getDistribution();
	}

	public RingBatches explodeBy(RingBatches ringersUsedRings) {
		RingBatches exploded = new RingBatches(ringSeriesComparator);
		for (String series : this.getSortedSeries()) {
			for (RingBatch ringBatch : this.getBatches(series)) {

				int start = ringBatch.start;
				boolean used = ringersUsedRings.contains(series, start);

				for (int number : ringBatch) {
					boolean thisIsIncluded = ringersUsedRings.contains(series, number);
					if (used != thisIsIncluded) {
						exploded.add(new RingBatch(series, start, number-1).setUsed(used)).setDistribution(ringBatch.getDistribution());
						start = number;
						used = !used;
					}
				}
				exploded.add(new RingBatch(series, start, ringBatch.end).setUsed(used).setDistribution(ringBatch.getDistribution()));

			}
		}
		return exploded;
	}

	public String getNext(Ring previousRing) {
		String series = previousRing.getSeries();
		int number = previousRing.getNumbers();
		RingBatch ringBatch = getBatch(series, number);
		if (ringBatch.end == number) {
			ringBatch = seriesBatches.get(series).ceiling(new RingBatch(series, number+1, number+1));
			if (ringBatch == null) return null;
			return series + ringBatch.start;
		}
		return series + (number + 1);
	}

}
