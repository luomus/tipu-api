package fi.hy.tipuapi.service.ringing.models;

import fi.luomus.commons.utils.Utils;

public class FieldReadable implements Comparable<FieldReadable> {

	private final String code;
	private final String backgroundColor;
	private final String shortenedSpeciesCode;
	private final String toString;
	
	public FieldReadable(String code, String backgroundColor, String species) {
		if (code == null) code = "";
		this.code = code.toUpperCase();
		if (backgroundColor == null) backgroundColor = "";
		this.backgroundColor = backgroundColor.toUpperCase();
		if (species == null) {
			this.shortenedSpeciesCode = ""; 
		} else {
			this.shortenedSpeciesCode = shortenSpecies(species).toUpperCase();
		}
		this.toString = toCatenadedString();
	}

	private String toCatenadedString() {
		return Utils.debugS("code", code, "backgound color", backgroundColor, "species", shortenedSpeciesCode);
	}
	
	private String shortenSpecies(String species) {
		if (species == null) return "";
		if (species.length() > 6) {
			species = species.substring(0, 6);
		}
		return species;
	}

	public String getCode() {
		return code;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public String getShortenedSpeciesCode() {
		return shortenedSpeciesCode;
	}
	
	@Override
	public int hashCode() {
		return toString.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		return this.toString.equals(o.toString());
	}
	
	@Override
	public String toString() {
		return toString;
	}

	@Override
	public int compareTo(FieldReadable o) {
		return this.toString().compareTo(o.toString());
	}
	
}
