package fi.hy.tipuapi.service.ringing;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zaxxer.hikari.HikariDataSource;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DAOImple.DAOImpleBuilder;
import fi.hy.tipuapi.service.ringing.dao.DatasourceDefinition;
import fi.hy.tipuapi.service.ringing.dao.LajiETLDAO;
import fi.hy.tipuapi.service.ringing.dao.LajiETLDAOImple;
import fi.hy.tipuapi.service.ringing.dao.SharedCachedResources;
import fi.hy.tipuapi.service.ringing.dao.WarehousingDAO;
import fi.hy.tipuapi.service.ringing.dao.WarehousingDAOImple;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Logger;

public abstract class BaseServlet extends fi.luomus.commons.services.BaseServlet {

	private static final long serialVersionUID = -1615403062353237458L;

	private static final Object LOCK = new Object();

	private static RowStructure structure;
	private static DAOImpleBuilder daoBuilder;
	private static HikariDataSource dataSource;
	private static SharedCachedResources sharedCachedResources;

	@Override
	protected String configFileName() {
		return "tipu-api.properties";
	}

	@Override
	protected void applicationInit() {
	}

	@Override
	protected void applicationInitOnlyOnce() {
	}

	@Override
	protected void applicationDestroy() {
		if (warehousingDAO != null) {
			try {
				warehousingDAO.close();
			} catch (Exception e) {}
		}
		if (etlDAO != null) {
			try {
				etlDAO.close();
			} catch (Exception e) {}
		}
		if (dataSource != null) {
			try {
				dataSource.close();
			} catch (Exception e) {}
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	protected ResponseData print(JSONObject response, HttpServletResponse res) throws IOException {
		res.setContentType("application/json; charset=utf-8");
		PrintWriter out = res.getWriter();
		out.write(response.reveal().toString());
		out.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

	protected String getText(String text, String locale) {
		Map<String, String> texts = getLocalizedTexts().getAllTexts(locale);
		String localizedText = texts.get(text);
		if (given(localizedText)) return localizedText;
		return text;
	}

	protected DAO createNewDAO(HttpServletRequest req) throws Exception {
		return createNewDAO(getUserId(req), req.getParameter("forceReload") != null);
	}

	private String getUserId(HttpServletRequest req) {
		return req.getParameter("userid");
	}

	protected DAO createNewDAO(String userId, boolean forceReload) throws Exception {
		if (staticNotInitialized() || forceReload) {
			initStaticDaoComponents(forceReload);
		}
		daoBuilder.setLogger(generateLogger());
		return daoBuilder.openForUserId(userId);
	}

	private Logger generateLogger() {
		File logFile = new File(getConfig().logFolder() + File.separator + "rows-to-db-log_" + DateUtils.getCurrentDateTime("yyyy-MM") + ".txt");
		Logger logger = new Logger(logFile);
		return logger;
	}

	private boolean staticNotInitialized() {
		return !staticInitialized;
	}

	private static boolean staticInitialized = false;

	private void initStaticDaoComponents(boolean forceReload) throws Exception {
		synchronized (LOCK) {
			if (staticInitialized && !forceReload) return;
			if (dataSource == null) {
				dataSource = DatasourceDefinition.initDataSource(getConfig());
			}
			if (structure == null || forceReload) {
				structure = new RowStructure(getConfig(), getLocalizedTexts(), forceReload);
			}
			if (sharedCachedResources == null || forceReload) {
				sharedCachedResources = new SharedCachedResources(getConfig(), dataSource);
			}
			if (daoBuilder == null || forceReload) {
				daoBuilder = new DAOImpleBuilder()
						.setDataSource(dataSource)
						.setSharedResources(sharedCachedResources)
						.setStructure(structure);
			}
			staticInitialized = true;
		}
	}

	protected DAOImpleBuilder getDAOBuilder() throws Exception {
		if (staticNotInitialized()) {
			initStaticDaoComponents(false);
		}
		return daoBuilder;
	}

	private static LajiETLDAO etlDAO = null;

	protected LajiETLDAO getETLDao() throws Exception {
		if (etlDAO == null) {
			etlDAO = initEtlDAO();
		}
		return etlDAO;
	}

	private LajiETLDAO initEtlDAO() throws Exception {
		synchronized (LOCK) {
			if (etlDAO != null) return etlDAO;
			return new LajiETLDAOImple(getErrorReporter(), getConfig(), getDAOBuilder());
		}
	}

	private static WarehousingDAO warehousingDAO = null;

	protected WarehousingDAO getWarehousingDAO() throws Exception {
		if (warehousingDAO == null) {
			warehousingDAO = initWarehousingDAO();
		}
		return warehousingDAO;
	}

	private WarehousingDAO initWarehousingDAO() throws Exception {
		synchronized (LOCK) {
			if (warehousingDAO != null) return warehousingDAO;
			return new WarehousingDAOImple(getETLDao(), getErrorReporter(), getDAOBuilder());
		}
	}

	protected static boolean given(String value) {
		return value != null && value.length() > 0;
	}

	protected JSONObject error(String errorName, String localizedErrorText) {
		JSONObject error = new JSONObject();
		error.setString("errorName", errorName);
		error.setString("localizedErrorText", localizedErrorText);
		return error;
	}

	protected JSONObject generateExceptionResponse(Exception e) {
		JSONObject response;
		response = new JSONObject();
		response.setInteger("countOfRows", 0);
		response.setBoolean("success", false);
		response.getArray("errors").appendObject(error(e.getClass().getName(), e.getMessage()));
		response.getArray("rows");
		return response;
	}

	protected List<Integer> getIds(HttpServletRequest req) {
		String recoveryIds = req.getParameter("ids");
		if (!given(recoveryIds)) {
			throw new IllegalArgumentException("ids parameter must be given. It should contain one id or a list of ids separated by a comma.");
		}
		List<Integer> ids = new ArrayList<>();
		for (String id : recoveryIds.split(",")) {
			id = id.trim();
			try {
				ids.add(Integer.valueOf(id));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Non-numeric id given: " + id);
			}
		}
		return ids;
	}

	protected Mode getMode(HttpServletRequest req) {
		String mode = req.getParameter("mode");
		if (mode == null) throw new IllegalArgumentException("You must give 'mode' parameter. It's value must be either 'ringings' or 'recoveries'.");
		if (mode.equals("ringings")) {
			return Mode.RINGINGS;
		}
		if (mode.equals("recoveries")) {
			return Mode.RECOVERIES;
		}
		throw new IllegalArgumentException("Unknown mode given: "+mode+". Mode must be either 'ringings' or 'recoveries'.");
	}

	private static Boolean notProductionMode = null;

	protected boolean notProductionMode() {
		if (notProductionMode == null) {
			notProductionMode = getConfig().developmentMode() || getConfig().stagingMode();
		}
		return notProductionMode;
	}

	protected String getResourceId(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return "";
		}
		String id = path.substring(path.lastIndexOf("/")+1);
		return id;
	}

	protected DAO.Action getAction(HttpServletRequest req) {
		String action = req.getParameter("action");
		if (!given(action) || action.equalsIgnoreCase("insert")) {
			return DAO.Action.INSERT;
		}
		if (action.equalsIgnoreCase("update")) {
			return DAO.Action.UPDATE;
		}
		if (action.equalsIgnoreCase("delete")) {
			return DAO.Action.DELETE;
		}
		if (action.equalsIgnoreCase("mass_update")) {
			return DAO.Action.MASS_UPDATE;
		}
		throw new IllegalArgumentException("Unknown action: " + action);
	}


}
