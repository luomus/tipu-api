package fi.hy.tipuapi.service.ringing.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.reporting.ErrorReporter;

public class RowValidationData {

	private final Row row;
	private boolean adminValidations = false;
	private final Mode mode;
	private final DAO.Action action;
	private final Config config;
	private final ErrorReporter errorReporter;
	private final DAO dao;
	private final Map<String, String> localizedTexts;
	private final Map<String, List<ValidationErrorOrWarning>> errors = new HashMap<>();
	private final Map<String, List<ValidationErrorOrWarning>> warnings = new HashMap<>();
	private String userid = "";

	public RowValidationData(Row row, Mode mode, DAO.Action action, Config config, ErrorReporter errorReporter, DAO dao, Map<String, String> localizedTexts) {
		this.row = row;
		this.mode = mode;
		this.action = action;
		this.config = config;
		this.errorReporter = errorReporter;
		this.dao = dao;
		this.localizedTexts = localizedTexts;
	}

	public Row getRow() {
		return row;
	}

	public boolean isForUpdate() {
		return action == DAO.Action.UPDATE;
	}

	public boolean isForDelete() {
		return action == DAO.Action.DELETE;
	}

	public boolean isForInsert() {
		return action == DAO.Action.INSERT;
	}

	public boolean isForMassUpdate() {
		return action == DAO.Action.MASS_UPDATE;
	}

	public boolean isForAdminValidations() {
		return adminValidations;
	}

	public boolean isForUserValidations() {
		return !isForAdminValidations();
	}

	public Mode getMode() {
		return mode;
	}

	public Config getConfig() {
		return config;
	}

	public RowValidationData setAdminValidationMode(boolean adminValidations) {
		this.adminValidations = adminValidations;
		return this;
	}

	public static class ValidationErrorOrWarning {
		private final String errorName;
		private final Column column;
		private final String[] valuesToAppend;
		public ValidationErrorOrWarning(Column column, String errorName) {
			this(column, errorName, null);
		}
		public ValidationErrorOrWarning(Column column, String errorName, String[] valuesToAppend) {
			this.errorName = errorName;
			this.column = column;
			this.valuesToAppend = valuesToAppend;
		}
		public String getErrorName() {
			return errorName;
		}
		public Column getColumn() {
			return column;
		}
		public String[] getValuesToAppend() {
			return valuesToAppend;
		}
		@Override
		public String toString() {
			return column.getName() + ": " + errorName;
		}
	}

	public List<ValidationErrorOrWarning> getErrors() {
		return tolist(errors);
	}

	private List<ValidationErrorOrWarning> tolist(Map<String, List<ValidationErrorOrWarning>> map) {
		List<ValidationErrorOrWarning> list = new ArrayList<>();
		for (List<ValidationErrorOrWarning> columnsErrors : map.values()) {
			list.addAll(columnsErrors);
		}
		return list;
	}

	public  List<ValidationErrorOrWarning> getWarnings() {
		return tolist(warnings);
	}

	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	public boolean hasWarnings() {
		return !warnings.isEmpty();
	}

	public void addError(String errorName, Column column) {
		addoTo(errors, errorName, column, null);
	}

	public void addError(String errorName, Column column, String ... valuesToAppend) {
		addoTo(errors, errorName, column, valuesToAppend);
	}

	private void addoTo(Map<String, List<ValidationErrorOrWarning>> map, String errorName, Column column, String[] valuesToAppend) {
		if (!map.containsKey(column.getName())) {
			map.put(column.getName(), new ArrayList<ValidationErrorOrWarning>());
		}
		if (containsErrorOrWarning(map, errorName, column)) return;
		map.get(column.getName()).add(new ValidationErrorOrWarning(column, errorName, valuesToAppend));
	}

	private boolean containsErrorOrWarning(Map<String, List<ValidationErrorOrWarning>> map, String errorName, Column column) {
		if (!map.containsKey(column.getName())) return false;
		for (ValidationErrorOrWarning e : map.get(column.getName())) {
			if (e.getErrorName().equals(errorName)) return true;
		}
		return false;
	}

	public void addWarning(String warningName, Column column, String ... valuesToAppend) {
		addoTo(warnings, warningName, column, valuesToAppend);
	}

	public boolean noErrors(Column column) {
		return !errors.containsKey(column.getName());
	}

	public void require(Column column) {
		if (!column.hasValue()) {
			addError("required_field", column);
		}
	}

	public boolean notNullNoErrors(Column ... columns) {
		for (Column c : columns) {
			if (!notNullNoErrors(c)) return false;
		}
		return true;
	}

	public boolean notNullNoErrors(Column column) {
		return column.hasValue() && noErrors(column);
	}

	public ErrorReporter getErrorReporter() {
		return errorReporter;
	}

	public DAO getDao() {
		return dao;
	}

	public boolean hasErrors(Column column) {
		return !noErrors(column);
	}

	public boolean hasErrors(Column ...columns) {
		for (Column c : columns) {
			if (hasErrors(c)) return true;
		}
		return false;
	}

	public Map<String, String> getLocalizedTexts() {
		return localizedTexts;
	}

	public boolean hasWarning(Column c, String warningName) {
		return containsErrorOrWarning(warnings, warningName, c);
	}

	public boolean hasError(Column c, String warningName) {
		return containsErrorOrWarning(errors, warningName, c);
	}

	public RowValidationData setUserId(String userid) {
		if (userid == null) this.userid  = "";
		else this.userid = userid;
		return this;
	}

	public String getUserId() {
		return userid;
	}

	public boolean isForMassDataEntry() {
		return getRow().get("dataEntry").hasValue();
	}

}
