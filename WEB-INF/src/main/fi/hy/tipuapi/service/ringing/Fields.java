package fi.hy.tipuapi.service.ringing;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.EnumerationValues.EnumerationValue;
import fi.hy.tipuapi.service.ringing.models.Field;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Group;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

public class Fields extends BaseServlet {

	private static final long serialVersionUID = 936789512877669564L;

	private final Map<String, JSONObject> localizedFieldsResponse = new HashMap<>(3);

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		String locale = getLocale(req);
		boolean forceReload = req.getParameter("forceReload") != null;
		if (!localizedFieldsResponse.containsKey(locale) || forceReload) {
			localizedFieldsResponse.put(locale, generateFieldsResponse(req));
		}
		JSONObject fields = localizedFieldsResponse.get(locale);
		return print(fields, res);
	}

	private JSONObject generateFieldsResponse(HttpServletRequest req) throws Exception {
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			JSONObject response = new JSONObject();
			String locale = getLocale(req);
			JSONArray groups = response.getArray("groups");

			for (Group group : dao.getRowStructure()) {
				JSONObject groupJson = new JSONObject();
				groups.appendObject(groupJson);
				groupJson.setString("name", group.getName());
				groupJson.setString("description", getText(group.getName(), locale));

				JSONArray fields = groupJson.getArray("fields");
				for (Field field : group) {
					fields.appendObject(field(field, locale));
				}
			}
			return response;
		} finally {
			if (dao != null) dao.close();
		}
	}

	private JSONObject field(Field field, String locale) {
		JSONObject fieldJson = new JSONObject();
		fieldJson.setString("field", field.getName());
		fieldJson.setString("name", getText(field.getName(), locale));
		fieldJson.setString("description", getText(field.getName()+"_desc", locale));
		fieldJson.setString("type", field.getType());
		fieldJson.setString("dbType", field.getDbType());
		fieldJson.setString("dbName", field.getDatabaseColumn());
		
		if (field.isDecimal()) {
			fieldJson.setInteger("inputSize", field.getLength() + 1); // +1 is the comma
		} if (!field.isDate() && !field.isEnumeration()) {
			fieldJson.setInteger("inputSize", field.getLength());
		}		
		if (field.isEnumeration()) {
			JSONArray enumerationValues = fieldJson.getArray("enumerationValues");
			for (EnumerationValue enumeration : field.getEnumerationValues()) {
				JSONObject enumerationJson = new JSONObject();
				enumerationJson.setString("value", enumeration.getValue().equals("EMPTY") ? "" : enumeration.getValue());
				enumerationJson.setString("description", enumeration.getDescription(locale));
				enumerationJson.setString("detailedDescription", enumeration.getDetailedDescription(locale));
				enumerationValues.appendObject(enumerationJson);
			}
		}
		JSONArray modes = fieldJson.getArray("modes");
		for (Mode mode : field.getModes()) {
			modes.appendString(mode.toString());
		}
		fieldJson.setBoolean("isAggregated", field.isAggregated());
		fieldJson.setBoolean("required", field.isRequired());
		
		return fieldJson;
	}
	
}
