package fi.hy.tipuapi.service.ringing;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DAO.RowHandler;
import fi.hy.tipuapi.service.ringing.dao.WarehousingDAO;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class PushAllToWarehousing extends BaseServlet {

	private static final int BATCH_SIZE = 50000 - 1;
	private static final long serialVersionUID = 1478535390593430962L;
	private static final List<PushAllToWarehousingRunner> workers = new ArrayList<>();

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		boolean stopAll = "true".equals(req.getParameter("stopAll"));
		if (stopAll) {
			return stopAll(res);
		}
		boolean pushAll = "true".equals(req.getParameter("pushAll"));
		int startAt = req.getParameter("startAt") == null ? 1 : Integer.valueOf(req.getParameter("startAt"));
		if (!pushAll) {
			return error(res, new IllegalArgumentException("Must give pushAll parameter as a safety"));
		}
		pushAllInThread(startAt);
		return ok("loading started", res);
	}

	private ResponseData stopAll(HttpServletResponse res) throws Exception {
		for (PushAllToWarehousingRunner runner : workers) {
			runner.stopWorking();
		}
		return ok("stopping all", res);
	}

	private void pushAllInThread(final int startAt) {
		PushAllToWarehousingRunner runner = new PushAllToWarehousingRunner(startAt);
		new PushAllToWarehousingThread(runner).start();
		workers.add(runner);
	}

	private class PushAllToWarehousingThread extends Thread {
		public PushAllToWarehousingThread(Runnable runnable) {
			super(runnable);
		}
	}

	private static class RunControl {
		private volatile boolean run = true;
		public void stop() {
			run = false;
		}
		public boolean shouldContinue() {
			return run;
		}
	}

	private class PushAllToWarehousingRunner implements Runnable {
		private final int startAt;
		private final RunControl runControl = new RunControl();

		public PushAllToWarehousingRunner(int startAt) {
			this.startAt = startAt;
		}
		public void stopWorking() {
			runControl.stop();
		}
		@Override
		public void run() {
			pushAll(startAt);
		}

		private void pushAll(int startAt) {
			DAO dao = null;
			try {
				dao = createNewDAO("push-to-elastic", false);
				int start = startAt;
				int end = start + BATCH_SIZE;
				while (runControl.shouldContinue()) {
					PushToWarehouseRowHandler rowHandler = new PushToWarehouseRowHandler(getWarehousingDAO(), runControl);
					Utils.debug(start, end);
					Row searchParams = dao.emptyRow();
					searchParams.get("id").setSearchParam(start +"/" + end);
					dao.search(rowHandler, searchParams, null, null);
					if (!rowHandler.hadRows()) {
						break;
					}
					start = end + 1;
					end = start + BATCH_SIZE;
				}
				System.out.println("done");
			} catch (Exception e) {
				getErrorReporter().report("Push all to elastic", e);
			} finally {
				if (dao != null) dao.close();
			}
		}
	}

	private static class PushToWarehouseRowHandler implements RowHandler {
		private final WarehousingDAO warehouseDAO;
		private final RunControl runControl;
		private boolean hadRows = false;
		public PushToWarehouseRowHandler(WarehousingDAO warehouseDAO, RunControl runControl) {
			this.warehouseDAO = warehouseDAO;
			this.runControl = runControl;
		}
		public boolean hadRows() {
			return hadRows;
		}
		@Override
		public boolean handleRow(Row row) {
			hadRows = true;
			warehouseDAO.push(row);
			return runControl.shouldContinue();
		}
	}

	private ResponseData ok(String message, HttpServletResponse res) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(200);
		PrintWriter writer = res.getWriter();
		writer.write(message);
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

	private ResponseData error(HttpServletResponse res, Exception e) throws IOException {
		res.setContentType("text/plain");
		res.setStatus(500);
		PrintWriter writer = res.getWriter();
		writer.write(LogUtils.buildStackTrace(e));
		writer.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

}
