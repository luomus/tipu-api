package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class PositiveDecimalValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column column) throws Exception {
		if (data.notNullNoErrors(column)) {
			if (Double.valueOf(column.getValue()) <= 0.0) {
				data.addError("must_be_positive_decimal", column);
			}
		}
	}

}
