package fi.hy.tipuapi.service.ringing;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;

public class RingFormatter extends BaseServlet {

	private static final long serialVersionUID = -7608072270904888077L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (notProductionMode()) log(req);
		String param = req.getParameter("ring");
		try {
			String formattedRing = formatRing(param);
			JSONObject response = new JSONObject();
			response.setBoolean("success", true);
			response.setString("formattedRing", formattedRing);
			response.setString("originalParameter", "" + param);
			return print(response, res);
		} catch (Exception e) {
			JSONObject response = new JSONObject();
			response.setBoolean("success", false);
			response.setString("formattedRing", "" + param);
			response.setString("originalParameter", "" + param);
			return print(response, res);
		}
	}

	private String formatRing(String param) {
		if (!given(param)) return "";
		Ring ring = new Ring(removeSpecialChars(param.toUpperCase()));
		if (ring.validFormat()) {
			return LegacyRingCodeHandlerUtil.dbToActual(ring.toString());
		}
		return param;
	}

	private static final Set<Character> ALLOWED_RING_CHARS = initAllowedRingChars();
	private static Set<Character> initAllowedRingChars() {
		Set<Character> allowedRingChars = new HashSet<>();
		String allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for (char c : allowed.toCharArray()) {
			allowedRingChars.add(c);
		}
		return allowedRingChars;
	}

	public static String removeSpecialChars(String ring) {
		StringBuilder b = new StringBuilder();
		for (char c : ring.toCharArray()) {
			if (ALLOWED_RING_CHARS.contains(c)) b.append(c);
		}
		return b.toString();
	}

}
