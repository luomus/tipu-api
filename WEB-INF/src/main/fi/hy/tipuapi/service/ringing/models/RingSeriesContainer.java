package fi.hy.tipuapi.service.ringing.models;

import java.util.HashMap;
import java.util.Map;


public class RingSeriesContainer {

	public static class RingSeries {

		private final Map<String, Boolean> allowedSpecies = new HashMap<>();
		
		public void addAllowedSpecies(String allowedSpecies, boolean warningForSpecies) {
			this.allowedSpecies.put(allowedSpecies, warningForSpecies);			
		}

		public boolean isAllowedForSpecies(String species) {
			return this.allowedSpecies.containsKey(species);
		}

		public boolean requiresWarningForSpecies(String value) {
			return this.allowedSpecies.get(value);
		}
		
	}

	private final Map<String, RingSeries> series = new HashMap<>();
	
	public boolean seriesExists(String series) {
		return this.series.containsKey(series);
	}

	public RingSeries getSeries(String series) {
		if (!seriesExists(series)) {
			this.series.put(series, new RingSeries());
		}
		return this.series.get(series);
	}
		
}
