package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class AttachmentLefRightValidator implements FieldValidation {

	private static final String NECK = "K";
	
	@Override
	public void validate(RowValidationData data, Column attachmentLeftRight) throws Exception {
		Column attachmentPoint = data.getRow().get("attachmentPoint");
		if (attachmentPoint.getValue().equals(NECK)) {
			if (attachmentLeftRight.hasValue()) {
				data.addError("attachmentLeftRight_can_not_be_given_for_neck_rings", attachmentLeftRight);
			}
		}
	}

}
