package fi.hy.tipuapi.service.ringing.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import fi.hy.tipuapi.general.RingSeriesComparator;
import fi.hy.tipuapi.service.ringing.models.DistributedFieldReadable;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.hy.tipuapi.service.ringing.models.RingSeriesContainer;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.Cached.ResourceWrapper;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class SharedCachedResources {

	private final Config config;
	private final DataSource dataSource;

	public SharedCachedResources(Config config, DataSource dataSource) {
		this.config = config;
		this.dataSource = dataSource;
	}

	private final Cached<ResourceWrapper<Integer, TransactionConnection>, DistributedFieldReadable> cachedDistributedFieldReadable = new Cached<>(
			new CacheLoader<ResourceWrapper<Integer, TransactionConnection>, DistributedFieldReadable>() {

				@Override
				public DistributedFieldReadable load(ResourceWrapper<Integer, TransactionConnection> request) {
					PreparedStatement p = null;
					ResultSet rs = null;
					try {
						p = request.getResource().prepareStatement(" SELECT code, mainColor, forSpecies, distributedDate FROM fieldReadable WHERE distributedTo = ? ");
						p.setInt(1, request.getKey());
						rs = p.executeQuery();
						rs.setFetchSize(4001);
						DistributedFieldReadable distributedRings = new DistributedFieldReadable(getSpeciesComparator());
						while (rs.next()) {
							String ringCode = rs.getString(1);
							String ringBackground = rs.getString(2);
							String species = rs.getString(3);
							DateValue distributionDate = DateUtils.convertToDateValue(DateUtils.sqlDateToString(rs.getDate(4)));
							distributedRings.add(ringCode, ringBackground, species, distributionDate);
						}
						return distributedRings;
					} catch (SQLException e) {
						throw new RuntimeException("Loading distributed field readable rings for " + request.getKey(), e);
					} finally {
						Utils.close(p, rs);
					}
				}

			}
			, 1, TimeUnit.HOURS, 50);

	public DistributedFieldReadable getDistributedFieldReadable(int ringerNumber, TransactionConnection con) {
		return cachedDistributedFieldReadable.get(new ResourceWrapper<>(ringerNumber, con));
	}

	private class TipuApiResourceCacheLoader implements CacheLoader<String, TipuApiResource> {

		@Override
		public TipuApiResource load(String resourceName) {
			TipuAPIClient client = null;
			try {
				try {
					client = new TipuAPIClientImple(config);
					return client.get(resourceName);
				} catch (Exception e) {
					throw new RuntimeException("TipuApiResource: " + resourceName, e);
				}
			} finally {
				if (client != null) client.close();
			}
		}

	}

	private final Cached<String, TipuApiResource> cachedTipuApiResources = new Cached<>(new TipuApiResourceCacheLoader(), 3, TimeUnit.HOURS, 100);

	public TipuApiResource getTipuApiResource(String resourceName) {
		return cachedTipuApiResources.get(resourceName);
	}

	private final SingleObjectCache<RingSeriesContainer> cachedSeries = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<RingSeriesContainer>() {

		@Override
		public RingSeriesContainer load() {
			Connection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = dataSource.getConnection();
				p = con.prepareStatement(" SELECT series, species, warning FROM series ORDER BY series, species ");
				rs = p.executeQuery();
				RingSeriesContainer ringSeries = new RingSeriesContainer();
				while (rs.next()) {
					String series = rs.getString(1);
					String allowedSpecies = rs.getString(2);
					boolean warningForSpecies = "H".equals(rs.getString(3));
					ringSeries.getSeries(series).addAllowedSpecies(allowedSpecies, warningForSpecies);
				}
				return ringSeries;
			} catch (SQLException e) {
				throw new RuntimeException("Cached series loader", e);
			} finally {
				close(con, p, rs);
			}
		}

	}, 1, TimeUnit.DAYS);

	public RingSeriesContainer getSeries() {
		return cachedSeries.get();
	}

	private void close(Connection con, PreparedStatement p, ResultSet rs) {
		Utils.close(p, rs);
		try {if (con != null) con.close(); } catch (Exception e) {}
	}

	private final SingleObjectCache<DistributedRings> cachedDistributedRings = new SingleObjectCache<>(
			new SingleObjectCache.CacheLoader<DistributedRings>() {

				@Override
				public DistributedRings load() {
					Connection con = null;
					PreparedStatement p = null;
					ResultSet rs = null;
					try {
						con = dataSource.getConnection();
						p = con.prepareStatement(" SELECT ringStart, ringEnd, distributedDate, distributedTo FROM distributed ORDER BY ringStart, ringEnd ");
						rs = p.executeQuery();
						rs.setFetchSize(4001);
						DistributedRings distributedRings = new DistributedRings(getRingSeriesComparator(new SimpleTransactionConnection(con)));
						while (rs.next()) {
							String ringStartDb = rs.getString(1);
							String ringEndDb = rs.getString(2);
							DateValue distributionDate = DateUtils.convertToDateValue(DateUtils.sqlDateToString(rs.getDate(3)));
							int ringerNumber = rs.getInt(4);
							distributedRings.add(ringStartDb, ringEndDb, distributionDate, ringerNumber);
						}
						return distributedRings;
					} catch (SQLException e) {
						throw new RuntimeException("Loading distributed rings", e);
					} finally {
						close(con, p, rs);
					}
				}

			}
			, 6, TimeUnit.HOURS);

	public DistributedRings getDistributedRings() {
		return cachedDistributedRings.get();
	}

	private Comparator<String> ringSeriesComparator = null;

	public Comparator<String> getRingSeriesComparator(final TransactionConnection con) throws SQLException {
		if (ringSeriesComparator == null) {
			ringSeriesComparator = new RingSeriesComparator(con);
		}
		return ringSeriesComparator;
	}

	private final Cached<String, List<Integer>> cachedTopAgeRecords = new Cached<>(
			new CacheLoader<String, List<Integer>>() {

				@Override
				public List<Integer> load(String species) {
					List<Integer> ages = new ArrayList<>();
					Connection con = null;
					PreparedStatement p = null;
					ResultSet rs = null;
					try {
						String sql = "" +
								" SELECT age FROM species_age_records " +
								" WHERE species  LIKE ? " +
								" ORDER BY species, age DESC ";
						con = dataSource.getConnection();
						p = con.prepareStatement(sql);
						p.setString(1, species + "%");
						rs = p.executeQuery();
						int count = 0;
						while (rs.next()) {
							if (count++ > 5) break;
							ages.add(rs.getInt(1));
						}
						return ages;
					} catch (SQLException e) {
						throw new RuntimeException("Age records for species " + species);
					} finally {
						close(con, p, rs);
					}
				}

			}, 1, TimeUnit.DAYS, 50);

	public List<Integer> getTopAgeRecords(String species) {
		return cachedTopAgeRecords.get(species);
	}

	private static Comparator<String> speciesComparator = null;

	public Comparator<String> getSpeciesComparator() {
		if (speciesComparator != null) {
			return speciesComparator;
		}

		TipuApiResource species = getTipuApiResource(TipuAPIClient.SPECIES);
		Map<String, Integer> orderMap = new HashMap<>(species.size());
		int i = 0;
		for (Node speciesNode : species) {
			orderMap.put(speciesNode.getNode("id").getContents(), i++);
		}
		speciesComparator = new SpeciesComparator(orderMap);
		return speciesComparator;
	}

	private static class SpeciesComparator implements Comparator<String> {
		private final Map<String, Integer> orderMap;
		public SpeciesComparator(Map<String, Integer> orderMap) {
			this.orderMap = orderMap;
		}
		@Override
		public int compare(String species1, String species2) {
			Integer o1 = orderMap.get(species1);
			Integer o2 = orderMap.get(species2);
			if (o1 == null) o1 = Integer.MAX_VALUE;
			if (o2 == null) o2 = Integer.MAX_VALUE;
			return o1.compareTo(o2);
		}

	}

	public void forceReload() {
		cachedDistributedRings.getForceReload();
		cachedSeries.getForceReload();
		cachedDistributedFieldReadable.invalidateAll();
		cachedTipuApiResources.invalidateAll();
	}

}
