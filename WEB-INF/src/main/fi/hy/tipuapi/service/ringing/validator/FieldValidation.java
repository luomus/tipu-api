package fi.hy.tipuapi.service.ringing.validator;

import fi.hy.tipuapi.service.ringing.models.Column;

public interface FieldValidation {

	public void validate(RowValidationData data, Column column) throws Exception;
	
}
