package fi.hy.tipuapi.service.ringing;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.r99.Rengas99JSONToRowConverter;
import fi.hy.tipuapi.service.ringing.r99.Rengas99ToJSONConverter;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

public class Rengas99Rowparser extends Rows {

	private static final long serialVersionUID = -6199928430127851123L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPost(req, res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (notProductionMode()) log(req);
		try {
			List<Row> rows = parseRows(req);
			if (rows.size() == 0) {
				return print(generateNoResultsResponse(), res);
			}
			JSONObject response = new JSONObject();
			response.setInteger("countOfRows", rows.size());
			response.setBoolean("success", true);
			appendBeautifyAndProtectRows(response, rows, null,  null, null, false, null);
			return print(response, res);
		} catch (IllegalArgumentException e) {
			return print(generateExceptionResponse(e), res);
		} catch (Exception e) {
			getErrorReporter().report(e);
			return print(generateExceptionResponse(e), res);
		}
	}

	private List<Row> parseRows(HttpServletRequest req) throws Exception {
		Mode mode = getMode(req);
		List<String> lines = new LinkedList<>();
		for (String line : req.getParameter("data").replace("\r", "").split("\n")) {
			lines.add(line);
		}
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			List<JSONObject> rowsAsJson = new Rengas99ToJSONConverter(dao.getRowStructure()).convert(lines, mode);
			List<Row> rows = new Rengas99JSONToRowConverter(dao).convert(rowsAsJson, mode);
			return rows;
		} finally {
			if (dao != null) dao.close();
		}
	}

}
