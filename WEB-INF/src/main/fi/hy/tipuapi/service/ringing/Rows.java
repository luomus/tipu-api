package fi.hy.tipuapi.service.ringing;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.hy.tipuapi.service.ringing.models.RowToEuring2020Converter;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class Rows extends BaseServlet {

	private static final long serialVersionUID = 9151568542012556592L;
	private final int DEFAULT_MAXIMUM_NUMBER_OF_RESULTS = 10000;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// if (notProductionMode()) log(req);
		log(req);
		String locale = getLocale(req);
		String userid = req.getParameter("userid");
		String protectedFieldLocalizedString = getLocalizedTexts().getAllTexts(locale).get("protectedField");
		int maxNumberOfResults = getMaxNumberOfResultsOrDefault(req);
		String viewMode = req.getParameter("viewMode");
		boolean euringCode = "true".equals(req.getParameter("includeEuringCode"));
		Row searchParams = parseSearchParameters(req);
		List<String> sortByFields = parseSortByFields(req);
		JSONObject response = null;

		try {
			response = oracleSearch(req, locale, userid, protectedFieldLocalizedString, maxNumberOfResults, viewMode, searchParams, sortByFields, euringCode);
		} catch (Exception e) {
			getErrorReporter().report("Search failed for request: " + Utils.debugS(userid, searchParams.getSearchParameters(), sortByFields), e);
			response = generateExceptionResponse(e);
		}
		return print(response, res);
	}

	private JSONObject oracleSearch(HttpServletRequest req, String locale, String userid, String protectedFieldLocalizedString, int maxNumberOfResults, String viewMode, Row searchParams, List<String> sortByFields, boolean euringCode) throws Exception {
		JSONObject response = new JSONObject();
		DAO dao = null;
		try {
			dao = createNewDAO(req);
			String ringerOrBirdStation = ringerOrBirdStation(userid, searchParams, dao);
			int countTotal = dao.countTotal(searchParams, ringerOrBirdStation);
			if (countTotal > maxNumberOfResults) {
				response = generateTooManyResultsResponse(locale, countTotal);
			} else if (countTotal == 0) {
				response = generateNoResultsResponse();
			} else {
				List<Row> searchResults = dao.search(searchParams, sortByFields, ringerOrBirdStation);
				response = addResultsToResponse(searchResults, userid, protectedFieldLocalizedString, viewMode, euringCode, dao);
			}
		} finally {
			if (dao != null) dao.close();
		}
		return response;
	}

	private String ringerOrBirdStation(String userid, Row searchParams, DAO dao) throws Exception {
		if (!searchParams.get("ringer").hasSearchParameterSet()) return null;
		if (!searchParams.get("ringer").getSearchParameter().getSingle().equals(userid)) return null;
		TipuApiResource birdStations = dao.getTipuApiResource(TipuAPIClient.BIRD_STATIONS);
		for (Node station : birdStations.getAll()) {
			String ringerFakeNumber = station.getAttribute("ringer-number");
			if (userid.equals(ringerFakeNumber)) {
				return station.getNode("id").getContents();
			}
		}
		return null;
	}

	private int getMaxNumberOfResultsOrDefault(HttpServletRequest req) {
		String max = req.getParameter("maxResultCount");
		if (given(max)) {
			try {
				return Integer.valueOf(max);
			} catch (NumberFormatException e) {}
		}
		return DEFAULT_MAXIMUM_NUMBER_OF_RESULTS;
	}

	private JSONObject addResultsToResponse(List<Row> searchResults, String userid, String protectedFieldLocalizedString, String viewMode, boolean euringCode, DAO dao) throws Exception {
		JSONObject response = new JSONObject();
		response.setInteger("countOfRows", searchResults.size());
		response.setBoolean("success", true);
		appendBeautifyAndProtectRows(response, searchResults, userid, protectedFieldLocalizedString, viewMode, euringCode, dao);
		return response;
	}

	protected JSONObject generateNoResultsResponse() {
		JSONObject response = new JSONObject();
		response.setInteger("countOfRows", 0);
		response.setBoolean("success", true);
		response.getArray("rows");
		return response;
	}

	private JSONObject generateTooManyResultsResponse(String locale, long countTotal) {
		JSONObject response = new JSONObject();
		response.setInteger("countOfRows", 0);
		response.setBoolean("success", false);
		String localizedErrorText = getText("too_many_results", locale) + " (" + countTotal + " " + getText("results", locale) + ")";
		response.getArray("errors").appendObject(error("too_many_results", localizedErrorText));
		response.getArray("rows");
		return response;
	}

	private List<String> parseSortByFields(HttpServletRequest req) {
		List<String> sortBy = parameters("sortBy", req, "nameRing", "eventDate", "type DESC");
		return sortBy;
	}

	private Row parseSearchParameters(HttpServletRequest req) throws Exception {
		Row row = getEmptyRow();
		Enumeration<String> e = req.getParameterNames();
		while (e.hasMoreElements()) {
			String param = e.nextElement();
			if (!row.hasField(param)) continue;
			Column c = row.get(param);
			for (String value : req.getParameterValues(param)) {
				c.setSearchParam(value);
			}
		}
		String ring = req.getParameter("ring");
		if (given(ring)) {
			row.get("nameRing").setSearchParam(ring.toUpperCase());
		}
		String ringSeries = req.getParameter("ringSeries");
		String ringNumbers = req.getParameter("ringNumbers");
		if (given(ringSeries) || given(ringNumbers)) {
			if (!given(ringSeries)) ringSeries = "";
			if (!given(ringNumbers)) ringNumbers = "";
			if (ringNumbers.contains("%") || ringNumbers.contains("_")) {
				ringSeries += "%";
			}
			row.get("nameRing").setSearchParam(ringSeries + ringNumbers);
		}

		// Backwards compability; now applications should use type=recovery  instead type=onlyRecoveries
		Column type = row.get("type");
		if (type.hasSearchParameterSet()) {
			if (type.getSearchParameter().getSingle().equalsIgnoreCase("onlyRingings")) {
				type.clearSearchParameter();
				type.setSearchParam("ringing");
			} else if (type.getSearchParameter().getSingle().equalsIgnoreCase("onlyRecoveries")) {
				type.clearSearchParameter();
				type.setSearchParam("recovery");
			}
		}
		return row;
	}

	private List<String> parameters(String name, HttpServletRequest req, String ... defaultValues) {
		List<String> parameters = new ArrayList<>();
		if (req.getParameterValues(name) != null) {
			for (String value : req.getParameterValues(name)) {
				for (String s : value.split(",")) {
					s = s.trim();
					if (given(s)) {
						parameters.add(s.trim());
					}
				}
			}
		}
		if (parameters.isEmpty()) {
			for (String defaultValue : defaultValues) {
				parameters.add(defaultValue);
			}
		}
		return parameters;
	}

	protected void appendBeautifyAndProtectRows(JSONObject response, List<Row> searchResults, String userid, String protectedFieldLocalizedString, String viewMode, boolean euringCode, DAO dao) throws Exception {
		JSONArray rows = response.getArray("rows");
		boolean ringingViewMode = "ringings".equals(viewMode);

		Iterator<String> euringCodes = null;
		if (euringCode) {
			euringCodes = euringConverter(dao).convert(new RingHistory(searchResults)).iterator();

		}

		for (Row searchResult : searchResults) {
			beautifyAndProtectForPublishing(searchResult, userid, protectedFieldLocalizedString);
			if (ringingViewMode) {
				searchResult.get("ringStart").setValue(searchResult.get("legRing").getValue());
			}
			JSONObject row = new JSONObject();
			for (Column c : searchResult) {
				if (c.hasValue()) {
					row.setString(c.getName(), c.getValue());
				}
			}
			if (euringCodes != null) {
				row.setString("euringCode", euringCodes.next());
			}
			rows.appendObject(row);
		}
	}

	private RowToEuring2020Converter euringConverter(DAO dao) throws Exception {
		return new RowToEuring2020Converter(dao.getTipuApiResource(TipuAPIClient.SPECIES), dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES));
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processGet(req, res);
	}

	private void beautifyAndProtectForPublishing(Row row, String userid, String protectedFieldLocalizedString) throws Exception {
		boolean protectedSpecies = isProtectedSpecies(row);
		beautifyWGS84DMS(row);

		String thisRowRinger = row.get("ringer").getValue();
		String ringingRinger = row.get("ringingRinger").getValue();

		for (Column c : row) {
			if (!c.hasValue()) continue;
			if (c.getFieldDescription().isDecimal()) {
				RowUtils.fixdecimals(c);
			}
			if (c.getValue().equals("-1")) {
				c.setValue("");
			}
			if (protectedSpecies && isProtectedField(c)) {
				if (!given(userid) || userid.startsWith("admin_")) continue;

				if (revealsRingingPlace(c)) {
					if (thisRowRinger.equals(ringingRinger) && userid.equals(thisRowRinger)) {
						// This is the only case, when field revealing ringing place will be shown to non-admin user
					} else {
						c.setValue(protectedFieldLocalizedString);
					}
				} else {
					if (!thisRowRinger.equals(userid)) {
						c.setValue(protectedFieldLocalizedString);
					}
				}
			}
		}
	}

	private boolean revealsRingingPlace(Column c) {
		//		String name = c.getName();
		//		if (name.equals("distanceToRingingInKilometers")) return true;
		//		if (name.equals("directionToRingingInDegrees")) return true;
		//		if (name.startsWith("ringing") && (name.endsWith("Lat") || name.endsWith("Lon"))) return true;
		//		return false;
		return c.getName().toLowerCase().contains("ringing");
	}

	private boolean isProtectedField(Column c) throws Exception {
		return getProtectedFields().contains(c.getName());
	}

	private Set<String> getProtectedFields() throws Exception {
		if (shouldInitResources()) {
			initResources();
		}
		return protectedFields;
	}

	private void initResources() throws Exception {
		DAO dao = null;
		try {
			dao = createNewDAO(Rows.class.getName(), false);
			protectedSpecies = initProtectedSpecies(dao);
			protectedFields = initProtectedFields(dao);
			rowStructure = dao.getRowStructure();
		} finally {
			if (dao != null) dao.close();
		}
	}

	private RowStructure rowStructure;

	private Row getEmptyRow() throws Exception {
		if (shouldInitResources()) {
			initResources();
		}
		return new Row(rowStructure);
	}

	private boolean shouldInitResources() {
		return protectedFields == null || protectedSpecies == null || rowStructure == null;
	}

	private boolean isProtectedSpecies(Row row) throws Exception {
		String ringedSpecies = row.get("ringedSpecies").getValue();
		String recoverySpecies = row.get("species").getValue();
		return getProtectedSpecies().contains(ringedSpecies) || protectedSpecies.contains(recoverySpecies);
	}

	private Set<String> getProtectedSpecies() throws Exception {
		if (shouldInitResources()) {
			initResources();
		}
		return protectedSpecies;
	}

	private Set<String> protectedFields = null;

	private synchronized Set<String> initProtectedFields(DAO dao) throws Exception {
		if (protectedFields != null) return protectedFields;
		RowStructure rowStructure = dao.getRowStructure();
		Set<String> protectedFields = new HashSet<>();
		for (String group : Utils.list("coordinates", "convertedCoordinates")) {
			for (Field f : rowStructure.getGroup(group)) {
				protectedFields.add(f.getName());
			}
		}
		for (String field : Utils.list("birdStation", "locality", "personalPlaceName", "distanceToRingingInKilometers", "directionToRingingInDegrees")) {
			if (!rowStructure.hasField(field)) throw new IllegalArgumentException("No field " + field); // to catch typos
			protectedFields.add(field);
		}
		return protectedFields;
	}

	private Set<String> protectedSpecies = null;

	private synchronized Set<String> initProtectedSpecies(DAO dao) throws Exception {
		if (protectedSpecies != null) return protectedSpecies;
		Set<String> protectedSpecies = new HashSet<>();
		TipuApiResource species = dao.getTipuApiResource(TipuAPIClient.SPECIES);
		for (Node speciesInfo : species.getAll()) {
			if (speciesInfo.hasAttribute("protection-status")) {
				String status = speciesInfo.getAttribute("protection-status");
				if (status.equals("2")) {
					protectedSpecies.add(speciesInfo.getNode("id").getContents());
				}
			}
		}
		return protectedSpecies;
	}

	public void beautifyWGS84DMS(Row row) {
		Column wgs84DegLat = row.get("wgs84DegreeLat");
		Column wgs84DegLon = row.get("wgs84DegreeLon");

		beautifyWGS84DMS(wgs84DegLat);
		beautifyWGS84DMS(wgs84DegLon);

		Column coordinateType = row.get("coordinateSystem");
		if (coordinateType.getValue().equals("WGS84-DMS")) {
			Column lat = row.get("lat");
			Column lon = row.get("lon");
			beautifyWGS84DMS(lat);
			beautifyWGS84DMS(lon);
		}
	}

	private void beautifyWGS84DMS(Column c) {
		if (!c.hasValue()) return;
		String value = c.getValue();
		boolean minusSigned = value.startsWith("-");
		if (minusSigned) {
			value = value.replace("-", "");
		}
		while (value.length() < 6) {
			value = "0" + value;
		}
		if (minusSigned) {
			value = "-" + value;
		}
		c.setValue(value);
	}


}
