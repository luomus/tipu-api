package fi.hy.tipuapi.service.ringing.dao;

import fi.hy.tipuapi.service.ringing.models.Row;

public interface WarehousingDAO {

	void close();

	void push(Row row);

	void delete(int eventId);

	void flush();

}
