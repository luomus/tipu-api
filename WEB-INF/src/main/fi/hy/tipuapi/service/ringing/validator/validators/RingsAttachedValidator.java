package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.utils.Utils;

import java.util.Set;

public class RingsAttachedValidator implements FieldValidation {

	private static Set<String> FIELD_READABLE_VALUES = Utils.set("A", "B", "K", "V", "L");
	private static Set<String> ATTACHMENT_POINT_ATTACHED_TO_LEG = Utils.set("A", "Y", "J");

	@Override
	public void validate(RowValidationData data, Column ringsAttached) throws Exception {
		boolean ringsAttachedSaysFieldReadableAttached = FIELD_READABLE_VALUES.contains(ringsAttached.getValue());
		crossCheckWithFieldReadableChangeAddRemove(data, ringsAttached, ringsAttachedSaysFieldReadableAttached);

		Column fieldReadableCode = data.getRow().get("fieldReadableCode");
		if (ringsAttachedSaysFieldReadableAttached && !fieldReadableCode.hasValue()) {
			data.addError("ringsAttached_says_fieldreadable_attached_but_is_not", ringsAttached);
			data.addError("ringsAttached_says_fieldreadable_attached_but_is_not", fieldReadableCode);
		}

		if (data.getMode() == Mode.RINGINGS) {
			if (fieldReadableCode.hasValue() && !ringsAttachedSaysFieldReadableAttached) {
				data.addError("fieldreadable_attached_but_ringsAttached_doesnt_state_that", ringsAttached);
			}
		}

		if (ringsAttachedSaysFieldReadableAttached || ringsAttached.getValue().equals("T")) { // T = unmarked color rings
			crossCheckWithFieldReadableAttachmentPoint(data, ringsAttached);
		} else if (ringsAttached.getValue().equals("M")) { // Some other color thing
			if (!data.getRow().get("fieldReadableNotes").hasValue()) {
				data.addWarning("some_other_color_markings_attached_warning_asking_for_notes", ringsAttached);
			}
		} else if (ringsAttached.hasValue()) { // Metal ring attached
			crossCheckMetalRingAttached(data, ringsAttached);
		}

	}

	private void crossCheckWithFieldReadableAttachmentPoint(RowValidationData data, Column ringsAttached) {
		if (!ringsAttached.hasValue()) return;
		Column attachmentPointColumn = data.getRow().get("attachmentPoint");
		if (!attachmentPointColumn.hasValue()) return;

		String attachmentPoint = attachmentPointColumn.getValue();

		String error = null;
		switch (ringsAttached.getValue().charAt(0)) {
		case 'V': if (!ATTACHMENT_POINT_ATTACHED_TO_LEG.contains(attachmentPoint)) error = "for_this_rings_attached_code_attachment_point_must_be_leg"; break;
		case 'T': if (!ATTACHMENT_POINT_ATTACHED_TO_LEG.contains(attachmentPoint)) error = "for_this_rings_attached_code_attachment_point_must_be_leg"; break;
		case 'K': if (!attachmentPoint.equals("K")) error ="for_this_rings_attached_code_attachment_point_must_be_neck"; break;
		case 'L': if (!attachmentPoint.equals("S")) error ="for_this_rings_attached_code_attachment_point_must_be_wing"; break;
		case 'A': if (!ATTACHMENT_POINT_ATTACHED_TO_LEG.contains(attachmentPoint) || attachmentPoint.equals("K")) error = "for_this_rings_attached_code_attachment_point_must_be_leg_or_neck"; break;
		case 'B': if (!ATTACHMENT_POINT_ATTACHED_TO_LEG.contains(attachmentPoint) || attachmentPoint.equals("S")) error = "for_this_rings_attached_code_attachment_point_must_be_leg_or_wing"; break;
		}
		if (error != null) {
			data.addError(error, ringsAttached);
			data.addError(error, attachmentPointColumn);
		}
		if (ringsAttached.getValue().equals("A") || ringsAttached.getValue().equals("B")) {
			data.addWarning("can_only_report_one_markings_please_report_those_which_are_more_visible_warning", ringsAttached);
		}
	}

	private void crossCheckMetalRingAttached(RowValidationData data, Column ringsAttached) {
		if (data.getMode() == Mode.RECOVERIES) {
			if (!data.getRow().get("newLegRing").hasValue()) {
				if (ringsAttached.getValue().equals("S")) { // metal wing mark used as ring
					data.addError("can_only_mark_metal_wing_mark_attached_if_new_leg_ring_given", ringsAttached);

				} else {
					data.addError("can_only_mark_metal_ring_attached_if_new_leg_ring_given", ringsAttached);
				}
			}
		}
		// For ringings metal ring attached is always ok
	}

	private void crossCheckWithFieldReadableChangeAddRemove(RowValidationData data, Column ringsAttached, boolean ringsAttachedSaysFieldReadableAttached) {
		Column chandedOrRemoved = data.getRow().get("fieldReadableChanges");
		//
		//		if (!chandedOrRemoved.hasValue()) {
		//			if (ringsAttachedSaysFieldReadableAttached && data.getMode() == Mode.RECOVERIES) {
		//				data.addError("ringsAttached_says_fieldreadable_attached_but_is_not", ringsAttached);
		//			}
		//		}
		if (chandedOrRemoved.getValue().equals("V") || chandedOrRemoved.getValue().equals("L")) {
			if (!ringsAttachedSaysFieldReadableAttached) {
				data.addError("fieldreadable_attached_but_ringsAttached_doesnt_state_that", ringsAttached);
			}
		}
		//		if (chandedOrRemoved.getValue().equals("P") && ringsAttachedSaysFieldReadableAttached) {
		//			data.addError("ringsAttached_says_fieldreadable_attached_but_is_not", ringsAttached);
		//		}
	}

}
