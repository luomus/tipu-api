package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator extends AccuracyOfSomethingValidator {

	public AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator(String ... fieldNamesWhichAccuracyThisFieldReflects) {
		super(fieldNamesWhichAccuracyThisFieldReflects);
	}

	@Override
	public void validate(RowValidationData data, Column accuracyField) throws Exception {
		super.validate(data, accuracyField);
		if (!data.noErrors(accuracyField)) return;

		for (String fieldName : getFieldNamesWhichAccuracyThisFieldReflects()) {
			Column valueField = data.getRow().get(fieldName);
			if (!data.notNullNoErrors(valueField)) continue;

			if (Double.valueOf(valueField.getValue()) % 1 != 0) {
				if (!accuracyField.hasValue()) {
					data.addError("accuracy_required_for_number_that_is_not_a_round_figure", accuracyField);
				}
			}
		}
	}

}
