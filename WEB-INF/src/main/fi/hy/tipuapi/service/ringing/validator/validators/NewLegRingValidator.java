package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.containers.Ring;

public class NewLegRingValidator extends RingStartEndValidator {

	@Override
	public void validate(RowValidationData data, Column newLegRing) throws Exception {
		if (data.getMode() == Mode.RINGINGS) return;
		if (!newLegRing.hasValue()) return;
		
		Column legRing = data.getRow().get("legRing");

		if (!data.notNullNoErrors(legRing)) return;
		if (!validFormat(data, newLegRing)) return;

		Ring ring = new Ring(newLegRing.getValue());

		if (!validateSeries(data, newLegRing, ring)) return;

		validateRingSeriesFitsForSpecies(data, newLegRing, data.getRow().get("species"), ring);

		validateDistributionAndUse(newLegRing, ring, data);
		
		if (data.isForAdminValidations()) {
			adminValidate(data, newLegRing, legRing);
		}
	}

	private void adminValidate(RowValidationData data, Column newLegRing, Column legRing) throws Exception {
		Column eventDate = data.getRow().get("eventDate");
		if (!data.notNullNoErrors(eventDate)) return;
		RingHistory ringHistory = data.getDao().getRingHistoryByLegRing(legRing.getValue());
		for (Row recovery : ringHistory.getRecoveries()) {
			if (sameDateNotSameEvent(data.getRow(), recovery)) {
				if (recovery.get("newLegRing").hasValue()) {
					data.addError("ADMIN_two_ring_changes_for_same_date", newLegRing);
				}
			}
		}
		if (data.isForUpdate()) return; // liian vaikeaa? joten poistettu päivityksille; huoh
		for (Row recovery : ringHistory.getRecoveries()) {
			if (RowUtils.rowAfterEventDate(recovery, eventDate)) {
				if (recovery.get("legRing").hasValue()) {
					data.addError("ADMIN_ring_change_but_recoveries_exist_after_change_for_the_old_ring", newLegRing);
				}
			}
		}
	}

	private boolean sameDateNotSameEvent(Row row, Row recovery) {
		if (row.get("id").getValue().equals(recovery.get("id").getValue())) return false;
		return recovery.get("eventDate").getDateValue().equals(row.get("eventDate").getDateValue());
	}

}
