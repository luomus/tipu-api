package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;

public class ClutchNumbersAndOwnerValidator implements FieldValidation {

	@Override
	public void validate(RowValidationData data, Column clutchNumber) throws Exception {
		Column someoneElsesClutchNumber = data.getRow().get("someoneElsesClutchNumber");
		Column clutchNumberOwner = data.getRow().get("clutchNumberOwner");
		Column ringer = data.getRow().get("ringer");

		if (data.hasErrors(clutchNumber) || data.hasErrors(someoneElsesClutchNumber) || data.hasErrors(clutchNumberOwner) || data.hasErrors(ringer)) {
			return;
		}

		if (clutchNumber.hasValue()) {
			if (someoneElsesClutchNumber.hasValue()) {
				data.addError("can_only_give_own_or_someone_elses_clutch_number_not_both", someoneElsesClutchNumber);
				return;
			}
			if (clutchNumberOwner.hasValue() && !clutchNumberOwner.getValue().equals(ringer.getValue())) {
				data.addError("if_own_clutch_number_given_cant_give_other_ringers_number_as_owner", clutchNumberOwner);
				return;
			}
			return;
		}

		if (someoneElsesClutchNumber.hasValue()) {
			if (!clutchNumberOwner.hasValue()) {
				data.addError("must_give_clutch_number_owner_number_if_someones_clutch_number_given", clutchNumberOwner);
				return;
			}
			if (clutchNumberOwner.getValue().equals(ringer.getValue())) {
				data.addError("cant_give_own_ringer_number_as_someone_elses_clutch_number_owner", clutchNumberOwner);
				return;
			}
		}
		
	}
	
}
