package fi.hy.tipuapi.service.ringing.models;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import fi.hy.tipuapi.service.ringing.models.RingBatches.RingBatch;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class DistributedRings {

	private final Comparator<String> ringSeriesComparator;
	private final RingBatches allDistributedRings;
	private final Map<Integer, RingBatches> ringersDistributedRings;

	public DistributedRings(Comparator<String> ringSeriesComparator) {
		this.ringSeriesComparator = ringSeriesComparator;
		this.allDistributedRings = new RingBatches(ringSeriesComparator);
		this.ringersDistributedRings = new HashMap<>();
	}

	public static class Distribution {
		private final int ringerNumber;
		private final DateValue distributionDate;
		public Distribution(int ringerNumber, DateValue distributionDate) {
			this.ringerNumber = ringerNumber;
			this.distributionDate = distributionDate;
		}
		public int getRingerNumber() {
			return ringerNumber;
		}
		public DateValue getDistributionDate() {
			return distributionDate;
		}
		@Override
		public String toString() {
			return Utils.debugS(ringerNumber, distributionDate);
		}
		@Override
		public boolean equals (Object other) {
			if (other instanceof Distribution) {
				return this.toString().equals(other.toString());
			}
			throw new UnsupportedOperationException("Can only compare " + this.getClass().getName() + " to same class.");
		}
		@Override
		public int hashCode() {
			return this.toString().hashCode();
		}
	}

	public void add(String ringStartDb, String ringEndDb, DateValue distributionDate, int ringerNumber) {
		RingBatch ringBatch = allDistributedRings.add(ringStartDb, ringEndDb).setDistribution(new Distribution(ringerNumber, distributionDate));
		if (!ringersDistributedRings.containsKey(ringerNumber)) {
			ringersDistributedRings.put(ringerNumber, new RingBatches(ringSeriesComparator));
		}
		ringersDistributedRings.get(ringerNumber).add(ringBatch);
	}

	public boolean isDistributedToSomeoneAtDate(Ring ring, DateValue eventDate) {
		Distribution distribution = getDistribution(ring);
		if (distribution == null) return false;
		return isDistributedAtDate(eventDate, distribution);
	}

	public boolean isDistributedToRingerAtDate(Ring ring, int ringerNumber, DateValue eventDate) {
		Distribution distribution = getDistribution(ring);
		if (distribution == null) return false;
		if (!isDistributedAtDate(eventDate, distribution)) return false;
		if (distribution.getRingerNumber() != ringerNumber) return false;
		return true;
	}

	private boolean isDistributedAtDate(DateValue eventDate, Distribution distribution) {
		DateValue distributionDate = distribution.getDistributionDate();
		long eventDateTimestamp = DateUtils.getEpoch(eventDate);
		long distributionDateTimestamp = DateUtils.getEpoch(distributionDate);
		return (eventDateTimestamp >= distributionDateTimestamp);
	}

	public Distribution getDistribution(Ring ring) {
		return allDistributedRings.getDistribution(ring);
	}

	/**
	 * When given a ring number, looks to whom that ring has been distributed and returns
	 * the next number in that series distributed to that ringer. If there are no more rings
	 * in that series that have been distributed to the user returns null.
	 * @param previousRing
	 * @return
	 */
	public String nextRing(String previousRing) {
		Ring ring = new Ring(previousRing);

		Distribution previousRingDistribution = getDistribution(ring);
		if (previousRingDistribution == null) return null;
		int ringerNumber = previousRingDistribution.getRingerNumber();

		RingBatches ringersRings = this.getRingersRings(ringerNumber);
		return ringersRings.getNext(ring);
	}

	public RingBatches getRingersRings(int ringerNumber) {
		return ringersDistributedRings.get(ringerNumber);
	}

}
