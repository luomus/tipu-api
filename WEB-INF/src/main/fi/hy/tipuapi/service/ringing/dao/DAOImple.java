package fi.hy.tipuapi.service.ringing.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.zaxxer.hikari.HikariDataSource;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.DistributedFieldReadable;
import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.hy.tipuapi.service.ringing.models.Field;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.FieldReadable;
import fi.hy.tipuapi.service.ringing.models.NameRingsRecoveryIDs;
import fi.hy.tipuapi.service.ringing.models.RingBatches;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.RingSeriesContainer.RingSeries;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.hy.tipuapi.service.ringing.models.RowUtils;
import fi.hy.tipuapi.service.ringing.models.SearchParameter;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;
import fi.luomus.commons.utils.Logger;
import fi.luomus.commons.utils.Utils;

public class DAOImple implements DAO {

	private static final String DATE_VALUE_TYPE = "_this_is_date_value_type_Ay7S7yLJU6_";
	private static final String DECIMAL_VALUE_TYPE = "_this_is_decimal_value_type_Ay7S7yLJU6_";

	private final TransactionConnection con;
	private final RowStructure structure;
	private final SharedCachedResources sharedCachedResources;
	private final Map<String, PreparedStatement> openedStatements = new HashMap<>();
	private final Logger logger;
	private final String userId;

	public static class DAOImpleBuilder {
		private HikariDataSource dataSource = null;
		private RowStructure structure = null;
		private SharedCachedResources sharedCachedResources = null;
		private Logger logger = null;
		public DAOImple openForUserId(String userId) throws Exception {
			return new DAOImple(
					dataSource != null ? new SimpleTransactionConnection(dataSource.getConnection()) : null,
							sharedCachedResources,
							structure,
							logger,
							userId);
		}
		public DAOImpleBuilder setLogger(Logger logger) {
			this.logger = logger;
			return this;
		}
		public DAOImpleBuilder setDataSource(HikariDataSource dataSource) {
			this.dataSource = dataSource;
			return this;
		}
		public DAOImpleBuilder setStructure(RowStructure structure) {
			this.structure = structure;
			return this;
		}
		public DAOImpleBuilder setSharedResources(SharedCachedResources sharedCachedResources) {
			this.sharedCachedResources = sharedCachedResources;
			return this;
		}
		public RowStructure getRowStructure() {
			return structure;
		}
	}

	protected DAOImple(TransactionConnection con, SharedCachedResources sharedCachedResources, RowStructure structure, Logger logger, String userId) {
		this.con = con;
		this.sharedCachedResources = sharedCachedResources;
		this.structure = structure;
		this.logger = logger;
		this.userId = userId;
	}

	@Override
	public PreparedStatement getPreparedStatement(String sql) throws SQLException {
		PreparedStatement p = openedStatements.get(sql);
		if (p == null) {
			p = con.prepareStatement(sql);
			openedStatements.put(sql, p);
		}
		return p;
	}

	@Override
	public void close() {
		for (PreparedStatement p : openedStatements.values()) {
			Utils.close(p);
		}
		Utils.close(con);
	}

	@Override
	public RowStructure getRowStructure() {
		return structure;
	}

	@Override
	public Row emptyRow() {
		return new Row(structure);
	}

	@Override
	public void search(RowHandler rowHandler, Row searchParams, List<String> sortBy, String ringerOrBirdStation) throws Exception {
		StringBuilder sql = new StringBuilder();
		generateEventSelect(sql);
		generateEventFromAndJoins(sql);
		List<String> queryParams = generateEventWhere(sql, searchParams, ringerOrBirdStation);
		generateOrderBy(sortBy, sql);
		executeQuery(rowHandler, sql.toString(), queryParams);
	}

	@Override
	public List<Row> search(Row searchParams, List<String> sortBy, String ringerOrBirdStation) throws Exception {
		RowsToListHandler rowsToListHandler = new RowsToListHandler();
		search(rowsToListHandler, searchParams, sortBy, ringerOrBirdStation);
		return rowsToListHandler.getList();
	}

	private void generateEventSelect(StringBuilder sql) {
		sql.append(" SELECT ");
		Iterator<Field> i = getRowStructure().getFields();
		while (i.hasNext()) {
			Field field = i.next();
			String toSelect = field.getDatabaseColumn();
			if (!given(toSelect)) continue;
			if (field.isDate()) {
				toSelect = "to_char("+toSelect+", 'FMDD.MM.YYYY') AS " + toSelect.replace(".", "") + "Text";
			}
			sql.append(toSelect);
			if (i.hasNext()) {
				sql.append(", ");
			}
		}
	}

	private void generateEventFromAndJoins(StringBuilder sql) {
		sql.append(" FROM event ");
		sql.append(" JOIN event ringing ON (event.nameRing = ringing.nameRing AND ringing.type = 'ringing') ");
		sql.append(" LEFT JOIN municipality ON (event.municipality = municipality.id) ");
		sql.append(" LEFT JOIN municipality currentMunicipality ON (municipality.joinedTo = currentMunicipality.id) ");
		sql.append(" LEFT JOIN euringProvince ON (event.euringProvinceCode = euringProvince.id) ");
		sql.append(" LEFT JOIN country ON (euringProvince.country = country.id) ");
		sql.append(" LEFT JOIN elyCentre ON (currentMunicipality.elyCentre = elyCentre.id) ");
		sql.append(" LEFT JOIN finnishProvince ON (currentMunicipality.finnishProvince = finnishProvince.id) ");
		sql.append(" LEFT JOIN finnishOldCounty ON (currentMunicipality.finnishOldCounty = finnishOldCounty.id) ");
		sql.append(" LEFT JOIN lylArea ON (currentMunicipality.lylArea = lylArea.id) ");
		sql.append(" LEFT JOIN layman ON (event.laymanID = layman.id) ");
	}

	private List<String> generateEventWhere(StringBuilder sql, Row searchParams, String ringerOrBirdStation) {
		List<String> values = new ArrayList<>();
		sql.append(" WHERE   1 = 1     ");
		boolean somethingSet = false;
		for (Column c : searchParams) {
			if (!c.hasSearchParameterSet()) continue;
			if (!given(c.getDatabaseColumn())) continue;
			if (isRingColumn(c)) {
				addRingClause(c.getDatabaseColumn(), sql, values, c.getSearchParameter().getSingle());
			} else if (c.getName().equals("ringer") && ringerOrBirdStation != null) {
				addRingerOrBirdStationClause(sql, values, c, ringerOrBirdStation);
			} else {
				appendToWhereClause(sql, values, c);
			}
			somethingSet = true;
		}
		if (!somethingSet) {
			return impossibleWhereStatement(sql, values);
		}
		return values;
	}

	private void addRingerOrBirdStationClause(StringBuilder sql, List<String> values, Column c, String ringerOrBirdStation) {
		sql.append(" AND ( event.ringer = ? OR event.birdStation = ? ) ");
		values.add(c.getSearchParameter().getSingle());
		values.add(ringerOrBirdStation);
	}

	private void addRingClause(String dbColumn, StringBuilder sql, List<String> values, String ring) {
		if (ring.contains("%") || ring.contains("_")) {
			sql.append(" AND ").append(dbColumn).append(" LIKE ? ");
			values.add(ring.toUpperCase());
			return;
		}
		if (ring.contains("+")) {
			sql.append(" AND ").append(dbColumn).append(" = ? ");
			values.add(ring.toUpperCase());
			return;
		}
		if (invalidRing(ring)) {
			impossibleWhereStatement(sql, values);
			return;
		}
		List<String> possibleValues =  LegacyRingCodeHandlerUtil.actualToDb(ring);
		Iterator<String> i = possibleValues.iterator();
		sql.append(" AND ( ");
		while (i.hasNext()) {
			String possibleValue = i.next();
			sql.append(dbColumn).append(" = ? ");
			values.add(possibleValue);
			if (i.hasNext()) sql.append(" OR ");
		}
		sql.append(" ) ");
	}

	private boolean invalidRing(String ring) {
		return !new Ring(ring).validFormat();
	}

	private void appendToWhereClause(StringBuilder sql, List<String> values, Column column) {
		SearchParameter parameter = column.getSearchParameter();
		String dbColumn = column.getDatabaseColumn();
		if (!given(dbColumn)) return;

		if (column.getFieldDescription().isDate()) {
			String value = parameter.getSingle();
			if (isFullDateRange(value)) {
				String beginDate = value.split("/")[0];
				String endDate = value.split("/")[1];
				sql.append(" AND  TRUNC(").append(dbColumn).append(") BETWEEN ? AND ? ");
				values.add(DATE_VALUE_TYPE + beginDate);
				values.add(DATE_VALUE_TYPE + endDate);
				return;
			}
			DateValue dateValue = DateUtils.convertToDateValue(value);
			if (dateValue.hasEmptyFields() || value.contains("/")) {
				dateFieldSearchSQL(sql, values, dbColumn, dateValue.getYear(), "year");
				dateFieldSearchSQL(sql, values,  dbColumn, dateValue.getMonth(), "month");
				dateFieldSearchSQL(sql, values,  dbColumn, dateValue.getDay(), "day");
				return;
			}
			sql.append(" AND TRUNC(").append(dbColumn).append(") = ? ");
			values.add(DATE_VALUE_TYPE + value);
			return;
		}
		if (parameter.hasMultiple()) {
			sql.append( " AND ").append(dbColumn).append(" IN (");
			for (String value : parameter.getMultiple()) {
				sql.append(" ?,");
				values.add(value);
			}
			Utils.removeLastChar(sql);
			sql.append(" ) ");
			return;
		}
		if (parameter.isLike()) {
			String value = parameter.getSingle();
			value = value.replace("*", "%");

			if (column.getFieldDescription().isNumeric()) {
				sql.append(" AND ").append(dbColumn).append(" LIKE ? ");
				values.add(value);
			} else {
				sql.append(" AND UPPER(").append(dbColumn).append(") LIKE ? ");
				values.add(value.toUpperCase());
			}
			return;
		}
		if (column.getName().equals("fieldReadableCode")) {
			sql.append(" AND ").append(dbColumn).append(" = ? ");
			values.add(parameter.getSingle().toUpperCase());
			return;
		}
		String value = parameter.getSingle();
		if (value.contains("/")) {
			String start = value.split("/")[0];
			String end = value.split("/")[1];
			sql.append(" AND ").append(dbColumn).append(" BETWEEN ? AND ? ");
			if (column.getFieldDescription().isDecimal()) {
				values.add(DECIMAL_VALUE_TYPE + start);
				values.add(DECIMAL_VALUE_TYPE + end);
			} else {
				values.add(start);
				values.add(end);
			}
			return;
		}
		sql.append(" AND ").append(dbColumn).append(" = ? ");
		if (column.getFieldDescription().isDecimal()) {
			values.add(DECIMAL_VALUE_TYPE + value);
		} else {
			values.add(value);
		}
	}

	private boolean isFullDateRange(String value) {
		if (!value.contains("/")) return false;
		String beginDate = value.split("/")[0];
		String endDate = value.split("/")[1];
		return Utils.countNumberOf(".", beginDate) == 2 && Utils.countNumberOf(".", endDate) == 2;
	}

	private void dateFieldSearchSQL(StringBuilder sql, List<String> values, String dbColumn, String datePart, String sqlDateFormat) {
		if (!given(datePart)) return;
		if (datePart.contains("/")) {
			sql.append(" AND extract(").append(sqlDateFormat).append(" from ").append(dbColumn).append(") BETWEEN ? AND ? ");
			String[] parts = datePart.split(Pattern.quote("/"));
			values.add(removeFrontZero(parts[0]));
			values.add(removeFrontZero(parts[1]));
		} else {
			sql.append(" AND extract(").append(sqlDateFormat).append(" from ").append(dbColumn).append(") = ? ");
			values.add(removeFrontZero(datePart));
		}
	}

	private String removeFrontZero(String datePart) {
		while (datePart.startsWith("0")) {
			datePart = datePart.substring(1);
		}
		return datePart;
	}

	private List<String> impossibleWhereStatement(StringBuilder sql, List<String> values) {
		sql.append(" AND 1 = 2 ");
		return values;
	}

	private static class RowsToListHandler implements RowHandler {
		private final List<Row> list = new ArrayList<>();
		public List<Row> getList() {
			return list;
		}
		@Override
		public boolean handleRow(Row row) {
			list.add(row);
			return true;
		}
	}

	private void executeQuery(RowHandler rowHandler, String sql, List<String> queryParams) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(sql);
			setQueryParamsToStatement(p, queryParams);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				Row row = emptyRow();
				int i = 1;
				for (Column c : row) {
					String dbColumn = c.getDatabaseColumn();
					if (!given(dbColumn)) continue;
					String value = rs.getString(i++);
					if (isRingColumn(c)) {
						if (given(value)) {
							String ring = LegacyRingCodeHandlerUtil.dbToActual(value);
							c.setValue(ring);
						}
					} else {
						c.setValue(value);
					}
				}
				boolean shouldContinue = rowHandler.handleRow(row);
				if (!shouldContinue) break;
			}
		} finally {
			Utils.close(rs);
		}
	}

	private void setQueryParamsToStatement(PreparedStatement p, List<String> values) throws SQLException {
		int i = 1;
		for (String value : values) {
			if (value == null) {
				p.setString(i++, value);
			} else if (value.startsWith(DATE_VALUE_TYPE)) {
				value = value.replace(DATE_VALUE_TYPE, "");
				p.setDate(i++, new Date(DateUtils.getEpoch(DateUtils.convertToDateValue(value)) *1000L));
			} else if (value.startsWith(DECIMAL_VALUE_TYPE)) {
				value = value.replace(DECIMAL_VALUE_TYPE, "");
				p.setDouble(i++, Double.valueOf(value));
			} else {
				p.setString(i++, value);
			}
		}
	}

	private void generateOrderBy(List<String> sortBy, StringBuilder sql) {
		if (sortBy == null) return;
		StringBuilder orderBy = new StringBuilder();
		for (String sortByField : sortBy) {
			boolean desc = sortByField.trim().toUpperCase().contains(" DESC");
			sortByField = sortByField.replace(" desc", "").replace(" DESC", "").replace(" asc", "").replace(" ASC", "").trim();
			if (!getRowStructure().hasField(sortByField)) {
				throw new IllegalArgumentException("No such field: " + sortByField);
			}
			String dbColumn = getRowStructure().get(sortByField).getDatabaseColumn();
			if (!given(dbColumn)) continue;
			orderBy.append(dbColumn);
			if (desc) {
				orderBy.append(" DESC");
			}
			orderBy.append(",");
		}
		if (orderBy.length() > 0) {
			Utils.removeLastChar(orderBy);
			sql.append(" ORDER BY ").append(orderBy);
		}
	}

	@Override
	public int countTotal(Row searchParams, String ringerOrBirdStation) throws Exception {
		if (somethingUniqueSelected(searchParams)) {
			return 1;
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*) ");
		generateEventFromAndJoins(sql);
		List<String> queryParams = generateEventWhere(sql, searchParams, ringerOrBirdStation);
		return getCount(sql.toString(), queryParams);
	}

	private boolean somethingUniqueSelected(Row searchParams) {
		return setAndNotLike(searchParams.get("id"))
				|| setAndNotLike(searchParams.get("legRing"))
				|| setAndNotLike(searchParams.get("nameRing"))
				|| setAndNotLike(searchParams.get("diario"));
	}

	private boolean setAndNotLike(Column c) {
		return c.hasSearchParameterSet() && !c.getSearchParameter().isLike();
	}

	private int getCount(String sql, List<String> values) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(sql);
			setQueryParamsToStatement(p, values);
			rs = p.executeQuery();
			if (!rs.next()) {
				throw new IllegalStateException(" count query did not return any results ");
			}
			int count = rs.getInt(1);
			if (rs.next()) {
				throw new IllegalStateException(" count query returned more than one row ");
			}
			return count;
		} finally {
			Utils.close(rs);
		}
	}

	private static final String LETTERS_NOT_YET_PRINTED_SQL = "" +
			" SELECT DISTINCT letter_queue.diario, event.nameRing, event.sourceOfRecovery  " +
			" FROM letter_queue                                                            " +
			" JOIN event ON (event.diario = letter_queue.diario)                           " +
			" ORDER BY letter_queue.diario                                                 ";

	@Override
	public NameRingsRecoveryIDs lettersNotYetPrinted(DAO.LetterType desiredLetterType) throws Exception {
		ResultSet rs = null;
		try {
			NameRingsRecoveryIDs data = new NameRingsRecoveryIDs();
			PreparedStatement p = getPreparedStatement(LETTERS_NOT_YET_PRINTED_SQL);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				DAO.LetterType rowType = getType(rs.getInt(3));
				if (desiredLetterType == DAO.LetterType.ALL || desiredLetterType == rowType) {
					String nameRing = LegacyRingCodeHandlerUtil.dbToActual(rs.getString(2));
					String recoveryId = rs.getString(1);
					data.addRecoveryIdOfNameRing(nameRing, recoveryId);
				}
			}
			return data;
		} finally {
			Utils.close(rs);
		}
	}

	private DAO.LetterType getType(int type) {
		if (type == 1) return DAO.LetterType.ONLY_CONTROL_LETTERS;
		if (type == 2) return DAO.LetterType.ONLY_FINDING_LETTERS;
		throw new IllegalStateException("Unknown letter type: " + type);
	}

	@Override
	public NameRingsRecoveryIDs lettersNotYetPrinted(List<Integer> recoveryIds) throws Exception {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			NameRingsRecoveryIDs data = new NameRingsRecoveryIDs();

			StringBuilder sql = new StringBuilder(" SELECT nameRing, diario FROM event WHERE diario IN ( ");
			Iterator<Integer> it = recoveryIds.iterator();
			while (it.hasNext()) {
				it.next();
				sql.append(" ? ");
				if (it.hasNext()) {
					sql.append(", ");
				}
			}
			sql.append(" ) ");
			p = con.prepareStatement(sql.toString());
			int i = 1;
			for (int recoveryId : recoveryIds) {
				p.setInt(i++, recoveryId);
			}
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				String nameRing = LegacyRingCodeHandlerUtil.dbToActual(rs.getString(1));
				String recoveryId = rs.getString(2);
				data.addRecoveryIdOfNameRing(nameRing, recoveryId);
			}
			return data;
		} finally {
			Utils.close(p, rs);
		}
	}

	private static final String DELETE_FROM_LETTER_PRINTING_SQL = " DELETE FROM letter_queue WHERE diario = ? ";

	@Override
	public void removeFromLetterPrinting(int recoveryId) throws Exception {
		startTransaction();
		PreparedStatement p = getPreparedStatement(DELETE_FROM_LETTER_PRINTING_SQL);
		p.setInt(1, recoveryId);
		p.executeUpdate();
		commitTransaction();
	}

	private static final String MARK_RECOVERIES_FOR_LETTER_PRINTING_USING_DIARIOS_SQL = " INSERT INTO letter_queue (diario) VALUES (?) ";

	@Override
	public void markRecoveriesForLetterPrintingUsingDiarios(List<Integer> diarioNumbers) throws Exception {
		startTransaction();
		PreparedStatement p = getPreparedStatement(MARK_RECOVERIES_FOR_LETTER_PRINTING_USING_DIARIOS_SQL);
		for (int id : diarioNumbers) {
			p.setInt(1, id);
			p.setInt(2, id);
			p.addBatch();
		}
		p.executeBatch();
		commitTransaction();
	}

	private static final String MARK_RECOVERIES_FOR_LETTER_PRINTING_USING_EVENT_IDS_SQL = "" +
			" INSERT INTO letter_queue (diario) " +
			" SELECT diario FROM event WHERE id = ? AND diario IS NOT NULL AND COALESCE(noletter, 0) != 1";

	@Override
	public void markRecoveriesForLetterPrintingUsingEventIDs(List<Integer> eventIds) throws Exception {
		PreparedStatement p = getPreparedStatement(MARK_RECOVERIES_FOR_LETTER_PRINTING_USING_EVENT_IDS_SQL);
		for (int eventId : eventIds) {
			p.setInt(1, eventId);
			p.addBatch();
		}
		p.executeBatch();
	}

	@Override
	public TipuApiResource getTipuApiResource(String resourceName) {
		return sharedCachedResources.getTipuApiResource(resourceName);
	}

	@Override
	public boolean seriesExists(Ring ring) {
		return sharedCachedResources.getSeries().seriesExists(ring.getSeries());
	}

	@Override
	public RingSeries getSeries(Ring ring) {
		return sharedCachedResources.getSeries().getSeries(ring.getSeries());
	}

	@Override
	public boolean ringDistributed(Ring ring, int ringerNumber, DateValue eventDate) throws SQLException {
		DistributedRings distributedRings = getDistributedRings();
		if (distributedRings == null) return false;
		return distributedRings.isDistributedToRingerAtDate(ring, ringerNumber, eventDate);
	}

	@Override
	public boolean ringDistributedToSomeone(Ring ring, DateValue eventDate) throws SQLException {
		DistributedRings distributedRings = getDistributedRings();
		if (distributedRings == null) throw new IllegalStateException("Ring distribution unavailable at this time. Please try again later after we have fixed the problem.");
		return distributedRings.isDistributedToSomeoneAtDate(ring, eventDate);
	}

	@Override
	public DistributedRings getDistributedRings() {
		return sharedCachedResources.getDistributedRings();
	}

	@Override
	public void forceReloadOfCachedResources() {
		sharedCachedResources.forceReload();
	}

	@Override
	public boolean ringUsed(String ring) throws SQLException {
		String nameRing = getNameRing(ring);
		return nameRing != null;
	}

	@Override
	public boolean fieldReadableDistributed(String fieldReadableCode, String backgroundColor, int ringerNumber, DateValue eventDate, String species) {
		DistributedFieldReadable distributedRings = sharedCachedResources.getDistributedFieldReadable(ringerNumber, con);
		if (distributedRings == null) return false;
		return distributedRings.isDistributedAtDate(fieldReadableCode, backgroundColor, species, eventDate);
	}

	@Override
	public boolean fieldReadableUsed(String fieldReadableCode, String backgroundColor, String species) throws Exception {
		List<String> nameRings = getNameRingByFieldReadable(fieldReadableCode, backgroundColor, species);
		return !nameRings.isEmpty();
	}

	private String trimSpecies(String species) {
		if (species.length() > 6) {
			species = species.substring(0, 6);
		}
		return species.toUpperCase();
	}

	@Override
	public String getNameRing(String legRing) throws SQLException {
		if (!given(legRing)) return null;
		if (invalidRing(legRing)) return null;

		StringBuilder sql = new StringBuilder();
		ResultSet rs = null;
		try {
			List<String> values = new ArrayList<>();
			sql.append(" SELECT nameRing FROM event WHERE 1=1 ");
			addRingClause("newLegRing", sql, values, legRing);
			PreparedStatement p = getPreparedStatement(sql.toString());
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			if (rs.next()) {
				String nameRing = LegacyRingCodeHandlerUtil.dbToActual(rs.getString(1));
				if (rs.next()) {
					throw new IllegalStateException("Several uses for ring: " + legRing);
				}
				return nameRing;
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(sql.toString(), e);
		} finally {
			Utils.close(rs);
		}
	}

	private static final String RINGERS_USED_RINGS_SQL = "" +
			" SELECT newLegRing AS usedRing FROM event WHERE ringer = ? AND newLegRing IS NOT NULL ORDER BY newLegRing ";

	@Override
	public RingBatches getRingersUsedRings(int ringerNumber) throws SQLException {
		RingBatches usedRings = new RingBatches(sharedCachedResources.getRingSeriesComparator(con));
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(RINGERS_USED_RINGS_SQL);
			p.setInt(1, ringerNumber);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				usedRings.add(rs.getString(1));
			}
			return usedRings;
		} finally {
			Utils.close(rs);
		}
	}

	private static final String BIRD_STATION_USED_RINGS_SQL = "" +
			" SELECT newLegRing AS usedRing FROM event WHERE birdStation = ? AND newLegRing IS NOT NULL ORDER BY newLegRing ";

	@Override
	public RingBatches getBirdStationUsedRings(String birdStationCode) throws SQLException {
		RingBatches usedRings = new RingBatches(sharedCachedResources.getRingSeriesComparator(con));
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(BIRD_STATION_USED_RINGS_SQL);
			p.setString(1, birdStationCode);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				usedRings.add(rs.getString(1));
			}
			return usedRings;
		} finally {
			Utils.close(rs);
		}
	}

	private static final String USED_RINGS_SQL = "" +
			" SELECT newLegRing, ringer, eventDate, id, diario FROM event WHERE newLegRing IS NOT NULL ORDER BY newLegRing ";

	@Override
	public ResultSet getUsedRings() throws SQLException {
		PreparedStatement p = getPreparedStatement(USED_RINGS_SQL);
		ResultSet rs = p.executeQuery();
		rs.setFetchSize(4001);
		return rs;
	}

	@Override
	public List<Integer> getTopAgeRecords(String species) throws Exception {
		return sharedCachedResources.getTopAgeRecords(trimSpecies(species));
	}

	@Override
	public RingHistory getRingHistoryByNameRing(String nameRing) throws Exception {
		if (!given(nameRing)) return new RingHistory();
		//if (invalidRing(nameRing)) return new RingHistory();

		Row searchParams = emptyRow();
		searchParams.get("nameRing").setSearchParam(nameRing.toUpperCase());
		List<String> sortByFields = Utils.list("nameRing", "eventDate", "type DESC");
		List<Row> searchResults = search(searchParams, sortByFields, null);

		return new RingHistory(searchResults);
	}

	@Override
	public RingHistory getRingHistoryByLegRing(String legRing) throws Exception {
		String nameRing = getNameRing(legRing);
		if (nameRing == null) return new RingHistory();
		return getRingHistoryByNameRing(nameRing);
	}

	private static final String NAME_RING_BY_FIELD_READABLE_RING_SQL = "" +
			" SELECT DISTINCT nameRing " +
			" FROM event " +
			" WHERE fieldreadableCode = ? AND mainColor = ? AND species LIKE ? ";

	@Override
	public List<String> getNameRingByFieldReadable(String fieldReadableCode, String backgroundColor, String species) throws Exception {
		List<String> nameRings = new ArrayList<>();
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(NAME_RING_BY_FIELD_READABLE_RING_SQL);
			p.setString(1, fieldReadableCode);
			p.setString(2, backgroundColor);
			p.setString(3, trimSpecies(species) + "%");
			rs = p.executeQuery();
			while (rs.next()) {
				nameRings.add(LegacyRingCodeHandlerUtil.dbToActual(rs.getString(1)));
			}
			return nameRings;
		} finally {
			Utils.close(rs);
		}
	}

	@Override
	public int insert(Row row, Mode mode) throws Exception {
		startTransaction();
		int id = insertEvent(row, mode);
		markChangedForWarehousing(id, DAO.Action.INSERT);
		logger.log("INSERTED ROW WITH ID " + id + ": " + generateRowLogLine(row));
		commitTransaction();
		return id;
	}

	private String generateRowLogLine(Row row) {
		StringBuilder b = new StringBuilder();
		for (Column c : row) {
			if (!c.hasValue()) continue;
			if (shouldLog(c)) {
				b.append("{").append(c.getName()).append(":").append(c.getValue()).append("} ");
			}
		}
		Utils.removeLastChar(b);
		return b.toString();
	}

	private boolean shouldLog(Column c) {
		return !c.isAggregated();
	}

	private int insertEvent(Row row, Mode mode) throws SQLException {
		List<String> values = new ArrayList<>(50);
		String mainInsertSQL = generateEventInsertSQL(row, mode, values);
		doInsertOrUpdate(values, mainInsertSQL);
		return Integer.valueOf(values.get(0));
	}

	private void doInsertOrUpdate(List<String> values, String sql) throws SQLException {
		PreparedStatement p = getPreparedStatement(sql);
		setQueryParamsToStatement(p, values);
		int i = p.executeUpdate();
		if (i != 1) throw new IllegalStateException("Inserted/updated " + i + " rows instead of 1: " + sql + " : " + values);
	}

	private String generateEventInsertSQL(Row row, Mode mode, List<String> values) throws SQLException {
		StringBuilder sql = new StringBuilder();
		String eventId = resolveEventIdForInsert(row);
		sql.append(" INSERT INTO event (id, type, ");
		values.add(eventId);
		if (mode == Mode.RINGINGS) {
			values.add("ringing");
		} else if (mode == Mode.RECOVERIES) {
			values.add("recovery");
			sql.append(" diario, ");
			values.add(resolveDiario(row));
		} else {
			throw new IllegalArgumentException(mode.toString());
		}

		sql.append("created, modifiedBy, ");
		values.add(DATE_VALUE_TYPE + DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		values.add(userId);

		for (Column c : row) {
			if (shouldSkip(c)) continue;
			sql.append(c.getDatabaseColumn()).append(",");
			addValueForInsertOrUpdate(values, c);
		}
		Utils.removeLastChar(sql);
		sql.append(" ) VALUES ( ");
		for (int i = values.size(); i > 0; i--) {
			sql.append("?,");
		}
		Utils.removeLastChar(sql);
		sql.append(" ) ");
		return sql.toString();
	}

	private String resolveEventIdForInsert(Row row) throws SQLException {
		Column eventId = row.get("id");
		if (!eventId.hasValue()) return getNextEventId();

		Column ringStart = row.get("ringStart");
		Column ringEnd = row.get("ringEnd");
		Column legRing = row.get("legRing");

		if (!ringEnd.hasValue() || ringStart.getValue().equals(ringEnd.getValue())) {
			return eventId.getValue();
		}

		if (!legRing.getValue().equals(ringStart.getValue())) {
			return getNextEventId();
		}
		return eventId.getValue();
	}

	private String resolveDiario(Row row) throws SQLException {
		if (row.get("diario").hasValue()) {
			return row.get("diario").getValue();
		}
		if (row.get("sourceOfRecovery").getValue().equals(RECOVERY_TYPE_CONTROL)) {
			return getNextControlDiario();
		}
		if (row.get("sourceOfRecovery").getValue().equals(RECOVERY_TYPE_FINDING)) {
			return getNextFindingDiario();
		}
		throw new IllegalStateException("Unknown source of recovery: " + row.toString());
	}

	private void addValueForInsertOrUpdate(List<String> values, Column c) {
		if (!c.hasValue()) {
			values.add(null);
		} else if (RING_COLUMNS.contains(c.getName())) {
			values.add(actualRingToDb(c.getValue()));
		} else if (c.getFieldDescription().isDate()) {
			values.add(DATE_VALUE_TYPE + c.getValue());
		} else if (c.getFieldDescription().isDecimal()) {
			values.add(DECIMAL_VALUE_TYPE + c.getValue());
		} else {
			values.add(c.getValue());
		}
	}

	private static final String GET_NEXT_CONTROL_DIARIO_SQL = " SELECT MAX (usedval)+1 FROM kontrolli_seq_table ";
	private static final String INSERT_USED_CONTROL_DIARIO_SQL = " INSERT INTO kontrolli_seq_table VALUES(?) ";

	private static final String GET_NEXT_FINDING_DIARIO_SQL = " SELECT MAX (usedval)+1 FROM rengloyto_seq_table ";
	private static final String INSERT_USED_FINDING_DIARIO_SQL = " INSERT INTO rengloyto_seq_table VALUES(?) ";

	private String getNextDiario(String getNextSQL, String insertSQL) throws SQLException {
		// Käytämme max+1 menetelmää, koska diarioihin ei haluta hyppyjä. Sekvenssin käyttö aiheuttaisi aina välillä kahdenkymmenen hyppäyksiä Oracle sekvenssicachesta johtuen.
		// Kun Sulan tai Rengastus-admin -käyttöliittymän kautta viedään sisään kamaa, se tehdään rivi kerrallaan, joten max+1 toimii. On hyvin epätodennäköistä, että kaksi adminia
		// vie sisään täsmälleen samaan aikaan kontrolleja tai löytöjä.
		ResultSet rs = null;
		try {
			PreparedStatement getStatement = getPreparedStatement(getNextSQL);
			PreparedStatement insertStatement = getPreparedStatement(insertSQL);

			rs = getStatement.executeQuery();
			rs.next();
			int nextval = rs.getInt(1);

			insertStatement.setInt(1, nextval);
			insertStatement.executeUpdate();

			return Integer.toString(nextval);
		} finally {
			Utils.close(rs);
		}
	}

	private String getNextControlDiario() throws SQLException {
		return getNextDiario(GET_NEXT_CONTROL_DIARIO_SQL, INSERT_USED_CONTROL_DIARIO_SQL);
	}

	private String getNextFindingDiario() throws SQLException {
		return getNextDiario(GET_NEXT_FINDING_DIARIO_SQL, INSERT_USED_FINDING_DIARIO_SQL);
	}

	private String getNextEventId() throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement("SELECT event_id_seq.NEXTVAL FROM DUAL");
			rs = p.executeQuery();
			rs.next();
			int nextval = rs.getInt(1);
			return Integer.toString(nextval);
		} finally {
			Utils.close(rs);
		}
	}

	private String actualRingToDb(String ringValue) {
		Ring ring = new Ring(ringValue);
		String series = ring.getSeries();
		String numbers = Integer.toString(ring.getNumbers());
		if (series.length() == 1) {
			series += " ";
		}
		while (numbers.length() < 7) {
			numbers = "0" + numbers;
		}
		return series + numbers;
	}

	private static final Set<String> COLUMNS_TO_SKIP = Utils.set(
			"event.id", "event.type", "event.diario", "event.created", "event.modified", "event.modifiedBy");

	private boolean shouldSkip(Column c) {
		String dbColumn = c.getDatabaseColumn();
		if (!given(dbColumn)) return true;
		if (c.isAggregated()) return true;
		return COLUMNS_TO_SKIP.contains(dbColumn);
	}

	private static final String FIELD_READABLE_RING_DISTRIBUTION_INFO_SQL = " SELECT attachmentPoint, codeColor, codeAlign FROM fieldReadable WHERE code = ? AND mainColor = ? AND forSpecies LIKE ? ";

	@Override
	public Row getFieldReadableDistributionInfo(String fieldReadableCode, String backgroundColor, String species) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(FIELD_READABLE_RING_DISTRIBUTION_INFO_SQL);
			p.setString(1, fieldReadableCode.toUpperCase());
			p.setString(2, backgroundColor.toUpperCase());
			p.setString(3, trimSpecies(species) + "%");
			rs = p.executeQuery();
			if (!rs.next()) {
				throw new IllegalStateException("Field readable ring info not found from distribution (getting distribution info): " + Utils.debugS(fieldReadableCode, backgroundColor, species) );
			}
			Row row = emptyRow();
			row.get("attachmentPoint").setValue(rs.getString(1));
			row.get("codeColor").setValue(rs.getString(2));
			row.get("codeAlign").setValue(rs.getString(3));
			if (rs.next()) {
				throw new IllegalStateException("Field readable ring info twice! (getting distribution info): " + Utils.debugS(fieldReadableCode, backgroundColor, species) );
			}
			return row;
		} finally {
			Utils.close(rs);
		}
	}

	private static final String FIELD_READABLE_RING_DOUBLE_DISTRIBUTED_SQL = " SELECT DOUBLEDISTRIBUTEDCOUNT FROM fieldReadable WHERE code = ? AND mainColor = ? AND forSpecies LIKE ? ";

	@Override
	public boolean fieldReadableDoubleDistributed(String code, String backgroundColor, String species) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(FIELD_READABLE_RING_DOUBLE_DISTRIBUTED_SQL);
			p.setString(1, code.toUpperCase());
			p.setString(2, backgroundColor.toUpperCase());
			p.setString(3, trimSpecies(species) + "%");
			rs = p.executeQuery();
			if (!rs.next()) {
				return false;
			}
			int count = rs.getInt(1);
			return count > 1;
		} finally {
			Utils.close(rs);
		}
	}

	@Override
	public void startTransaction() throws SQLException {
		con.startTransaction();
	}

	@Override
	public void commitTransaction() throws Exception {
		if (logger != null) logger.commit(Utils.generateGUID(), userId);
		con.commitTransaction();
	}

	@Override
	public void rollbackTransaction() throws SQLException {
		if (logger != null) logger.rollback();
		con.rollbackTransaction();
	}

	@Override
	public void update(Row row) throws Exception {
		int eventId = row.get("id").getIntValue();
		Row originalData = getRowByEventId(eventId);
		String logMessage = "UPDATED ROW WITH ID " + eventId + ": " + generateRowComparison(originalData, row);
		startTransaction();
		updateEvent(row);
		markChangedForWarehousing(eventId, DAO.Action.UPDATE);
		logger.log(logMessage);
		commitTransaction();
	}

	private String generateRowComparison(Row original, Row row) {
		StringBuilder b = new StringBuilder();
		Iterator<Field> i = getRowStructure().getFields();
		while (i.hasNext()) {
			Field field = i.next();
			Column originalData = original.get(field.getName());
			Column newData = row.get(field.getName());

			if (!shouldLog(newData)) continue;

			if (field.isDate()) {
				if (!originalData.getDateValue().equals(newData.getDateValue())) {
					String originalDate = originalData.hasValue() ? originalData.getDateValue().toString() : "";
					String newDate = newData.hasValue() ? newData.getDateValue().toString() : "";
					generateFieldComparisonLine(b, field, originalDate, newDate);
				}
			} else if (field.isDecimal()) {
				RowUtils.fixdecimals(originalData);
				String roundedOriginal = Utils.trimToLength(originalData.getValue(), field.getLength() + 1);
				String roundedNew = Utils.trimToLength(newData.getValue(), field.getLength() + 1);
				if (!roundedOriginal.equals(roundedNew)) {
					generateFieldComparisonLine(b, field, roundedOriginal, roundedNew);
				}
			} else {
				if (!originalData.getValue().equals(newData.getValue())) {
					generateFieldComparisonLine(b, field, originalData.getValue(), newData.getValue());
				}
			}
		}
		Utils.removeLastChar(b);
		return b.toString();
	}

	private void generateFieldComparisonLine(StringBuilder b, Field field, String originalValue, String newValue) {
		b.append("{").append(field.getName()).append(":").append(originalValue).append("=>").append(newValue).append("} ");
	}

	@Override
	public Row getRowByEventId(int eventId) throws Exception {
		List<Row> results = getRowByEventIdNonFailing(eventId);
		if (results.size() != 1) {
			throw new IllegalStateException("Found " + results.size() + " rows instead of 1 when searching for eventId " + eventId);
		}
		return results.get(0);
	}

	private List<Row> getRowByEventIdNonFailing(int eventId) throws Exception {
		Row searchparams = emptyRow();
		searchparams.get("id").setSearchParam(Integer.toString(eventId));
		List<Row> results = search(searchparams, null, null);
		if (results.size() > 1) {
			throw new IllegalStateException("Found " + results.size() + " rows when searching for eventId " + eventId);
		}
		return results;
	}

	private void updateEvent(Row row) throws SQLException {
		List<String> values = new ArrayList<>(50);
		String mainUpdateSQL = generateEventUpdateSQL(row, values);
		doInsertOrUpdate(values, mainUpdateSQL);
	}

	private String generateEventUpdateSQL(Row row, List<String> values) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE event SET ");

		sql.append("modified = ?, modifiedBy = ?, ");
		values.add(DATE_VALUE_TYPE + DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		values.add(userId);

		for (Column c : row) {
			if (shouldSkip(c)) continue;
			sql.append(c.getDatabaseColumn()).append(" = ?,");
			addValueForInsertOrUpdate(values, c);
		}
		Utils.removeLastChar(sql);
		sql.append(" WHERE id = ? ");
		values.add(row.get("id").getValue());
		return sql.toString();
	}

	private static final String RINGERS_USED_FIELDREADALBE_SQL = "" +
			" SELECT fieldReadableCode, mainColor, species FROM event WHERE ringer = ? AND (type = 'ringing' OR fieldReadableChanges IN ('L', 'V')) ";

	@Override
	public Set<FieldReadable> getRingersUsedFieldReadables(int ringerNumber) throws SQLException {
		Set<FieldReadable> set = new HashSet<>();
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(RINGERS_USED_FIELDREADALBE_SQL);
			p.setInt(1, ringerNumber);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				String code = rs.getString(1);
				String backgroundColor = rs.getString(2);
				String species = rs.getString(3);
				FieldReadable fieldReadable = new FieldReadable(code, backgroundColor, species);
				set.add(fieldReadable);
			}
			return set;
		} finally {
			Utils.close(rs);
		}
	}

	@Override
	public DistributedFieldReadable getDistributedFieldReadables(int ringerNumber) throws Exception {
		return sharedCachedResources.getDistributedFieldReadable(ringerNumber, con);
	}

	private static String DELETE_EVENT_SQL = "DELETE FROM event WHERE id = ? ";

	@Override
	public void delete(int eventId) throws Exception {
		startTransaction();
		Row row = getRowByEventId(eventId);
		PreparedStatement p = getPreparedStatement(DELETE_EVENT_SQL);
		p.setInt(1, eventId);
		markChangedForWarehousing(eventId, DAO.Action.DELETE);
		int i = p.executeUpdate();
		if (i != 1) {
			throw new IllegalStateException("Delete removed " + i + " rows instead of 1 for : " + row.toString());
		}
		logger.log("DELETED ROW WITH ID " + eventId + ": " + generateRowLogLine(row));
		if (row.get("type").getValue().equals("recovery")) {
			p = getPreparedStatement(DELETE_FROM_LETTER_PRINTING_SQL);
			p.setInt(1, row.get("diario").getIntValue());
			p.executeUpdate();
			logger.log("DELETED FROM letter_queue " + row.get("diario").getValue());
		}
		commitTransaction();
	}

	@Override
	public RingHistory getRingHistoryByEventId(int eventId) throws Exception {
		Row row = getRowByEventId(eventId);
		return getRingHistoryByNameRing(row.get("nameRing").getValue());
	}

	private static boolean given(String value) {
		return value != null && value.length() > 0;
	}

	private static final Collection<String> RING_COLUMNS = Utils.collection("legRing", "nameRing", "ringStart", "ringEnd", "newLegRing", "updateRing");

	private boolean isRingColumn(Column c) {
		return RING_COLUMNS.contains(c.getName());
	}

	@Override
	public boolean laymanExists(String laymanId) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(" SELECT count(id) FROM layman WHERE id = ? ");
			p.setString(1, laymanId);
			rs = p.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} finally {
			Utils.close(rs);
		}
	}

	private static final String MARK_CHANGED_FOR_WAREHOUSING_SQL = " INSERT INTO warehouse_queue (queueid, eventId, action) VALUES (warehouse_queue_seq.nextval, ?, ?) ";

	private void markChangedForWarehousing(int eventId, Action action) throws Exception {
		PreparedStatement p = getPreparedStatement(MARK_CHANGED_FOR_WAREHOUSING_SQL);
		p.setInt(1, eventId);
		p.setString(2, action.toString());
		int i = p.executeUpdate();
		if (i != 1) {
			throw new IllegalStateException("Inserting " + eventId + " to warehouse_queue failed. (" +i+ ").");
		}
	}

	private static final String GET_LATEST_FOR_WAREHOUSING_QUEUE_SQL = "" +
			" SELECT queueid, eventid, action FROM  " +
			" 	(	 SELECT queueid, eventid, action FROM warehouse_queue WHERE etl_success < 1 " +
			"   	ORDER BY attempted, queueid ) " +
			" WHERE ROWNUM <= ? ";

	private static final String MARK_ATTEMPTED_WAREHOUSING_QUEUE_SQL = " UPDATE warehouse_queue SET attempted = attempted + 1 WHERE queueid = ? ";

	@Override
	public List<DataWarehousingEntry> getLatestWarehouseQueueRows(int limit) throws Exception {
		List<DataWarehousingEntry> list = new ArrayList<>();
		ResultSet rs = null;
		try {
			startTransaction();
			PreparedStatement p = getPreparedStatement(GET_LATEST_FOR_WAREHOUSING_QUEUE_SQL);
			PreparedStatement markAttemptedSmt = getPreparedStatement(MARK_ATTEMPTED_WAREHOUSING_QUEUE_SQL);
			p.setInt(1, limit);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				int queueId = rs.getInt(1);
				int eventId = rs.getInt(2);
				Action action = resolveAction(rs.getString(3));
				if (action == Action.DELETE) {
					list.add(new DataWarehousingEntry(Action.DELETE, eventId, null));
				} else {
					List<Row> results = getRowByEventIdNonFailing(eventId);
					if (results.isEmpty()) {
						list.add(new DataWarehousingEntry(Action.DELETE, eventId, null));
					} else {
						Row row = results.get(0);
						list.add(new DataWarehousingEntry(action, eventId, row));
					}
				}
				markAttemptedSmt.setInt(1, queueId);
				markAttemptedSmt.addBatch();
			}
			markAttemptedSmt.executeBatch();
			commitTransaction();
			return list;
		} finally {
			Utils.close(rs);
		}
	}

	private Action resolveAction(String string) {
		for (Action action : DAO.Action.values()) {
			if (action.toString().equals(string)) return action;
		}
		throw new UnsupportedOperationException("Unknown action " + string);
	}

	private static final String MARK_ELASTIC_SUCCESS_WAREHOUSING_QUEUE_SQL = " UPDATE warehouse_queue SET elastic_success = 1 WHERE eventId = ? ";

	@Override
	public void markElasticPushSuccess(int eventId) throws Exception {
		startTransaction();
		PreparedStatement p = getPreparedStatement(MARK_ELASTIC_SUCCESS_WAREHOUSING_QUEUE_SQL);
		p.setInt(1, eventId);
		p.executeUpdate();
		commitTransaction();
	}

	private static final String MARK_ETL_SUCCESS_WAREHOUSING_QUEUE_SQL = " UPDATE warehouse_queue SET etl_success = 1 WHERE eventId = ? ";

	@Override
	public void markETLPushSuccess(int eventId) throws Exception {
		startTransaction();
		PreparedStatement p = getPreparedStatement(MARK_ETL_SUCCESS_WAREHOUSING_QUEUE_SQL);
		p.setInt(1, eventId);
		p.executeUpdate();
		commitTransaction();
	}

	private static final String CLEAR_SUCCESS_WAREHOUSING_QUEUE_SQL = " DELETE FROM warehouse_queue WHERE etl_success = 1 ";

	@Override
	public void clearSuccessfulFromWarehousingQueue() throws Exception {
		startTransaction();
		PreparedStatement p = getPreparedStatement(CLEAR_SUCCESS_WAREHOUSING_QUEUE_SQL);
		p.executeUpdate();
		commitTransaction();
	}

	@Override
	public List<String> getAllNameRingsBySpeciesThatHaveRecoveries(String species) throws SQLException {
		ResultSet rs = null;
		try {
			PreparedStatement p = getPreparedStatement(queryFor(species));
			if (species != null) {
				p.setString(1, species);
			}
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			List<String> nameRings = new ArrayList<>();
			while (rs.next()) {
				nameRings.add(rs.getString(1));
			}
			return nameRings;
		} finally {
			Utils.close(rs);
		}
	}

	private String queryFor(String species) {
		StringBuilder b = new StringBuilder();
		b.append(" SELECT ringing.namering ");
		b.append(" FROM event ringing ");
		b.append(" JOIN event recovery ON (recovery.namering = ringing.namering AND recovery.type = 'recovery') ");
		if (species == null) {
			b.append(" WHERE ringing.species IS NULL ");
		} else {
			b.append(" WHERE ringing.species = ? ");
		}
		b.append(" AND ringing.type = 'ringing' ");
		b.append(" GROUP BY ringing.namering ");
		b.append(" ORDER BY ringing.namering ");
		return b.toString();
	}

}
