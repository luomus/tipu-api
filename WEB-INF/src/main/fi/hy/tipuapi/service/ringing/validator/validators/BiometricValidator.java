package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.validator.FieldValidation;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.containers.MinMax;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.xml.Document.Node;

public class BiometricValidator implements FieldValidation {

	public static final boolean IGNORE_MIN_IF_MOULTING = true;
	
	private final String biometricSpeciesResourceVariableNamePostfix;
	private final boolean ignoreMinIfMoulting;
	
	public BiometricValidator(String biometricSpeciesResourceVariableNamePostfix) {
		this(biometricSpeciesResourceVariableNamePostfix, false);
	}
	
	public BiometricValidator(String biometricSpeciesResourceVariableNamePostfix, boolean ignoreMinIfMoulting) {
		this.biometricSpeciesResourceVariableNamePostfix = biometricSpeciesResourceVariableNamePostfix;
		this.ignoreMinIfMoulting = ignoreMinIfMoulting;
	}
	
	@Override
	public void validate(RowValidationData data, Column biometricMeasurement) throws Exception {
		if (!data.notNullNoErrors(biometricMeasurement)) return;
		
		Column age = data.getRow().get("age");
		Column species = data.getRow().get("species");
		if (data.hasErrors(age) || !data.notNullNoErrors(species)) return;
		
		MinMax<Double> minMax = getMinMax(species, age, data.getDao());
		Double measurementValue = Double.valueOf(biometricMeasurement.getValue());
		String agePostFix = isYoung(age) ? "_young" : "_adult";
		
		if (ignoreMinIfMoulting) {
			if (notMoulting(data)) {
				if (measurementValue < minMax.getMin()) {
					data.addWarning("biometric_too_small" + agePostFix, biometricMeasurement);
				}
			}
		} else {
			if (measurementValue < minMax.getMin()) {
				data.addWarning("biometric_too_small" + agePostFix, biometricMeasurement);
			}
		}
		
		if (measurementValue > minMax.getMax()) {
			data.addWarning("biometric_too_large" + agePostFix, biometricMeasurement);
		}
	}

	private boolean notMoulting(RowValidationData data) {
		return !data.getRow().get("moulting").getValue().equals("S");
	}

	private MinMax<Double> getMinMax(Column species, Column age, DAO dao) throws Exception {
		Node speciesInfo = dao.getTipuApiResource(TipuAPIClient.SPECIES).getById(species.getValue());
		Node youngMinMaxInfo = speciesInfo.getNode("young-" + biometricSpeciesResourceVariableNamePostfix);
		Node adultMinMaxInfo = speciesInfo.getNode("adult-" + biometricSpeciesResourceVariableNamePostfix);
		
		double youngMin = doubleValueOr(Double.MIN_VALUE, youngMinMaxInfo.getAttribute("min"));
		double youngMax =  doubleValueOr(Double.MAX_VALUE, youngMinMaxInfo.getAttribute("max"));
		double adultMin = doubleValueOr(Double.MIN_VALUE, adultMinMaxInfo.getAttribute("min"));
		double adultMax =  doubleValueOr(Double.MAX_VALUE, adultMinMaxInfo.getAttribute("max"));
		
		if (isYoung(age)) {
			if (notGiven(youngMax)) {
				youngMax = adultMax;
			}
			return new MinMax<>(youngMin, youngMax);
		}
		if (notGiven(adultMin)) {
			adultMin = youngMin;
		}
		return new MinMax<>(adultMin, adultMax);
	}

	private boolean notGiven(double d) {
		return d == Double.MAX_VALUE || d == Double.MIN_VALUE;
	}

	private Double doubleValueOr(double defaultIfNotGiven, String value) {
		if (value.length() > 0) {
			return Double.valueOf(value);
		}
		return defaultIfNotGiven;
	}

	private boolean isYoung(Column age) {
		return age.getValue().startsWith("P");
	}

}
