package fi.hy.tipuapi.service.ringing.validator.validators;

import fi.hy.tipuapi.service.ringing.models.Column;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

public class MunicipalitySchemeEuringProviceValidator extends RequiredFieldValidator {

	@Override
	public void validate(RowValidationData data, Column municipality) throws Exception {
		if (data.isForUserValidations() || data.getMode() == Mode.RINGINGS) {
			super.validate(data, municipality);
			return;
		}
		Column euringProvinceCode = data.getRow().get("euringProvinceCode");
		if (!municipality.hasValue() && !euringProvinceCode.hasValue()) {
			data.addError("ADMIN_municipality_or_euring_place_must_ge_given", municipality);
			data.addError("ADMIN_municipality_or_euring_place_must_ge_given", euringProvinceCode);
			return;
		}
		if (municipality.hasValue()) {
			if (euringProvinceCode.hasValue() && !euringProvinceCode.getValue().startsWith("SF")) {
				data.addError("ADMIN_can_not_give_foreign_if_municipality_given", euringProvinceCode);
			}
		}

		if (euringProvinceCode.getValue().startsWith("SF") && !municipality.hasValue()) {
			data.addError("ADMIN_must_give_municipality_if_euring_place_in_Finland", municipality);
			data.addError("ADMIN_must_give_municipality_if_euring_place_in_Finland", euringProvinceCode);
		}

		if (data.isForInsert() && data.notNullNoErrors(municipality, euringProvinceCode)) {
			validateMunicipalityAndFinnishEuringPlaceMatch(municipality, euringProvinceCode, data);
		}
	}

	private void validateMunicipalityAndFinnishEuringPlaceMatch(Column municipality, Column euringProvinceCode, RowValidationData data) throws Exception {
		TipuApiResource municipalities = data.getDao().getTipuApiResource(TipuAPIClient.MUNICIPALITIES);
		Node municipalityInfo = municipalities.getById(municipality.getValue());
		String municipalityEuringProvince = municipalityInfo.getNode("euring-province").getContents();
		if (!municipalityEuringProvince.equals(euringProvinceCode.getValue())) {
			data.addWarning("ADMIN_municipality_euring_province_mismatch", municipality);
			data.addWarning("ADMIN_municipality_euring_province_mismatch", euringProvinceCode);
		}
	}

}
