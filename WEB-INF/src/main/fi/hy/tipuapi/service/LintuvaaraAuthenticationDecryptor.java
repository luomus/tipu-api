package fi.hy.tipuapi.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import fi.hy.tipuapi.general.BaseServlet;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.AESDecryptor.AESDecryptionException;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;

public class LintuvaaraAuthenticationDecryptor extends BaseServlet {

	private static final long serialVersionUID = -6732999739584817482L;
	
	public LintuvaaraAuthenticationDecryptor() {
		super();
	}
	
	@Override
	protected void process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String key = req.getParameter("key");
		String iv = req.getParameter("iv");
		String data =  req.getParameter("data");
		
		if (!given(key) || !given(iv) || !given(data)) {
			error("Invalid login parameters given", req, res);
			return;
		}
		
		String rsaKey = config.get(Config.LINTUVAARA_PUBLIC_RSA_KEY);
		
		AESDecryptor decryptor = new AESDecryptor(rsaKey);
		try {
			String xml = decryptor.decrypt(key, iv, data);
			Document doc = new XMLReader().parse(xml);
			doc.getRootNode().addAttribute("pass", "true");
			xml = new XMLWriter(doc).generateXML();
			view(req, res, xml);
		} catch (AESDecryptionException e) {
			error(e.getMessage(), req, res);
		}
	}

	private void error(String message, HttpServletRequest req, HttpServletResponse res) throws Exception {
		Document doc = new Document("login");
		Node root = doc.getRootNode();
		root.addAttribute("pass", "false");
		root.addChildNode("error").setContents(message);
		String xml = new XMLWriter(doc).generateXML();
		view(req, res, xml);
	}
	
	private void view(HttpServletRequest req, HttpServletResponse res, String xml) throws JSONException, IOException {
		PrintWriter out;
		String format = getFormat(req);
		if (jsonRequested(format)) {
			JSONObject jsonObject = XML.toJSONObject(xml);
			res.setContentType("application/json; charset=utf-8");
			out = res.getWriter();
			out.write(jsonObject.toString());
		} else {
			res.setContentType("application/xml; charset=utf-8");
			out = res.getWriter();
			out.write(xml);
		}
		out.flush();
	}
	
}
