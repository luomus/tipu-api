package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class ObserverSynonyms extends ResourceBaseServlet {

	private static final long serialVersionUID = 4465232966677098415L;

	public ObserverSynonyms() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("observer-synonyms", "synonym", "ringerObserverSynonyms", "synonymId", this);
		resource.defineField(new Field("correct-id", "ringerId"));
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
