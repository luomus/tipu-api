package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class Codes extends ResourceBaseServlet {

	private static final long serialVersionUID = -2778379649731077319L;

	public Codes() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("codes", "code", " (SELECT DISTINCT code, variable FROM codes ) ", "c", "c.code", this);
		resource.defineJoin("codes", "c_fi", " c.code = c_fi.code AND c.variable = c_fi.variable AND c_fi.language = 'S'");
		resource.defineJoin("codes", "c_sv", " c.code = c_sv.code AND c.variable = c_sv.variable AND c_sv.language = 'R'");
		resource.defineJoin("codes", "c_en", " c.code = c_en.code AND c.variable = c_en.variable AND c_en.language = 'E'");
		
		Field code = new Field("code", "c.variable");
		Field metadata = new Field("metadata", "c_fi.metadata");
		Field descFinnish = new Field("desc", Resource.Lang.FI, "c_fi.description");
		Field descSwedish = new Field("desc", Resource.Lang.SV, "c_sv.description");
		Field descEnglish = new Field("desc", Resource.Lang.EN, "c_en.description");
		Field descLongFinnish = new Field("detailed-desc", Resource.Lang.FI, "c_fi.detailedDescription");
		Field descLongSwedish = new Field("detailed-desc", Resource.Lang.SV, "c_sv.detailedDescription");
		Field descLongEnglish = new Field("detailed-desc", Resource.Lang.EN, "c_en.detailedDescription");
		
		resource.defineField(code);
		resource.defineField(descFinnish);
		resource.defineField(descSwedish);
		resource.defineField(descEnglish);
		resource.defineField(descLongFinnish);
		resource.defineField(descLongSwedish);
		resource.defineField(descLongEnglish);
		resource.defineField(metadata);
		
		resource.orderBy(Resource.ID_FIELD, metadata, code, descFinnish);
		return resource;
	}

	
}
