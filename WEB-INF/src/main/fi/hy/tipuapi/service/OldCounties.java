package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.Resource.Lang;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class OldCounties extends ResourceBaseServlet {

	private static final long serialVersionUID = -8962193981192506790L;
	
	public OldCounties() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("old-counties", "county", "finnishOldCounty", "id", this);
		resource.defineField(new Field("name", Lang.FI, "name"));
		resource.defineField(new Field("name", Lang.SV, "nameSwedish"));
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
