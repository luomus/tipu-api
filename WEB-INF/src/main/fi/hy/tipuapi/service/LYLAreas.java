package fi.hy.tipuapi.service;

import fi.hy.tipuapi.general.Resource;
import fi.hy.tipuapi.general.Resource.Field;
import fi.hy.tipuapi.general.ResourceBaseServlet;

public class LYLAreas extends ResourceBaseServlet {
	
	private static final long serialVersionUID = -8661448681587992735L;

	public LYLAreas() {
		super();
	}
	
	@Override
	protected Resource defineResource() {
		Resource resource = new Resource("lyl-areas", "lyl-area", "lylArea", "id", this);
		resource.defineField(new Field("name", "name"));
		resource.orderBy(Resource.ID_FIELD);
		return resource;
	}
	
}
