package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

import org.junit.Test;

import fi.hy.tipuapi.service.ringing.dao.DAOImple;
import fi.hy.tipuapi.service.ringing.dao.DAOImple.DAOImpleBuilder;
import fi.hy.tipuapi.service.ringing.dao.SharedCachedResources;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;

public class TestConfig {

	public static Config get() {
		try {
			return new ConfigReader("c:/apache-tomcat/app-conf/tipu-api.properties");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static DAOImple buildDAO() throws Exception {
		return getDaoBuilder().openForUserId("userid");
	}

	private static DAOImpleBuilder getDaoBuilder() throws Exception {
		LocalizedTextsContainer localizedTexts = new LocalizedTextsContainer() {
			@Override
			public String getText(String text, String language) throws IllegalArgumentException {
				return null;
			}
			@Override
			public Map<String, String> getAllTexts(String language) throws IllegalArgumentException {
				return Collections.emptyMap();
			}
		};
		RowStructure structure = new RowStructure(TestConfig.get(), localizedTexts, false);
		SharedCachedResources sharedCachedResources = new SharedCachedResources(TestConfig.get(), null);
		DAOImpleBuilder builder = new DAOImpleBuilder().setStructure(structure).setSharedResources(sharedCachedResources);
		return builder;
	}
	
	@Test
	public void test__language_files_have_texts_for_all_languages() throws Exception {
		// LanguageFileReader throws exception if some text is not present in all the files, anyway make sure they have the same amount..
		LocalizedTextsContainer texts = new LanguageFileReader(get()).readUITexts();
		int fi = texts.getAllTexts("fi").size();
		int sv = texts.getAllTexts("sv").size();
		int en = texts.getAllTexts("en").size();
		assertTrue(fi == sv && sv == en);
	}

	public static TransactionConnection getConnection(Config config) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			return new SimpleTransactionConnection(config.connectionDescription());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
