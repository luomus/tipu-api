package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import fi.hy.tipuapi.service.ringing.models.DistributedRings;
import fi.hy.tipuapi.service.ringing.models.RingBatches;
import fi.hy.tipuapi.service.ringing.models.RingBatches.RingBatch;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Ring;
import fi.luomus.commons.utils.DateUtils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import org.junit.Test;

public class DistributedRingsTesting {

	private static final Comparator<String> ringSeriesComparator = new Comparator<String>() {
		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}
	};

	@Test
	public void test_distribution() {
		DistributedRings distributedRings = new DistributedRings(ringSeriesComparator);
		distributedRings.add("A 065", "A 065", date("1.1.2000"), 846);
		distributedRings.add("A 066", "A 066", date("1.1.2000"), 468);
		distributedRings.add("A 067", "A 069", date("1.1.2001"), 846);
		distributedRings.add("A 100", "A 105", date("1.1.2001"), 846);

		assertFalse(distributedRings.isDistributedToSomeoneAtDate(new Ring("A 65"), date("1.1.1999")));
		assertTrue(distributedRings.isDistributedToSomeoneAtDate(new Ring("A 65"), date("1.1.2000")));
		assertFalse(distributedRings.isDistributedToSomeoneAtDate(new Ring("A 655"), date("1.1.2000")));

		assertTrue(distributedRings.isDistributedToRingerAtDate(new Ring("A65"), 846, date("1.1.2000")));
		assertFalse(distributedRings.isDistributedToRingerAtDate(new Ring("A65"), 1, date("1.1.2000")));

		assertEquals("A67", distributedRings.nextRing("A 65"));
		assertEquals("A68", distributedRings.nextRing("A 67"));
		assertEquals(null, distributedRings.nextRing("A 66")); // no other rings for ringer 468
		assertEquals(null, distributedRings.nextRing("A 64")); // A 64 not distributed
		assertEquals(null, distributedRings.nextRing("A 99")); // A 99 not distributed
		assertEquals("A100", distributedRings.nextRing("A 69"));
		assertEquals("A101", distributedRings.nextRing("A 100"));
		assertEquals(null, distributedRings.nextRing("A 105"));
	}

	private DateValue date(String date) {
		return DateUtils.convertToDateValue(date);
	}

	@Test
	public void test_ring_batch_iterator() {
		RingBatch batch = new RingBatch("A", 1, 5);
		assertEquals(1, batch.start());
		assertEquals(5, batch.end());
		int i = 1;
		StringBuilder b = new StringBuilder();
		for (int batchNumber : batch) {
			b.append(i).append(":").append(batchNumber).append(". ");
		}
		assertEquals("1:1. 1:2. 1:3. 1:4. 1:5. ", b.toString());
	}

	@Test
	public void test_ring_batch_join_to() {
		RingBatch first = new RingBatch("A", 3, 5);
		RingBatch second = new RingBatch("A", 6, 7);
		RingBatch third = new RingBatch("A", 1, 2);

		RingBatch result = first.joinGreater(second); 
		result = result.joinLesser(third);

		assertEquals("A : 1 : 7", result.toString());
	}

	@Test
	public void test_ring_batches() {
		RingBatches ringBatches = new RingBatches(ringSeriesComparator);
		ringBatches.add("A 067"); // 2.
		ringBatches.add("A 068"); // 2.
		ringBatches.add("A 069"); // 2.
		ringBatches.add("A 071"); // 3.
		ringBatches.add("A 065"); // 1.
		ringBatches.add("A 001", "A 064"); // 1.
		TreeSet<RingBatch> batches = ringBatches.getBatches("A");
		assertEquals(3, batches.size());
		Iterator<RingBatch> i = batches.iterator();
		assertEquals("A : 1 : 65", i.next().toString());
		assertEquals("A : 67 : 69", i.next().toString());
		assertEquals("A : 71 : 71", i.next().toString());
	}

	@Test 
	public void test_ring_batches_2() {
		RingBatches ringBatches = new RingBatches(ringSeriesComparator);
		ringBatches.add("HL 5802");
		ringBatches.add("HL 5803");
		ringBatches.add("HL 5810");
		ringBatches.add("HL 5819");
		ringBatches.add("HL 5821");
		ringBatches.add("HL 5822");
		ringBatches.add("HL 5804");
		ringBatches.add("HL 5805");
		ringBatches.add("HL 5806");
		ringBatches.add("HL 5807");
		ringBatches.add("HL 5809");
		ringBatches.add("HL 5811");
		ringBatches.add("HL 5812");
		ringBatches.add("HL 5813");
		ringBatches.add("HL 5814");
		ringBatches.add("HL 5815");
		ringBatches.add("HL 5801");
		ringBatches.add("HL 5808");
		Iterator<RingBatch> i = ringBatches.getBatches("HL").iterator();
		assertEquals("HL : 5801 : 5815", i.next().toString());
		assertEquals("HL : 5819 : 5819", i.next().toString());
		assertEquals("HL : 5821 : 5822", i.next().toString());
	}

	@Test
	public void test_ring_batches_maintains_serie_order() {
		RingBatches ringBatches = new RingBatches(ringSeriesComparator);
		ringBatches.add("A 067"); 
		ringBatches.add("B 068"); 
		ringBatches.add("E 069"); 
		ringBatches.add("C 071"); 
		ringBatches.add("AA 065");
		assertEquals("[A, AA, B, C, E]", ringBatches.getSortedSeries().toString());
	}

	@Test
	public void test_ring_batches_explode() {
		RingBatches distributedRings = new RingBatches(ringSeriesComparator);
		distributedRings.add("A 44", "A 49"); 
		distributedRings.add("A 60", "A 61");
		distributedRings.add("A 70", "A 85");
		distributedRings.add("A 90", "A 95");
		distributedRings.add("A 110", "A 110");
		distributedRings.add("A 120", "A 120");

		RingBatches usedRings = new RingBatches(ringSeriesComparator);
		usedRings.add("A 44", "A 45");
		usedRings.add("A 48", "A 49");
		usedRings.add("A 61");
		usedRings.add("A 71", "A 80");
		usedRings.add("A 110", "A 110");
		usedRings.add("A 120", "A 120");

		RingBatches exploded = distributedRings.explodeBy(usedRings);
		TreeSet<RingBatch> set = exploded.getBatches("A");
		assertEquals(11, set.size());
		Iterator<RingBatch> i = set.iterator();
		assertEquals("A : 44 : 45 : used", 	i.next().toString());
		assertEquals("A : 46 : 47", 		i.next().toString());
		assertEquals("A : 48 : 49 : used", 	i.next().toString());
		assertEquals("A : 60 : 60", 		i.next().toString());
		assertEquals("A : 61 : 61 : used", 	i.next().toString());
		assertEquals("A : 70 : 70", 		i.next().toString());
		assertEquals("A : 71 : 80 : used", 	i.next().toString());
		assertEquals("A : 81 : 85", 		i.next().toString());
		assertEquals("A : 90 : 95", 		i.next().toString());
		assertEquals("A : 110 : 110 : used", i.next().toString());
		assertEquals("A : 120 : 120 : used", i.next().toString());
	}

}
