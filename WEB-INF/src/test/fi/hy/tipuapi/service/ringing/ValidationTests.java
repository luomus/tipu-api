package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.zaxxer.hikari.HikariDataSource;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DatasourceDefinition;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.hy.tipuapi.service.ringing.validator.RowValidationData;
import fi.hy.tipuapi.service.ringing.validator.RowValidator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.DateUtils;

public class ValidationTests {

	private static Config config = TestConfig.get();
	private static Map<String, String> localizedTexts = initLocalizedTexts();
	private static ErrorReporter errorReporter = new ErrorReportingToSystemErr();
	private static TransactionConnection con = TestConfig.getConnection(config);
	private static HikariDataSource dataSource;
	private static DAO dao = initDao();

	@AfterClass
	public static void clean() {
		if (con != null) con.release();
		dataSource.close();
	}

	private static DAO initDao() {
		try {
			dataSource = DatasourceDefinition.initDataSource(config);
			RowStructure structure = new RowStructure(config, new LocalizedTextsContainerImple("fi"), false);
			return new TweakedDAOForTests(con, dataSource, config, structure);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static Map<String, String> initLocalizedTexts() {
		try {
			return new LanguageFileReader(config).readUITexts().getAllTexts("fi");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void test_initiation() {
		JSONObject validationResponse = validateInsertForUser(dao.emptyRow(), Mode.RINGINGS);
		assertEquals(false, validationResponse.getBoolean("passes"));
	}

	private JSONObject validateInsertForUser(Row row, Mode mode) {
		return validate(row, mode, DAO.Action.INSERT, RowValidator.USER_VALIDATIONS);
	}

	private JSONObject validateInsert(Row row, Mode mode, boolean adminValidations) {
		return validate(row, mode, DAO.Action.INSERT, adminValidations);
	}

	private JSONObject validate(Row row, Mode mode, DAO.Action action, boolean adminValidations) {
		RowValidationData data = new RowValidationData(row, mode, action, config, errorReporter, dao, localizedTexts).setAdminValidationMode(adminValidations);
		RowValidator validator = new RowValidator(data);
		JSONObject validationResponse = validator.getValidationResponse().getResponse();
		return validationResponse;
	}

	private void debug(JSONObject validationResponse) {
		System.out.println(validationResponse.toString());
	}

	private void assertNoErrors(String fieldName, JSONObject validationResponse) {
		if (validationResponse.getObject("errors").hasKey(fieldName)) {
			Assert.fail("Should not have error for " + fieldName + " but has " + validationResponse.getObject("errors").getArray(fieldName).toString());
		}
	}

	private void assertContainsError(String fieldName, String errorName, JSONObject validationResponse) {
		if (validationResponse.getObject("errors").hasKey(fieldName)) {
			JSONArray errors =  validationResponse.getObject("errors").getArray(fieldName);
			for (JSONObject errorEntry : errors.iterateAsObject()) {
				if (errorEntry.getString("errorName").equals(errorName)) {
					return;
				}
			}
		}
		Assert.fail("No error " + errorName + " for field " + fieldName);
	}

	private void assertNoError(String fieldName, String errorName, JSONObject validationResponse) {
		if (validationResponse.getObject("errors").hasKey(fieldName)) {
			JSONArray errors =  validationResponse.getObject("errors").getArray(fieldName);
			for (JSONObject errorEntry : errors.iterateAsObject()) {
				if (errorEntry.getString("errorName").equals(errorName)) {
					Assert.fail("Error " + errorName + " for field " + fieldName + " exists!");
					return;
				}
			}
		}
	}

	private void assertNoWarnings(String fieldName, JSONObject validationResponse) {
		if (validationResponse.getObject("warnings").hasKey(fieldName)) {
			Assert.fail("Should not have warning for " + fieldName + " but has " + validationResponse.getObject("warnings").getArray(fieldName).toString());
		}
	}

	private void assertContainsWarning(String fieldName, String errorName, JSONObject validationResponse) {
		if (validationResponse.getObject("warnings").hasKey(fieldName)) {
			JSONArray errors =  validationResponse.getObject("warnings").getArray(fieldName);
			for (JSONObject errorEntry : errors.iterateAsObject()) {
				if (errorEntry.getString("errorName").equals(errorName)) {
					return;
				}
			}
		}
		Assert.fail("No warning " + errorName + " for field " + fieldName);
	}

	private void assertNoWarning(String fieldName, String errorName, JSONObject validationResponse) {
		if (validationResponse.getObject("warnings").hasKey(fieldName)) {
			JSONArray errors =  validationResponse.getObject("warnings").getArray(fieldName);
			for (JSONObject errorEntry : errors.iterateAsObject()) {
				if (errorEntry.getString("errorName").equals(errorName)) {
					Assert.fail("Warning " + errorName + " for field " + fieldName + " exists!");
					return;
				}
			}
		}
	}

	private void assertErrorCount(int expectedCount, JSONObject validationResponse) {
		if (validationResponse.hasKey("errors")) {
			int errorCount = 0;
			for (String fieldName : validationResponse.getObject("errors").getKeys()) {
				errorCount += validationResponse.getObject("errors").getArray(fieldName).size();
			}
			if (errorCount != expectedCount) {
				Assert.fail("Expected " + expectedCount + " errors but there were " + errorCount + ". " + validationResponse.getObject("errors").toString());
			}
		} else if (expectedCount != 0) {
			Assert.fail("Expected " + expectedCount + " errors but there were none. ");
		}
	}

	private void assertWarningCount(int expectedCount, JSONObject validationResponse) {
		if (validationResponse.hasKey("warnings")) {
			int errorCount = 0;
			for (String fieldName : validationResponse.getObject("warnings").getKeys()) {
				errorCount += validationResponse.getObject("warnings").getArray(fieldName).size();
			}
			if (errorCount != expectedCount) {
				Assert.fail("Expected " + expectedCount + " warnings but there were " + errorCount + ". " + validationResponse.getObject("warnings").toString());
			}
		} else if (expectedCount != 0) {
			Assert.fail("Expected " + expectedCount + " warnings but there were none. ");
		}
	}

	private Row fillRequiredRinging() {
		Row row = dao.emptyRow();
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("ACRARU");
		row.get("ringStart").setValue("P619301");
		row.get("municipality").setValue("INKOO");
		row.get("lat").setValue("66666");
		row.get("lon").setValue("33333");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("coordinateMeasurementMethod").setValue("GPS");
		row.get("coordinateAccuracy").setValue("100");
		row.get("age").setValue("1");
		row.get("captureMethod").setValue("A");
		return row;
	}

	@Test
	public void test__number_length() {
		Row row = dao.emptyRow();
		row.get("uniformLat").setValue("1234567");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("uniformLat", validationResponse);
	}

	@Test
	public void test__number_length_1() {
		Row row = dao.emptyRow();
		row.get("uniformLat").setValue("-1234567");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("uniformLat", validationResponse);
	}

	@Test
	public void test__number_length_2() {
		Row row = dao.emptyRow();
		row.get("uniformLat").setValue("12345678");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("uniformLat", "invalid_length_too_long", validationResponse);
	}

	@Test
	public void test_varchar_length() {
		Row row = dao.emptyRow();
		row.get("diario").setValue("123456789"); // max length 9
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		assertNoErrors("diario", validationResponse);
	}

	@Test
	public void test_varchar_length_2() {
		Row row = dao.emptyRow();
		row.get("diario").setValue("1234567890"); // max length 9
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertContainsError("diario", "invalid_length_too_long", validationResponse);
	}

	@Test
	public void test_decimal_length_1() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue(""); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal_length_2() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("123"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal_length_3() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("123.0"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal_length_4() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("123.1234567"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal_length_4_2() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("-123.1234567"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal_length_5() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("1234.1"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("wgs84DecimalLat", "invalid_length_too_long", validationResponse);
	}

	@Test
	public void test_decimal_length_6() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("123.1234567823423"); // number(15,8)
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("wgs84DecimalLat", "decimal_too_precise", validationResponse);
	}

	@Test
	public void test_decimal__comma__() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("23,123"); // number(15,8)
		assertEquals("23.123", row.get("wgs84DecimalLat").getValue());
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("wgs84DecimalLat", validationResponse);
	}

	@Test
	public void test_decimal__comma__2() {
		Row row = dao.emptyRow();
		row.get("wgs84DecimalLat").setValue("23,1,23"); // number(15,8)
		assertEquals("23.1.23", row.get("wgs84DecimalLat").getValue());
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("wgs84DecimalLat", "invalid_decimal", validationResponse);
	}

	@Test
	public void test_ring_not_used_requires_eventDate() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("P619301");
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertEquals(false, validationResponse.getBoolean("passes"));
		assertErrorCount(1, validationResponse);
		assertContainsError("eventDate", "required_field", validationResponse);
	}

	@Test
	public void test_ring_not_used() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("P619301"); // not used, distributed to 846
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		row.get("hour").setValue("3");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertEquals(true, validationResponse.getBoolean("passesWithWarnings"));
	}

	@Test
	public void test_ring_not_used_with_series() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("P619301"); // not used -
		row.get("ringEnd").setValue("P619303"); // not used, all distributed to 846
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		row.get("hour").setValue("3");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertEquals(true, validationResponse.getBoolean("passesWithWarnings"));
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("A203094"); // used
		row.get("ringEnd").setValue("A203095");
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("ringStart", "ring-already-used", validationResponse);
		assertErrorCount(1, validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings__for_letter_in_end_ring() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("FL0000001"); // used
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("ringStart", "ring-already-used", validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings__for_letter_in_end_ring_2() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("1F"); // used (same ring as FL0000001 above!)
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("ringStart", "ring-already-used", validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings__for_letter_in_end_ring_3() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("F1"); // not used! (NOT same ring as 1F above!)
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("ringStart", validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings__except_for_update_if_ring_not_changed() {
		Row row = fillRequiredRinging();
		row.get("id").setValue("8643337");
		row.get("type").setValue("ringing");
		row.get("legRing").setValue("A 0203094"); // used
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("31.07.1974");
		JSONObject validationResponse = validate(row, Mode.RINGINGS, DAO.Action.UPDATE, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("ringStart", "ring-already-used", validationResponse);
		assertNoError("legRing", "ring-already-used", validationResponse);
		assertNoError("nameRing", "ring-already-used", validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_ring_not_used__does_not_allow_to_report_used_rings___reporting_not_ringStart_end_but_legRing() { // This is for Tipu admin ui
		Row row = dao.emptyRow();
		row.get("legRing").setValue("A 0203094"); // used
		row.get("ringStart").setValue("");
		row.get("ringEnd").setValue("");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ring-already-used", validationResponse);
		assertNoErrors("ringStart", validationResponse);
	}

	@Test
	public void test_ring_not_used_does_not_allow_other_columns() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("P619301");
		row.get("ringNotUsedReason").setValue("T");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.2010");
		row.get("hour").setValue("3");
		row.get("species").setValue("PANHAL");
		row.get("age").setValue("PM");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertEquals(false, validationResponse.getBoolean("passes"));
		assertContainsError("species", "ringNotUsed_not_allowed_column", validationResponse);
		assertContainsError("age", "ringNotUsed_not_allowed_column", validationResponse);
		assertNoErrors("hour", validationResponse);
	}

	@Test
	public void test_bird_station_ringer_distribution() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("YL0038151"); // distributed to 670 == ASPI fake ringer
		row.get("birdStation").setValue("ASPI");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.1999");
		row.get("species").setValue("AEGCAU");
		row.get("age").setValue("PM");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("ringStart", "ring-already-used", validationResponse);
		assertNoError("ringStart", "ring-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_bird_station_ringer_distribution_2() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("YL0038151"); // distributed to 670 == ASPI fake ringer
		row.get("birdStation").setValue("");
		row.get("ringer").setValue("846");
		row.get("eventDate").setValue("1.1.1999");
		row.get("species").setValue("AEGCAU");
		row.get("age").setValue("PM");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("ringStart", "ring-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_coordinates_inside_bird_station_municipality__neither_correct() {
		Row row = fillRequiredRinging();
		row.get("birdStation").setValue("ASPI");
		row.get("municipality").setValue("HELSIN");
		row.get("lat").setValue("6666");
		row.get("lon").setValue("3333");
		row.get("coordinateSystem").setValue("YKJ");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("municipality", "coordinates_not_inside_municipality", validationResponse);
		assertContainsError("birdStation", "coordinates_not_inside_birdStation", validationResponse);
	}

	@Test
	public void test_coordinates_inside_bird_station_municipality__municipality_correct() {
		Row row = fillRequiredRinging();
		row.get("birdStation").setValue("ASPI");
		row.get("municipality").setValue("INKOO");
		row.get("lat").setValue("6666");
		row.get("lon").setValue("3333");
		row.get("coordinateSystem").setValue("YKJ");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoError("municipality", "coordinates_not_inside_municipality", validationResponse);
		assertContainsError("birdStation", "coordinates_not_inside_birdStation", validationResponse);
	}

	@Test
	public void test_coordinates_inside_bird_station_municipality__birdstation_correct() {
		Row row = fillRequiredRinging();
		row.get("birdStation").setValue("ASPI");
		row.get("municipality").setValue("INKOO");
		row.get("lat").setValue("6682");
		row.get("lon").setValue("3467");
		row.get("coordinateSystem").setValue("YKJ");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("municipality", "coordinates_not_inside_municipality", validationResponse);
		assertNoError("birdStation", "coordinates_not_inside_birdStation", validationResponse);
	}

	@Test
	public void test_coordinates_inside_bird_station_municipality__both_correct() {
		Row row = fillRequiredRinging();
		row.get("birdStation").setValue("ASPI");
		row.get("municipality").setValue("PERNAJ");
		row.get("lat").setValue("6682");
		row.get("lon").setValue("3467");
		row.get("coordinateSystem").setValue("YKJ");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoError("municipality", "coordinates_not_inside_municipality", validationResponse);
		assertNoError("birdStation", "coordinates_not_inside_birdStation", validationResponse);
	}

	@Test
	public void test_accuracy_of_something_validator() {
		Row row = fillRequiredRinging();
		row.get("sexDeterminationMethod").setValue("H");
		row.get("sex").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("sexDeterminationMethod", validationResponse);
		assertEquals("", row.get("sexDeterminationMethod").getValue());
	}

	@Test
	public void test_accuracy_of_something_validator_2() {
		Row row = fillRequiredRinging();
		row.get("sexDeterminationMethod").setValue("H");
		row.get("sex").setValue("K");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("sexDeterminationMethod", validationResponse);
	}

	@Test
	public void test_biometric_validations__all_empty() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("STRURA");
		row.get("weightInGrams").setValue("");
		row.get("wingLengthInMillimeters").setValue("");
		row.get("thirdFeatherLengthInMillimeters").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarnings("weightInGrams", validationResponse);
		assertNoWarnings("wingLengthInMillimeters", validationResponse);
		assertNoWarnings("thirdFeatherLengthInMillimeters", validationResponse);
	}

	@Test
	public void test_biometric_validations__biometric_not_given() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("STRURA");
		row.get("thirdFeatherLengthInMillimeters").setValue("0.1"); // biometric range not defined for species
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarnings("weightInGrams", validationResponse);
		assertNoWarnings("wingLengthInMillimeters", validationResponse);
		assertNoWarnings("thirdFeatherLengthInMillimeters", validationResponse);
	}

	@Test
	public void test_biometric_validations__all_too_small() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ACRDUM");
		row.get("age").setValue("2");
		row.get("weightInGrams").setValue("8");
		row.get("wingLengthInMillimeters").setValue("55");
		row.get("thirdFeatherLengthInMillimeters").setValue("41.1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsWarning("weightInGrams", "biometric_too_small_adult", validationResponse);
		assertContainsWarning("wingLengthInMillimeters", "biometric_too_small_adult", validationResponse);
		assertContainsWarning("thirdFeatherLengthInMillimeters", "biometric_too_small_adult", validationResponse);
	}

	@Test
	public void test_biometric_validations__all_too_small_but_moulting() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ACRDUM");
		row.get("age").setValue("2");
		row.get("moulting").setValue("S");
		row.get("weightInGrams").setValue("1");
		row.get("wingLengthInMillimeters").setValue("1");
		row.get("thirdFeatherLengthInMillimeters").setValue("1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsWarning("weightInGrams", "biometric_too_small_adult", validationResponse);
		assertNoWarning("wingLengthInMillimeters", "biometric_too_small_adult", validationResponse);
		assertNoWarning("thirdFeatherLengthInMillimeters", "biometric_too_small_adult", validationResponse);
	}

	@Test
	public void test_biometric_validations__all_too_large() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ACRDUM");
		row.get("age").setValue("2");
		row.get("weightInGrams").setValue("18");
		row.get("wingLengthInMillimeters").setValue("68.1");
		row.get("thirdFeatherLengthInMillimeters").setValue("56");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsWarning("weightInGrams", "biometric_too_large_adult", validationResponse);
		assertContainsWarning("wingLengthInMillimeters", "biometric_too_large_adult", validationResponse);
		assertContainsWarning("thirdFeatherLengthInMillimeters", "biometric_too_large_adult", validationResponse);
	}

	@Test
	public void test_biometric_validations__all_in_range() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ACRDUM");
		row.get("age").setValue("2");
		row.get("weightInGrams").setValue("9.00");
		row.get("wingLengthInMillimeters").setValue("68.000");
		row.get("thirdFeatherLengthInMillimeters").setValue("42.0001");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarnings("weightInGrams", validationResponse);
		assertNoWarnings("wingLengthInMillimeters", validationResponse);
		assertNoWarnings("thirdFeatherLengthInMillimeters", validationResponse);
	}

	@Test
	public void test_biometric_validations__young() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ACRDUM");
		row.get("age").setValue("PM");
		row.get("weightInGrams").setValue("3"); // adult range is 9-18, young 3-17
		row.get("wingLengthInMillimeters").setValue("69"); // young range is not defined, adult max range is 68, which should be used for youngs also
		row.get("thirdFeatherLengthInMillimeters").setValue("40"); // young range is not defined, adult min range is 4.2 which should not be used for youngs
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarnings("weightInGrams", validationResponse);
		assertContainsWarning("wingLengthInMillimeters", "biometric_too_large_young", validationResponse);
		assertNoWarnings("thirdFeatherLengthInMillimeters", validationResponse);
	}

	@Test
	public void test_AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator() {
		Row row = fillRequiredRinging();
		row.get("wingLengthInMillimeters").setValue("69");
		row.get("wingLengthAccuracy").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("wingLengthAccuracy", validationResponse);
	}

	@Test
	public void test_AccuracyOfSomethingWhichIsRequiredForNonRoundFigureNumbersValidator_2() {
		Row row = fillRequiredRinging();
		row.get("wingLengthInMillimeters").setValue("69.1");
		row.get("wingLengthAccuracy").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("wingLengthAccuracy", "accuracy_required_for_number_that_is_not_a_round_figure", validationResponse);
	}

	@Test
	public void test_AccuracyOfSomethingWhichIsRequiredForNonEvenNumbersValidator_3() {
		Row row = fillRequiredRinging();
		row.get("wingLengthInMillimeters").setValue("69.1");
		row.get("wingLengthAccuracy").setValue("1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("wingLengthAccuracy", validationResponse);
	}

	@Test
	public void test_positive_integer() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("clutchNumber", validationResponse);
	}

	@Test
	public void test_positive_integer_2() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("clutchNumber", validationResponse);
	}

	@Test
	public void test_positive_integer_3() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("a");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "invalid_integer", validationResponse);
	}

	@Test
	public void test_positive_integer_4() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("2.1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "invalid_integer", validationResponse);
	}

	@Test
	public void test_positive_integer_5() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("2,1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "invalid_integer", validationResponse);
	}

	@Test
	public void test_positive_integer_6() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("-5");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "must_be_positive_integer", validationResponse);
	}

	@Test
	public void test_positive_integer_7() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("-5.5");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "invalid_integer", validationResponse);
	}

	@Test
	public void test_positive_integer_8() {
		Row row = fillRequiredRinging();
		row.get("clutchNumber").setValue("0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("clutchNumber", "must_be_positive_integer", validationResponse);
	}

	@Test
	public void test_range_validator_1() {
		Row row = fillRequiredRinging();
		row.get("fat").setValue("0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("fat", validationResponse);
	}

	@Test
	public void test_range_validator_2() {
		Row row = fillRequiredRinging();
		row.get("fat").setValue("5.0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("fat", validationResponse);
	}

	@Test
	public void test_range_validator_3() {
		Row row = fillRequiredRinging();
		row.get("fat").setValue("-0.1");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("fat", "too_small_value", validationResponse);
	}

	@Test
	public void test_range_validator_4() {
		Row row = fillRequiredRinging();
		row.get("fat").setValue("9");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("fat", "too_large_value", validationResponse);
	}

	@Test
	public void test_numberOfYoungs_warning_1() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ALAARV"); // Limit is 6
		row.get("numberOfYoungs").setValue("7");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsWarning("numberOfYoungs", "young-count-species-max-warning", validationResponse);
	}

	@Test
	public void test_numberOfYoungs_warning_2() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ALAARV"); // Limit is 6
		row.get("numberOfYoungs").setValue("6");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarning("numberOfYoungs", "young-count-species-max-warning", validationResponse);
	}

	@Test
	public void test_numberOfYoungs_warning_3() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("ALAARV"); // Limit is 6
		row.get("numberOfYoungs").setValue("5");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarning("numberOfYoungs", "young-count-species-max-warning", validationResponse);
	}

	@Test
	public void test_numberOfYoungs_warning__no_limits_for_species() {
		Row row = fillRequiredRinging();
		row.get("species").setValue("HIPCALR"); // no limit specified
		row.get("numberOfYoungs").setValue("5");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoWarning("numberOfYoungs", "young-count-species-max-warning", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__all_ok__but_used() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("418");
		row.get("species").setValue("LARFUS");
		row.get("fieldReadableCode").setValue("C0AA"); // distributed 22.06.1999 to 418, is used
		row.get("mainColor").setValue("VA");
		row.get("eventDate").setValue("01.01.2000");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-already-used", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__all_ok_subspecies() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("418");
		row.get("species").setValue("LARFUSF"); // distributed for LARFUS
		row.get("fieldReadableCode").setValue("C0AA"); // distributed 22.06.1999 to 418, is used
		row.get("mainColor").setValue("VA");
		row.get("eventDate").setValue("01.01.2000");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__too_early() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("418");
		row.get("species").setValue("LARFUS");
		row.get("fieldReadableCode").setValue("C0AA"); // distributed 22.06.1999 to 418, is used
		row.get("mainColor").setValue("VA");
		row.get("eventDate").setValue("01.01.1999"); // distributed 22.06.1999
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__wrong_species() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("418");
		row.get("species").setValue("LARCAN");
		row.get("fieldReadableCode").setValue("C0AA"); // distributed 22.06.1999 to 418 for LARFUS, is used
		row.get("mainColor").setValue("VA");
		row.get("eventDate").setValue("01.01.2000");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__wrong_ringer() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");
		row.get("fieldReadableCode").setValue("C0AA");
		row.get("mainColor").setValue("VA");
		row.get("eventDate").setValue("01.01.2000"); // distributed 22.06.1999 to 418 (not 846) for LARFUS, is used
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_field_readable_distribution__all_ok__and_not_used() {
		Row row = fillRequiredRinging();
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");
		row.get("fieldReadableCode").setValue("XX0XX");
		row.get("mainColor").setValue("VA");
		row.get("attachmentLeftRight").setValue("E");
		row.get("eventDate").setValue("29.7.2014"); // distributed 22.06.1999
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("fieldReadableCode", validationResponse);
	}

	@Test
	public void test_only_for_adults() {
		Row row = fillRequiredRinging();
		row.get("age").setValue("PM");
		row.get("broodPatch").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("broodPatch", validationResponse);
	}

	@Test
	public void test_only_for_adults_2() {
		Row row = fillRequiredRinging();
		row.get("age").setValue("PM");
		row.get("broodPatch").setValue("0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("broodPatch", "only_for_adults", validationResponse);
	}

	@Test
	public void test_only_for_adults_3() {
		Row row = fillRequiredRinging();
		row.get("age").setValue("1");
		row.get("broodPatch").setValue("0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertContainsError("broodPatch", "only_for_adults", validationResponse);
	}

	@Test
	public void test_only_for_adults_4() {
		Row row = fillRequiredRinging();
		row.get("age").setValue("2");
		row.get("broodPatch").setValue("0");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("broodPatch", validationResponse);
	}

	private Row fillRequiredRecovery() {
		Row row = dao.emptyRow();
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("STRALU");
		row.get("municipality").setValue("INKOO");
		row.get("lat").setValue("6666666");
		row.get("lon").setValue("3333333");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("coordinateMeasurementMethod").setValue("GPS");
		row.get("coordinateAccuracy").setValue("1");
		row.get("age").setValue("1");
		row.get("captureMethod").setValue("A");
		row.get("legRing").setValue("D 89115");
		return row;
	}

	@Test
	public void test_recovery() {
		Row row = fillRequiredRecovery();
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertTrue(validationResponse.getBoolean("passes"));
	}

	@Test
	public void test_recovery_metal_ring_or_fieldreadable_must_be_given() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("");
		row.get("fieldReadableCode").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertContainsError("legRing", "metal_ring_or_field_readable_must_be_given", validationResponse);
		assertContainsError("fieldReadableCode", "metal_ring_or_field_readable_must_be_given", validationResponse);
	}

	@Test
	public void test_recovery_metal_ring_or_fieldreadable_must_be_given_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("");
		row.get("fieldReadableCode").setValue("BLAA");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertNoError("legRing", "metal_ring_or_field_readable_must_be_given", validationResponse);
		assertNoError("fieldReadableCode", "metal_ring_or_field_readable_must_be_given", validationResponse);
	}

	@Test
	public void test_recovery_metal_ring_or_fieldreadable_must_be_given_3() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("BLAA");
		row.get("fieldReadableCode").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertNoError("legRing", "metal_ring_or_field_readable_must_be_given", validationResponse);
		assertNoError("fieldReadableCode", "metal_ring_or_field_readable_must_be_given", validationResponse);
	}

	@Test
	public void test_recovery_metal_ring_or_fieldreadable_must_be_given_4() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("BLAA");
		row.get("fieldReadableCode").setValue("BLAA");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertNoError("legRing", "metal_ring_or_field_readable_must_be_given", validationResponse);
		assertNoError("fieldReadableCode", "metal_ring_or_field_readable_must_be_given", validationResponse);
	}

	@Test
	public void test_recovery__ring_must_be_correctly_formatted() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("BLAA");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertContainsError("legRing", "not-valid-ring-format", validationResponse);
	}

	@Test
	public void test_recovery__ring_must_be_correctly_formatted_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("BL 4444");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertNoError("legRing", "not-valid-ring-format", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_invalid_ring() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("BLAA");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "not-valid-ring-format", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_ring_must_be_distributed_to_someone_before_the_date() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("A 9999999"); // not distributed
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ring-not-distributed-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_ring_must_be_distributed_to_someone_before_the_date_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 80029"); // distributed at 1980 to ringer 589
		row.get("eventDate").setValue("1.1.1978");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ring-not-distributed-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_ring_must_be_distributed_to_someone_before_the_date_3() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 80029");  // distributed at 1980 to ringer 589
		row.get("eventDate").setValue("1.12.1980");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("legRing", "ring-not-distributed-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_1() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0038950"); // distributed but not used
		row.get("eventDate").setValue("1.1.2010");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ADMIN_leg_ring_not_used", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0030284"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 1995 renkaaseen YL0038920
		row.get("eventDate").setValue("15.9.1990"); // ennen rengastusta
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ADMIN_reporting_recovery_before_ringing", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_3() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0030284"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 1995 renkaaseen YL0038920
		row.get("eventDate").setValue("1.1.1991"); // kaikki ok
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("legRing", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_4() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0030284"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 5.1.1995 renkaaseen YL0038920
		row.get("eventDate").setValue("5.1.1995"); // vaihtopäivänä käy sekä vanha että uusi
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("legRing", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_5() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0038920"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 5.1.1995 renkaaseen YL0038920
		row.get("eventDate").setValue("5.1.1995"); // vaihtopäivänä käy sekä vanha että uusi
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("legRing", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_6() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0030284"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 5.1.1995 renkaaseen YL0038920
		row.get("eventDate").setValue("6.1.1995"); // vaihtopäivän jälkeen vain uusi
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("legRing", "ring_not_in_birds_leg_at_the_date", validationResponse);
	}

	@Test
	public void test_recovery__leg_ring_ADMIN_7() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("YL0038920"); // YL0030284 jaettu 14.9.1990, rengastettu 28.9.1990 REGREG:n jalkaan, vaihdettu 5.1.1995 renkaaseen YL0038920
		row.get("eventDate").setValue("6.1.1995"); // vaihtopäivän jälkeen vain uusi
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("legRing", validationResponse);
	}

	@Test
	public void test_recovery__species_ADMIN_1() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 0231926");
		row.get("eventDate").setValue("1.1.2009");
		row.get("species").setValue("STRALU"); // in history STRALU and STRALUP
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("species", validationResponse);
		assertNoErrors("legRing", validationResponse); // if not distributed we wont even go this far in the validations, so make sure we do..
	}

	@Test
	public void test_recovery__species_ADMIN_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 0231926");
		row.get("eventDate").setValue("1.1.2009");
		row.get("species").setValue("STRALUP"); // in history STRALU and STRALUP
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("species", validationResponse);
		assertNoErrors("legRing", validationResponse); // if not distributed we wont even go this far in the validations, so make sure we do..
	}

	@Test
	public void test_recovery__species_ADMIN_3() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 0231926");
		row.get("eventDate").setValue("1.1.2009");
		row.get("species").setValue("STRALUH"); // in history STRALU and STRALUP
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("species", validationResponse);
		assertNoErrors("legRing", validationResponse); // if not distributed we wont even go this far in the validations, so make sure we do..
	}

	@Test
	public void test_recovery__species_ADMIN_4() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 0231926");
		row.get("eventDate").setValue("1.1.2009");
		row.get("species").setValue("PANHAL"); // in history STRALU and STRALUP
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("species", "ADMIN_recovery_species_mismatch", validationResponse);
		assertNoErrors("legRing", validationResponse); // if not distributed we wont even go this far in the validations, so make sure we do..
	}

	@Test
	public void test_recovery__species_ADMIN_5_recovery_species_not_accurate() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 0231926");
		row.get("eventDate").setValue("1.1.2009");
		row.get("species").setValue("PANHAL"); // in history STRALU and STRALUP
		row.get("speciesAccuracy").setValue("E");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("species", "ADMIN_recovery_species_mismatch", validationResponse);
	}

	@Test
	public void test_recovery__species_ADMIN_history_is_wrong() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("DT0028142"); // in history LARMAR and LARARG without neither being marked as inaccurate
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("legRing", "ADMIN_species_history_mismatch", validationResponse);
	}

	@Test
	public void test_recovery__species_ADMIN_history_is_not_wrong() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("A 0586790"); // In history DENMAJ, DENLEU but DENLEU marked innaccurate
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("legRing", validationResponse);
	}

	@Test
	public void test_fieldReadable_in_neck_then_attachmentLefRight_not_a_valid_variable() {
		Row row = fillRequiredRecovery();
		row.get("attachmentPoint").setValue("K"); // kaulassa
		row.get("attachmentLeftRight").setValue("O");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("attachmentPoint", validationResponse);
		assertContainsError("attachmentLeftRight", "attachmentLeftRight_can_not_be_given_for_neck_rings", validationResponse);
	}

	@Test
	public void test_fieldReadable_in_neck_then_attachmentLefRight_not_a_valid_variable_2() {
		Row row = fillRequiredRecovery();
		row.get("attachmentPoint").setValue("S"); // siipi
		row.get("attachmentLeftRight").setValue("O");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		debug(validationResponse);
		assertNoErrors("attachmentPoint", validationResponse);
		assertNoErrors("attachmentLeftRight", validationResponse);
	}

	//	#	laji	ikä p	ikä vuosina
	//	1	STRALU	6890	18,87671233
	//	2	STRALU	6570	18
	//	3	STRALU	6320	17,31506849
	//	4	STRALU	6240	17,09589041
	//	5	STRALU	6230	17,06849315
	//	6	STRALU	6220	17,04109589
	//	7	STRALU	6180	16,93150685
	//	8	STRALU	5890	16,1369863
	//	9	STRALU	5860	16,05479452
	//	10	STRALU	5850	16,02739726 --- raja
	//	11	STRALU	5840	16

	@Test
	public void test_ADMIN_species_age_record_warning_1() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 89115"); // ringed 1983 as +5 old
		row.get("eventDate").setValue("1.1.2014"); // at 2014 this makes the age to be 31 years + 5 years = 36 years
		row.get("species").setValue("STRALU"); // tenth highest age record is 16 years
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("species", "ADMIN_warning_species_age", validationResponse); // 36 > 16 so we warn
	}

	@Test
	public void test_ADMIN_species_age_record_warning_2() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 89115"); // ringed 1983 as +5 old
		row.get("eventDate").setValue("1.1.1997"); // without ringing age being +5 years 1997-1983 = 14 years -> we would not warn. We test here that the age +5 years is added.
		row.get("species").setValue("STRALU"); // tenth highest age record is 16 years
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("species", "ADMIN_warning_species_age", validationResponse); // 19 > 16 so we warn
	}

	@Test
	public void test_ADMIN_species_age_record_warning_3() {
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("D 89115"); // ringed 1983 as +5 old
		row.get("eventDate").setValue("1.1.1993"); // age = 10 + 5 = 15 years
		row.get("species").setValue("STRALU"); // tenth highest age record is 16 years
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoWarning("species", "ADMIN_warning_species_age", validationResponse); // 15 < 16 so we don't warn
	}

	@Test
	public void test_ringing__field_readable_combinations_must_give_code_and_mainColor_if_something_given() {
		Row row = fillRequiredRecovery();
		row.get("mainShade").setValue("T");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("fieldReadableCode", "required_field", validationResponse);
		assertContainsError("mainColor", "required_field", validationResponse);
	}

	@Test
	public void test_ringing__field_readable_combinations_must_give_code_and_mainColor_if_something_given_2() {
		Row row = fillRequiredRecovery();
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("fieldReadableCode", validationResponse);
		assertNoErrors("mainColor", validationResponse);
	}

	@Test
	public void test_recovery__field_readable__requires_background_colors() {
		Row row = fillRequiredRecovery();
		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("");
		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("mainColor", "required_field", validationResponse);
		assertContainsError("removedFieldReadableMainColor", "required_field", validationResponse);
	}

	@Test
	public void test_recovery__field_readable__requires_background_colors_2() {
		Row row = fillRequiredRecovery();
		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("mainColor", validationResponse);
		assertNoErrors("removedFieldReadableMainColor", validationResponse);
	}

	//							normal	removed	change
	//#		type		mode	given	given	value
	//
	//1		RECOVERY	USER	YES		YES		empty		virhe: change field annettava
	//2		RECOVERY	USER	YES		YES		P			virhe: normal fields ei olisi saanut antaa
	//3		RECOVERY	USER	YES		YES		V			ringsAttached oltava "A", "B", "K", "V", normal field- ring must be distributed not used
	//4		RECOVERY	USER	YES		NO		empty		ringsAttached ei saa olla "A", "B", "K", "V"
	//5		RECOVERY	USER	YES		NO		P			virhe: removed olisi pitänyt antaa
	//6		RECOVERY	USER	YES		NO		V			virhe: removed olisi pitänyt antaa
	//7		RECOVERY	USER	NO		YES		empty		virhe: change field annettava
	//8		RECOVERY	USER	NO		YES		P			ringsAttached ei saa olla "A", "B", "K", "V"
	//9		RECOVERY	USER	NO		YES		V			virhe: normal fields annettava
	//10	RECOVERY	USER	NO		NO		empty		ringsAttached ei saa olla "A", "B", "K", "V"
	//11	RECOVERY	USER	NO		NO		P			virhe: removed olisi pitänyt antaa
	//12	RECOVERY	USER	NO		NO		V			virhe: sekä removed että normal olisi pitänyt antaa
	//13	RECOVERY	ADMIN	YES		YES		V			ringsAttached oltava "A", "B", "K", "V",  normal field- ring must be distributed not used, poistetun oltava jalassa
	//14	RECOVERY	ADMIN	YES		NO		empty		ringsAttached ei saa olla "A", "B", "K", "V", normal field täytyy olla linnun jalassa
	//15	RECOVERY	ADMIN	NO		YES		P			ringsAttached ei saa olla "A", "B", "K", "V", poistetun oltava jalassa

	@Test
	public void test_recovery__field_readable_combinations_1() {
		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableChanges", "should-give-fieldreadable-changedOrRemoved-code", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_2() {
		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "should-not-give-new-info-if-fieldreadable-removed", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_3() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX"); // ei lainkaan jaettu kenellekkään
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(6, validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
		assertContainsError("ringsAttached", "fieldreadable_attached_but_ringsAttached_doesnt_state_that", validationResponse);

		assertContainsError("fieldReadableCode", "removed_and_added_fieldreadable_cant_be_the_same", validationResponse);
		assertContainsError("removedFieldReadableMainColor", "removed_and_added_fieldreadable_cant_be_the_same", validationResponse);
		assertContainsError("mainColor", "removed_and_added_fieldreadable_cant_be_the_same", validationResponse);
		assertContainsError("removedFieldReadableMainColor", "removed_and_added_fieldreadable_cant_be_the_same", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_3_2() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("CE73");
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-already-used", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_3_3() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("CE74");
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX"); // Testatataan vasta admin puolella
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_4() { // tavallinen värihavainto

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_5() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(2, validationResponse);
		assertContainsError("fieldReadableCode", "should-not-give-new-info-if-fieldreadable-removed", validationResponse);
		assertContainsError("removedFieldReadableCode", "should-give-removedFieldReadableCode", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_6() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("removedFieldReadableCode", "should-give-normal-and-removed-fieldReadableCode", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_7() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("fieldReadableChanges", "should-give-fieldreadable-changedOrRemoved-code", validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertErrorCount(3, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_8() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_8_2() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(2, validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_8_3() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue(""); // siipimerkki

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_9() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");
		row.get("attachmentPoint").setValue("J");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("V");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("fieldReadableCode", "should-give-normal-and-removed-fieldReadableCode", validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertErrorCount(3, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_10() {  // Ei mitään väritietoja

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_10_2() {  // Ei mitään väritietoja

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(2, validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_10_3() {  // Ei mitään väritietoja

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("V");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(2, validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_11() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("removedFieldReadableCode", "should-give-removedFieldReadableCode", validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_12() {

		Row row = fillRequiredRecovery();

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");
		row.get("attachmentPoint").setValue("");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(3, validationResponse);
		assertContainsError("fieldReadableCode", "should-give-normal-and-removed-fieldReadableCode", validationResponse);
		assertContainsError("removedFieldReadableCode", "should-give-normal-and-removed-fieldReadableCode", validationResponse);
		assertContainsError("ringsAttached", "fieldreadable_attached_but_ringsAttached_doesnt_state_that", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("CE73"); // käytetty
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX"); // ei käytetty, ei jaettu
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(4, validationResponse);
		assertContainsError("removedFieldReadableCode", "ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-already-used", validationResponse);
		assertContainsError("ringsAttached", "fieldreadable_attached_but_ringsAttached_doesnt_state_that", validationResponse);
		assertContainsError("legRing", "metal_leg_ring_must_be_given", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_3() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("PANHAL");

		row.get("fieldReadableCode").setValue("CE73"); // distributed for LARFUS (not PANHAL)
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("removedFieldReadableCode", "ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
		assertContainsError("ringsAttached", "fieldreadable_attached_but_ringsAttached_doesnt_state_that", validationResponse);
		assertContainsError("legRing", "metal_leg_ring_must_be_given", validationResponse);
		assertErrorCount(4, validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_4() { // lukumerkkien vaihto
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT0155875");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("");
		row.get("attachmentPoint").setValue("");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("mainColor", "required_field", validationResponse);
		assertContainsWarning("attachmentPoint", "fieldreadable_attachmentPoint_not_given_will_come_from_distribution_warning", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_5() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT0155875");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_6() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT0155875");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("XXXT1"); // distributed to PANHAL
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_7() { // lukumerkkien vaihto
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT0155875");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("XXXT2"); // distributed to LARFUS at 2010
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_13_8() { // lukumerkkien vaihto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT0155875");
		row.get("eventDate").setValue("1.1.2000");
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("XXXT2");  // distributed 2010
		row.get("mainColor").setValue("VA");
		row.get("attachmentPoint").setValue("J");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("V");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "fieldreadable-not-distributed-to-ringer-at-the-date", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_14() { // tavallinen värihavainto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("PANHAL");

		row.get("fieldReadableCode").setValue("XXX");
		row.get("mainColor").setValue("PUVA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_14_3() { // tavallinen värihavainto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("309");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("CE74");
		row.get("mainColor").setValue("VA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("fieldReadableCode", "ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_14_4() { // tavallinen värihavainto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("CE75");
		row.get("mainColor").setValue("VA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__field_readable_combinations_14_5_nonadmin() { // tavallinen värihavainto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("ringer").setValue("846");
		row.get("species").setValue("PANHAL");

		row.get("fieldReadableCode").setValue("CE75");
		row.get("mainColor").setValue("VA");
		row.get("attachmentLeftRight").setValue("E");

		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue("");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_15() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("");

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("A");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(3, validationResponse);
		assertContainsError("legRing", "metal_leg_ring_must_be_given", validationResponse);
		assertContainsError("ringsAttached", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
		assertContainsError("fieldReadableCode", "ringsAttached_says_fieldreadable_attached_but_is_not", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_15_2_1() { // lukumerkkien poisto - poistokiinnitysvalidointi

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("X 1");

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("XXX");
		row.get("removedFieldReadableMainColor").setValue("PUVA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(2, validationResponse);
		assertContainsError("legRing", "ring-not-distributed-at-the-date", validationResponse);
		assertContainsError("removedFieldReadableCode", "ADMIN_no_name_ring_can_be_found_with_this_fieldReadable", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_15_2() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT 155875");

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("species", "ADMIN_recovery_species_mismatch", validationResponse);
	}

	@Test
	public void test_recovery__ADMIN_field_readable_combinations_15_3() { // lukumerkkien poisto

		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("HT 155875");
		row.get("species").setValue("LARFUS");

		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");

		row.get("removedFieldReadableCode").setValue("CE75");
		row.get("removedFieldReadableMainColor").setValue("VA");

		row.get("fieldReadableChanges").setValue("P");
		row.get("ringsAttached").setValue("");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
	}

	@Test
	public void test_recovery__fieldreadable_must_give_some_rings_if_some_values_given() {

		Row row = fillRequiredRecovery();

		row.get("mainShade").setValue("T");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(1, validationResponse);
		assertContainsError("mainShade", "must_give_some_identifying_fields_if_field_readable_values_given", validationResponse);
	}

	@Test
	public void test_recovery__duplicate_warning() {
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("A 0224979");
		row.get("eventDate").setValue("24.7.1990");
		row.get("hour").setValue("20");
		row.get("ringer").setValue("6");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("legRing", "dublicate_recovery_exists", validationResponse);
		assertContainsWarning("eventDate", "dublicate_recovery_exists", validationResponse);
		assertContainsWarning("hour", "dublicate_recovery_exists", validationResponse);
	}

	@Test
	public void test_recovery__duplicate_warning_2() {
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("A 0224979");
		row.get("eventDate").setValue("24.7.1990");
		row.get("hour").setValue("");
		row.get("ringer").setValue("6");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("legRing", "dublicate_recovery_exists", validationResponse);
		assertContainsWarning("eventDate", "dublicate_recovery_exists", validationResponse);
		assertNoWarning("hour", "dublicate_recovery_exists", validationResponse);
	}

	@Test
	public void test_recovery__duplicate_warning_2__ringer_not_used_after_all() {
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("A 0224979");
		row.get("eventDate").setValue("24.7.1990");
		row.get("hour").setValue("");
		row.get("ringer").setValue("123");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("legRing", "dublicate_recovery_exists", validationResponse);
		assertContainsWarning("eventDate", "dublicate_recovery_exists", validationResponse);
		assertNoWarning("hour", "dublicate_recovery_exists", validationResponse);
	}

	@Test
	public void test_recovery__duplicate_warning_3() {
		Row row = fillRequiredRecovery();

		row.get("legRing").setValue("A 0224979");
		row.get("eventDate").setValue("24.7.1990");
		row.get("hour").setValue("21");
		row.get("ringer").setValue("6");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoWarning("eventDate", "dublicate_recovery_exists", validationResponse);
		assertNoWarning("hour", "dublicate_recovery_exists", validationResponse);
	}

	@Test
	public void test_recovery__duplicate_warning____not_for_update_if_same_event_id() {
		Row row = fillRequiredRecovery();

		row.get("id").setValue("17288159");
		row.get("legRing").setValue("A 0224979");
		row.get("eventDate").setValue("24.7.1990");
		row.get("hour").setValue("20");
		row.get("ringer").setValue("6");

		JSONObject validationResponse = validate(row, Mode.RECOVERIES, DAO.Action.UPDATE, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoWarning("legRing", "dublicate_recovery_exists", validationResponse);
		assertNoWarning("eventDate", "dublicate_recovery_exists", validationResponse);
		assertNoWarning("hour", "dublicate_recovery_exists", validationResponse);
	}

	@Test
	public void test__numeric_measurer() {
		Row row = fillRequiredRecovery();

		row.get("measurer").setValue("blaabaa");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoWarnings("measurer", validationResponse);
		assertNoErrors("measurer", validationResponse);
	}

	@Test
	public void test__numeric_measurer_2() {
		Row row = fillRequiredRecovery();

		row.get("measurer").setValue("846");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoWarnings("measurer", validationResponse);
		assertNoErrors("measurer", validationResponse);
	}

	@Test
	public void test__numeric_measurer_3() {
		Row row = fillRequiredRecovery();

		row.get("measurer").setValue("9989999");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("measurer", "measurer_not_valid_ringer", validationResponse);
	}

	@Test
	public void actual_data_recovery_1() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H");
		row.get("eventDate").setValue("30.3.2013");
		row.get("hour").setValue("9");
		row.get("eventDateAccuracy").setValue("0");
		row.get("species").setValue("PARCAE");
		row.get("sex").setValue("N");
		row.get("sexDeterminationMethod").setValue("V");
		row.get("weightInGrams").setValue("10,5");
		row.get("wingLengthInMillimeters").setValue("65");
		row.get("municipality").setValue("KOKKOL");
		row.get("birdActivities").setValue("E");
		row.get("ageDeterminationMethod").setValue("V");
		row.get("ringer").setValue("388");
		row.get("age").setValue("+1");
		row.get("additionalInformationCode").setValue("B");
		row.get("captureMethod").setValue("V");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("63,9500008");
		row.get("lon").setValue("23,3166676");
		row.get("legacyFat").setValue("0");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
		assertWarningCount(1, validationResponse);
	}

	@Test
	public void actual_data_recovery_1_ADMIN() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H"); // Important test because "check ring in birds leg" -test failed because it did not work with alpha in end ring. This is the only test where that is tested.
		row.get("eventDate").setValue("30.3.2013");
		row.get("hour").setValue("9");
		row.get("eventDateAccuracy").setValue("0");
		row.get("species").setValue("PARCAE");
		row.get("sex").setValue("N");
		row.get("sexDeterminationMethod").setValue("V");
		row.get("weightInGrams").setValue("10,5");
		row.get("wingLengthInMillimeters").setValue("65");
		row.get("municipality").setValue("KOKKOL");
		row.get("birdActivities").setValue("E");
		row.get("ageDeterminationMethod").setValue("V");
		row.get("ringer").setValue("388");
		row.get("age").setValue("+1");
		row.get("additionalInformationCode").setValue("B");
		row.get("captureMethod").setValue("V");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("63,9500008");
		row.get("lon").setValue("23,3166676");
		row.get("legacyFat").setValue("0");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertErrorCount(0, validationResponse);
		assertWarningCount(1, validationResponse);
	}

	@Test
	public void coordinates_are_required() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H");
		row.get("eventDate").setValue("30.3.2013");
		row.get("species").setValue("PARCAE");
		row.get("municipality").setValue("KOKKOL");
		row.get("ringer").setValue("388");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("");
		row.get("lon").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertContainsError("lat", "required_field", validationResponse);
		assertContainsError("lon", "required_field", validationResponse);
	}

	@Test
	public void ring_is_required_for_ringing() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("ringStart", "required_field", validationResponse);
	}

	@Test
	public void ring_is_required_for_ringing_2() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("ASD");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoError("ringStart", "required_field", validationResponse);
	}

	@Test
	public void ring_is_required_for_ringing_3() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("ASD"); // Rengastustoimiston Tipu-käli
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoError("ringStart", "required_field", validationResponse);
	}

	@Test
	public void coordinates_are_required_admin() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H");
		row.get("eventDate").setValue("30.3.2013");
		row.get("species").setValue("PARCAE");
		row.get("municipality").setValue("KOKKOL");
		row.get("ringer").setValue("388");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("");
		row.get("lon").setValue("");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("lat", "required_field", validationResponse);
		assertContainsError("lon", "required_field", validationResponse);
	}

	@Test
	public void coordinates_are_required_birdstation() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H");
		row.get("eventDate").setValue("30.3.2013");
		row.get("species").setValue("PARCAE");
		row.get("municipality").setValue("KOKKOL");
		row.get("birdStation").setValue("TANKAR");
		row.get("ringer").setValue("388");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("");
		row.get("lon").setValue("");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RECOVERIES);
		debug(validationResponse);
		assertContainsError("lat", "required_field", validationResponse);
		assertContainsError("lon", "required_field", validationResponse);
	}

	@Test
	public void coordinates_are_required_admin_birdstation() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("267200H");
		row.get("eventDate").setValue("30.3.2013");
		row.get("species").setValue("PARCAE");
		row.get("municipality").setValue("KOKKOL");
		row.get("birdStation").setValue("TANKAR");
		row.get("ringer").setValue("388");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("100");
		row.get("lat").setValue("");
		row.get("lon").setValue("");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("lat", "required_field", validationResponse);
		assertContainsError("lon", "required_field", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_1() {
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("C96A"); // distribution: J
		row.get("mainColor").setValue("VA");
		row.get("ringer").setValue("2799");
		row.get("species").setValue("CORONE");
		row.get("attachmentPoint").setValue("");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsWarning("attachmentPoint", "fieldreadable_attachmentPoint_not_given_will_come_from_distribution_warning", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_2() {
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("C96A"); // distribution: J
		row.get("mainColor").setValue("VA");
		row.get("ringer").setValue("2799");
		row.get("species").setValue("CORONE");
		row.get("attachmentPoint").setValue("J");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("attachmentPoint", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_3() {
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("C96A"); // distribution: J
		row.get("mainColor").setValue("VA");
		row.get("ringer").setValue("2799");
		row.get("species").setValue("CORONE");
		row.get("attachmentPoint").setValue("A"); // A-Y-J are synonyms
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("attachmentPoint", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_4() {
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("C96A"); // distribution: J
		row.get("mainColor").setValue("VA");
		row.get("ringer").setValue("2799");
		row.get("species").setValue("CORONE");
		row.get("attachmentPoint").setValue("K");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("attachmentPoint", "ADMIN_attachmentPoint_distribution_mismatch", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_7() {
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("07C"); // distribution: K
		row.get("mainColor").setValue("PU");
		row.get("ringer").setValue("453");
		row.get("species").setValue("ANSFAB");
		row.get("attachmentPoint").setValue("K");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("attachmentPoint", validationResponse);
	}

	@Test
	public void ADMIN_field_readable_attachment_point_validation_8() { // seems this also tests the case where ringCode and mainColor alone are not enough to distinguish a ring: species is needed.
		Row row = fillRequiredRinging();
		row.get("fieldReadableCode").setValue("07C"); // distribution: K
		row.get("mainColor").setValue("PU");
		row.get("attachmentLeftRight").setValue("E");
		row.get("ringer").setValue("453");
		row.get("species").setValue("ANSFAB");
		row.get("attachmentPoint").setValue("J");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("attachmentPoint", "ADMIN_attachmentPoint_distribution_mismatch", validationResponse);
	}

	@Test
	public void ADMIN_age_species_capture_method_not_required_for_findings() {
		Row row = fillRequiredRecovery();
		row.get("species").setValue("");
		row.get("age").setValue("");
		row.get("captureMethod").setValue("");
		row.get("sourceOfRecovery").setValue(DAO.RECOVERY_TYPE_CONTROL);
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("species", "required_field", validationResponse);
		assertContainsError("age", "required_field", validationResponse);
		assertContainsError("captureMethod", "required_field", validationResponse);
	}

	@Test
	public void ADMIN_age_species_capture_method_not_required_for_findings_2() {
		Row row = fillRequiredRecovery();
		row.get("species").setValue("");
		row.get("age").setValue("");
		row.get("captureMethod").setValue("");
		row.get("sourceOfRecovery").setValue(DAO.RECOVERY_TYPE_FINDING);
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("species", "required_field", validationResponse);
		assertNoError("age", "required_field", validationResponse);
		assertNoError("captureMethod", "required_field", validationResponse);
	}

	@Test
	public void ringsAttached_attachmentPoint_crosscheck() { // There are plenty more of these combinations but won't test..
		Row row = fillRequiredRinging();
		row.get("ringsAttached").setValue("K"); // neck
		row.get("attachmentPoint").setValue("J"); // leg
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("ringsAttached", "for_this_rings_attached_code_attachment_point_must_be_neck", validationResponse);
		assertContainsError("attachmentPoint", "for_this_rings_attached_code_attachment_point_must_be_neck", validationResponse);
	}

	@Test
	public void allow_ringsAttached_V_even_if_no_changes___just_to_be_sure_the_case_with_a_change() {
		Row row = fillRequiredRecovery();
		row.get("fieldReadableCode").setValue("XX1");
		row.get("mainColor").setValue("PU");
		row.get("removedFieldReadableCode").setValue("YY1");
		row.get("removedFieldReadableMainColor").setValue("LI");

		row.get("fieldReadableChanges").setValue("V"); // vaihto

		row.get("ringsAttached").setValue("V"); // metalli+väri

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("ringsAttached", validationResponse);
	}

	@Test
	public void allow_ringsAttached_V_even_if_no_changes() {
		Row row = fillRequiredRecovery();
		row.get("fieldReadableCode").setValue("XX1");
		row.get("mainColor").setValue("PU");
		row.get("removedFieldReadableCode").setValue("");
		row.get("removedFieldReadableMainColor").setValue("");

		row.get("fieldReadableChanges").setValue(""); // tavallinen havainto

		row.get("ringsAttached").setValue("V"); // metalli+väri

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("ringsAttached", validationResponse);
	}

	@Test
	public void length_of_dms_coordinates() {
		Row row = fillRequiredRecovery();
		row.get("sourceOfRecovery").setValue(DAO.RECOVERY_TYPE_FINDING);
		row.get("municipality").setValue("");
		row.get("lat").setValue("5416");
		row.get("lon").setValue("1836");
		row.get("coordinateSystem").setValue("WGS84-DMS");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("lat", "invalid_length_too_short", validationResponse);
		assertNoError("lon", "invalid_length_too_short", validationResponse);
	}

	@Test
	public void length_of_dms_coordinates_2() {
		Row row = fillRequiredRecovery();
		row.get("sourceOfRecovery").setValue(DAO.RECOVERY_TYPE_FINDING);
		row.get("euringProvinceCode").setValue("PL81");
		row.get("municipality").setValue("");
		row.get("lat").setValue("541600");
		row.get("lon").setValue("183600");
		row.get("coordinateSystem").setValue("WGS84-DMS");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertNoError("lat", "invalid_length_too_short", validationResponse);
		assertNoError("lon", "invalid_length_too_short", validationResponse);
	}

	@Test
	public void length_of_dms_coordinates_3() {
		Row row = fillRequiredRecovery();
		row.get("sourceOfRecovery").setValue(DAO.RECOVERY_TYPE_FINDING);
		row.get("euringProvinceCode").setValue("PL81");
		row.get("municipality").setValue("");
		row.get("lat").setValue("22541600");
		row.get("lon").setValue("1183600");
		row.get("coordinateSystem").setValue("WGS84-DMS");

		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("lat", "invalid_length_too_long", validationResponse);
		assertNoError("lon", "invalid_length_too_short", validationResponse);
	}

	@Test
	public void accuracy_vs_length_of_coordinates() {
		Row row = fillRequiredRinging();
		row.get("lat").setValue("66666");
		row.get("lon").setValue("3333333");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("coordinateAccuracy").setValue("1");

		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("coordinateAccuracy", "coordinate_length_accuracy_missmatch", validationResponse);
		assertContainsError("lat", "coordinate_length_accuracy_missmatch", validationResponse);
		assertNoError("lon", "coordinate_length_accuracy_missmatch", validationResponse);
	}

	@Test
	public void missing_species_when_reporting_field_readable() {
		Row row = dao.emptyRow();
		row.get("fieldReadableCode").setValue("CC111");
		row.get("mainColor").setValue("OR");
		// Species puuttuu!

		row.get("schemeID").setValue("PLG");
		row.get("eventDate").setValue("28.09.2014");
		row.get("lon").setValue("18,956909");
		row.get("birdConditionEURING").setValue("7");
		row.get("sourceOfRecovery").setValue("2");
		row.get("countryCode").setValue("PL");
		row.get("type").setValue("recovery");
		row.get("coordinateAccuracy").setValue("500");
		row.get("euringProvinceCode").setValue("PLPM");
		row.get("recoveryMethod").setValue("81");
		row.get("locality").setValue("Ujście Wisły, Mikoszewo");
		row.get("notes").setValue("colour ring read by Maciej Nagler, Zbigniew Wnuk");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("attachmentPoint").setValue("A");
		row.get("lat").setValue("54,359357");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.ADMIN_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("species", "species_must_be_given_if_reporting_via_field_readable", validationResponse);
	}

	@Test
	public void only_ringEnd() {
		Row row = dao.emptyRow();
		row.get("ringEnd").setValue("A 1234");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertContainsError("ringStart", "required_field", validationResponse);
	}

	@Test
	public void same_ringStart_and_end() {
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("A 1234");
		row.get("ringEnd").setValue("A 1234");
		JSONObject validationResponse = validateInsert(row, Mode.RINGINGS, RowValidator.USER_VALIDATIONS);
		debug(validationResponse);
		assertNoErrors("ringStart", validationResponse);
		assertNoErrors("ringEnd", validationResponse);
	}

	@Test
	public void fieldreadable_notes_for_ringsAttached_M_T_V_is_allowed() {
		Row row = fillRequiredRecovery();
		row.get("fieldReadableNotes").setValue("foo");
		row.get("ringsAttached").setValue("M");
		JSONObject validationResponse = validateInsert(row, Mode.RECOVERIES, RowValidator.USER_VALIDATIONS);
		assertErrorCount(0, validationResponse);
		assertWarningCount(0, validationResponse);
	}

	@Test
	public void coordinateValidationsWgs84() {
		Row row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84");
		row.get("lat").setValue("-89.123");
		row.get("lon").setValue("-160.123");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("lat", validationResponse);
		assertNoErrors("lon", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84");
		row.get("lat").setValue("-99.123");
		row.get("lon").setValue("-180.123");
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("lat", "too_small_value", validationResponse);
		assertContainsError("lon", "too_small_value", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84");
		row.get("lat").setValue("99");
		row.get("lon").setValue("180.123");
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("lat", "too_large_value", validationResponse);
		assertContainsError("lon", "too_large_value", validationResponse);
	}

	@Test
	public void coordinateValidationsWgs84Dms() {
		Row row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84-DMS");
		row.get("lat").setValue("-890059");
		row.get("lon").setValue("-1605900");
		JSONObject validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("lat", validationResponse);
		assertNoErrors("lon", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84-DMS");
		row.get("lat").setValue("0001"); // 000d 00m 01s
		row.get("lon").setValue("0100"); // 000d 01m 00s
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertNoErrors("lat", validationResponse);
		assertNoErrors("lon", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84-DMS");
		row.get("lat").setValue("-890060");
		row.get("lon").setValue("-1606000");
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("lat", "too_large_value", validationResponse);
		assertContainsError("lon", "too_large_value", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84-DMS");
		row.get("lat").setValue("0070");
		row.get("lon").setValue("6000");
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("lat", "too_large_value", validationResponse);
		assertContainsError("lon", "too_large_value", validationResponse);

		row = dao.emptyRow();
		row.get("coordinateSystem").setValue("WGS84-DMS");
		row.get("lat").setValue("910070");
		row.get("lon").setValue("1816000");
		validationResponse = validateInsertForUser(row, Mode.RINGINGS);
		assertContainsError("lat", "too_large_value", validationResponse);
		assertContainsError("lon", "too_large_value", validationResponse);
	}

}
