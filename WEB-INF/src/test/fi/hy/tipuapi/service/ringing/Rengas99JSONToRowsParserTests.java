package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.r99.InvalidDataException;
import fi.hy.tipuapi.service.ringing.r99.Rengas99JSONToRowConverter;
import fi.luomus.commons.json.JSONObject;

public class Rengas99JSONToRowsParserTests {

	private static DAO dao;

	@BeforeClass
	public static void init() throws Exception {
		dao = TestConfig.buildDAO();
	}

	@Test
	public void test_init() {
		new Rengas99JSONToRowConverter(dao);
	}

	@Test
	public void test_adding_couple_valid_fields() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("ringStart", "XXX 12345");
		Row row = converter.convert(json, Mode.RINGINGS);
		assertEquals("XXX 12345", row.get("ringStart").getValue());
	}

	@Test(expected=InvalidDataException.class)
	public void test_adding_invalid_fields() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("foobardfoor", "XXX 12345");
		converter.convert(json, Mode.RINGINGS);
	}

	@Test(expected=InvalidDataException.class)
	public void test_adding_fields_that_are_not_defined_for_that_mode() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("ringStart", "XXX 12345");
		converter.convert(json, Mode.RECOVERIES);
	}

	@Test(expected=InvalidDataException.class)
	public void test_adding_fields_that_are_not_defined_for_that_mode_2() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("legRing", "XXX 12345");
		converter.convert(json, Mode.RINGINGS);
	}

	@Test
	public void test_it_replaces_bird_stations_from_municipality() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("municipality", "ASPI");
		Row row = converter.convert(json, Mode.RECOVERIES);
		assertEquals("LOVIIS", row.get("municipality").getValue());
		assertEquals("ASPI", row.get("birdStation").getValue());
	}

	@Test
	public void test_handling_coordinates__ykj() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("lat", "67840").setString("lon", "3597");
		Row row = converter.convert(json, Mode.RECOVERIES);
		assertEquals("67840", row.get("lat").getValue());
		assertEquals("33597", row.get("lon").getValue());
		assertEquals("YKJ", row.get("coordinateSystem").getValue());
		assertEquals("100", row.get("coordinateAccuracy").getValue());
	}

	@Test
	public void test_handling_coordinates__kkj_aste() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject().setString("lat", "5950").setString("lon", "2137");
		Row row = converter.convert(json, Mode.RECOVERIES);
		assertEquals("595000", row.get("lat").getValue());
		assertEquals("213700", row.get("lon").getValue());
		assertEquals("WGS84-DMS", row.get("coordinateSystem").getValue());
		assertEquals("1000", row.get("coordinateAccuracy").getValue());
	}

	@Test
	public void test_handling_tenths() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject()
		.setString("thirdFeatherLengthInMillimeters", "1200")
		.setString("weightInGrams", "4500")
		.setString("hirrusTailNotchDeepnessInMillimeters", "900")
		.setString("hirrusLongestMiddleRectrixLengthInMillimeters", "350")
		.setString("hirrusOutmostLeftRectrixLengthInMillimeters", "700")
		.setString("hirrusOutmostRightRectrixLengthInMillimeters", "0");
		Row row = converter.convert(json, Mode.RECOVERIES);
		assertEquals("120.0", row.get("thirdFeatherLengthInMillimeters").getValue());
		assertEquals("450.0", row.get("weightInGrams").getValue());
		assertEquals("90.0", row.get("hirrusTailNotchDeepnessInMillimeters").getValue());
		assertEquals("35.0", row.get("hirrusLongestMiddleRectrixLengthInMillimeters").getValue());
		assertEquals("70.0", row.get("hirrusOutmostLeftRectrixLengthInMillimeters").getValue());
		assertEquals("0.0", row.get("hirrusOutmostRightRectrixLengthInMillimeters").getValue());
	}
	
	@Test
	public void test_aste_koords() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject()
		.setString("lat", "6357")
		.setString("lon", "2251");
		Row row = converter.convert(json, Mode.RINGINGS);
		assertEquals("635700", row.get("lat").getValue());
		assertEquals("225100", row.get("lon").getValue());
		assertEquals("WGS84-DMS", row.get("coordinateSystem").getValue());
		assertEquals("1000", row.get("coordinateAccuracy").getValue());
		assertEquals("", row.get("coordinateMeasurementMethod").getValue());
	}

	@Test
	public void test_remove_same_value_from_ring_start_end() throws Exception {
		Rengas99JSONToRowConverter converter = new Rengas99JSONToRowConverter(dao);
		JSONObject json = new JSONObject()
		.setString("ringStart", "A 123")
		.setString("ringEnd", "A 123");
		Row row = converter.convert(json, Mode.RINGINGS);
		assertEquals("A 123", row.get("ringStart").getValue());
		assertEquals("", row.get("ringEnd").getValue());
	}
	
}
