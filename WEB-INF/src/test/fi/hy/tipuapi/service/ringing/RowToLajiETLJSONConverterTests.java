package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowToLajiETLJSONConverter;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.FileUtils;

public class RowToLajiETLJSONConverterTests {

	private static DAO dao;

	@BeforeClass
	public static void init() throws Exception {
		dao = TestConfig.buildDAO();
	}

	@AfterClass
	public static void destroy() {
		if (dao != null) dao.close();
	}

	@Test
	public void test_1() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("229296");
		row.get("type").setValue("ringing");
		row.get("nameRing").setValue("E 11001");
		row.get("legRing").setValue("E 11001");
		row.get("newLegRing").setValue("E 11001");
		row.get("eventDate").setValue("28.6.1984");
		row.get("euringProvinceCode").setValue("SF80");
		row.get("municipality").setValue("PÄLKÄN");
		row.get("currentMunicipality").setValue("PÄLKÄN");
		row.get("lat").setValue("6805500");
		row.get("lon").setValue("3359700");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("coordinateAccuracy").setValue("100");
		row.get("species").setValue("PARMAJ");
		row.get("ringedSpecies").setValue("PARMAJ");
		row.get("age").setValue("PM");
		row.get("weightInGrams").setValue("2000");
		row.get("wingLengthInMillimeters").setValue("280");
		row.get("clutchNumber").setValue("1");
		row.get("numberOfYoungs").setValue("1");
		row.get("wgs84DegreeLat").setValue("612000");
		row.get("wgs84DegreeLon").setValue("242300");
		row.get("ringer").setValue("846");

		JSONObject json = new RowToLajiETLJSONConverter(row, dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES), dao.getTipuApiResource(TipuAPIClient.RINGERS), "seeecret").toJson();

		String expected = getFileData("etl-expected-1.json");
		assertEquals(expected, json.beautify());
	}

	@Test
	public void test_2() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("1");
		row.get("type").setValue("recovery");
		row.get("nameRing").setValue("X1");
		row.get("legRing").setValue("X2");
		row.get("eventDate").setValue("28.6.1984");
		row.get("eventDateAccuracy").setValue("8");
		row.get("euringProvinceCode").setValue("SF80");
		row.get("municipality").setValue("PÄLKÄN");
		row.get("currentMunicipality").setValue("PÄLKÄN");
		row.get("wgs84DecimalLat").setValue("60.123");
		row.get("wgs84DecimalLon").setValue("30.123");
		row.get("coordinateSystem").setValue("WGS84");
		row.get("coordinateAccuracy").setValue("EPÄT");
		row.get("species").setValue("PARMAJ");
		row.get("ringedSpecies").setValue("PARMAJ");
		row.get("ringer").setValue("1");
		row.get("birdConditionEURING").setValue("1");

		JSONObject json = new RowToLajiETLJSONConverter(row, dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES), dao.getTipuApiResource(TipuAPIClient.RINGERS), "seeecret").toJson();

		String expected = getFileData("etl-expected-2.json");
		assertEquals(expected, json.beautify());
	}

	public static String getFileData(String filename) {
		URL url = RowToLajiETLJSONConverterTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
