package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.regex.Pattern;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.models.RingHistory;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowToEuring2020Converter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;

public class RowToEuring2020ConverterTests {

	private static DAO dao;
	private static RowToEuring2020Converter converter;

	@BeforeClass
	public static void init() throws Exception {
		dao = TestConfig.buildDAO();
		converter = new RowToEuring2020Converter(dao.getTipuApiResource(TipuAPIClient.SPECIES), dao.getTipuApiResource(TipuAPIClient.MUNICIPALITIES));
	}

	@AfterClass
	public static void destroy() {
		if (dao != null) dao.close();
	}

	@Test
	public void test_minimumum() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("229296");
		row.get("type").setValue("ringing");
		row.get("nameRing").setValue("E 11001");
		row.get("legRing").setValue("E 11001");
		row.get("newLegRing").setValue("E 11001");
		row.get("eventDate").setValue("28.6.1984");
		row.get("euringProvinceCode").setValue("SF80");
		row.get("municipality").setValue("PÄLKÄN");
		row.get("lat").setValue("6805500");
		row.get("lon").setValue("3359700");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("coordinateAccuracy").setValue("100");
		row.get("species").setValue("PARMAJ");
		row.get("ringedSpecies").setValue("PARMAJ");
		row.get("age").setValue("PM");
		row.get("weightInGrams").setValue("2000");
		row.get("wingLengthInMillimeters").setValue("280");
		row.get("clutchNumber").setValue("1");
		row.get("numberOfYoungs").setValue("1");
		row.get("wgs84DecimalLat").setValue("61.20111");
		row.get("wgs84DecimalLon").setValue("24.23333");

		RingHistory ringHistory = new RingHistory(Utils.list(row));

		List<String> euringDataRows = converter.convert(ringHistory);
		assertEquals(1, euringDataRows.size());

		String euringData = euringDataRows.get(0);
		assertEquals(
				"SFH|A0|E....11001|0|2|ZZ|14640|14640|N|0|-|U|U|U|1|1|-|01|99|U|28061984|0|----|SF80|...............|0|8|20|0|3|-----|---|-----|280|||2000|U|U|||||||||||||||||||PÄLKÄNE||SFH229296|61.2011|24.2333|SF80|",
				euringData);
		assertEquals(63, Utils.countNumberOf("|", euringData));
	}

	@Test
	public void test_procected_species_level_1() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("229296");
		row.get("type").setValue("ringing");
		row.get("nameRing").setValue("E 11001");
		row.get("legRing").setValue("E 11001");
		row.get("eventDate").setValue("28.6.1984");
		row.get("coordinateAccuracy").setValue("1");
		row.get("species").setValue("BUBBUB");
		row.get("wgs84DecimalLat").setValue("61.272333");
		row.get("wgs84DecimalLon").setValue("24.2345552");

		RingHistory ringHistory = new RingHistory(Utils.list(row));

		List<String> euringDataRows = converter.convert(ringHistory);
		assertEquals(1, euringDataRows.size());

		String euringData = euringDataRows.get(0);
		String[] splitted = euringData.split(Pattern.quote("|"));

		assertEquals("...............", splitted[24]);
		assertEquals("2", splitted[25]);
		assertEquals("61.3", splitted[60]);
		assertEquals("24.2", splitted[61]);
	}

	@Test
	public void test_procected_species_level_2() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("229296");
		row.get("type").setValue("ringing");
		row.get("nameRing").setValue("E 11001");
		row.get("legRing").setValue("E 11001");
		row.get("eventDate").setValue("28.6.1984");
		row.get("coordinateAccuracy").setValue("1");
		row.get("species").setValue("MILMIG");
		row.get("wgs84DecimalLat").setValue("61.212333");
		row.get("wgs84DecimalLon").setValue("24.9345552");

		RingHistory ringHistory = new RingHistory(Utils.list(row));

		List<String> euringDataRows = converter.convert(ringHistory);
		assertEquals(1, euringDataRows.size());

		String euringData = euringDataRows.get(0);
		String[] splitted = euringData.split(Pattern.quote("|"));

		assertEquals("...............", splitted[24]);
		assertEquals("5", splitted[25]);
		assertEquals("61.0", splitted[60]);
		assertEquals("25.0", splitted[61]);
	}

	@Test
	public void test_procected_species_level_2_negative_coords() throws Exception {
		Row row = dao.emptyRow();
		row.get("id").setValue("229296");
		row.get("type").setValue("ringing");
		row.get("nameRing").setValue("E 11001");
		row.get("legRing").setValue("E 11001");
		row.get("eventDate").setValue("28.6.1984");
		row.get("coordinateAccuracy").setValue("1");
		row.get("species").setValue("MILMIG");
		row.get("wgs84DecimalLat").setValue("-161.212333");
		row.get("wgs84DecimalLon").setValue("-24.9345552");

		RingHistory ringHistory = new RingHistory(Utils.list(row));

		List<String> euringDataRows = converter.convert(ringHistory);
		assertEquals(1, euringDataRows.size());

		String euringData = euringDataRows.get(0);
		String[] splitted = euringData.split(Pattern.quote("|"));

		assertEquals("...............", splitted[24]);
		assertEquals("5", splitted[25]);
		assertEquals("-161.0", splitted[60]);
		assertEquals("-25.0", splitted[61]);
	}

	@Test
	public void test_ring_format() {
		assertEquals("A...600001", RowToEuring2020Converter.formatRing("A600001"));
		assertEquals("PA..012001", RowToEuring2020Converter.formatRing("PA 012001"));
		assertEquals("E....12101", RowToEuring2020Converter.formatRing("E 12101"));
		assertEquals("...123456H", RowToEuring2020Converter.formatRing("123456H"));
		assertEquals("...123456H", RowToEuring2020Converter.formatRing("123456 H"));
		assertEquals("D+.0017757", RowToEuring2020Converter.formatRing("D+0017757"));
	}

	@Test
	public void test_string_contains() {
		assertTrue("FTRS".contains("T"));
		//assertFalse("FRRS".contains("")); //Uupsie...

		assertTrue(RowToEuring2020Converter.oneOf("FTRS", "T"));
		assertFalse(RowToEuring2020Converter.oneOf("FRRS", ""));
	}

	@Test
	public void test_only_one_dead_bird() throws Exception {
		Row ringing = dao.emptyRow();
		ringing.get("id").setValue("123");
		ringing.get("type").setValue("ringing");
		ringing.get("nameRing").setValue("E 11001");
		ringing.get("legRing").setValue("E 11001");
		ringing.get("eventDate").setValue("28.6.1984");
		ringing.get("wgs84DecimalLat").setValue("61.2123");
		ringing.get("wgs84DecimalLon").setValue("24.2345");

		Row rec1 = dao.emptyRow();
		rec1.get("id").setValue("1234");
		rec1.get("type").setValue("recovery");
		rec1.get("nameRing").setValue("E 11001");
		rec1.get("legRing").setValue("E 11001");
		rec1.get("eventDate").setValue("28.7.1984");
		rec1.get("recoveryMethod").setValue("7");
		rec1.get("wgs84DecimalLat").setValue("61.2123");
		rec1.get("wgs84DecimalLon").setValue("24.2345");

		Row rec2 = dao.emptyRow();
		rec2.get("id").setValue("1235");
		rec2.get("type").setValue("recovery");
		rec2.get("nameRing").setValue("E 11001");
		rec2.get("legRing").setValue("E 11001");
		rec2.get("eventDate").setValue("28.8.1984");
		rec2.get("recoveryMethod").setValue("1");
		rec2.get("birdCondition").setValue("D");
		rec2.get("wgs84DecimalLat").setValue("61.2123");
		rec2.get("wgs84DecimalLon").setValue("24.2345");

		Row rec3 = dao.emptyRow();
		rec3.get("id").setValue("1236");
		rec3.get("type").setValue("recovery");
		rec3.get("nameRing").setValue("E 11001");
		rec3.get("legRing").setValue("E 11001");
		rec3.get("eventDate").setValue("28.7.1984");
		rec3.get("recoveryMethod").setValue("7");
		rec3.get("birdCondition").setValue("D");
		rec3.get("wgs84DecimalLat").setValue("61.2123");
		rec3.get("wgs84DecimalLon").setValue("24.2345");

		RingHistory ringHistory = new RingHistory(Utils.list(ringing, rec1, rec2, rec3));

		List<String> euringDataRows = converter.convert(ringHistory);
		assertEquals(3, euringDataRows.size());

		String[] splitted = euringDataRows.get(2).split(Pattern.quote("|"));
		assertEquals("SFH1236", splitted[59]);
	}

}
