package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.hy.tipuapi.service.ringing.r99.InvalidDataException;
import fi.hy.tipuapi.service.ringing.r99.Rengas99ToJSONConverter;
import fi.hy.tipuapi.service.ringing.r99.RingingsAndRecoveriesMixedException;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;

public class Rengas99ToJsonConverterTests {

	private static Rengas99ToJSONConverter converter = init();

	private static Rengas99ToJSONConverter init() {
		try {
			return new Rengas99ToJSONConverter(new RowStructure(TestConfig.get(), new LocalizedTextsContainerImple("fi"), false));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test(expected=RingingsAndRecoveriesMixedException.class)
	public void test_doesnt_accept_ringings_and_recoveries_mixed() {
		List<String> lines = new ArrayList<>();
		lines.add("R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ");
		lines.add("L1BV  48275  8462010         GLAPAS NH+2V 4 6HAUHO 678523627  PP  1 108       710   11        ");
		converter.convert(lines, Mode.RECOVERIES);
	}

	@Test
	public void test_no_exception_with_just_ringins_in_lines() {
		List<String> lines = new ArrayList<>();
		lines.add("R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ");
		lines.add("R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ");
		converter.convert(lines, Mode.RINGINGS);
	}

	@Test(expected=RingingsAndRecoveriesMixedException.class)
	public void test_does_accept_ringings_with_recoveries_mode() {
		List<String> lines = new ArrayList<>();
		lines.add("R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ");
		lines.add("R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ");
		converter.convert(lines, Mode.RECOVERIES);
	}

	@Test(expected=RingingsAndRecoveriesMixedException.class)
	public void test_doesnt_accept_recoveries_with_ringings_mode() {
		List<String> lines = new ArrayList<>();
		lines.add("L1BV  48275  8462010         GLAPAS NH+2V 4 6HAUHO 678523627  PP  1 108       710   11        ");
		converter.convert(lines, Mode.RINGINGS);
	}

	@Test
	public void test_no_exception_with_just_recoviers_in_lines() {
		List<String> lines = new ArrayList<>();
		lines.add("L1BV  48275  8462010         GLAPAS NH+2V 4 6HAUHO 678523627  PP  1 108       710   11        ");
		lines.add("L1BV  48275  8462010         GLAPAS NH+2V 4 6HAUHO 678523627  PP  1 108       710   11        ");
		converter.convert(lines, Mode.RECOVERIES);
	}

	@Test(expected=InvalidDataException.class)
	public void test_unknown_type_rows_blank() {
		convertSingleLine(" R1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ", Mode.RINGINGS);
	}

	@Test(expected=InvalidDataException.class)
	public void test_unknown_type_rows_unknown_char_1() {
		convertSingleLine("XR1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ", Mode.RINGINGS);
	}

	@Test(expected=InvalidDataException.class)
	public void test_unknown_type_rows_unknown_char_2() {
		convertSingleLine("rR1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ", Mode.RINGINGS);
	}

	@Test(expected=InvalidDataException.class)
	public void test_unknown_type_rows_unknown_char_3() {
		convertSingleLine("lR1B  123490 8462009         ACCNIS KKPP 28 6HAUHO 678403597        1  113      1220   15                  ", Mode.RINGINGS);
	}

	private JSONObject convertSingleLine(String line, Mode mode) {
		List<String> lines = new ArrayList<>();
		lines.add(line);
		return converter.convert(lines, mode).get(0);
	}

	@Test
	public void test_ringing_rows() {
		List<String> lines = new ArrayList<>();
		lines.add("R1PT  5333928542013         CALMAR   1   211JURMO  59502137   K       137       650   10                  ");
		lines.add("R2PT  53339HEINÄSAARI                                                                      ");
		lines.add("R3PT  53339VARI  ELV         KE       MU       PE A V   02112013         AL            LISÄKSI OR. MERKKIRENGAS OIK. JALAN TI  ");                                                                                                          
		lines.add("R4PT  53339LISÄKSI OR. MERKKIRENGAS OIK. JALAN TIBI                                        ");
		lines.add("R1BV  48268 8462009         GLAPAS KK2 V18 4HAUHO 679233763  PP       100       558   21");
		lines.add("R2BV  48268ISOMÄKI");                                                                         
		lines.add("R4BV  48268AARTO TUOMINEN RENGASTI NAARAAN JA POIKASET");        

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		assertEquals("", row1.getString("id"));
		assertEquals("", row2.getString("id"));

		String ring1 = LegacyRingCodeHandlerUtil.dbToActual("PT  53339");
		String ring2 = LegacyRingCodeHandlerUtil.dbToActual("BV  48268");
		assertEquals(ring1, row1.getString("ringStart"));
		assertEquals(ring2, row2.getString("ringStart"));
		assertEquals("", row1.getString("ringEnd"));
		assertEquals("", row2.getString("ringEnd"));

		assertEquals("", row1.getString("ringUse"));
		assertEquals("CALMAR", row1.getString("species"));
		assertEquals("2854", row1.getString("ringer"));
		assertEquals("2.11.2013", row1.getString("eventDate"));
		assertEquals("JURMO", row1.getString("municipality"));
		assertEquals("5950", row1.getString("lat"));
		assertEquals("2137", row1.getString("lon"));
		assertEquals("", row1.getString("coordinateSystem"));
		assertEquals("", row1.getString("coordinateMeasurementMethod"));

		assertEquals("K", row1.getString("captureMethod"));
		assertEquals("P", row2.getString("captureMethod"));
		assertEquals("P", row2.getString("birdActivities"));

		assertEquals("137", row1.getString("wingLengthInMillimeters"));
		assertEquals("100", row2.getString("wingLengthInMillimeters"));

		assertEquals("ELV", row1.getString("fieldReadableCode"));
		assertEquals("KE", row1.getString("mainColor"));
		assertEquals("MU", row1.getString("codeColor"));
		assertEquals("PE", row1.getString("codeAlign"));

		assertEquals("HEINÄSAARI", row1.getString("locality"));
		assertEquals("LISÄKSI OR. MERKKIRENGAS OIK. JALAN TI", row1.getString("fieldReadableNotes"));
		assertEquals("AARTO TUOMINEN RENGASTI NAARAAN JA POIKASET", row2.getString("notes"));
	}

	@Test
	public void test_ringing_with_hirrus_and_vari() {
		List<String> lines = new ArrayList<>();
		lines.add("R1PT  5334328542013         CALMAR   +1  511JURMO  59502137   K       122       560   10                  ");
		lines.add("R2PT  53343HEINÄSAARI                                                                      ");
		lines.add("R3PT  53343VARI  EMC         KE       MU       PE A V   05112013         AL            LISÄKSI OR. MERKKIRENGAS OIK. JALAN TIBIHIRRUS  2  400  700  700       300 1 000KAIKKI HIRRUSTIEDOT KEKSITTYJÄ");                                             
		lines.add("R4PT  53343SIIPI KASVUSSA                                                                  ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(1, jsonRows.size());
		JSONObject row = jsonRows.get(0);

		assertEquals("EMC", row.getString("fieldReadableCode"));
		assertEquals("KE", row.getString("mainColor"));
		assertEquals("MU", row.getString("codeColor"));
		assertEquals("PE", row.getString("codeAlign"));
		assertEquals("AL", row.getString("metalRingColor"));
		assertEquals("V", row.getString("attachmentLeftRight"));
		assertEquals("A", row.getString("attachmentPoint"));
		assertEquals("LISÄKSI OR. MERKKIRENGAS OIK. JALAN TIBI", row.getString("fieldReadableNotes"));

		assertEquals("2", row.getString("muscleFitness"));
		assertEquals("400", row.getString("hirrusLongestMiddleRectrixLengthInMillimeters"));
		assertEquals("700", row.getString("hirrusOutmostLeftRectrixLengthInMillimeters"));
		assertEquals("700", row.getString("hirrusOutmostRightRectrixLengthInMillimeters"));
		assertEquals("SIIPI KASVUSSA", row.getString("notes"));
	}

	@Test(expected=InvalidDataException.class)
	public void test_ringing_with_hirrus_and_vari_inverse_order__is_not_allowed() {
		List<String> lines = new ArrayList<>();
		lines.add("R1PT  5334328542013         CALMAR   +1  511JURMO  59502137   K       122       560   10                  ");
		lines.add("R2PT  53343HEINÄSAARI                                                                      ");
		lines.add("R3PT  53343HIRRUS  2  400  700  700       300 1 000KAIKKI HIRRUSTIEDOT KEKSITTYJÄVARI  EMC         KE       MU       PE A V   05112013         AL            LISÄKSI OR. MERKKIRENGAS OIK. JALAN TIBI                                             ");                                             
		lines.add("R4PT  53343SIIPI KASVUSSA                                                                  ");

		converter.convert(lines, Mode.RINGINGS);
	}

	@Test
	public void test_ringing_with_only_hirrus() {
		List<String> lines = new ArrayList<>();
		lines.add("R1HL 179994 8462009         HIRRUS KH+1V25 6HAUHO 678393685  PP       126       180  110                  ");
		lines.add("R2HL 179994VANHA NAVETTA                                                                   ");
		lines.add("R3HL 179994HIRRUS 11  430  900  900                                                   ");                  

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(1, jsonRows.size());
		JSONObject row = jsonRows.get(0);

		assertEquals("1", row.getString("fat"));
		assertEquals("1", row.getString("muscleFitness"));
		assertEquals("430", row.getString("hirrusLongestMiddleRectrixLengthInMillimeters"));
		assertEquals("900", row.getString("hirrusOutmostLeftRectrixLengthInMillimeters"));
		assertEquals("900", row.getString("hirrusOutmostRightRectrixLengthInMillimeters"));
	}

	@Test
	public void test_ringing_with_only_vari() {
		List<String> lines = new ArrayList<>();
		lines.add("R1D  260570 3152013         FALPER KKPP  3 7KUIVAN728174369     3  1  152      4600                       ");
		lines.add("R2D  260570ISO HIRVIAAPA                                                                   ");
		lines.add("R3D  260570VARI  D1          SI                     O   03072013         KE                       ");                             

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(1, jsonRows.size());
		JSONObject row = jsonRows.get(0);

		assertEquals("D1", row.getString("fieldReadableCode"));
		assertEquals("SI", row.getString("mainColor"));
		assertEquals("", row.getString("codeColor"));
		assertEquals("O", row.getString("attachmentLeftRight"));
	}

	@Test
	public void test_ringing_with_startAndEnd() {
		List<String> lines = new ArrayList<>();
		lines.add("R1HL 232914 5272013 232917  FICHYP   PP  9 6HKYRÖ 684892894     5  9                                      ");
		lines.add("R1HL 232920 5272013 232926  PARCAE   PP  9 6HKYRÖ 684892893     7  1                              ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		String ring1Start = LegacyRingCodeHandlerUtil.dbToActual("HL 232914");
		String ring1End = LegacyRingCodeHandlerUtil.dbToActual("HL 232917");
		String ring2Start = LegacyRingCodeHandlerUtil.dbToActual("HL 232920");
		String ring2End = LegacyRingCodeHandlerUtil.dbToActual("HL 232926");

		assertEquals(ring1Start, row1.getString("ringStart"));
		assertEquals(ring1End, row1.getString("ringEnd"));

		assertEquals(ring2Start, row2.getString("ringStart"));
		assertEquals(ring2End, row2.getString("ringEnd"));
	}

	@Test
	public void test_lost_destroyed() {
		List<String> lines = new ArrayList<>();
		lines.add("R1UL  7357223312013                      7 7HALIAS 59492254         H                  7                  ");
		lines.add("R1UL  7397223312013                      210HALIAS 59492254         T                 10   ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		assertEquals("H", row1.getString("ringNotUsedReason"));
		assertEquals("7", row1.getString("hour"));
		assertEquals("T", row2.getString("ringNotUsedReason"));
		assertEquals("10", row2.getString("hour"));
	}

	@Test
	public void test_recovery_rows() {
		List<String> lines = new ArrayList<>();
		lines.add("L1HL  31123  8462010         HIRRUS KH+1K 6 7HATTUL677523557  PP    128       171  110        ");
		lines.add("L2HL  31123NAVETTA                                                                         ");
		lines.add("L3HL  31123HIRRUS 11  410 1150                     ULOIN OIK PYS POIKKI                      ");
		lines.add("L1A  692148  9672013         PERINF KK+2S27 9MUONIO751213804V  K    146       880  0          ");
		lines.add("L2A  692148ELÄMÄNLUUKKU                                                                    ");
		lines.add("L3A  692148VARI  Z49         KE       MU       PE A OA              ");                                                                                                          
		lines.add("L4A  692148VÄRIRENGAS ENNALLAAN                                ");                            

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RECOVERIES);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		assertEquals("", row1.getString("id"));
		assertEquals("", row2.getString("id"));

		String ring1 = LegacyRingCodeHandlerUtil.dbToActual("HL  31123");
		String ring2 = LegacyRingCodeHandlerUtil.dbToActual("A  692148");
		assertEquals(ring1, row1.getString("legRing"));
		assertEquals(ring2, row2.getString("legRing"));

		assertEquals("NAVETTA", row1.getString("locality"));
		assertEquals("1", row1.getString("fat"));
		assertEquals("1", row1.getString("muscleFitness"));

		assertEquals("", row1.getString("ringUse"));
		assertEquals("HIRRUS", row1.getString("species"));
		assertEquals("846", row1.getString("ringer"));
		assertEquals("6.7.2010", row1.getString("eventDate"));
		assertEquals("10", row1.getString("hour"));
		assertEquals("HATTUL", row1.getString("municipality"));
		assertEquals("67752", row1.getString("lat"));
		assertEquals("3557", row1.getString("lon"));
		assertEquals("", row1.getString("coordinateSystem"));
		assertEquals("", row1.getString("coordinateMeasurementMethod"));

		assertEquals("P", row1.getString("birdActivities"));
		assertEquals("P", row1.getString("captureMethod"));

		assertEquals("1", row1.getString("muscleFitness"));
		assertEquals("K", row1.getString("sex"));
		assertEquals("H", row1.getString("sexDeterminationMethod"));
		assertEquals("1", row1.getString("fat"));
		assertEquals("128", row1.getString("wingLengthInMillimeters"));
		assertEquals("171", row1.getString("weightInGrams"));
		assertEquals("+1", row1.getString("age"));
		assertEquals("K", row1.getString("ageDeterminationMethod"));

		assertEquals("410", row1.getString("hirrusLongestMiddleRectrixLengthInMillimeters"));
		assertEquals("1150", row1.getString("hirrusOutmostLeftRectrixLengthInMillimeters"));
		assertEquals("ULOIN OIK PYS POIKKI", row1.getString("biometricNotes"));

		// lines.add("L3A  692148VARI  Z49         KE       MU       PE A OA              ");     
		assertEquals("Z49", row2.getString("fieldReadableCode"));
		assertEquals("KE", row2.getString("mainColor"));
		assertEquals("MU", row2.getString("codeColor"));
		assertEquals("PE", row2.getString("codeAlign"));
		assertEquals("A", row2.getString("attachmentPoint"));
		assertEquals("O", row2.getString("attachmentLeftRight"));
		assertEquals("A", row2.getString("upsideDown"));
		assertEquals("VÄRIRENGAS ENNALLAAN", row2.getString("notes"));
	}

	@Test
	public void test_renkaanvaihto() {
		List<String> lines = new ArrayList<>();
		lines.add("L1BT  43958  8302013  43959BTNUCCAR   +2V14 2LOHJA 667423295V  V                              ");
		lines.add("L3BT  43958VARI  ANA      V  VA       MU                1402201314022013       TEST                                            ");
		List<JSONObject> jsonRows = converter.convert(lines, Mode.RECOVERIES);
		assertEquals(1, jsonRows.size());
		JSONObject row = jsonRows.get(0);

		//		BT43958 ->  BT43959 
		//		TEST -> ANA

		String legRing = LegacyRingCodeHandlerUtil.dbToActual("BT  43958");
		String newLegRing = LegacyRingCodeHandlerUtil.dbToActual("BT  43959");

		assertEquals(legRing, row.getString("legRing"));
		assertEquals(newLegRing, row.getString("newLegRing"));

		assertEquals("TEST", row.getString("removedFieldReadableCode"));
		assertEquals("ANA", row.getString("fieldReadableCode"));
	}

	@Test
	public void test_monta_kontrollia_samasta_renkaasta() {
		List<String> lines = new ArrayList<>();
		lines.add("L1E   11778 91922012         AQUCHR   FLV1412YLÄNE 675522470   T                              ");
		lines.add("L3E   11778VARI  320N        MUPU                 A V                                                                          ");
		lines.add("L1E   11778 91922012         AQUCHR   FLV 712YLÄNE 675522470   T                              ");
		lines.add("L3E   11778VARI  320N        MUPU                 A V                                                                          ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RECOVERIES);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		String legRing = LegacyRingCodeHandlerUtil.dbToActual("E 11778");

		assertEquals(legRing, row1.getString("legRing"));
		assertEquals(legRing, row2.getString("legRing"));

		assertEquals("320N", row1.getString("fieldReadableCode"));
		assertEquals("320N", row2.getString("fieldReadableCode"));
	}

	@Test
	public void test_varirengastus_ilman_jalkarengasta() {
		List<String> lines = new ArrayList<>();
		lines.add("L1          91922012         AQUCHR   FLV 612YLÄNE 675522470   T                              ");
		lines.add("L3         VARI  015A        VASI                 A V                                                                          ");
		lines.add("L1          91922012         AQUCHR   FLV 712YLÄNE 675522470   T                              ");
		lines.add("L3         VARI  077B        VASI                 A V                                                                          ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RECOVERIES);
		assertEquals(2, jsonRows.size());
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);

		assertEquals("", row1.getString("legRing"));
		assertEquals("", row2.getString("legRing"));

		assertEquals("015A", row1.getString("fieldReadableCode"));
		assertEquals("077B", row2.getString("fieldReadableCode"));
	}

	@Test 
	public void test_astekoords_1() {
		List<String> lines = new ArrayList<>();
		lines.add("R1A  742106 3882014         TURPIL   +1V25 6TANKAR 63572251  PW       140              7   B     ");

		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		assertEquals(1, jsonRows.size());
		JSONObject row = jsonRows.get(0);

		assertEquals("6357", row.getString("lat"));
		assertEquals("2251", row.getString("lon"));
		assertEquals("", row.getString("coordinateSystem"));
		assertEquals("", row.getString("coordinateAccuracy"));
		assertEquals("", row.getString("coordinateMeasurementMethod"));
	}

	@Test 
	public void test_wing_length_and_measurement_method() {
		List<String> lines = new ArrayList<>();
		lines.add("R1PT  5333928542013         CALMAR   1   211JURMO  59502137   K      S137       650   10                  ");
		lines.add("R1PT  5333928542013         CALMAR   1   211JURMO  59502137   K       137       650   10                  ");
		
		List<JSONObject> jsonRows = converter.convert(lines, Mode.RINGINGS);
		
		JSONObject row1 = jsonRows.get(0);
		JSONObject row2 = jsonRows.get(1);
		
		assertEquals("", row1.getString("_wingLengthMeasurementMethod"));
		assertEquals("", row1.getString("wingLengthInMillimeters"));
		assertEquals("137", row1.getString("wingLengthMinMeasurement"));
		
		assertEquals("", row2.getString("_wingLengthMeasurementMethod"));
		assertEquals("137", row2.getString("wingLengthInMillimeters"));
		assertEquals("", row2.getString("wingLengthMinMeasurement"));
	}
	
	@Test 
	public void erroneous_r99_line_that_should_be_rejected() {
		List<String> lines = new ArrayList<>();
		lines.add("R1A  742106 3882014         TURPIL   +1V25 6TANKAR 63572251  PW       140              7   B     R1A  742106 3882014         TURPIL   +1V25 6TANKAR 63572251  PW       140              7   B     ");

		try {
			converter.convert(lines, Mode.RINGINGS);
			fail("Should throw exception");
		} catch (InvalidDataException e) {
			assertTrue(e.getMessage().startsWith("Too long line."));
		}
	}

	@Test 
	public void erroneous_r99_line_that_should_be_rejected_2() {
		List<String> lines = new ArrayList<>();
		lines.add("L1          91922012         AQUCHR   FLV 612YLÄNE 675522470   T                              ");
		lines.add("L3         VARI  015A        VASI                 A V                          L1          91922012         AQUCHR   FLV 612YLÄNE 675522470   T    ");
		
		try {
			converter.convert(lines, Mode.RECOVERIES);
			fail("Should throw exception");
		} catch (InvalidDataException e) {
			assertTrue(e.getMessage().startsWith("Long card 3 type line but doesn't start with HIRRUS"));
		}
	}
	
	@Test 
	public void erroneous_r99_line_that_should_be_rejected_3() {
		List<String> lines = new ArrayList<>();
		lines.add("L1          91922012         AQUCHR   FLV 612YLÄNE 675522470   T                              ");
		lines.add("L3          ");
		
		try {
			converter.convert(lines, Mode.RECOVERIES);
			fail("Should throw exception");
		} catch (InvalidDataException e) {
			assertTrue(e.getMessage().startsWith("Too short line."));
		}
	}
	
}
