package fi.hy.tipuapi.service.ringing;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;

import com.zaxxer.hikari.HikariDataSource;

import fi.hy.tipuapi.service.ringing.dao.DAO;
import fi.hy.tipuapi.service.ringing.dao.DatasourceDefinition;
import fi.hy.tipuapi.service.ringing.dao.RowToDbNonInputFieldsValueResolver;
import fi.hy.tipuapi.service.ringing.models.Field.Mode;
import fi.hy.tipuapi.service.ringing.models.Row;
import fi.hy.tipuapi.service.ringing.models.RowStructure;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import fi.luomus.commons.utils.DateUtils;

public class RowToDbNonInputFieldsValueResolverTests {

	private static Config config = TestConfig.get();
	private static TransactionConnection con = TestConfig.getConnection(config);
	private static HikariDataSource dataSource;
	private static DAO dao = initDao();

	@AfterClass
	public static void clean() {
		if (con != null) con.release();
		if (dataSource != null) dataSource.close();
	}

	private static DAO initDao() {
		try {
			dataSource = DatasourceDefinition.initDataSource(config);
			RowStructure structure = new RowStructure(config, new LocalizedTextsContainerImple("fi"), false);
			return new TweakedDAOForTests(con, dataSource, config, structure);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void test_initiation() {
		new RowToDbNonInputFieldsValueResolver(dao);
	}

	@Test
	public void test_exploding_rining_rows() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("A 1");
		row.get("municipality").setValue("HELSIN");
		List<Row> resultRows = resolver.fillValues(row, Mode.RINGINGS, DAO.Action.INSERT);
		assertEquals(1, resultRows.size());
		assertEquals("A 1", resultRows.get(0).get("ringStart").getValue());
		assertEquals("A 1", resultRows.get(0).get("nameRing").getValue());
		assertEquals("A 1", resultRows.get(0).get("legRing").getValue());
		assertEquals("", resultRows.get(0).get("ringEnd").getValue());
		assertEquals("HELSIN", resultRows.get(0).get("municipality").getValue());
	}

	@Test
	public void test_exploding_ringing_rows_2() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = dao.emptyRow();
		row.get("ringStart").setValue("A 1");
		row.get("ringEnd").setValue("A 5");
		row.get("municipality").setValue("HELSIN");
		List<Row> resultRows = resolver.fillValues(row, Mode.RINGINGS, DAO.Action.INSERT);
		assertEquals(5, resultRows.size());
		assertEquals("A 1", resultRows.get(0).get("ringStart").getValue());
		assertEquals("A 5", resultRows.get(0).get("ringEnd").getValue());
		assertEquals("A 1", resultRows.get(0).get("nameRing").getValue());
		assertEquals("A 1", resultRows.get(0).get("legRing").getValue());
		assertEquals("HELSIN", resultRows.get(0).get("municipality").getValue());

		assertEquals("A 1", resultRows.get(1).get("ringStart").getValue());
		assertEquals("A 5", resultRows.get(1).get("ringEnd").getValue());
		assertEquals("A 2", resultRows.get(1).get("nameRing").getValue());
		assertEquals("A 2", resultRows.get(1).get("legRing").getValue());
		assertEquals("HELSIN", resultRows.get(1).get("municipality").getValue());

		assertEquals("A 1", resultRows.get(4).get("ringStart").getValue());
		assertEquals("A 5", resultRows.get(4).get("ringEnd").getValue());
		assertEquals("A 5", resultRows.get(4).get("nameRing").getValue());
		assertEquals("A 5", resultRows.get(4).get("legRing").getValue());
		assertEquals("HELSIN", resultRows.get(4).get("municipality").getValue());
	}

	@Test
	public void test_fieldReadableRingChangeEuringCode_1() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals(1, resultRows.size());
		assertEquals("", resultRows.get(0).get("fieldReadableChangeEuringCode").getValue());  // no color markings mentioned
	}

	@Test
	public void test_fieldReadableRingChangeEuringCode_2() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("fieldReadableCode").setValue("FOO123");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals(1, resultRows.size());
		assertEquals("1", resultRows.get(0).get("fieldReadableChangeEuringCode").getValue()); // no changes
	}

	@Test
	public void test_fieldReadableRingChangeEuringCode_3() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("fieldReadableChanges").setValue("P");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals(1, resultRows.size());
		assertEquals("3", resultRows.get(0).get("fieldReadableChangeEuringCode").getValue()); // removed
	}

	@Test
	public void test_fieldReadableRingChangeEuringCode_4() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("fieldReadableChanges").setValue("V");
		row.get("fieldReadableCode").setValue("CE74");
		row.get("mainColor").setValue("VA");
		row.get("species").setValue("LARFUS");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals(1, resultRows.size());
		assertEquals("2", resultRows.get(0).get("fieldReadableChangeEuringCode").getValue()); // changed
	}

	private Row fillRequiredRecovery() {
		Row row = dao.emptyRow();
		row.get("legRing").setValue("HT 12345");
		row.get("eventDate").setValue(DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		row.get("lat").setValue("66666");
		row.get("lon").setValue("33333");
		row.get("coordinateSystem").setValue("YKJ");
		row.get("ringer").setValue("846");
		return row;
	}

	@Test
	public void test_clutch_number_owner_1() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("clutchNumber").setValue("");
		row.get("someoneElsesClutchNumber").setValue("");
		row.get("clutchNumberOwner").setValue("");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("", resultRows.get(0).get("clutchNumber").getValue());
		assertEquals("", resultRows.get(0).get("someoneElsesClutchNumber").getValue());
		assertEquals("", resultRows.get(0).get("clutchNumberOwner").getValue());
	}

	@Test
	public void test_clutch_number_owner_2() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("clutchNumber").setValue("1111");
		row.get("someoneElsesClutchNumber").setValue("");
		row.get("clutchNumberOwner").setValue("");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("1111", resultRows.get(0).get("clutchNumber").getValue());
		assertEquals("", resultRows.get(0).get("someoneElsesClutchNumber").getValue());
		assertEquals("846", resultRows.get(0).get("clutchNumberOwner").getValue());
	}

	@Test
	public void test_clutch_number_owner_3() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("clutchNumber").setValue("");
		row.get("someoneElsesClutchNumber").setValue("1111");
		row.get("clutchNumberOwner").setValue("563");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("1111", resultRows.get(0).get("clutchNumber").getValue());
		assertEquals("", resultRows.get(0).get("someoneElsesClutchNumber").getValue());
		assertEquals("563", resultRows.get(0).get("clutchNumberOwner").getValue());
	}

	@Test
	public void test_resolving_name_ring_1() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("HT 12345");
		row.get("fieldReadableCode").setValue("");
		row.get("mainColor").setValue("");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("HT 12345", resultRows.get(0).get("legRing").getValue());
		assertEquals("HT 012345", resultRows.get(0).get("nameRing").getValue());
	}

	@Test
	public void test_resolving_name_ring_2() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("legRing").setValue("");
		row.get("fieldReadableCode").setValue("CT04");
		row.get("mainColor").setValue("MU");
		row.get("species").setValue("LARARG");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("", resultRows.get(0).get("legRing").getValue());
		assertEquals("HT 204257", resultRows.get(0).get("nameRing").getValue());
	}

	@Test
	public void test_birdConditionEuring_1() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("birdConditionEURING").setValue("foobar");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("foobar", resultRows.get(0).get("birdConditionEURING").getValue());
	}

	@Test
	public void test_birdConditionEuring_2() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("birdConditionEURING").setValue("");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("8", resultRows.get(0).get("birdConditionEURING").getValue());
	}

	@Test
	public void test_birdConditionEuring_3() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("birdConditionEURING").setValue("");
		row.get("birdCondition").setValue("foobar");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("8", resultRows.get(0).get("birdConditionEURING").getValue());
	}

	@Test
	public void test_birdConditionEuring_4() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("birdConditionEURING").setValue("");
		row.get("birdCondition").setValue("D");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("2", resultRows.get(0).get("birdConditionEURING").getValue());
	}

	@Test
	public void test_recoveryMethod__if_given_return_that_value_in_all_cases() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("fiibar");
		row.get("birdCondition").setValue("D");
		row.get("captureMethod").setValue("");
		row.get("legRing").setValue("HT 12345");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("fiibar", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_2__nothing_special() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("");
		row.get("legRing").setValue("HT 12345");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("20", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_3__Ring_number_of_metal_ring_read_in_field_without_the_bird_being_caught() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("R");
		row.get("legRing").setValue("HT 12345");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("28", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_4__Coloured_or_numbered_mark_read_in_field_without_the_bird_being_caught() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("T");
		row.get("legRing").setValue("HT 12345");
		row.get("fieldReadableCode").setValue("082C"); // no attachment point
		row.get("mainColor").setValue("SIPU");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("29", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_5__Coloured_or_numbered_mark_read_in_field_without_the_bird_being_caught() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("T");
		row.get("legRing").setValue("HT 12345");
		row.get("fieldReadableCode").setValue("C042"); // has attachment point: neck
		row.get("mainColor").setValue("VA");
		row.get("species").setValue("HALALB");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("82", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_6__Coloured_or_numbered_mark_read_in_field_without_the_bird_being_caught() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("T");
		row.get("legRing").setValue("HT 12345");
		row.get("fieldReadableCode").setValue("H318"); // has attachment point: leg
		row.get("mainColor").setValue("VAMU");
		row.get("species").setValue("HALALB");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("81", resultRows.get(0).get("recoveryMethod").getValue());
	}

	@Test
	public void test_recoveryMethod_6__only_field_readable_reported() throws Exception {
		RowToDbNonInputFieldsValueResolver resolver = new RowToDbNonInputFieldsValueResolver(dao);
		Row row = fillRequiredRecovery();
		row.get("recoveryMethod").setValue("");
		row.get("birdCondition").setValue("");
		row.get("captureMethod").setValue("");
		row.get("legRing").setValue("");
		row.get("fieldReadableCode").setValue("H318"); // has attachment point: leg
		row.get("mainColor").setValue("VAMU");
		row.get("species").setValue("HALALB");
		List<Row> resultRows = resolver.fillValues(row, Mode.RECOVERIES, DAO.Action.INSERT);
		assertEquals("81", resultRows.get(0).get("recoveryMethod").getValue());
	}


}
