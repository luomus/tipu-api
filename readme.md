Tipu-API
========

For tipu-api/ringing/* see documentation/ringing-api.html


##Utility services

### tipu-api/coordinate-conversion-service

####Parameters
* lat - latitude, N
* lon - longitude, E
* type - coordinate system, one of the following
	* "ykj, "uniform", "kkj-yhtenaiskoord"   -- YKJ yhtenäiskoordinaatit (3. kaista); nollia lisätään loppuun jotta pituudeksi tulee 7
	* "euref", "etrs-tm35fin", "trs-tm35fin" -- EUREF-FIN koordinaatit; täytyy käyttää täysipitkää muotoa
	* "etrs89-decimal", "etrs89", "wgs84", "wgs84-decimal" -- 63.213545
	* "etrs89-degrees", "wgs84-degrees", "wgs84-dms" -- -1231501 == -123° 15' 01"
	* "kkj-decimal", "decimal" -- 63.213545
	* "kkj-degrees", "degrees" -- 231501 == 23° 15' 01"

#### Returns
Given coordinates converted to all supported formats

### tipu-api/coordinate-validation-service

Checks if given coordinates are inside the given region (municipality, bird station or euring province). For municipalities, current municipality border (polygons) are used. For the two other a more unprecise centerpoint + radius is used.

####Parameters
* lat - latitude, N
* lon - longitude, E
* type - coordinate system (same as conversion service)
* municipality - name in finnish, swedish or tipu-api/municipality -code
* birdstation - tipu-api/bird-stations -code
* euringProvince - tipu-api/euring-provices -code

You must give one of the municipality, birdstation or euringProvince parameters.

#### Returns
pass=true or pass=false and distance of error

### tipu-api/municipality-by-coordinates

Get current municipality of given coordinates

* lat - latitude, N
* lon - longitude, E
* type - coordinate system (same as conversion service)



### tipu-api/html2pdf
### tipu-api/html2pdf-no-svg

####Parameters
Html in request body or as value of html-parameter


### tipu-api/lintuvaara-authentication-decryptor

####Parameters
* key
* iv
* data

Returns decrypted Lintuvaara xml datamodel.


### tipu-api/permitsheet/{ringerid}

Returns ringer permit sheet as PDF for current year or, starting from December, for the next year.





##Resource services

RESTful services, for example 

* GET tipu-api/ringers returns all ringers
* GET tipu-api/ringers/1 returns ringer 1
* parameter filter:  /ringers?filter=Jukka returns all ringers where "jukka" (case insensitive) exists in any of the resource fields
 
 
### Persons

* tipu-api/admins  (Lintuvaara admins)
* tipu-api/laymen
* tipu-api/ringers
* tipu-api/observer-synonyms

### Locations

* tipu-api/bird-stations
* tipu-api/countries
* tipu-api/current-municipalities
* tipu-api/municipalities
* tipu-api/ely-centres
* tipu-api/euring-provinces
* tipu-api/lyl-areas
* tipu-api/old-counties
* tipu-api/provinces
* tipu-api/winterbird-routes


### Species
* tipu-api/selectable-species
* tipu-api/species
* tipu-api/winterbird-species


### Glossary
* tipu-api/codes
* tipu-api/descriptions-of-codes


### Ringing stuff
* tipu-api/ring-series
* tipu-api/schemes



### Dev

1. Clone git repo to <catalina.base>/webapps/tipu-api
2. Add project to IDE
3. Set class output folder to /WEB-INF/classes
4. Include <catalina.base>/lib/servlet-api.jar  to build path
5. Include JUNIT 4 to build path 
6. Follow deployment instructions 1, 2
7. Run tests - tomcat / tipu api must be running in localhost  

### Installation

1. Place tipu-api.properties config file to <catalina.base>/app-conf/
2. Place ojdbc6.jar  to <catalina.base>/lib
3. Deploy tipu-api.war
5. Make sure JRE has Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files

tipu-api.properties example
~~~
DevelopmentMode = YES
StagingMode = NO

DBdriver = oracle.jdbc.OracleDriver
DBurl = jdbc:oracle:thin:@ 
DBusername =  
DBpassword = 

TipuAPI_URI = http://self
TipuAPI_Username = 
TipuAPI_Password = 

LajiETLPushURL = https://..
LajiETLPushApiKey =
 
LajiETLSecretSalt = 

MunicipalityElasticClusterURL = https://..:9200
MunicipalityElasticCluster_Username = 
MunicipalityElasticCluster_Password = 


#For tipu-api/winterbird-routes
Talvilintu_DB_Name = prod/staging

BaseURL = https://.../tipu-api
StaticURL = https://.../tipu-api/static

ErrorReporting_SMTP_Host = localhost
ErrorReporting_SMTP_Username =
ErrorReporting_SMTP_Password =
ErrorReporting_SMTP_SendTo =  
ErrorReporting_SMTP_SendFrom =  
ErrorReporting_SMTP_Subject = Tipu-API WS TEST STAGING Error Report

BaseFolder = /home/tomcat/tomcat/
TemplateFolder = webapps/tipu-api/template
LogFolder = application-logs/Tipu-API
FontFolder = webapps/tipu-api/fonts
LanguageFileFolder = webapps/tipu-api/locales
LanguageFiles = ringing
SupportedLanguages = fi,en,sv

LintuvaaraPubRSAKey = 

~~~